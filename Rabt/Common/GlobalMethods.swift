//
//  GlobalMethods.swift
//  Rabt
//
//  Created by Ali Sher on 29/07/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit
import SCLAlertView
import Localize_Swift
import Adhan
import CoreData

class GlobalMethods: NSObject {

//    public static func getPrayerIcon(prayer: Prayer) -> String {
//
//        if prayer == .fajr {
//
//            return "fajr.png"
//
//        }else if prayer == .dhuhr {
//
//            return "dhuhr.png"
//
//        }else if prayer == .asr {
//
//            return "asr.png"
//
//        }else if prayer == .maghrib {
//
//            return "magrib.png"
//
//        }else if prayer == .isha {
//
//            return "isha.png"
//
//        }
//
//        return ""
//
//    }
//
//    public static func getCalculationMethod() -> CalculationParameters {
//
//        let selected = UserDefaults.standard.value(forKey: "calculationMethod") as! String
//
//        if selected == "Muslim World League" {
//
//            return CalculationMethod.muslimWorldLeague.params
//
//        }else if selected == "Egyptian General Authority of Survey" {
//
//            return CalculationMethod.egyptian.params
//
//        }else if selected == "University of Islamic Sciences" {
//
//            return CalculationMethod.karachi.params
//
//        }else if selected == "Umm al-Qura University, Makkah" {
//
//            return CalculationMethod.ummAlQura.params
//
//        }else if selected == "The Gulf Region" {
//
//            return CalculationMethod.dubai.params
//
//        }else if selected == "Moonsighting Committee" {
//
//            return CalculationMethod.moonsightingCommittee.params
//
//        }else if selected == "North America (ISNA)" {
//
//            return CalculationMethod.northAmerica.params
//
//        }else if selected == "Kuwait" {
//
//            return CalculationMethod.kuwait.params
//
//        }else if selected == "Qatar" {
//
//            return CalculationMethod.qatar.params
//
//        }else if selected == "Singapore" {
//
//            return CalculationMethod.singapore.params
//
//        }else if selected == "Institute of Geophysics, University of Tehran" {
//
//            return CalculationMethod.tehran.params
//
//        }else {
//
//            return CalculationMethod.turkey.params
//
//        }
//    }
    
    
    public static func showAlert(msg: String){
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue-Bold", size: 20)!,
            showCloseButton: false
        )
        
        let alertView = SCLAlertView(appearance: appearance)

        alertView.addButton("Ok".localized(), backgroundColor: GlobalVariables.AppColor, textColor: .white, showTimeout: nil, action: {
            
            print("Ok button tapped")
        })
        
        alertView.showSuccess("appname".localized(), subTitle: msg)
    
    }
    
    public static func getKeyValue(driverInfo: [String:Any], key: String) -> String {
        
        if (driverInfo[key] != nil) {
            
            if driverInfo[key] as! String != "" {
                
                return driverInfo[key] as! String
            }else{
                
                return "---"
            }
            
        }else{
            return "---"
        }
    }
    
    public static func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    public static func convertStringToDate(dateStr: String, format: String) -> Date{
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from:dateStr)!
                
        return date
        
    }
    
    public static func convertDateToString(date: Date, format: String) -> String{
        

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = format
        let myStringafd = formatter.string(from: yourDate!)

        return myStringafd
        
    }
    
    public static func convertDateFormat(dateStr: String, sourceformat: String, destformat: String) -> String{
        

        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = sourceformat
        let showDate = inputFormatter.date(from: dateStr)
        if showDate == nil{
            return dateStr
        }
        inputFormatter.dateFormat = destformat
        let resultString = inputFormatter.string(from: showDate!)
        
        return resultString
        
    }
    
//    public static func removeSpecificPrayerNotification(nameStr: String, CompletionHandler: @escaping (Bool) -> Void) {
//
//        var removed = true
//        let center = UNUserNotificationCenter.current()
//        center.getPendingNotificationRequests { (notifications) in
//            print("Notification Count: \(notifications.count)")
//            for item in notifications {
//
//                let name = nameStr.lowercased()
//                if item.identifier.contains(name) {
//
//                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [name])
//                    removed = false
//                    break
//
//                }
//            }
//
//            CompletionHandler(removed)
//        }
//    }
    
    
    
//    public static func checkNotificaionExists(nameStr: String, CompletionHandler: @escaping (Bool) -> Void) {
//        
//        var exists = false
//        let center = UNUserNotificationCenter.current()
//        center.getPendingNotificationRequests { (notifications) in
//            print("Notification Count: \(notifications.count)")
//            for item in notifications {
//                
//                let name = nameStr.lowercased()
//                if item.identifier == name {
//                    
//                    exists = true
//                    break
//                    
//                }
//            }
//            
//            CompletionHandler(exists)
//        }
//    }
    
    public static func getDayOfWeek(_ today:String) -> Int? {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
        
    }
    
    public static func greetingLogic() -> String{
        let date = Date()
        let calendar = Calendar.current
        let currentHour = calendar.component(.hour, from: date as Date)
        let hourInt = Int(currentHour.description)!
        
        var greeting = ""
        
        if hourInt >= 6 && hourInt <= 11 {
            
            greeting = "Good Morning"
        }
        else if hourInt > 11 && hourInt <= 16 {
            
            greeting = "Good Afternoon"
            
        }
        else if hourInt > 16 && hourInt <= 20 {
            
            greeting = "Good Evening"
            
        }
        else if hourInt >= 20 && hourInt <= 24 {
            
            greeting = "Good Night"
            
        }
        else if hourInt >= 0 && hourInt <= 6 {
            
            greeting = "Good Night"
            
        }

        return greeting
    }
    
    public static func getFavouriteType(app: FavouriteType) -> String {
        
        if app == FavouriteType.Artist {
            
            return "ARTIST"
        }else if app == FavouriteType.Audio {
            
            return "AUDIO"
        }else if app == FavouriteType.Nasheet{
            
            return "NASHEET"
        }
        else{
            return "BOOK"
        }
    }
    
//    public static func fetchData() -> PrayersModel {
//
//        guard let appDelegate =
//                UIApplication.shared.delegate as? AppDelegate else {
//            return prayerNotificaionsData
//        }
//
//        var notificaions: [NSManagedObject] = []
//
//        let managedContext = appDelegate.persistentContainer.viewContext
//
//        //2
//        let fetchRequest =
//            NSFetchRequest<NSManagedObject>(entityName: "PrayerNotificaions")
//
//        //3
//        do {
//            notificaions = try managedContext.fetch(fetchRequest)
//
//            if notificaions.count > 0 {
//
//                let dict = notificaions[0]
//
//                prayerNotificaionsData.fajr = dict.value(forKey: "fajr") as? String
//                prayerNotificaionsData.sunrise = dict.value(forKey: "sunRise") as? String
//                prayerNotificaionsData.dhuhr = dict.value(forKey: "dhuhr") as? String
//                prayerNotificaionsData.asr = dict.value(forKey: "asr") as? String
//                prayerNotificaionsData.maghrib = dict.value(forKey: "maghrib") as? String
//                prayerNotificaionsData.isha = dict.value(forKey: "isha") as? String
//                prayerNotificaionsData.nextPrayer = dict.value(forKey: "nextPrayer") as? String
//                prayerNotificaionsData.nextPrayerName = dict.value(forKey: "nextPrayerName") as? String
//
//            }else {
//
//                prayerNotificaionsData.fajr = "0"
//                prayerNotificaionsData.sunrise = "0"
//                prayerNotificaionsData.dhuhr = "0"
//                prayerNotificaionsData.asr = "0"
//                prayerNotificaionsData.maghrib = "0"
//                prayerNotificaionsData.isha = "0"
//                prayerNotificaionsData.nextPrayer = "0"
//                prayerNotificaionsData.nextPrayerName = " "
//
//            }
//        } catch let error as NSError {
//            print("Could not fetch. \(error), \(error.userInfo)")
//        }
//
//        return prayerNotificaionsData
//    }
    
//    public static func updateData(key: String, value: String) -> PrayersModel {
//
//        guard let appDelegate =
//            UIApplication.shared.delegate as? AppDelegate else {
//              return prayerNotificaionsData
//          }
//
//        var notificaions: [NSManagedObject] = []
//
//        //1
//
//        let managedContext = appDelegate.persistentContainer.viewContext
//
//        //2
//        let fetchRequest =
//          NSFetchRequest<NSManagedObject>(entityName: "PrayerNotificaions")
//
//        //We need to create a context from this container
//
//          //3
//          do {
//
//            notificaions = try managedContext.fetch(fetchRequest)
//
//            if notificaions.count > 0 {
//
//                let obj = notificaions[0]
//                obj.setValue(value, forKey: key)
//
//                try managedContext.save()
//                prayerNotificaionsData = self.fetchData()
//
//            }
//
//          } catch let error as NSError {
//            print("Could not fetch. \(error), \(error.userInfo)")
//          }
//
//        return prayerNotificaionsData
//    }
    
}
