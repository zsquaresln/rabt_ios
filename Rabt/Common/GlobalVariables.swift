//
//  GlobalVariables.swift
//  Rabt
//
//  Created by Ali Sher on 25/07/2021.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit
import CoreLocation


//When artist favrt btn tapped, need to referesh listen favourite section
var isFavouriteNeedsRefresh = false
//When books from see all favrt btn tapped, need to referesh all data
var isAllDataNeedsRefresh = false

var isCollapsedShown = false
var isPlaying = false
var isPlayAllClcked = false
var isAlreadyPlayAllClcked = false
var currentIndexPlayList = 0

var isPlayFromStart = false

var selectedOption = ""
var allPlayList : [Any] = []
var selectedAudio : [String : Any] = [:]
var selectedArtist : [String : Any] = [:]


//var isAudioPaused = false
//var isAlreadyPlaying = false
var artistName = ""
var selectedartistId = "0"
var isSameArtist = false


var myTableView = UITableView()
var statusLbl1 = UILabel()
var statusImgView1 = UIImageView()
var playerViewBottom: UIView!

var saveNotificaionDateFormat = "yyyy/MM/dd"
var saveNotificaionTimeFormat = "HH:mm"




var userInfo : [String:Any] = [:]


var isTesting: Bool = false
var DefaultCountryCode = "PK"
var DeviceType = "ios"
var DecimalOffset: Int = 2
var Current = ""
var MyCurrentView = UIViewController()
var FromPush: Bool = false

var CornerRadius: CGFloat = 8.0

var myLocation = CLLocation()
var myLocationPlacemark: CLPlacemark?

var MainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
var AuthenticationStoryBoard = UIStoryboard(name: "Authentications", bundle: nil)
var QuranStoryBoard = UIStoryboard(name: "Quran", bundle: nil)
var ListenStoryBoard = UIStoryboard(name: "Listen", bundle: nil)
var PlayerStoryBoard = UIStoryboard(name: "MyPlayer", bundle: nil)
var PrayerStoryBoard = UIStoryboard(name: "Prayer", bundle: nil)

var ProfileStoryBoard = UIStoryboard(name: "Profile", bundle: nil)

enum FavouriteType {
    case Artist
    case Audio
    case Book
    case Nasheet
}

class GlobalVariables: NSObject {
    
    public static var isLeftMenuVisible = false
    public static var CurrentView = UIViewController()
    
    static var StripeKey: String = ""
    static var GoogleMapsKey: String =  "AIzaSyDlx0Cj4rOTLfkmM8uomLF_K0oIHIzHzlo"
    

    static var BaseUrl: String =    //  "http://35.173.108.2/beta/api/user/"
     "http://35.173.108.2/api/user/"
    static var ImageBaseUrl: String =   //"http://35.173.108.2/beta/api/user/"
     "http://35.173.108.2/api/user/"
    
    static var contactUsUrl: String =   "https://rabt.co.uk/"
    

    public static var AppColor = UIColor(named: "AppColor")!
    
    public static var LightAppColor = UIColor(named: "AppColorLight")!
    
    public static var TitleFontColor = UIColor(named: "TitleFontColor")!
    
    
    public static var DarkGrayColor = UIColor(named: "DarkGrayColor")!
    
    
    public static var LightGrayColor = UIColor(named: "LightGrayColor")!
    
    
    public static var PlaceHolderColor = UIColor(named: "PlaceHolderColor")!
    
    
    public static var LineAndSubViewsColor = UIColor(named: "LineAndSubViewsColor")!
    
    public static var RedColor = UIColor(named: "RedColor")!
    
    
    public static var GreenColor = UIColor(named: "GreenColor")!
    
    
    public static var DefaultViewBackColor = UIColor(named: "DefaultViewBackColor")!
    
    public static var SelectedColor = UIColor(named: "SelectedColor")!
    
}
