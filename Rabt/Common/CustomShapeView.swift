//
//  CustomShapeView.swift
//  Rabt
//
//  Created by Ali Sher on 23/07/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit

@IBDesignable
class CustomShapeView: UIView {

    @IBInspectable var color : UIColor? = UIColor.gray {
        didSet {
        }

    }
    
    @IBInspectable var desiredCurve : CGFloat = 2.0 {
        didSet {
        }

    }
    
    override func draw(_ rect: CGRect) {
        
        
        let offset:CGFloat =  self.frame.width/desiredCurve
        let bounds: CGRect = self.bounds

        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y+bounds.size.height / 2, width: bounds.size.width, height: bounds.size.height / 2)

        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
        rectPath.append(ovalPath)

        // Create the shape layer and set its path
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath

        // Set the newly created shape layer as the mask for the view's layer
        self.layer.mask = maskLayer
        

    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clear

    }
    
    
}
