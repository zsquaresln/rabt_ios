//
//  LoadingOrNoRecordView.swift
//  Rabt
//
//  Created by Ali Sher on 07/07/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit

class LoadingOrNoRecordView: UIView {
    
    
    @IBOutlet var LoadingOrNoRecordMainView: UIView!
    @IBOutlet var msgLbl: UILabel!
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var loadingLbl: UILabel!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }

    func customInit() {
        Bundle.main.loadNibNamed("LoadingOrNoRecordView", owner: self, options: nil)
        addSubview(LoadingOrNoRecordMainView)
        LoadingOrNoRecordMainView.frame = bounds
    }
    func showLoading(){
        self.msgLbl.isHidden = true
        self.loadingLbl.isHidden = false
        self.iconImageView.isHidden = true
        self.isHidden = false
    }
    func hideLoading(){
        self.isHidden = true
    }
    func showLoadingWithIconAnd(message: String){
        self.msgLbl.isHidden = false
        self.loadingLbl.isHidden = true
        self.iconImageView.isHidden = false
        self.isHidden = false
        self.msgLbl.text = message
    }
}
