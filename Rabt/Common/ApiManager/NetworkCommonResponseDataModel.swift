//
//  NetworkCommonResponseDataModel.swift
//  Rabt
//
//  Created by Admin on 12/9/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class NetworkCommonResponseDataModel: Codable {
    let result, status, message: String?
    private enum CodingKeys: String, CodingKey {
        case result = "result"
        case status = "status"
        case message = "message"
    }
}

