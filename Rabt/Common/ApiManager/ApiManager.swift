//
//  ApiManager.swift
//  OnlineMaid
//
//  Created by Apple on 28/06/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit
import Alamofire
import SwiftData

protocol ApiManagerProtocol {
    
    func onError(msg : String, name: String)
    func onSuccess(data : [String:Any], name: String)
    
}


class ApiManager: NSObject {
    
    var delegate: Any? = nil
    var protocolmain_Catagory : ApiManagerProtocol! = nil
    static let sharedInstance = ApiManager()

    
    
    func webServiceWithURlJson(url: String, parameter: Parameters?, method: HTTPMethod, header: HTTPHeaders?, name: String, sdelegate: Any?)  {
        delegate = sdelegate
        AF.request(url, method: .put, parameters: parameter, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
             switch response.result
            {
            case .success(let json):
                 let responseData = json as! [String:Any]
                //self.delegate.
                
            case .failure(let error):
                print(error.localizedDescription)
                //self.errorFailer(error: error)
            }
        }
        
        
    }
    
    func getAPIResponse(url: String, parameter: Parameters?, method: HTTPMethod, header: HTTPHeaders?, completion:@escaping ((([String:Any]) -> Void))) {
        
        print(url)
        
        AF.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: { response in
            switch response.result {
            case .success(let JSON):
                
                let responseData = JSON as! [String:Any]
                completion(responseData)
                
            case .failure(let error):

                print("Failure\(error)")
            }
        })
    }
    
    func WebService(url: String, parameter: Parameters?, method: HTTPMethod, encoding: URLEncoding, _ onSuccess: @escaping([String:Any]) -> Void, onFailure: @escaping(Error) -> Void) {
        
        var headers: HTTPHeaders = [:]
        var access_token = ""
        
        if GlobalMethods.isKeyPresentInUserDefaults(key: "access_token") {
            
            access_token = UserDefaults.standard.value(forKey: "access_token") as! String
        
        }
                
        if access_token == "" {
            
            headers = ["Content-Type": "application/json"
            ]
            
        }else{
            
            headers = ["Content-Type": "application/json",
                       "Authorization": String(format: "Bearer %@", access_token)
            ]
            
        }
//        AF.session.configuration.requestCachePolicy = .reloadIgnoringCacheData
//        AF.sessionConfiguration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
//        AF.session.configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        AF.request(url, method: method, parameters: parameter, encoding: encoding, headers: headers).responseJSON(completionHandler: { response in
        switch response.result {
            case .success(let JSON):
            let JSONString = String(data: response.data! , encoding: .utf8)
            
                let responseData = JSON as! [String:Any]
           // print(JSONString ?? "")
            print(responseData)
                onSuccess(responseData)
            
            
            case .failure(let error):
            print("Error  is \( error.errorDescription ?? "")")
            print("Error  description is \(error.localizedDescription)")
            if let errorMessage = error.localizedDescription as? String
            {
                if errorMessage.lowercased().contains("offline"){
                    if kAPPDelegate.isOfflineShown == false {
                        self.showInternetConnectionFalurePopup(error: "Please connect to internet to use Rabt")
                    }
                }
                // No internet
            }
           
            AnalyticsManger.logEventWith(name: "ApiFailure", attributes: ["description": error.localizedDescription , "errorCode" : error.responseCode ?? "not found", "apiUrl": url])
                print("Failure\(error.localizedDescription)")
                onFailure(error)
            }
        })
    
        
    }
    func WebService<T: Decodable>(url: String, parameter: Parameters?, method: HTTPMethod, encoding: URLEncoding, _ onSuccess: @escaping(T?) -> Void, onFailure: @escaping(Error) -> Void) {
        
        var headers: HTTPHeaders = [:]
        var access_token = ""
        
        if GlobalMethods.isKeyPresentInUserDefaults(key: "access_token") {
            
            access_token = UserDefaults.standard.value(forKey: "access_token") as! String
        
        }
                
        if access_token == "" {
            
            headers = ["Content-Type": "application/json"
            ]
            
        }else{
            
            headers = ["Content-Type": "application/json",
                       "Authorization": String(format: "Bearer %@", access_token)
            ]
            
        }
        
        AF.request(url, method: method, parameters: parameter, encoding: encoding, headers: headers).responseJSON(completionHandler: { response in
        switch response.result {
            case .success(let JSON):
            let JSONString = String(data: response.data! , encoding: .utf8)
            
             
            print(JSONString ?? "")
            do{
                if response.data != nil{
                    let data =   try? JSONDecoder().decode(T.self, from: response.data!)
                    onSuccess(data)
                }else{
                    onSuccess(nil)
                }
            }
            
            
            case .failure(let error):
                print(error.responseCode ?? "")
            print("Error  is \( error.errorDescription ?? "")")
            print("Error  description is \(error.localizedDescription)")
            if let errorMessage = error.localizedDescription as? String
            {
                if errorMessage.lowercased().contains("offline"){
                    if kAPPDelegate.isOfflineShown == false {
                        self.showInternetConnectionFalurePopup(error: "Please connect to internet to use Rabt")
                    }
                    
                }
                // No internet
            }
                print("Failure\(error.localizedDescription)")
            AnalyticsManger.logEventWith(name: "ApiFailure", attributes: ["description": error.localizedDescription , "errorCode" : error.responseCode ?? "not found", "apiUrl": url])
                onFailure(error)
            }
        })
    
        
    }
    
    func WebServiceDefault(url: String, parameter: Parameters?, method: HTTPMethod, encoding: URLEncoding, _ onSuccess: @escaping([String:Any]) -> Void, onFailure: @escaping(Error) -> Void) {
        
        var headers: HTTPHeaders = [:]
        var access_token = ""
        
        if GlobalMethods.isKeyPresentInUserDefaults(key: "access_token") {
            
            access_token = UserDefaults.standard.value(forKey: "access_token") as! String
        
        }
                
        if access_token == "" {
            
            headers = ["Content-Type": "application/json"
            ]
            
        }else{
            
            headers = ["Content-Type": "application/json",
                       "Authorization": String(format: "Bearer %@", access_token)
            ]
            
        }
        
        AF.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
        switch response.result {
            case .success(let JSON):
                let responseData = JSON as! [String:Any]
                onSuccess(responseData)
            
            
            case .failure(let error):
                print("Failure\(error.localizedDescription)")
                onFailure(error)
            }
        })
    }
    
    func WebServiceWithImage(url: String, imgName: String, image: UIImage, parameter: Parameters?, method: HTTPMethod, encoding: URLEncoding, _ onSuccess: @escaping([String:Any]) -> Void, onFailure: @escaping(Error) -> Void) {
                
        var headers: HTTPHeaders = [:]
        var access_token = ""
        
        if GlobalMethods.isKeyPresentInUserDefaults(key: "access_token") {
            
            access_token = UserDefaults.standard.value(forKey: "access_token") as! String
        
        }
                
        if access_token == "" {
            
            headers = ["Content-Type": "application/json"
            ]
            
        }else{
            
            headers = ["Content-Type": "application/json",
                       "Authorization": String(format: "Bearer %@", access_token)
            ]
            
        }
        AF.sessionConfiguration.timeoutIntervalForRequest = 120
        AF.sessionConfiguration.timeoutIntervalForResource = 120
        AF.upload(multipartFormData: { (multipartFormData) in
            
            if parameter != nil {
                
                let myParams = parameter! as [String:Any]
                
                    for (key, value) in myParams {
                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                
            }
            
            if let jpegData = (image).jpegData(compressionQuality: 0.3) {
                let str = imgName + ".jpg"
                
                       multipartFormData.append(jpegData, withName: imgName, fileName: str, mimeType: "image/jpeg,image/png")
                   }


        },to: url, usingThreshold: UInt64.init(),
              method: .post,
              headers: headers).responseJSON(completionHandler: { response in
                
                switch response.result {
                    case .success(let JSON):
                        let responseData = JSON as! [String:Any]
                        print(responseData)
                        onSuccess(responseData)
                    
                    
                    case .failure(let error):
                        
                        print("\n\n===========Error===========")
                        print("Error Code: \(error._code)")
                        print("Error Messsage: \(error.localizedDescription)")
                        if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                            print("Server Error: " + str)
                        }
                        debugPrint(error as Any)
                        print("===========================\n\n")
                        
                        print("Failure\(error.localizedDescription)")
                        onFailure(error)
                    }
                })
    }
    
    
    func WebService1WithImages(url: String, images: [Any], parameter: Parameters?, method: HTTPMethod, encoding: URLEncoding, _ onSuccess: @escaping([String:Any]) -> Void, onFailure: @escaping(Error) -> Void) {
                
        var headers: HTTPHeaders = [:]
        var access_token = ""
        
        if GlobalMethods.isKeyPresentInUserDefaults(key: "access_token") {
            
            access_token = UserDefaults.standard.value(forKey: "access_token") as! String
        
        }
                
        if access_token == "" {
            
            headers = ["Content-Type": "application/json"
            ]
            
        }else{
            
            headers = ["Content-Type": "application/json",
                       "Authorization": String(format: "Bearer %@", access_token)
            ]
            
        }
        
        AF.upload(multipartFormData: { (multipartFormData) in

            let myParams = parameter! as [String:Any]
            
                for (key, value) in myParams {
                    multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key )
                }

            var i = 0
            
            for image in images {
                
                i = i + 1
                let str = String(format: "image%d", i)
                
                if url.contains(ApiUrls.editProfileApi) {
                    //[formData appendPartWithFileData:imageToUpload name:strName fileName:[NSString stringWithFormat:@"ParcelImage%d.jpg",i] mimeType:@"image/jpeg,image/png"];
                if let jpegData = (image as! UIImage).jpegData(compressionQuality: 0.5) {
                           
                           multipartFormData.append(jpegData, withName: "driver_image", fileName: "driver_image.jpg", mimeType: "image/jpeg")
                       }
                    
                }else{
                    
                 if let jpegData = (image as! UIImage).jpegData(compressionQuality: 0.5) {
                        
                        multipartFormData.append(jpegData, withName:str, fileName: String(format: "ParcelImage%d.jpg", i), mimeType: "image/jpeg,image/png")
                    }
                    
                }
                
                
            }


        },to: url, usingThreshold: UInt64.init(),
              method: .post,
              headers: headers).responseJSON(completionHandler: { response in
                
                switch response.result {
                    case .success(let JSON):
                    let responseData = JSON as! [String:Any]
                        print(responseData)
                        onSuccess(responseData)
                    
                    
                    case .failure(let error):
                        print("Failure\(error.localizedDescription)")
                        onFailure(error)
                    }
                })
        
    }
    
    func showInternetConnectionFalurePopup(error:String){
        kAPPDelegate.isOfflineShown = true
        let alert = UIAlertController(title: "Info".local, message: nil, preferredStyle: .alert)
        alert.message = "Please connect to internet to use Rabt"
        alert.addAction(UIAlertAction(title: "Cancel".local, style: UIAlertAction.Style.default,handler: nil))
      //  print( (UIApplication.shared.delegate?.window??.rootViewController as? SWRevealViewController)?.revealViewController()?.navigationController?.viewControllers.last!)
     //   UIApplication.shared.delegate?.window??.rootViewController
        (((UIApplication.shared.delegate?.window??.rootViewController as? SWRevealViewController)?.frontViewController as? UITabBarController)?.selectedViewController as? UINavigationController)?.viewControllers.last?.present(alert, animated: true, completion: nil)
    }
}
