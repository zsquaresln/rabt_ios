//
//  ApiUrls.swift
//  OnlineMaid
//
//  Created by Apple on 28/06/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit

class ApiUrls: NSObject {
    
    
    public static var isTesting = true
    
    public static var basedomain = GlobalVariables.BaseUrl
    public static var imagedomain = GlobalVariables.ImageBaseUrl
    
    
    //MARK: Authentication
    
    public static var sendOTPApi = basedomain + "send/one_time_password"
    public static var registerApi = basedomain + "signup/customer"
    public static var loginApi = basedomain + "signin/customer"
    
    
    public static var editProfileApi = basedomain + "customer/createCustomerProfile"
    
    public static var homeDataApi = basedomain + "home_v2"
    
    
    //MARK: Prayer Timings
    
    public static var prayerServicesApi = basedomain + "get_prayer_details"
    
    
    //Setting
    
    public static var getLinksApi = basedomain + "url_links"
    
    
    //MARK: Quran
    
    public static var quranReadApi = basedomain + "create_quran_logs"
    
    //MARK: Profile
    
    //update image and Bio
    
    public static var updateImageAndBioApi = basedomain + "update_profile"
    public static var updateNameApi = basedomain + "update_name"
    
    //MARK: Listen
    
    public static var listenServicesApi = basedomain + "listen"
    public static var audioPlayCount = basedomain + "audio_play_count"
    public static var listenFavouriteListApi = basedomain + "favourite_list_v2"
    public static var artistDetailsApi = basedomain + "artist_details"
    
    public static var allArtistApi = basedomain + "all_artist"
    public static var allPlayListApi = basedomain + "playlist_list"
    
    public static var allBooksApi = basedomain + "all_audiobooks"
    public static var audioBookDetailsApi = basedomain + "audiobooks_details"
    
    public static var addToFavouriteApi = basedomain + "create_favourite"
    public static var deleteToFavouriteApi = basedomain + "delete_favourite"
    public static var getSplashDataApi = basedomain + "splash"
    public static var prayerTimingsApi =  basedomain + "calendar_list"
   
    
    //MARK: - Subscription
    public static var artistRewarded =  basedomain + "reward"
    public static var premiumPurchase =  basedomain + "package"
    
    public static var listenRevampedServicesApi = basedomain + "listen_v2"
    public static var playlistDetailServicesApi = basedomain + "category_playlist_details"
}

