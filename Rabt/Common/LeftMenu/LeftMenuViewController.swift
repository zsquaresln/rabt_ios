//
//  LeftMenuViewController.swift
//  OnlineMaid
//
//  Created by Ali Sher on 25/06/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData
class LeftMenuViewController: UIViewController {

    
    @IBOutlet var MenusTableView: UITableView!
    
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet var TopView: UIView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    
    @IBOutlet var appVersionLabel: UILabel!
    var AllMenus : [Any] = []
    
    var Selected = String()
    var userInfo : [String:Any] = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        Selected = "";
        TopView.backgroundColor = GlobalVariables.DarkGrayColor
        MenusTableView.backgroundColor = GlobalVariables.DarkGrayColor
        
        
        let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "0"
        
        appVersionLabel.text = "Version" + " " + version
        appVersionLabel.textColor = GlobalVariables.DarkGrayColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if GeneralMethods.isKeyPresentInUserDefaults(key: "userinfo") {
            
            userInfo = UserDefaults.standard.value(forKey: "userinfo") as! [String:Any]
            print(userInfo)
            
        }
        
        getArray()
        GlobalVariables.isLeftMenuVisible = true
        
        let url_str = userInfo["profilePic"] as? String ?? ""
        let url = URL(string: url_str)
        
        profileImgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
        profileImgView.layer.cornerRadius = profileImgView.frame.size.height/2
        profileImgView.clipsToBounds = true
        
        nameLabel.text = userInfo["userName"] as? String
        emailLbl.text = userInfo["emailId"] as? String
        
    }
    
    func getArray() -> Void {
        
        AllMenus  = []
        
        var RecordDict : [String:Any] = [:]
        RecordDict = ["title":"My Profile".localized(),"count":"0","image":"MyProfile"]
        AllMenus.append(RecordDict)
       // AllMenus.add(RecordDict)
        
        
        RecordDict = ["title":"Support".localized(),"count":"0","image":"Support"]
        AllMenus.append(RecordDict)
       // AllMenus.add(RecordDict)
        
        
        RecordDict = ["title":"Logout".localized(),"count":"0","image":"Logout"]
        AllMenus.append(RecordDict)
       // AllMenus.add(RecordDict)
        
        self.MenusTableView.delegate = self;
        self.MenusTableView.dataSource = self;
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        GlobalVariables.isLeftMenuVisible = false
    }
    
    
    @IBAction func ProfileBtnClicked(_ sender: Any) {
        
        //let deleg = GlobalVariables.CurrentView as? HomeViewController
        //self.revealViewController()?.revealToggle(nil)
        //deleg!.MoveToProfile()
    }
    
    @IBAction func changeStatusBtnClicked(_ sender: Any) {
        
    }
    
    
}


extension LeftMenuViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AllMenus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataRecord = AllMenus[indexPath.row] as! [String:Any]
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell") as! LeftMenuTableViewCell
        cell.TitleLabel.text = dataRecord["title"] as? String
        cell.ImgView.image = UIImage(named: dataRecord["image"] as! String)
        
        cell.SeparatorView.isHidden = true
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        
        
        cell.counterLabel.layer.cornerRadius = cell.counterLabel.frame.size.height/2
        cell.counterLabel.layer.masksToBounds = true
        cell.counterLabel.backgroundColor = GlobalVariables.AppColor
        
        
        let count = dataRecord["count"] as! String
        cell.counterLabel.isHidden = true
        
        cell.counterLabel.text = count
        if count != "0" {
            
            cell.counterLabel.isHidden = false
            
        }
        
        cell.TitleLabel.textColor = .white
        GeneralMethods.ChangeImgViewTintColor(imgView: cell.ImgView, color: .white)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected index = \(indexPath.row)")
        
        let selected = AllMenus[indexPath.row] as! [String:Any]
        
        let deleg = GlobalVariables.CurrentView as? MenuButtonViewController
        
        Selected = selected["title"] as? String ?? ""
        
        MenusTableView.reloadData()
        
        if (selected["title"] as? String == "My Profile".localized()){
            self.revealViewController()?.revealToggle(nil)
            //deleg!.MoveToProfile()
            
        }else if (selected["title"] as? String == "Support".localized()){
            self.revealViewController()?.revealToggle(nil)
            //deleg!.MoveToSupport()
            
        }else if (selected["title"] as? String == "Logout".localized()){
            self.revealViewController()?.revealToggle(nil)
            //deleg!.MoveToLogout()
            
        }else{
            
            self.revealViewController()?.revealToggle(nil)
            
        }
        
    }
}
