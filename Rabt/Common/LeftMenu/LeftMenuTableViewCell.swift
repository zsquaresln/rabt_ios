//
//  LeftMenuTableViewCell.swift
//  OnlineMaid
//
//  Created by Ali Sher on 25/06/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {

    @IBOutlet var SeparatorView: UIView!
    @IBOutlet var ImgView: UIImageView!
    @IBOutlet var TitleLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    
    
    @IBOutlet var titleLabelLeadingCons: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
