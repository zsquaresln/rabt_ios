//
//  ViewModel.swift
//  Google Places Demo
//
//  Created by Wahyd on 20/10/2020.
//

import Foundation
import UIKit

struct ViewModel {
    
    private var key = GlobalVariables.GoogleMapsKey
    
//    func getRequest(searchString: String, text: String) -> URLRequest {
//       
//        //&components=country:pk
//        let url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\((searchString + text as String).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? "")&language=en&key=\(key)"
//        var request = URLRequest(url: URL(string: url)!)
//        request.cachePolicy = .useProtocolCachePolicy
//        request.timeoutInterval = 60.0
//        
//        return request
//    }
//    
    func getLocationDetail(place: String) -> URLRequest {
        
        
        let url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(place)&key=\(key)"
        var request = URLRequest(url: URL(string: url)!)
        request.cachePolicy = .useProtocolCachePolicy
        request.timeoutInterval = 60.0
        
        return request
    }
    
    func jsonValue(_ data: Data?) -> [String:Any]? {
        var decodedString: String? = nil
        if let data = data {
            decodedString = String(data: data, encoding: .utf8)
        }
        let jsonData = decodedString?.data(using: .utf8)
        
        var _: Error?
        var allKeys: Any? = nil
        do {
            if let jsonData = jsonData {
                allKeys = try JSONSerialization.jsonObject(with: jsonData, options: .init())
            }
        } catch _ {
        }
        
        let dict = allKeys as? [String:Any]
        return dict
    }
}
