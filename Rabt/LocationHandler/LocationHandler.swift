//
//  LocationHandler.swift
//  GoogleMapsDemo
//
//  Created by Tayyab Faran on 11/06/2020.
//  Copyright © 2020 Tayyab Faran. All rights reserved.
//

import Foundation
import CoreLocation
import GooglePlaces
import Alamofire
import UIKit
protocol locationHandlerDelegate {
    func locationUpdatedWith(coordinates :CLLocation)
    func locationServicesDisabledError()
    func locationUpdatedWith(address:String, city:String)
}
class LocationHandler : UIViewController,CLLocationManagerDelegate{
    //Properties
    fileprivate var locationManager : CLLocationManager?
   // private var mapView :GMSMapView?
    private var currentLocation = CLLocation()
    private var updateContinuosly = false
    var delegate : locationHandlerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // initializeLocationManager()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //locationManager.stopUpdatingLocation()
        
    }
    //MARK: - Helper Functions
    func isLocationServicesEnabled()->Bool{
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                print("No access")
                self.locationManager?.requestWhenInUseAuthorization()
                return false
            case .restricted, .denied:
                print("denied")
                delegate?.locationServicesDisabledError()
                stopLocationMangaer()
                return false
            case .authorizedAlways, .authorizedWhenInUse,.authorized:
                print("Access")
                return true
            }
        } else {
            print("Location services are not enabled")
             delegate?.locationServicesDisabledError()
            stopLocationMangaer()
            return false
        }
        
    }
    func allowLocationServices(){
        if !CLLocationManager.locationServicesEnabled() {
            /*if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
                // If general location settings are disabled then open general location settings
                UIApplication.shared.openURL(url)
            }*/
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl)  {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
            
        } else {
            if let url = URL(string: UIApplication.openSettingsURLString) {
                // If general location settings are enabled then open location settings for the app
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    //MARK:- location manager functions
   private func initializeLocationManager() {
       locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
      //  locationManager?.activityType = .automotiveNavigation
        //locationManager?.distanceFilter = 10.0  // Movement threshold for new events
        //    locationManager.allowsBackgroundLocationUpdates = true // allow in background
        locationManager?.requestAlwaysAuthorization()
    }
    func beginLocationUpdates(){
        if locationManager == nil{
            initializeLocationManager()
        }
        if isLocationServicesEnabled() {
            updateContinuosly = true
            startLocationManger()
        }
        else{
            // give promp to all locatin services
        }
    }
    func getCurrentLocation(){
        if locationManager == nil{
            initializeLocationManager()
        }
        if isLocationServicesEnabled() {
            updateContinuosly = false
            startLocationManger()
        }
        else{
            // give promp to all locatin services
        }
    }
    func stopUpdatingLocation(){
        locationManager?.stopUpdatingLocation()
    }
    private func startLocationManger(){
        locationManager?.startUpdatingLocation()
    }
    private func stopLocationMangaer(){
        locationManager?.stopUpdatingLocation()
    }
    //MARK: - Location manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latestLocation = locations.last!
        currentLocation = latestLocation
        //  stopLocationMangaer()
        print(latestLocation.horizontalAccuracy)
        if latestLocation.horizontalAccuracy < 0 {
            // location wrongly updated, some time gps gives you a big jump away from your current location
            return
        }
        if updateContinuosly == false{
            locationManager?.stopUpdatingLocation()
            delegate?.locationUpdatedWith(coordinates: latestLocation)
        }
        else{
            delegate?.locationUpdatedWith(coordinates: latestLocation)
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedWhenInUse, .authorizedAlways:
                // Enable features that require location services here.
                PrayerTimeManger.shared.selectedLocationName = nil
                self.locationManager?.startUpdatingLocation()
            case .notDetermined:
                return
            case .restricted:
                return
            case .denied:
                return
            @unknown default:
                return
            }
            
        }
    }
//    func geocode(latitude: Double, longitude: Double, completion: @escaping (_ address :String ,_ city:String, _ error: Error?) -> Void)  {
//       let geocoder = GMSGeocoder()
//        geocoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: latitude, longitude: longitude), completionHandler: {resopnse, error in
//                  guard let address = resopnse?.firstResult(), let lines = address.lines else {
//                    completion("","", error)
//                       return
//                     }
//            completion(lines.first ?? ""  ,address.locality ?? "", nil)
//                      print(lines)
//                     // 3
//                   //  self.addressLabel.text = lines.joined(separator: "\n")
//
//
//              })
//    }
//    func getAddressFromLatLong(latitude: Double, longitude : Double) {
//        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\((UIApplication.shared.delegate as! AppDelegate).gmsServicesKey)"
//        AF.request(url).validate().responseJSON { response in
//            switch response.result {
//            case .success:
//
//                let responseJson = response.value
//
//                if let results = responseJson.object(forKey: "results")!  {
//                    if results.count > 0 {
//                        if let addressComponents = results[0]["address_components"]!  {
//                          //  self.address = results[0]["formatted_address"] as? String
//                            for component in addressComponents {
//                                if let temp = component.object(forKey: "types") as? [String] {
//                                    if (temp[0] == "postal_code") {
//                                ////        self.pincode = component["long_name"] as? String
//                                    }
//                                    if (temp[0] == "locality") {
//                                        self.delegate?.locationUpdatedWith(address: results[0]["formatted_address"] as? String ?? "", city: component["long_name"] as? String ?? "")
//                                 //       self.city = component["long_name"] as? String
//                                    }
//                                    if (temp[0] == "administrative_area_level_1") {
//                                //        self.state = component["long_name"] as? String
//                                    }
//                                    if (temp[0] == "country") {
//                                //        self.country = component["long_name"] as? String
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            case .failure(let error):
//                print(error)
//            }
//        }
//    }
}
