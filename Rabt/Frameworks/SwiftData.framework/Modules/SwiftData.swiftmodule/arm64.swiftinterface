// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.2 (swiftlang-1200.0.45 clang-1200.0.32.28)
// swift-module-flags: -target arm64-apple-ios11.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name SwiftData
import LocalAuthentication
import Swift
@_exported import SwiftData
import SystemConfiguration
import UIKit
@_hasMissingDesignatedInitializers public class InternetConnectionManager {
  public static func isConnectedToNetwork() -> Swift.Bool
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class GeneralMethods : ObjectiveC.NSObject {
  public static func CheckInternetConnection() -> Swift.Bool
  public static func validateEmail(enteredEmail: Swift.String) -> Swift.Bool
  public static func getValue(dic: Foundation.NSDictionary, val: Swift.String) -> Swift.String
  public static func ChangeBtnTintColor(btn: UIKit.UIButton, color: UIKit.UIColor)
  public static func image(with image: UIKit.UIImage?, scaledTo newSize: CoreGraphics.CGSize) -> UIKit.UIImage?
  public static func ChangeImgViewTintColor(imgView: UIKit.UIImageView, color: UIKit.UIColor)
  public static func getRoundOfCredit(credit: Swift.String, decimalValue: Swift.Int) -> Swift.String
  public static func isIphoneXShaped() -> Swift.Bool
  public static func CreateCardView(radius: CoreGraphics.CGFloat, view: UIKit.UIView)
  public static func CreateCardViewWithoutBackColor(radius: CoreGraphics.CGFloat, view: UIKit.UIView)
  public static func CreateCardViewWithoutShadow(radius: CoreGraphics.CGFloat, view: UIKit.UIView)
  public static func RoundedCorners(shadow: UIKit.UIColor, view: UIKit.UIView)
  public static func setCorners(radius: CoreGraphics.CGFloat, corners: UIKit.UIRectCorner, view: UIKit.UIView)
  public static func CheckIsIphone() -> Swift.Bool
  public static func showAlert(msg: Swift.String, title_text: Swift.String, btn_text: Swift.String, view: UIKit.UIViewController)
  public static func isRTL() -> Swift.Bool
  public static func getDecimalValueForKey(dict: Foundation.NSDictionary, key: Swift.String) -> Swift.String
  public static func getFloatValueForKey(dict: Foundation.NSDictionary, key: Swift.String) -> Swift.String
  public static func getTagedValue(dic: Foundation.NSDictionary, val: Swift.String) -> Swift.Int
  public static func getStringKeyValue(userInfo: Foundation.NSDictionary, key: Swift.String, defaultV: Swift.String) -> Swift.String
  public static func getChargingValue() -> Swift.String
  public static func getBateryStates() -> Swift.String
  public static func ConvertTimeZone(from sourceTimeZone: Foundation.TimeZone, to destinationTimeZone: Foundation.TimeZone, date C_Date: Foundation.Date) -> Foundation.Date
  public static func ConvertTimeFormat(from sourceFormat: Swift.String, to destinationFormat: Swift.String, date C_Date: Swift.String) -> Swift.String
  public static func convertStringToDate(dateStr: Swift.String, format: Swift.String) -> Foundation.Date
  public static func convertDateToString(date: Foundation.Date, format: Swift.String) -> Swift.String
  public static func convertDateFormat(dateStr: Swift.String, sourceformat: Swift.String, destformat: Swift.String) -> Swift.String
  public static func isKeyPresentInUserDefaults(key: Swift.String) -> Swift.Bool
  public static func checkBioMetricAvailable() -> Swift.Bool
  @objc deinit
}
@_inheritsConvenienceInitializers @IBDesignable @objc open class ShapesCardView : UIKit.UIView {
  @objc @IBInspectable dynamic open var BackColor: UIKit.UIColor? {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable dynamic open var cornerRounding: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable dynamic open var borderWidth: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable dynamic open var borderColor: UIKit.UIColor? {
    @objc get
    @objc set(value)
  }
  @objc override dynamic open func prepareForInterfaceBuilder()
  @objc override dynamic open func awakeFromNib()
  @objc deinit
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
}
@_inheritsConvenienceInitializers @IBDesignable @objc open class CardView : UIKit.UIView {
  @objc @IBInspectable dynamic open var BackColor: UIKit.UIColor? {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable dynamic open var cornerRounding: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable dynamic open var borderWidth: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable dynamic open var borderColor: UIKit.UIColor? {
    @objc get
    @objc set(value)
  }
  @objc override dynamic open func prepareForInterfaceBuilder()
  @objc override dynamic open func awakeFromNib()
  @objc deinit
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
}
@_inheritsConvenienceInitializers @IBDesignable @objc open class HighlightedButton : UIKit.UIButton {
  @objc @IBInspectable dynamic open var BackColor: UIKit.UIColor? {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable dynamic open var cornerRounding: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable dynamic open var borderWidth: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable dynamic open var borderColor: UIKit.UIColor? {
    @objc get
    @objc set(value)
  }
  @objc override dynamic open func prepareForInterfaceBuilder()
  @objc override dynamic open func awakeFromNib()
  @objc deinit
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
}
@_inheritsConvenienceInitializers @IBDesignable @objc open class FloatingTextField : UIKit.UITextField {
  @objc @IBInspectable open var disableFloatingLabel: Swift.Bool
  @objc @IBInspectable open var shakeLineWithError: Swift.Bool
  @objc @IBInspectable open var lineColor: UIKit.UIColor {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable open var selectedLineColor: UIKit.UIColor {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable open var placeHolderColor: UIKit.UIColor {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable open var selectedPlaceHolderColor: UIKit.UIColor {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable open var errorLineColor: UIKit.UIColor {
    @objc get
    @objc set(value)
  }
  @objc override dynamic open var text: Swift.String? {
    @objc get
    @objc set(value)
  }
  @objc override dynamic open var placeholder: Swift.String? {
    @objc get
    @objc set(value)
  }
  @objc override dynamic open func draw(_ rect: CoreGraphics.CGRect)
  @objc override dynamic open func awakeFromNib()
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic open func textRect(forBounds bounds: CoreGraphics.CGRect) -> CoreGraphics.CGRect
  @objc override dynamic open func editingRect(forBounds bounds: CoreGraphics.CGRect) -> CoreGraphics.CGRect
  @objc override dynamic open func becomeFirstResponder() -> Swift.Bool
  @objc override dynamic open func resignFirstResponder() -> Swift.Bool
  @objc deinit
}
