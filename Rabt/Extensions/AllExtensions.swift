//
//  AllExtensions.swift
//  Rabt
//
//  Created by Ali Sher on 22/01/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import Foundation
import ACFloatingTextfield_Swift

extension UIImage {
    func imageResized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}

extension StringProtocol {
    var firstUppercased: String { prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { prefix(1).capitalized + dropFirst() }
}


extension UIViewController {
    
    func EnableNavigation() -> Void {
        
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        self.navigationController?.navigationBar.alpha = 1.0
        
    }
    
    func DisableNavigation() -> Void {
        
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        self.navigationController?.navigationBar.alpha = 0.6
        
    }
    
}


extension Dictionary {
    
    
    func removeNullFromDict () -> [String:Any]
    {
        var dictt = [String:Any]()
        
        var dict : [String:Any] = [:]
        dict = self as! [String:Any]
        
        var dic = dict;

        for (key, value) in dict {

            let val : NSObject = value as! NSObject;
            if(val.isEqual(NSNull()))
            {
                dic[key] = ""
            }
            else
            {
                dic[key] = value
            }

        }
        
        dictt = dic
        return dictt;
    }
}


extension ACFloatingTextfield {
    
    
    func setFloatingTextField(unSelectedColor: UIColor, SelectedColor: UIColor) {
        
        self.placeHolderColor = unSelectedColor
        self.selectedPlaceHolderColor = SelectedColor
        self.lineColor = unSelectedColor
        self.selectedLineColor = SelectedColor
        
        self.textColor = GlobalVariables.TitleFontColor
        
    }
    
}


extension String {
    
    func convertStringTodouble() -> Double {
        
        let price = Double(self)!
        return price
        
    }
    
}


extension Bundle {

    var appName: String {
        return infoDictionary?["CFBundleName"] as! String
    }

    var bundleId: String {
        return bundleIdentifier!
    }

    var versionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as! String
    }

    var buildNumber: String {
        return infoDictionary?["CFBundleVersion"] as! String
    }

}
