//
//  SecodsConversion.swift
//  WahydTravel
//
//  Created by Ali Sher on 12/08/2021.
//

import Foundation


extension UIViewController {
    
    func convertSecondsToHoursMinutesSeconds (seconds:Double, format: String) -> String {
        let (h, m, s) = secondsToHoursMinutesSeconds (seconds: seconds)
        
        let hours = Int(h)
        let mins = Int(m)
        let seconds = Int(s)
        
        let text1 = hours == 1 ? "hr" : "hrs"
        let text2 = mins == 1 ? "min" : "mins"
        let text3 = seconds == 1 ? "sec" : "sec"
        
        let text11 = hours < 10 ? "0\(hours)" : "\(hours)"
        let text12 = mins < 10 ? "0\(mins)" : "\(mins)"
        let text13 = seconds < 10 ? "0\(seconds)" : "\(seconds)"
        
        
        if hours > 0 {
            
            if format == ":" {
                
                return "\(text11):\(text12):\(text13)"
            }else {

                return "\(hours) \(text1), \(mins) \(text2), \(seconds) \(text3)"
            }
            
        }else if mins > 0 {
            
            if format == ":" {
                
                return "\(text12):\(text13)"
            }else {
                
                return "\(mins) \(text2), \(seconds) \(text3)"
            }
            
        }else {
            
            if format == ":" {
                
                return "\(text12):\(text13)"
            }else {
                
                return "\(seconds) \(text3)"
            }
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Double) -> (Double, Double, Double) {
        
      let (hr,  minf) = modf (seconds / 3600)
      let (min, secf) = modf (60 * minf)
      return (hr, min, 60 * secf)
        
    }
}
