//
//  UIViewControllerExtensions.swift
//  Rabt
//
//  Created by Muhammad on 20/08/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func setTabBarCorner() -> Void {
        
        self.tabBarController?.tabBar.layer.cornerRadius = 0
        
        if !isCollapsedShown {
            
            self.tabBarController?.tabBar.layer.cornerRadius = 24
        }
        
        self.tabBarController?.tabBar.layer.masksToBounds = true
        self.tabBarController?.tabBar.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
                
        self.view.layoutIfNeeded()
        
    }
    
    func getCurrentTimeZone() -> String{
        
        return TimeZone.current.identifier
    }
}


