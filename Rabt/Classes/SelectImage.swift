//
//  SelectImage.swift
//  Rabt
//
//  Created by Ali Sher on 10/09/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit

class SelectImage: NSObject {
    
    var delegate: (UIImagePickerControllerDelegate & UINavigationControllerDelegate)?


    var VC = UIViewController()
    var imgView = UIImageView()
    
    func select() -> Void {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
        }))



        actionSheet.addAction(UIAlertAction(title: "Pick from Library".localized(), style: .default, handler: { action in
            self.selectImageFromGallery()
        }))

        actionSheet.addAction(UIAlertAction(title: "Take a Photo".localized(), style: .default, handler: { action in
            self.takeImageFromCamera()
        }))
        actionSheet.popoverPresentationController?.sourceView = imgView // works for both iPhone & iPad

        VC.present(actionSheet, animated: true)
        
    }
    
    
    func selectImageFromGallery() {
        let pickerController = UIImagePickerController()
        pickerController.allowsEditing = true
        pickerController.delegate = delegate
        VC.present(pickerController, animated: true)
    }

    func takeImageFromCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.delegate = delegate
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        }
        VC.present(imagePicker, animated: true)
    }
}
