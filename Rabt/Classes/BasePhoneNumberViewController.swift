//
//  BasePhoneNumberViewController.swift
//  WahydTravel
//
//  Created by Ali Sher on 01/03/2021.
//

import UIKit
import libPhoneNumber_iOS
import SwiftData
import CountryPickerView

class BasePhoneNumberViewController: UIViewController {
    
    var PhoneMainView: UIView?
    
    var phoneIcon: UIImageView?
    var countryCodeLabel: UILabel?
    var downArrowIcon: UIImageView?
    var countryCodeBtn: UIButton?
    var phoneTF: UITextField?
    var lineView: UIView?
    
    var isChangeLineColor = false
    
    var phoneUtil = NBPhoneNumberUtil()
    var AsYouType = NBAsYouTypeFormatter()
    var ExampleNumber = ""
    var EFINumber = ""
    var NationalNumber = ""
    let countryPickerView = CountryPickerView()
    
    var availableCountries = [String]()
    var selectedCountryCode = "PK"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
    }
    
    @objc func countryCodeBtnClicked(sender: UIButton) -> Void {
        
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        countryPickerView.showCountriesList(from: self)
        
    }
    
    func LoadPhoneView() -> Void {
        
        phoneIcon = (PhoneMainView!.viewWithTag(100) as! UIImageView)
        countryCodeLabel = (PhoneMainView!.viewWithTag(101) as! UILabel)
        downArrowIcon = (PhoneMainView!.viewWithTag(102) as! UIImageView)
        countryCodeBtn = (PhoneMainView!.viewWithTag(103) as! UIButton)
        phoneTF = (PhoneMainView!.viewWithTag(104) as! UITextField)
        lineView = (PhoneMainView!.viewWithTag(105)!)
        
        phoneTF?.delegate = self
        phoneTF?.setTextField()
        
     
        phoneIcon?.tintColor = GlobalVariables.DarkGrayColor
        countryCodeLabel!.textColor = GlobalVariables.DarkGrayColor

        downArrowIcon?.tintColor = GlobalVariables.DarkGrayColor
        phoneTF?.textColor = GlobalVariables.TitleFontColor
        lineView?.backgroundColor = GlobalVariables.LightGrayColor
        
        lineView?.isHidden = true
        GeneralMethods.CreateCardViewWithoutShadow(radius: 4, view: phoneIcon!)
        phoneIcon?.backgroundColor = .clear
        phoneIcon?.clipsToBounds = true
        
        Initialze()
        
        let country = countryPickerView.getCountryByCode(selectedCountryCode)
        countryCodeLabel!.text = country?.phoneCode
        phoneIcon?.image = country?.flag
        
        countryCodeBtn?.addTarget(self, action: #selector(countryCodeBtnClicked(sender:)), for: .touchUpInside)
        
    }
    
    func Initialze(){
        
        AsYouType = NBAsYouTypeFormatter(regionCode: selectedCountryCode)
        phoneUtil = NBPhoneNumberUtil()
        getExampleNumber()
        do {
            let num = try phoneUtil.getExampleNumber(forType: selectedCountryCode, type: NBEPhoneNumberType.MOBILE)
            let str = try phoneUtil.format(num, numberFormat: .NATIONAL)
            ExampleNumber = str
            phoneTF?.text = ""
            
        } catch {
        }
        
    }
    
    func getExampleNumber() -> Void {
        
        do {
            //MARK: PlaceHolder
            
            AsYouType = NBAsYouTypeFormatter.init(regionCode: selectedCountryCode)
            
            let num = try phoneUtil.getExampleNumber(forType: selectedCountryCode, type: .MOBILE)
            let str = try phoneUtil.format(num, numberFormat: .NATIONAL)
            phoneTF?.placeholder = str
            
            let number = try phoneUtil.format(num, numberFormat: .INTERNATIONAL)
            var nationalNumber: NSString? = nil
            
            let CountryDialCode = phoneUtil.extractCountryCode(number, nationalNumber: &nationalNumber)
            
            let DialCode = String(format: "+%@", CountryDialCode!)
            countryCodeLabel?.text = DialCode
            
            
        }
        catch let error as Error {
            print(error.localizedDescription)
        }
    }
    
    func IsThisValidNumber(str: String) -> Bool {
        
        do {
            let number:NBPhoneNumber = try phoneUtil.parse(str, defaultRegion:selectedCountryCode)
            var isValid = false
            var IsMobileNumber = false
            
            var type = -1
            
            if isTesting {
                
                isValid = phoneUtil.isPossibleNumber(number)
                type = NBEPhoneNumberType.MOBILE.rawValue
                
            }else {
                
                isValid = phoneUtil.isValidNumber(number)
                type = phoneUtil.getNumberType(number).rawValue
            }
            
            if type == NBEPhoneNumberType.MOBILE.rawValue || type == NBEPhoneNumberType.FIXED_LINE_OR_MOBILE.rawValue  {
                
                IsMobileNumber = true
                EFINumber = String(format: "%@", try phoneUtil.format(number, numberFormat: .E164))
                EFINumber = String(EFINumber.dropFirst())
                
                NationalNumber = String(format: "%@", try phoneUtil.format(number, numberFormat: .NATIONAL))
            }
            
            if isValid && IsMobileNumber {
                
                return true
                
            }else {
                
                return false
            }
        }catch  let error as Error {
            print(error.localizedDescription)
            return false
        }
    }
}

extension BasePhoneNumberViewController: UITextFieldDelegate {
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        lineView?.backgroundColor = GlobalVariables.AppColor
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        lineView?.backgroundColor = GlobalVariables.LightGrayColor
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        lineView?.backgroundColor = GlobalVariables.LightGrayColor
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 104 {
            if string == "" {
                textField.text = AsYouType.removeLastDigit()
            } else {
                let output = AsYouType.inputDigit(string)
                textField.text = output
            }
            return false
        }
        return true
    }
}

extension BasePhoneNumberViewController: CountryPickerViewDelegate {
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        
        countryCodeLabel!.text = country.phoneCode
        selectedCountryCode = country.code
        phoneIcon?.image = country.flag
        Initialze()
    }
}

extension BasePhoneNumberViewController: CountryPickerViewDataSource {
//    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
//
//        return availableCountries.compactMap { countryPickerView.getCountryByCode($0) }
//    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return ""
    }
    
    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return ""
    }
        
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {

        
        //case 0: return .tableViewHeader
        //case 1: return .navigationBar
        //default: return .hidden
        
        return .tableViewHeader
    }
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
    func showCountryCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return false
    }
}

