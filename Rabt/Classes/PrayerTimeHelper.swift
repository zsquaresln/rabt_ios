////
////  PrayerTimeHelper.swift
////  Rabt
////
////  Created by Muhammad on 04/08/2021.
////  Copyright © 2021 Ali Sher. All rights reserved.
////
//
//import UIKit
//import Adhan
//import SwiftData
//
//enum prayerTypeName : String{
//    case prayer_0 = "Fajr Time"
//    case prayer_1 = "Sunrise Time"
//    case prayer_2 = "Dhuhr Time"
//     case prayer_3 = "Asr Time"
//    case prayer_4 = "Maghrib Time"
//    case prayer_5 = "Isha Time"
//}
//class PrayerTimeHelper: NSObject {
//
//    var param: CalculationParameters? = nil
//
//    func scheduleNotifications() {
//
//        DispatchQueue.global(qos: .background).async {
//
//            DispatchQueue.main.async {
//
//                self.removeAllPendingAndDeliveredNotifications()
//                if UserDefaults.standard.value(forKey: "pauseNotifications") as? Bool == true{
//                    return
//                }
//                // create notifications for the next coming 12 days
//                for index in 0..<12 {
//
//                    let newDate = Calendar.current.date(byAdding: .day, value: index, to: Date())!
//                    let prayers = self.getPrayerDatetime(date: newDate)
//
//                    // create notification for each prayer
//                    for iterator in 0..<prayers.count {
//                        // Skip sunrise
//                        if iterator == 1 { continue }
//
//                        // Skip the passed dates
//                        let calendar = Calendar.current
//                        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: prayers[iterator] as! Date)
//
//                        self.scheduleNotificationFor(prayerId: iterator, prayerTime: components)
//                    }
//                }
//            }
//        }
//    }
//
//    /// Schedule a notification for a specific prayer
//    @objc private func scheduleNotificationFor(prayerId: Int, prayerTime: DateComponents) {
//
//        let notifContent = UNMutableNotificationContent()
//
//        // create the title
//        let title = "appname".localized()
//        // create the prayer name
//        let idd = String("prayer_" + String(prayerId))
//
//        let prayerName = getPrayerName(type: idd).rawValue//idd.localized()
//        print(prayerName)
//        let month = prayerTime.month!
//        var month_str = ""
//
//        if month < 10 {
//            month_str = "0\(month)"
//        }else{
//
//            month_str = "\(month)"
//        }
//
//
//        let day = prayerTime.day!
//        var day_str = ""
//
//        if day < 10 {
//            day_str = "0\(day)"
//        }else{
//
//            day_str = "\(day)"
//        }
//
//        let hour = prayerTime.hour!
//        var hour_str = ""
//
//        if hour < 10 {
//            hour_str = "0\(hour)"
//        }else{
//
//            hour_str = "\(hour)"
//        }
//
//
//        let mins = prayerTime.minute!
//        var mins_str = ""
//
//        if mins < 10 {
//            mins_str = "0\(mins)"
//        }else{
//
//            mins_str = "\(mins)"
//        }
//
//
//        let date_Str = String(format: "%@/%@/%@", String(prayerTime.year!), month_str, day_str)
//        let time_Str = String(format: "%@:%@", hour_str, mins_str)
//
//        let save_Str = "\(date_Str) \(time_Str)"
//
//        var key = prayerName.lowercased()
//        let arr = key.components(separatedBy: " ")
//        key = arr[0]
//
//        let identifier = "\(key)_\(save_Str)"
//
//        // set notification items
//        notifContent.title = title
//        notifContent.subtitle = ""
//        notifContent.body = prayerName
//        notifContent.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "adhan.mp3"))
//
//        let notifTrigger = UNCalendarNotificationTrigger(dateMatching: prayerTime, repeats: false)
//        let notifRequest = UNNotificationRequest(identifier: identifier.lowercased(), content: notifContent, trigger: notifTrigger)
//
//        var val = "0"
//        val = prayerNotificaionsData.value(forKey: key) as? String ?? "0"
//
//        if val == "1" {
//
//            UNUserNotificationCenter.current().add(notifRequest, withCompletionHandler: nil)
//
//        }
//    }
//
//    func getCurrentTimeZone() -> String{
//
//        return TimeZone.current.identifier
//    }
//
//    /// This removes all current notifications before creating the new ones
//    func removeAllPendingAndDeliveredNotifications() {
//
//        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
//        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
//
//    }
//
//    func getPrayerDatetime(date: Date) -> [Any] {
//
//        var prayersArr : [Any] = []
//
//        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
//        let date1 = cal.dateComponents([.year, .month, .day], from: date)
//        let coordinates = Coordinates(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude)
//        var params = GlobalMethods.getCalculationMethod()
//        params.madhab = .hanafi
//
//        if let prayers = PrayerTimes(coordinates: coordinates, date: date1, calculationParameters: params) {
//
//            let formatter = DateFormatter()
//            formatter.timeStyle = .short
//            formatter.timeZone = TimeZone(identifier: self.getCurrentTimeZone())!
//            formatter.dateFormat = "HH:mm"
//
//
//            prayersArr = [prayers.fajr, prayers.sunrise, prayers.dhuhr, prayers.asr, prayers.maghrib, prayers.isha]
//
//        }
//
//        return prayersArr
//    }
//
//    func scheduleTodayPrayerNotification(title: String, body: String, identifier: String, sound: String, dateStr: String, dayOfWeek: Int) {
//
//        //Create content for your notification
//        let content = UNMutableNotificationContent()
//        content.title = title
//        content.subtitle = ""
//        content.body = body
//        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: sound))
//
//        let arr = dateStr.components(separatedBy: ":")
//        let hour = Int(arr[0])
//        let min = Int(arr[1])
//
//        //Use it to define trigger condition
//        var date = DateComponents()
//        date.calendar = Calendar.current
//        date.weekday = dayOfWeek //1 means sunday
//        date.hour = hour //Hour of the day
//        date.minute = min //Minute at which it should be sent
//
//
//        let trigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: false)
//        let iden = identifier.lowercased()
//
//        let request = UNNotificationRequest(identifier: iden, content: content, trigger: trigger)
//
//        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
//    }
//
//    func getPrayerName(type: String) -> prayerTypeName{
//        if type == "prayer_0"{
//            return .prayer_0
//        }
//        else if type == "Sunrise"{
//            return .prayer_1
//        }
//        else if type == "Dhuhr Time"{
//            return .prayer_2
//        }
//        else if type == "Asr Time"{
//            return .prayer_3
//        }
//        else if type == "Maghrib Time"{
//            return .prayer_4
//        }
//        else{
//            return .prayer_5
//        }
//    }
//}
