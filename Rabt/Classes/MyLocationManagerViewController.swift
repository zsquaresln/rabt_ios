//
//  MyLocationManager.swift
//  WahydTravel
//
//  Created by Ali Sher on 25/05/2021.
//

import UIKit
import CoreLocation

class MyLocationManagerViewController: MasterViewController {
    
    var locationManager = CLLocationManager()
    var location = CLLocation()
    
    let ceo: CLGeocoder = CLGeocoder()
    //these two have location option
    var parentController : HomeViewController?
    var prayController : PrayViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeLocationManager()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //locationManager.stopUpdatingLocation()
        
    }
    
    func initializeLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.headingFilter = CLLocationDegrees(1)
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startUpdatingLocation()
    }
    
    
}

extension MyLocationManagerViewController: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedWhenInUse, .authorizedAlways:
                // Enable features that require location services here.
                PrayerTimeManger.shared.selectedLocationName = nil
                PrayerTimeManger.shared.prayerTimeCoordinates = nil
                PrayerTimeManger.shared.shouldRefreshPrayerTimings = true
                PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
                locationManager.startUpdatingLocation()
            case .notDetermined:
                return
            case .restricted:
                return
            case .denied:
                return
            @unknown default:
                return
            }
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
            break
        case .restricted:
            break
        case .denied:
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        location = locations.last ?? CLLocation(latitude: 0.0000, longitude: 0.0000)
        myLocation = location
        
        ceo.reverseGeocodeLocation(location, completionHandler:
                                    {(placemarks, error) in
                                        if (error != nil)
                                        {
                                            print("reverse geodcode fail: \(error!.localizedDescription)")
                                        }
                                        guard let pm = placemarks?.first else {
                                            print("No placemark from Apple: \(String(describing: error))")
                                            return
                                        }
                                        myLocationPlacemark = pm
                                        print(pm.country ?? "country")
                                        print(pm.locality ?? "locality")
                                        print(pm.subLocality ?? "subLocality")
                                        print(pm.thoroughfare ?? "thoroughfare")
                                        print(pm.postalCode ?? "postalCode")
                                        print(pm.subThoroughfare ?? "subThoroughfare")
                                        var addressString : String = ""
                                        
                                        if pm.subLocality != nil {
                                            addressString = addressString + pm.subLocality! + ", "
                                        }
                                        if pm.thoroughfare != nil {
                                            addressString = addressString + pm.thoroughfare! + ", "
                                        }
                                        if pm.locality != nil {
                                            addressString = addressString + pm.locality! + ", "
                                        }
                                        if pm.country != nil {
                                            addressString = addressString + pm.country! + ", "
                                        }
                                        if pm.postalCode != nil {
                                            addressString = addressString + pm.postalCode! + " "
                                        }
                                        print(addressString)
                                        self.parentController?.getTimings()
                                        if self.prayController != nil{
                                        self.prayController?.getTimings()
                                        }
                                    })
        
        self.locationManager.stopUpdatingLocation()
    }
     
    func isLocationServicesEnabled() -> Bool{
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
               return false
            case .restricted, .denied:
                // Disable location features
                return false

            case .authorizedWhenInUse, .authorizedAlways:
                // Enable features that require location services here.
                return true
            }
        }
        return false
    }
    func enableUsersLocationServicesAuthorization(){
            /// Check if user has authorized Total Plus to use Location Services
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined:
                    // Request when-in-use authorization initially
                    // This is the first and the ONLY time you will be able to ask the user for permission
                    self.locationManager.delegate = self
                    locationManager.requestWhenInUseAuthorization()
                    break

                case .restricted, .denied:
                    // Disable location features
    
                    let alert = UIAlertController(title: "Allow Location Access", message: "RABT needs access to your location. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)

                    // Button to Open Settings
                    alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                print("Settings opened: \(success)")
                            })
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    if self.prayController != nil{
                        prayController?.present(alert, animated: true, completion: nil)
                    }else{
                        self.present(alert, animated: true, completion: nil)
                    }
                   

                    break

                case .authorizedWhenInUse, .authorizedAlways:
                    // Enable features that require location services here.
                    print("Full Access")
                    break
                }
            } else{
                // this means that iphone location services is disabled in privacy->location services
                let alert = UIAlertController(title: "Allow Location Access", message: "RABT needs access to your location. Turn on Location Services in your device settings under privacy.", preferredStyle: UIAlertController.Style.alert)

                // Button to Open Settings
//                alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
//                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
//                        return
//                    }
//                    if UIApplication.shared.canOpenURL(settingsUrl) {
//                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                            print("Settings opened: \(success)")
//                        })
//                    }
//                }))
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                if self.prayController != nil{
                    prayController?.present(alert, animated: true, completion: nil)
                }else{
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
}
