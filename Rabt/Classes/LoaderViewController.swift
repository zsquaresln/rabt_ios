//
//  LoaderViewController.swift
//  PhysioPerform
//
//  Created by Ali Sher on 26/11/2020.
//

import UIKit

class LoaderViewController: UIViewController {
    
    var LoaderView: UIView?
    
    var loadingLabel: UILabel?
    var imgView: UIImageView?
    var noRecordLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
    }
    
    
    func showLoader() -> Void {
        
        
        loadingLabel = (LoaderView!.viewWithTag(100) as! UILabel)
        imgView = (LoaderView!.viewWithTag(101) as! UIImageView)
        noRecordLabel = (LoaderView!.viewWithTag(102) as! UILabel)

        loadingLabel!.textColor = .lightGray
        noRecordLabel!.textColor = .darkGray

        loadingLabel!.text = "Loading".localized()
        noRecordLabel!.text = "no record found".localized()


        loadingLabel!.isHidden = false
        noRecordLabel!.isHidden = true
        imgView!.isHidden = true
        
        
    }
    
}
