//
//  MenuButtonViewController.swift
//  CarCare
//
//  Created by Ali Sher on 29/01/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData

class MenuButtonViewController: UIViewController {
    
    var menuBtn = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        GlobalVariables.CurrentView = self
        
        menuBtn.setImage(UIImage(named: "Menu"), for: .normal)
        menuBtn.setTitleColor(.black, for: .normal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuBtn)
        
        
        self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        self.view.addGestureRecognizer((self.revealViewController()?.tapGestureRecognizer())!)
        self.revealViewController().rightViewRevealOverdraw = 0.0
        
        if (self.revealViewController() != nil) {
                    
            if GeneralMethods.isRTL() {
                        
                menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchDown)
                        
            }else{
                        
                menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchDown)
            }
        }
    }
}
