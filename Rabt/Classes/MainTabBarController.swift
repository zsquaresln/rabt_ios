//
//  MainTabBarController.swift
//  SampleSporify
//
//  Created by Emre Havan on 30.03.20.
//  Copyright © 2020 Emre Havan. All rights reserved.
//

import UIKit
import StickyTabBarViewController

class MainTabBarController: StickyViewControllerSupportingTabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        kAPPDelegate.tabBarController = self
        self.delegate = self
    }
}
extension MainTabBarController{
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if isCollapsedShown{
            self.collapseChild()
        }
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if self.selectedViewController == viewController{
            return false
        }
        return true
    }
}
