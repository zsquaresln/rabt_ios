//
//  AppVersionManager.swift
//  Rabt
//
//  Created by Asad Khan on 17/03/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class AppVersionManager: NSObject {
    var parentController : HomeViewController?
 
    
    func isUpdateAvailable() throws -> Bool {
        guard let info = Bundle.main.infoDictionary,
              let currentVersion = info["CFBundleShortVersionString"] as? String
                // let identifier = info["CFBundleIdentifier"] as? String,
                //let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)")
        else {
            return false
        }
        if currentVersion != splashSingelton.sharedInstance.appstoreVersion{
            return true
        }
        //        let data = try Data(contentsOf: url)
        //        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
        //            return false
        //        }
        //        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
        //            print("version in app store", version,currentVersion);
        //            updatedVersion = version
        //            return version != currentVersion
        //        }
        return false
    }
    func popupUpdateDialogue(){
        
//        let alertMessage = "\(splashSingelton.sharedInstance.updateMessage) \(splashSingelton.sharedInstance.appstoreVersion)";
//        let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: UIAlertController.Style.alert)
//
//        let okBtn = UIAlertAction(title: "Update", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//            //  https://apps.apple.com/pk/app/rabt-nasheed-muslim-prayer/id1572844646
//            if let url = URL(string: "itms-apps://itunes.apple.com/pk/app/rabt-nasheed-muslim-prayer/id1572844646"),
//               UIApplication.shared.canOpenURL(url){
//                if #available(iOS 10.0, *) {
//                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                } else {
//                    UIApplication.shared.openURL(url)
//                }
//            }
//        })
//        alert.addAction(okBtn)
//        if splashSingelton.sharedInstance.isForceUpdate == false{
//            let noBtn = UIAlertAction(title:"Skip this Version" , style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
//            })
//            alert.addAction(noBtn)
//        }
//        parentController?.present(alert, animated: true, completion: nil)
        let versionUpdatePopup = VersionUpdatePopup(nibName: "VersionUpdatePopup", bundle: nil)
        parentController?.tabBarController?.parent?.add(versionUpdatePopup)
    }
}
