//
//  VersionUpdatePopup.swift
//  Rabt
//
//  Created by Asad Khan on 21/03/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class VersionUpdatePopup: MasterViewController {
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var msgLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        showVersionPopup()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Helper functions
    func showVersionPopup(){
        self.msgLabel.text =  "\(splashSingelton.sharedInstance.updateMessage) \(splashSingelton.sharedInstance.appstoreVersion)"
        if splashSingelton.sharedInstance.isForceUpdate == false{
            cancelBtn.isHidden = false
        }
        else{
            cancelBtn.isHidden = true
        }
    }
    //MARK: - IBActions
    @IBAction func cancelBtnTapped(_ sender: Any) {
        self.remove()
    }
    
    @IBAction func updateBtnTapped(_ sender: Any) {
        if let url = URL(string: "itms-apps://itunes.apple.com/pk/app/rabt-nasheed-muslim-prayer/id1572844646"),
           UIApplication.shared.canOpenURL(url){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
