//
//  PhoneView.swift
//  WahydTravel
//
//  Created by Ali Sher on 01/03/2021.
//

import UIKit

class PhoneView: UIView {

    @IBOutlet var PhoneMainView: UIView!
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }

    func customInit() {
        Bundle.main.loadNibNamed("PhoneView", owner: self, options: nil)
        addSubview(PhoneMainView)
        PhoneMainView.frame = bounds
    }
}
