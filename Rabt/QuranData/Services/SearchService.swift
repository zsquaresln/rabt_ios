//
//  SearchService.swift
//  Qur'an Pro
//
//  Created by Adil Ben Moussa on 10/27/15.
//  Copyright © 2015 https://github.com/adilbenmoussa All rights reserved.
//  GNU GENERAL PUBLIC LICENSE https://raw.githubusercontent.com/adilbenmoussa/Quran-Pro-iOS/master/LICENSE
//

private let _SearchServiceSharedInstance = SearchService()

class SearchService {
    
    class func sharedInstance() -> SearchService {
        return _SearchServiceSharedInstance
    }

    
    //hierarchical chapters and versers
    //Get a list of keys and contents from the persistent data
    // to be used in the tableview
    func initialKeysAndContents() -> (keys: [Any], contents: [String:Any]){
        var contents:[String:Any] = [:]
        var keys:[Any] = []
        var key: String
        
        //Construct the key list with empty content
        for chapter in dollar.chapters {
            key = dollar.getKeyId(chapter)
            if (contents[key] == nil) {
                keys.append(key)
                var list : [Any] = []
                for verse in chapter.verses {
                    if verse.id != -1 {
                        list.append(verse)
                    }
                }
                contents[key] = list
            }
        }
        return (keys: keys, contents: contents)
    }
    
    //Get a list of keys and contents from the persistent data
    // to be used in the tableview
    func sortedKeysAndContents(_ list: [Any]) -> (keys: [Any], contents: [String:Any]){
        let sortedByChapter: [Any] = list.sorted{(($0 as! [String:Any])["chapterId"] as! Int)  < (($1 as! [String:Any])["chapterId"] as! Int)}//list.sortedArray(using: [NSSortDescriptor(key: "chapterId", ascending: true)]) as [Any]
        let sortedByVerse: [Any] = list.sorted{(($0 as! [String:Any])["id"] as! Int)  < (($1 as! [String:Any])["id"] as! Int)}//list.sortedArray(using: [NSSortDescriptor(key: "id", ascending: true)]) as [Any]
        
        var contents:[String:Any] = [:]
        var keys:[Any] = []
        
        var chapter: Chapter
        var verse: Verse
        var values: [Any]
        var key: String
        
        //Construct the key list with empty content
        for item in sortedByChapter {
            if let v: Verse = item as? Verse {
                chapter = dollar.chapters[v.chapterId]
                key = dollar.getKeyId(chapter)
                if (contents[key] == nil) {
                    keys.append(key)
                    contents[key] = []
                }
            }
        }
        
        //fill in the content of the keys
        for item in sortedByVerse {
            if let v: Verse = item as? Verse {
                chapter = dollar.chapters[v.chapterId]
                key = dollar.getKeyId(chapter)
                if (contents[key] != nil) {
                    values = (contents[key] as? [Any])!
                    if v.chapterId == chapter.id {
                        var verseId: Int = v.id
                        if chapter.id == kFatihaIndex || chapter.id == kTaubahIndex {
                            verseId = verseId - 1
                        }
                        verse = chapter.verses[verseId]
                        values.append(verse)
                    }
                    contents[key] = values
                }
            }
        }
        return (keys: keys, contents: contents)
    }

}
