//
//  BookmarkService.swift
//  Qur'an Pro
//
//  Created by Adil Ben Moussa on 10/27/15.
//  Copyright © 2015 https://github.com/adilbenmoussa All rights reserved.
//  GNU GENERAL PUBLIC LICENSE https://raw.githubusercontent.com/adilbenmoussa/Quran-Pro-iOS/master/LICENSE
//
import UIKit

private let _ABRepeatServiceSharedInstance = ABRepeatService()

class ABRepeatService {

    class func sharedInstance() -> ABRepeatService {
        return _ABRepeatServiceSharedInstance
    }
    
    //keep a reference to the bookmarks
    var bookMarks: [Any]!

    init() {
        self.load()
    }

    //Load the persitent bookmarks
    fileprivate func load(){
        // Loads the bookmarks
        if let bookMarksData = Bundle.readArrayPlistFromDocumentFolder(kABRepeatFile) {
            bookMarks = bookMarksData as! [Any]
        }
        // No bookmarkt found yet, so create a new empty file
        else{
            bookMarks = []
            Bundle.writeArrayPlistToDocumentFolder(filename: kABRepeatFile, array: self.bookMarks)
        }
    }
    
    //Check the passed wheter the passed verse is bookmarked or not
    func has(_ verse: Verse) -> Bool {
        if(bookMarks != nil) {
            for bookmark in bookMarks {
                if let kBookmark  = bookmark as? [String:Any] {
                    if (kBookmark[kChapterhId] as? Int == verse.chapterId) && (kBookmark[kVerseId] as? Int == verse.id) {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    //Remove the passed verse from the bookmark
    func remove(_ verse: Verse){
        var dirty = false
        var currentIndex = 0
        for bookmark in bookMarks {
            if let kBookmark  = bookmark as? [String:Any] {
                if (kBookmark[kChapterhId] as? Int == verse.chapterId) && (kBookmark[kVerseId] as? Int == verse.id) {
                    bookMarks.remove(at: currentIndex)
                   // bookMarks.remove(bookmark)
                    dirty = true
                    break
                }
            }
            currentIndex += 1
        }
        
        if dirty {
            Bundle.writeArrayPlistToDocumentFolder(filename: kABRepeatFile, array: bookMarks)
        }
    }
    
    //Add the passed verse from the bookmark
    func add(_ verse: Verse){
        bookMarks.append([kChapterhId: verse.chapterId, kVerseId: verse.id])
        Bundle.writeArrayPlistToDocumentFolder(filename: kABRepeatFile, array: bookMarks)
    }
    
    //Remove all bookmarks
    func clear() {
        bookMarks = []
        Bundle.writeArrayPlistToDocumentFolder(filename: kABRepeatFile, array: bookMarks)
    }
    
    //Check if the bookmark list is empty
    func isEmpty () -> Bool{
        return bookMarks.count == 0
    }
    
    
    //Get a list of keys and contents from the persistent data
    // to be used in the tableview
    func sortedKeysAndContents() -> (keys: [Any], contents: [String:Any]){
        let sortedBookmarksByChapter: [Any] = bookMarks.sorted{(($0 as! [String:Any])[kChapterhId] as! Int)  < (($1 as! [String:Any])[kChapterhId] as! Int)} //.sortedArray(using: [NSSortDescriptor(key: kChapterhId, ascending: true)]) as [Any]
        let sortedBookmarksByVerse: [Any] = bookMarks.sorted{(($0 as! [String:Any])[kVerseId] as! Int)  < (($1 as! [String:Any])[kVerseId] as! Int)} //.sortedArray(using: [NSSortDescriptor(key: kVerseId, ascending: true)]) as [Any]
        
        var contents:[String:Any] = [:]
        var keys:[Any] = []
        
        var chapter: Chapter
        var verse: Verse
        var values: [Any]
        var key: String
        
        //Construct the key list with empty content
        for item in sortedBookmarksByChapter {
            chapter = dollar.chapters[(item as AnyObject).object(forKey: kChapterhId) as! Int]
            key = dollar.getKeyId(chapter)
            if (contents[key] == nil) {
                keys.append(key)
                contents[key] = []//([], forKey: key as NSCopying)
            }
        }
        
        //fill in the content of the keys
        for item in sortedBookmarksByVerse {
            chapter = dollar.chapters[(item as AnyObject).object(forKey: kChapterhId) as! Int]
            key = dollar.getKeyId(chapter)
            if (contents[key] != nil) {
                values = (contents[key] as? [Any])!
                if (item as AnyObject).object(forKey: kChapterhId) as? Int == chapter.id {
                    var verseId: Int = ((item as AnyObject).object(forKey: kVerseId) as? Int)!
                    if chapter.id == kFatihaIndex || chapter.id == kTaubahIndex {
                        verseId = verseId - 1
                    }
                    verse = chapter.verses[verseId]
                    values.append(verse)
                }
                contents[key] = values
            }
        }
        return (keys: keys, contents: contents)
    }
}

