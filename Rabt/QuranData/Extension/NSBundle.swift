//
//  NSBundle.swift
//  Qur'an Pro
//
//  Created by Adil Ben Moussa on 10/27/15.
//  Copyright © 2015 https://github.com/adilbenmoussa All rights reserved.
//  GNU GENERAL PUBLIC LICENSE https://raw.githubusercontent.com/adilbenmoussa/Quran-Pro-iOS/master/LICENSE
//

import Foundation

extension Bundle {
    
    @objc class func documents() -> String! {
        let dirs: [AnyObject] = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true) as [AnyObject]
        return dirs[0] as? String
    }
    
    @objc class func readPlist(_ filename: String, fromDocumentsFolder: Bool=false) -> AnyObject? {
        let plist:AnyObject?
        if let path = Bundle.main.path(forResource: filename, ofType: "plist") {
            let data: Data?
            do {
                data = try Data(contentsOf: URL(fileURLWithPath: path), options: Data.ReadingOptions())
            } catch  {
                data = nil
            }
            do {
                plist = try PropertyListSerialization.propertyList(from: data!, options: PropertyListSerialization.MutabilityOptions(), format: nil) as AnyObject
                return plist
            } catch  {
                plist = nil
            }
        }
        return nil
    }
    
    @objc class func readArrayPlist(_ filename: String) -> [Any]? {
        if let array: AnyObject = readPlist(filename) {
            if let array = array as? [Any]? {
                return array
            }
            else{
                print("Loaded plist file '\(filename)' is not Array")
            }
        }
        return nil
    }
    
    @objc class func readDictionayPlist(_ filename: String) -> [String:Any]? {
        if let dictinary: AnyObject = readPlist(filename) {
            if let dictinary = dictinary as? [String:Any]? {
                return dictinary
            }
            else{
                print("Loaded plist file '\(filename)' is not Dictionary")
            }
        }
        return nil
    }
    
    @objc class func writeArrayPlistToDocumentFolder(filename: String, array: [Any]) {
        let convertedToNSArray =  NSArray(array: array)
        let path:String = documents().stringByAppendingPathComponent("\(filename).plist")
        convertedToNSArray.write(toFile: path, atomically:true)
    }
    
    @objc class func readArrayPlistFromDocumentFolder(_ filename: String) -> [Any]? {
        let path:String = documents().stringByAppendingPathComponent("\(filename).plist")
        if let array: NSArray = NSArray(contentsOfFile: path) {
            return array as? [Any]
        }
        return nil;
    }
    
    @objc class func writeDictionaryPlistToDocumentFolder(filename: String, dictionary: [String:Any]) {
        let convertedToNSDictionary =  NSDictionary(dictionary: dictionary)
        let path:String = documents().stringByAppendingPathComponent("\(filename).plist")
        convertedToNSDictionary.write(toFile: path, atomically:true)
    }
    
    @objc class func readDictionaryPlistFromDocumentFolder(_ filename: String) -> [String:Any]? {
        let path:String = documents().stringByAppendingPathComponent("\(filename).plist")
        if let dictionary: NSDictionary = NSDictionary(contentsOfFile: path) {
            return dictionary as? [String:Any]
        }
        return nil;
    }
}
