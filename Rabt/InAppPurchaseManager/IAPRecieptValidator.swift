//
//  IAPRecieptValidator.swift
//  Rabt
//
//  Created by Asad Khan on 21/08/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import TPInAppReceipt
class IAPRecieptValidator: NSObject {
   
    static let sharedInstance = IAPRecieptValidator()
    var isSubscriptionActive = false
    let verifyReceiptURL = //"https://sandbox.itunes.apple.com/verifyReceipt" // sandbox
     "https://buy.itunes.apple.com/verifyReceipt" // production
    let password = "2bbab82424b149b5ad26224d88b6fdc5"

    var receiptJson : [String:Any] = [:]
//    static func isVIPuser(identifier: String,completionHandler: @escaping(_ success:Bool) -> Void){
//
//        InAppReceipt.refresh { (error) in
//            if let _ = error
//            {
//                completionHandler(false)
//            }else{
//                if let receipt = try? InAppReceipt.localReceipt(){
//                    //  let filtered =  receipt.purchases(ofProductIdentifier: identifier)
//                    // Print(filtered.first?.cancellationDate)
//                    if receipt.hasActiveAutoRenewableSubscription(ofProductIdentifier: identifier, forDate: Date()) {
//                        // user has subscription of the product, which is still active at the specified date
//                        completionHandler(true)
//                    }else{
//                        completionHandler(false)
//                    }
//                }
//            }
//        }
//    }

    
//    static func referestSubscriptionReceipts(){
//        InAppReceipt.refresh { (error) in
//            if let err = error
//            {
//                print(err)
//            }else{
//                // do your stuff with the receipt data here
//                //            if let receipt = try? InAppReceipt.localReceipt() {
//                //              // ...
//                //            }
//            }
//        }
//    }
    func fetchLatestReceiptData(completion: @escaping(_ isPurchaseSchemeActive: Bool, _ error: String?) -> ()) {
       // InAppReceipt.refresh { (error) in
//            if let _ = error
//            {
//                completion(false, "No receipts found")
//            }else{
                let receiptFileURL = Bundle.main.appStoreReceiptURL
                guard let receiptData = try? Data(contentsOf: receiptFileURL!) else {
                    //This is the First launch app VC pointer call
                    completion(false, nil)
                    return
                }
                let recieptString = receiptData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString as AnyObject, "password" : self.password as AnyObject]
                
                do {
                    let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let storeURL = URL(string: self.verifyReceiptURL)!
                    var storeRequest = URLRequest(url: storeURL)
                    storeRequest.httpMethod = "POST"
                    storeRequest.httpBody = requestData
                    let session = URLSession(configuration: URLSessionConfiguration.default)
                    let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
                        do {
                            guard let _ = data else {
                                completion(false, "Parse Error")
                                return
                            }
                            if let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any] {
                               // self?.receiptJson = jsonResponse
                                IAPRecieptValidator.sharedInstance.receiptJson = jsonResponse
                                if let receiptInfo: [Any] = self?.receiptJson["latest_receipt_info"] as? [Any] {
                                    let tempReceiptInfo = receiptInfo.sorted(by: {(dict1, dict2)  -> Bool in
                                        return (Double((dict1 as! [String:Any])["purchase_date_ms"] as! String) ?? 0 < Double((dict2 as! [String:Any])["purchase_date_ms"] as! String) ?? 0) // It sorted the values and return to the mySortedArray
                                      })
                                    self?.receiptJson = [:]
                                    self?.receiptJson = jsonResponse
                                    self?.receiptJson["latest_receipt_info"] = tempReceiptInfo
                                    
                                }
                                completion(true, "")
                              //  print("json response \(jsonResponse)")
                                //                                if let expiresDate = self?.getPurchaseAndExpirationDateFromResponse(jsonResponse) {
                                //                                    //print("expiresDate \(expiresDate)")
                                //                                    let purchaseStatus = self?.isSubscriptionActive(expireDate: expiresDate)
                                //                                    if let purchaseStatus = purchaseStatus {
                                //                                        completion(true, nil)
                                //                                    }
                                //                                }
//                                if let date = self?.getExpirationDateFromResponse(jsonResponse as! [String:Any]) {
//                                    print(date)
//                                }
                            }
                        } catch let parseError {
                            completion(false, "Parse Error")
                        }
                    })
                    task.resume()
                } catch let parseError {
                    completion(false, "Parse Error")
                }
       //     }
  //      }
    }
    func isVIPuser() -> (Bool,Bool) {
        //print(receiptJson)
        if let receiptInfo: [Any] = receiptJson["latest_receipt_info"] as? [Any] {
            let lastReceipt = receiptInfo.last as! [String:Any]
          //  print(lastReceipt)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            var isSubscriptionValid = false
            if let expiresDate = lastReceipt["expires_date"] as? String, let purchaseDate =  lastReceipt["purchase_date"] as? String {
                if let purchase =  formatter.date(from: purchaseDate), let expiry = formatter.date(from: expiresDate){
                    if Date() >= purchase &&  Date() < expiry{
                        isSubscriptionValid = true
                    }
                }else{
                    isSubscriptionActive = false
                    return (false,false)
                }
            }
            if isSubscriptionValid{
                if let renewalInfo = (receiptJson["pending_renewal_info"] as? [[String:Any]])?.last{
                    if let autoRenewStatus = renewalInfo["auto_renew_status"] as? String , autoRenewStatus == "1"{
                        isSubscriptionActive = true
                        splashSingelton.sharedInstance.disableAllAds()
                        return (true,true)
                    }else{
                        splashSingelton.sharedInstance.disableAllAds()
                        isSubscriptionActive = true
                        return (true,false)
                    }
                }else{
                    isSubscriptionActive = false
                    return (false, false)
                }
            }else{
                isSubscriptionActive = false
                return (false, false)
            }
        }
        else {
            isSubscriptionActive = false
            return (false, false)
        }
    }
    func getExpirationDateOfReceipt() -> String? {

        if let receiptInfo: [Any] = receiptJson["latest_receipt_info"] as? [Any] {

            let lastReceipt = receiptInfo.last as! [String:Any]
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            if let expiresDate = lastReceipt["expires_date"] as? String {
                if let expiry = formatter.date(from: expiresDate){
                    formatter.dateFormat = "dd/MM/yyyy"
                    return formatter.string(from: expiry)
                }else{
                    return nil
                }
            }

            return nil
        }
        else {
            return nil
        }
    }
}
