//
//  RabtPremiumProduct.swift
//  Rabt
//
//  Created by Asad Khan on 20/08/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit


import Foundation

public struct RabtPremiumProduct {
  
  public static let SwiftShopping = "com.rabt.islamic.application.MonthlySubscription"
  
  private static let productIdentifiers: Set<ProductIdentifier> = [RabtPremiumProduct.SwiftShopping]

  public static let store = IAPHelper(productIds: RabtPremiumProduct.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
  return productIdentifier.components(separatedBy: ".").last
}

