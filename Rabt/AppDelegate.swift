//
//  AppDelegate.swift
//  Rabt
//
//  Created by Ali Sher on 25/06/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//import SwiftData
import Firebase
//import RealmSwift
import CoreData
import AVFoundation
import UserNotifications
import GoogleMobileAds

//import AppLovinSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    
    
    var window: UIWindow?
    //let urlScheme = "com.brainTree.Donate.payments"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //   application.isStatusBarHidden = true
        //DBHelper.initilizeDB()
        
        print(UserDefaults.standard.value(forKey: "device_token") ?? "token")
        //MARK: IQKeyboard Settings
        // Please make sure to set the mediation provider value to "max" to ensure proper functionality

//               ALSdk.shared()!.mediationProvider = "max"
//               
//               ALSdk.shared()!.userIdentifier = "qFQy_uuQD7z5AL7T0m2J2r-ThtXyc4cPKiJXrw0LCL2K9b75h7p63hTxQVw1DMHFV4SX-WaoIvTn6-MCHgWXO7"
//               
//               ALSdk.shared()!.initializeSdk { (configuration: ALSdkConfiguration) in
//                   // Start loading ads
//               }

        
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.shared.layoutIfNeededOnUpdate = true
        
        //MARK: Navigation Bar Settings
        
        UINavigationBar.appearance().barTintColor = GlobalVariables.AppColor
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        UINavigationBar.appearance().isTranslucent = false
        UIApplication.shared.statusBarUIView?.backgroundColor = .clear
        
        UITabBar.appearance().unselectedItemTintColor = GlobalVariables.DarkGrayColor
        UITabBar.appearance().tintColor = GlobalVariables.AppColor
        
        UITabBar.appearance().barTintColor = UIColor.white
        
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
        
        
        FirebaseApp.configure()
        
        registerForPushNotifications()
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        UIApplication.shared.setMinimumBackgroundFetchInterval(12 * 3600)
        if launchOptions != nil {
                // opened from a push notification when the app is closed
                let userInfo = launchOptions?[.remoteNotification] as? [AnyHashable : Any]
                if userInfo != nil {
                    UserDefaults.standard.set(userInfo, forKey: "isFromNotification")
                    UserDefaults.standard.synchronize()
                }
            } else {
                // opened app without a push notification.
            }
        return true
    }
    
    //    func InitializeRealmDB() -> Void {
    //
    //        //let realm = try! Realm()
    //        //print(Realm.Configuration.defaultConfiguration.fileURL ?? "")
    //
    //        let obj = PrayersModel()
    //
    //        obj.fajr = "0"
    //        obj.sunrise = "0"
    //        obj.dhuhr = "0"
    //        obj.asr = "0"
    //        obj.maghrib = "0"
    //        obj.isha = "0"
    //
    //        obj.nextPrayer = "0"
    //        obj.nextPrayerName = ""
    //
    ////        try! realm.write {
    ////
    ////            realm.add(obj)
    ////
    ////        }
    //
    //
    //        let managedContext = self.persistentContainer.viewContext
    //
    //        // 2
    //        let entity =
    //            NSEntityDescription.entity(forEntityName: "PrayerNotificaions",
    //                                       in: managedContext)!
    //
    //        let notificaion = NSManagedObject(entity: entity,
    //                                          insertInto: managedContext)
    //
    //        // 3
    //        notificaion.setValue("0", forKeyPath: "fajr")
    //        notificaion.setValue("0", forKeyPath: "sunRise")
    //        notificaion.setValue("0", forKeyPath: "dhuhr")
    //        notificaion.setValue("0", forKeyPath: "asr")
    //        notificaion.setValue("0", forKeyPath: "maghrib")
    //        notificaion.setValue("0", forKeyPath: "isha")
    //        notificaion.setValue("0", forKeyPath: "nextPrayer")
    //        notificaion.setValue(" ", forKeyPath: "nextPrayerName")
    //
    //        // 4
    //        do {
    //            try managedContext.save()
    //
    //        } catch let error as NSError {
    //            print("Could not save. \(error), \(error.userInfo)")
    //        }
    //
    //        UserDefaults.standard.setValue("yes", forKey: "isAlready")
    //        UserDefaults.standard.synchronize()
    //
    //    }
    
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Should schedule new notifications from background
        
        completionHandler(.newData)
        //let realm = try! Realm()
        //let result = realm.objects(PrayersModel.self)
        //prayerNotificaionsData = result[0]
        
        //    prayerNotificaionsData = GlobalMethods.fetchData()
        
        //     PrayerTimeHelper().scheduleNotifications()
        
    }
    
    
    //MARK: Push Notification Code
    
    func registerForPushNotifications() {
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) {
                (granted, error) in
                if granted {
                    print("yes")
                    self.getNotificationSettings()
                } else {
                    print("No")
                }
            }
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
      //  UserDefaults.standard.set(token, forKey: "deviceToken")
     //   print("Device Token: \(token)")
    }
    func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error
    ) {
        print("Failed to register: \(error)")
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        let userinfo = notification.request.content.userInfo
        let notiData = userinfo //as [String:Any]
        print("Notification arrived = \(notiData)")
        //"type,id"
        completionHandler([.alert])
        //handlePushType(data: notiData, nav: getCurrentNavigationController())
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let notiData = userInfo //as [String:Any]
        print("Notification arrivedddd = \(notiData)")
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        let notiData = userInfo //as [String:Any]
        print("Notification arrived = \(notiData)")
        DeepLinkManager().performDeepLinkIfAny(notification: notiData)
        //handlePushType(data: notiData, nav: getCurrentNavigationController())
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Device Token: \(fcmToken ?? "")")
        UserDefaults.standard.setValue(fcmToken, forKey: "device_token")
        UserDefaults.standard.synchronize()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        
        
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Rabt")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

extension UIApplication {
    var statusBarUIView: UIView? {
        
        if #available(iOS 13.0, *) {
            let tag = 3848245
            
            let keyWindow = UIApplication.shared.connectedScenes
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows.first
            
            if let statusBar = keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                let height = keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? .zero
                let statusBarView = UIView(frame: height)
                statusBarView.tag = tag
                statusBarView.layer.zPosition = 999999
                
                keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
            
        } else {
            
            if responds(to: Selector(("statusBar"))) {
                return value(forKey: "statusBar") as? UIView
            }
        }
        return nil
    }
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UIView {
    
    func roundCornersWithLayerMask(cornerRadii: CGFloat, corners: UIRectCorner) {
        let path = UIBezierPath(roundedRect: bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: cornerRadii, height: cornerRadii))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }
}

extension NSMutableAttributedString {
    
    func setStrikeText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        
        self.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        
    }
    
}

extension NSMutableAttributedString {
    
    func setBoldText(textForAttribute: String, withColor color: UIColor, withFontSize size: Int) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        
        
        self.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: CGFloat(size), weight: .bold), range: range)
        
    }
    
}

extension NSMutableAttributedString {
    
    func setSmallerText(textForAttribute: String, withColor color: UIColor, withFontSize size: Int) {
        
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        
        self.setAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(size), weight: .bold),
                            NSAttributedString.Key.foregroundColor: color],
                           range: range)
        
    }
    
}

extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
}

extension NSMutableAttributedString {
    
    func setUnderLineText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        
        self.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: range)
    }
    
}


extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}
extension NSMutableAttributedString {
    
    public func appendWith(text:String,addNewLine:Bool,color: UIColor) {
        
        if text.count > 0{
            let attributesOfString = [
                NSAttributedString.Key.foregroundColor: color,
                NSAttributedString.Key.font: UIFont(name: "Al Qalam Quran Majeed Web", size: 14)!
            ] as [NSAttributedString.Key : Any]
            if addNewLine{
                let attributedString = NSMutableAttributedString.init(string: "\n\(text)", attributes: attributesOfString)
                self.append(attributedString)
            }else{
                let attributedString = NSMutableAttributedString.init(string: " \(text)", attributes: attributesOfString)
                self.append(attributedString)
            }
            
        }
        
    }
}

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}


extension Date {
    
    //    func offsetFrom(date: Date) -> String {
    //
    //        let dayHourMinuteSecond: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
    //        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self)
    //
    //        let seconds = difference.second ?? 0
    //        let sec_str = seconds == 1 ? "\(seconds) second ago" : "\(seconds) seconds ago"
    //
    //        let minutes = difference.minute ?? 0
    //        let min_str = minutes == 1 ? "\(minutes) mint ago" : "\(minutes) mints ago"
    //
    //        let hours = difference.hour ?? 0
    //        let hour_str = hours == 1 ? "\(hours) hour ago" : "\(hours) hours ago"
    //
    //        let days = difference.day ?? 0
    //        let day_str = days == 1 ? "\(days) day ago" : "\(days) days ago"
    //
    //        let months = difference.month ?? 0
    //        let month_str = months == 1 ? "\(months) month ago" : "\(months) months ago"
    //
    //        let years = difference.year ?? 0
    //        let year_str = years == 1 ? "\(years) year ago" : "\(years) years ago"
    //
    //        if let year = difference.year, year       > 0 { return year_str }
    //        if let month = difference.month, month    > 0 { return month_str }
    //        if let day = difference.day, day          > 0 { return day_str }
    //        if let hour = difference.hour, hour       > 0 { return hour_str }
    //        if let minute = difference.minute, minute > 0 { return min_str }
    //        if let second = difference.second, second > 0 { return sec_str }
    //        return ""
    //    }
    
}
