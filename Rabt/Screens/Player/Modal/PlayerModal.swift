//
//  PlayerModal.swift
//  Rabt
//
//  Created by Asad Khan on 23/01/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class PlayerModal: NSObject {
   
    var audioTitle : String?
    var id, playlistId, artistID, favourites: Int?
    var name, arabicName, image, audio, duration, des, author: String?
    var artistPlaylist: [PlayerModal?]?
    var categoryType: String?
    private func emptyAll(){
        self.id = nil
        self.artistID = nil
        self.favourites = 0
        self.name = ""
        self.arabicName = ""
        self.image = ""
        self.audio = ""
        self.duration = ""
        self.des = ""
        self.author = ""
        self.audioTitle = ""
        self.artistPlaylist = []
        self.categoryType = ""
    }
    init(selectedAudioBook: AudioBookModal) {
        super.init()
        self.emptyAll()
        id = selectedAudioBook.id
        name = selectedAudioBook.name
        audioTitle = selectedAudioBook.name
        arabicName = selectedAudioBook.arabicName
        des = selectedAudioBook.des
        author = selectedAudioBook.author
        image = selectedAudioBook.image
        audio = selectedAudioBook.audio
        duration  = selectedAudioBook.duration
        favourites = selectedAudioBook.favourites
    }
    init(selectedNasheed: NasheedModal) {
        super.init()
        self.emptyAll()
        id = selectedNasheed.id
        artistID = selectedNasheed.artistID
        name = selectedNasheed.name
        audioTitle = selectedNasheed.name
        arabicName = selectedNasheed.arabicName
        image = selectedNasheed.image
        audio = selectedNasheed.audio
        duration  = selectedNasheed.duration
        favourites = Int(selectedNasheed.favourites ?? "0")
    }
   
    init(selectedArtist: ArtistDetails) {
        super.init()
        self.emptyAll()
        id = selectedArtist.id
        name = selectedArtist.name
        des = selectedArtist.des
        image = selectedArtist.image
        favourites = Int(selectedArtist.favourites ?? "0")
        for audio in selectedArtist.artistPlaylist!{
            
            artistPlaylist?.append(PlayerModal.init(selectedArtistAudio: audio! , selectedArtist: selectedArtist))
        }
    }
    init(selectedArtistAudio: ArtistPlaylist , selectedArtist: ArtistDetails) {
        super.init()
        self.emptyAll()
        id = selectedArtistAudio.id
        artistID = selectedArtistAudio.artistID
        arabicName = selectedArtistAudio.arabicName
        name = selectedArtist.name
        image = selectedArtist.image
        audioTitle = selectedArtistAudio.audioTitle
        audio = selectedArtistAudio.audio
        duration  = selectedArtistAudio.duration
        favourites = Int(selectedArtistAudio.favourites ?? "0")
    }
    
    init(selectedPlaylist: PlaylistDetailData, category: String = "") {
        super.init()
        self.emptyAll()
        id = selectedPlaylist.id
        name = selectedPlaylist.artist_name
        des = ""//selectedArtist.des
        image = selectedPlaylist.playlist_image
        favourites = Int(selectedPlaylist.favourites ?? "0")
        artistID = selectedPlaylist.id
        categoryType = category
        for audio in selectedPlaylist.audioData!{
            
            artistPlaylist?.append(PlayerModal.init(selectedPlaylistAudio: audio! , selectedPlaylist: selectedPlaylist))
        }
    }
    
    init(selectedPlaylistAudio: PlaylistAudioDetail , selectedPlaylist: PlaylistDetailData) {
        super.init()
        self.emptyAll()
        id = selectedPlaylistAudio.audio_id
        playlistId = selectedPlaylistAudio.playlist_id
        artistID = selectedPlaylist.id
        arabicName = selectedPlaylistAudio.arabic_name
        name = selectedPlaylist.artist_name
        image = selectedPlaylist.playlist_image
        audioTitle = selectedPlaylistAudio.audio_title
        audio = selectedPlaylistAudio.audio
        duration  = selectedPlaylistAudio.duration
        favourites = Int(selectedPlaylist.favourites ?? "0")
    }
    
    init(selectedAudio: PlaylistAudioDetail, playlistImage: String, artistName: String, artistId: Int, favourite: String?, category: String = "") {
        super.init()
        self.emptyAll()
        id = selectedAudio.audio_id
        playlistId = selectedAudio.playlist_id
        artistID = artistId
        name = artistName
        audioTitle = selectedAudio.audio_title
        arabicName = selectedAudio.arabic_name
        image = playlistImage
        audio = selectedAudio.audio
        duration  = selectedAudio.duration
        favourites = Int(favourite ?? "0")
        categoryType = category
    }
}
