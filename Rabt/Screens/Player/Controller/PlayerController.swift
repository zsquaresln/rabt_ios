//
//  PlayerController.swift
//  Rabt
//
//  Created by Asad Khan on 21/12/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import UIKit
import StickyTabBarViewController
import AVKit
import MarqueeLabel
import IBAnimatable
import GoogleMobileAds
//import MediaPlayer

 protocol audioPlayerDelegate {
    func audioPlayerClosed()
     func didAudioChanged(playingIndex: Int,isPlaying: Bool)
     func audioDidFinished()
     func audioFavouriteStatusChanged(isFavourite:Bool ,selectedType: FavouriteType , audioId: Int)
}

class PlayerController: UIViewController , Expandable {
    
    //IBOutles
    @IBOutlet weak var collapsedStateView: UIView!
    @IBOutlet weak var playOrPauseBtn: UIButton!
  //  @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet var view1: UIView!
    @IBOutlet var bgView1: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var favouriteBtn1: UIButton!
    @IBOutlet weak var seekBackBtn: UIButton!
    @IBOutlet weak var playOrPauseBtn1: UIButton!
    @IBOutlet weak var seekForwardBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var currentTimeLbl: UILabel!
    @IBOutlet weak var totalTimeLbl: UILabel!
    @IBOutlet weak var audioSlider: UISlider!
    @IBOutlet weak var titleLbl1: UILabel!
    @IBOutlet weak var subTitleLbl1: MarqueeLabel!
    @IBOutlet var view2: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet var bgView2: UIView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var titleLbl2: UILabel!
    @IBOutlet weak var subTitleLbl2: UILabel!
    

    @IBOutlet weak var collapsedImageView: AnimatableImageView!
    @IBOutlet weak var bannerView: GADBannerView!
    //Properties
    var playerViewModal : PlayerViewModal?
    var delegate: audioPlayerDelegate?
    var player: AVPlayer?
    var playerItem:AVPlayerItem?
    fileprivate let seekDuration: Float64 = 5
    var selectedType : FavouriteType = .Audio
    var selectedSegment : SegmentType = .All
   // var playOrPause = ""
    
    var isAudioPlaying : Bool = false
    var isPlayAsPlaylist : Bool = false
    var currentAudioIndex = 0
    var lastValue = 0.0
    var isSliderMoved = false
  //  var navController = UINavigationController()
    var selectedAudio : PlayerModal?
    var minimisedView: UIView {
        
      //  NotificationCenter.default.addObserver(self, selector: #selector(hideViews), name: Notification.Name(rawValue: "hideViews"), object: nil)
        
     //   NotificationCenter.default.addObserver(self, selector: #selector(changePlayingIcon(dict:)), name: Notification.Name(rawValue: "isPlaying"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(closeListen), name: Notification.Name(rawValue: "closeListen"), object: nil)
        if selectedType == .Artist{
            if playerViewModal?.selectedAudio?.artistPlaylist?.count ?? 0 > 0 {
                setNext()
            }
        }
        else{
            selectedAudio = playerViewModal?.selectedAudio
            setTextAndDesign()
        }

        loadBannerAd()
        return collapsedStateView
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if selectedType == .Artist{
            if playerViewModal?.selectedAudio?.artistPlaylist?.count ?? 0 > 0 {
                
                imgView.image = UIImage(named: "PlayList.png")
                collapsedImageView.image = UIImage(named: "PlayList.png")
            }
        }
//        if selectedType == .Nasheet{
//            favouriteBtn1.isHidden = true
//        }
//        else{
//            favouriteBtn1.isEnabled = false
//            favouriteBtn1.isHidden = false
//        }

        
    }
    override func viewWillAppear(_ animated: Bool) {
       
    }
    func loadBannerAd() {
        if splashSingelton.sharedInstance.splashDataModel?.audio_screen_banner == true{
        //Load Banner
        bannerView.adUnitID = AdManager().bannerId
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        }
    }
    func setNext() -> Void {
        
        if currentAudioIndex < playerViewModal?.selectedAudio?.artistPlaylist?.count ?? 0 {
            if currentAudioIndex >= playerViewModal?.selectedAudio?.artistPlaylist?.count ?? 0 {
                //if currentAudioIndex < (playerViewModal?.selectedAudio?.artistPlaylist?.count)! - 1{
                    currentAudioIndex = 1
             //   }
            }
            selectedAudio = playerViewModal?.selectedAudio?.artistPlaylist?[currentAudioIndex]
            setTextAndDesign()
          //  delegate?.didAudioChanged(playingIndex: currentAudioIndex, isPlaying: isAudioPlaying)
            
        }else {
            currentAudioIndex = 0
            playOrPauseBtn.setImage(UIImage(named: "playWhiteIcon"), for: UIControl.State.normal)
            playOrPauseBtn1.setImage(UIImage(named: "playerPlayIcon"), for: UIControl.State.normal)
              delegate?.audioDidFinished()
        }
    }
    
    func setTextAndDesign() -> Void {
        let image = UIImage(named: "close.png")?.withRenderingMode(.alwaysTemplate)
        closeBtn.setImage(image, for: .normal)
        closeBtn.tintColor = UIColor.white
        let url_str = selectedAudio?.audio ?? ""//selectedDict["audio"] as? String ?? ""
        self.initAudioPlayer(url_str: url_str)
        setFovuriteIcon()
      //  if selectedType == .Nasheet{
           // favouriteBtn1.isHidden = true
       // }
//        else{
//            favouriteBtn1.isHidden = false
//        }
        titleLbl.text = selectedAudio?.name//artistName
        titleLbl1.text = selectedAudio?.name//artistName
        titleLbl2.text = selectedAudio?.name//artistName
        subTitleLbl.text = selectedAudio?.audioTitle//selectedDict["audio_title"] as? String
        subTitleLbl1.text = selectedAudio?.audioTitle//selectedDict["audio_title"] as? String
        subTitleLbl2.text = selectedAudio?.audioTitle//selectedDict["audio_title"] as? String
        descriptionTextView.text = selectedAudio?.des
        descriptionTextView.isEditable = false
        descriptionTextView.isSelectable = false
        commentBtn.isHidden = true
 //       if selectedType == .Nasheet {
            let url_image = selectedAudio?.image ?? ""
            let url = URL(string: url_image)
            imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            imgView2.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            collapsedImageView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            //nameToDisplayOnLockscreenPlayer = selectedAudio?.audioTitle
  //      }
//        else if selectedType == .Book {
//            if selectedAudio?.des != "" {
//                commentBtn.isHidden = false
//            }
//            let url_str = selectedAudio?.image ?? ""
//            let url = URL(string: url_str)
//            imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
//            imgView2.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
//            subTitleLbl.text = selectedAudio?.name
//            subTitleLbl1.text = selectedAudio?.name
//            subTitleLbl2.text = selectedAudio?.name
//        }else if selectedType == .Nasheet {
//            let url_str = selectedAudio?.image ?? ""
//            let url = URL(string: url_str)
//            imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
//            imgView2.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
//            if (selectedAudio?.arabicName ?? "").count > 0{
//                let attributedHeading = NSMutableAttributedString.init(string: "")
//                attributedHeading.appendWith(text: selectedAudio?.arabicName ?? "", addNewLine: false,color: UIColor.white)
//                subTitleLbl.attributedText = attributedHeading
//                subTitleLbl1.attributedText = attributedHeading
//            }
//            else{
//                subTitleLbl.text = selectedAudio?.name ?? ""
//                subTitleLbl1.text = ""
//            }
//            subTitleLbl2.text = selectedAudio?.name ?? ""
//        }
        bgView1.layer.cornerRadius = 10
        bgView1.clipsToBounds = true
        bgView2.layer.cornerRadius = 6
        bgView2.clipsToBounds = true
        changeAudioStatus()
    }
    
    func changeAudioStatus() -> Void {
        
        if isAudioPlaying == false {
            isAudioPlaying = true
            player?.play()
            playOrPauseBtn.setImage(UIImage(named: "pauseWhiteIcon"), for: UIControl.State.normal)
            playOrPauseBtn1.setImage(UIImage(named: "playerPauseIcon"), for: UIControl.State.normal)
        }else {
            isAudioPlaying = false
            player?.pause()
            playOrPauseBtn.setImage(UIImage(named: "playWhiteIcon"), for: UIControl.State.normal)
            playOrPauseBtn1.setImage(UIImage(named: "playerPlayIcon"), for: UIControl.State.normal)
        }
        delegate?.didAudioChanged(playingIndex: currentAudioIndex, isPlaying: isAudioPlaying)
    }
    func setupAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: .default, options: .allowAirPlay)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print("Error setting the AVAudioSession:", error.localizedDescription)
        }
    }
   
//    @objc func hideViews() -> Void {
//
//    }
    
    @objc func closeListen() -> Void {
        self.tabBarController?.tabBar.barTintColor = UIColor.white
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.unselectedItemTintColor = GlobalVariables.DarkGrayColor
        self.tabBarController?.tabBar.tintColor = GlobalVariables.AppColor
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.closeAudio()
    }
    
    @objc func playNew() {
        if selectedType == .Artist{
            if playerViewModal?.selectedAudio?.artistPlaylist?.count ?? 0 > 0 {
                setNext()
            }
        }
        else{
            selectedAudio = playerViewModal?.selectedAudio
            setTextAndDesign()
        }
    }
//    @objc func changePlayingIcon(dict: Notification) -> Void {
//        let dict1 = dict.userInfo! as? [String:Any] ?? [:]
//        let status = dict1["isPlaying"] as? String
//        if status == "0"{
//            isAudioPlaying = false
//            playOrPauseBtn.setImage(UIImage(named: "PlayIcon"), for: UIControl.State.normal)
//            playOrPauseBtn1.setImage(UIImage(named: "PlayIcon"), for: UIControl.State.normal)
//        }else{
//            isAudioPlaying = true
//            playOrPauseBtn.setImage(UIImage(named: "PauseIcon"), for: UIControl.State.normal)
//            playOrPauseBtn1.setImage(UIImage(named: "PauseIcon"), for: UIControl.State.normal)
//        }
//    }
    
    @IBAction func downBtnClicked(_ sender: Any) {
        self.tabBarController?.tabBar.barTintColor = UIColor.white
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.unselectedItemTintColor = GlobalVariables.DarkGrayColor
        self.tabBarController?.tabBar.tintColor = GlobalVariables.AppColor
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        container?.collapseChild()
    }
    
    @IBAction func CommentBtnClicked(_ sender: Any) {
        view1.isHidden = true
        view2.isHidden = false
    }
    
    //MARK: CollapsedView Functionality
    @IBAction func seekBackClicked(_ sender: Any) {
        self.seekBackWards()
    }
    
    @IBAction func playOrPauseBtnClicked(_ sender: UIButton) {
        
        if player?.rate == 0 {
            isAudioPlaying = true
            player!.play()
            playOrPauseBtn.setImage(UIImage(named: "pauseWhiteIcon"), for: UIControl.State.normal)
            playOrPauseBtn1.setImage(UIImage(named: "playerPauseIcon"), for: UIControl.State.normal)
            let dict = ["isPlaying": "1"]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "playStatus"), object: nil, userInfo: dict)
        }else {
            
            isAudioPlaying = false
            player!.pause()
            playOrPauseBtn.setImage(UIImage(named: "playWhiteIcon"), for: UIControl.State.normal)
            playOrPauseBtn1.setImage(UIImage(named: "playerPlayIcon"), for: UIControl.State.normal)
            let dict = ["isPlaying": "0"]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "playStatus"), object: nil, userInfo: dict)
        }
        delegate?.didAudioChanged(playingIndex: currentAudioIndex, isPlaying: isAudioPlaying)
    }
    
    @IBAction func seekForwardClicked(_ sender: Any) {
        self.seekForward()
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        closeAudio()
    }
    
    func closeAudio() -> Void {
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "playNew"), object: nil)
      //  NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "hideViews"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "playOrPaused"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "isPlaying"), object: nil)
        isAudioPlaying = false
        audioSlider.value = 0
        player?.pause()
        container?.removeCollapsableChild(animated: true)
          delegate?.audioPlayerClosed()
    }
    
    @IBAction func favouriteBtnClicked(_ sender: Any) {
        addOrRemoveFromFavourite()
    }
}

extension PlayerController {
    
    func initAudioPlayer(url_str: String) {
        setupAudioSession()
        let url = URL(string: url_str)
        playerItem = AVPlayerItem(url: url!)
        player = AVPlayer(playerItem: playerItem)
        audioSlider.minimumValue = 0
        audioSlider.value = 0
        let seconds = Double(selectedAudio?.duration ?? "0") ?? 0
        totalTimeLbl.text = self.convertSecondsToHoursMinutesSeconds(seconds: seconds, format: ":")
        let currentDuration : CMTime = playerItem!.currentTime()
        let currentSeconds : Float64 = CMTimeGetSeconds(currentDuration)
        currentTimeLbl.text = self.convertSecondsToHoursMinutesSeconds(seconds: currentSeconds, format: ":")
        audioSlider.maximumValue = Float(seconds)
        audioSlider.isContinuous = true
        player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if self.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                self.lastValue = time
                self.audioSlider.value = Float (time);
                self.currentTimeLbl.text = self.convertSecondsToHoursMinutesSeconds(seconds: time, format: ":")
            }
            let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
            if playbackLikelyToKeepUp == false{
                
                print("IsBuffering")
                //self.ButtonPlay.isHidden = true
                //self.loadingView.isHidden = false
            } else {
                //stop the activity indicator
                print("Buffering completed")
                //self.ButtonPlay.isHidden = false
                //self.loadingView.isHidden = true
            }
        }
        //change the progress value
        audioSlider.addTarget(self, action: #selector(sliderDraggingStopped(_:)), for: .touchUpInside)
        audioSlider.addTarget(self, action: #selector(sliderDraggingBegin(_:)), for: .touchDown)
        
        audioSlider.isContinuous = false
        //check player has completed playing audio
        NotificationCenter.default.addObserver(self, selector: #selector(self.finishedPlaying(_:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
    }
    
    @objc func finishedPlaying( _ myNotification:Notification) {
        isAudioPlaying = false
        playOrPauseBtn.setImage(UIImage(named: "playWhiteIcon"), for: UIControl.State.normal)
        playOrPauseBtn1.setImage(UIImage(named: "playerPlayIcon"), for: UIControl.State.normal)
        //reset player when finish
        audioSlider.value = 0
        let targetTime:CMTime = CMTimeMake(value: 0, timescale: 1)
        player!.seek(to: targetTime)
        if selectedType == .Artist{
            // all playing than move to next
            if isPlayAsPlaylist == true{
                currentAudioIndex += 1
                setNext()
            }
            else{
                delegate?.audioDidFinished()
            }
        }
        else{
            delegate?.audioDidFinished()
        }
    }
    @objc func sliderDraggingBegin(_ playbackSlider:UISlider){
        if isAudioPlaying || player!.rate != 0{
            player?.pause()
        }
    }
    @objc func sliderDraggingStopped(_ playbackSlider:UISlider){
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        player!.seek(to: targetTime)
       // if player!.rate == 0 {
            if isAudioPlaying{
                  player?.play()
            }
       // }
    }
    @objc func playbackSliderValueChanged(_ playbackSlider:UISlider) {
        if isAudioPlaying || player!.rate != 0{
            playOrPauseBtnClicked(playOrPauseBtn)
        }
        let seconds : Int64 = Int64(playbackSlider.value)
        let sec = Float64 (seconds)
        let current = sec - lastValue
        isSliderMoved = false
        if current > 5 || current < -5  {
            
            isSliderMoved = true
            
        }
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        player!.seek(to: targetTime)
        
        if player!.rate == 0 {
            if isAudioPlaying{
                //  player?.play()
            }
        }
    }
    
    @objc func seekBackWards() {
        
        if player == nil { return }
        let playerCurrenTime = CMTimeGetSeconds(player!.currentTime())
        var newTime = playerCurrenTime - seekDuration
        if newTime < 0 { newTime = 0 }
        let selectedTime: CMTime = CMTimeMake(value: Int64(newTime * 1000 as Float64), timescale: 1000)
        player?.seek(to: selectedTime)
    }
    
    @objc func seekForward() {
        if player == nil { return }
        if let duration = player!.currentItem?.duration {
            let playerCurrentTime = CMTimeGetSeconds(player!.currentTime())
            let newTime = playerCurrentTime + seekDuration
            if newTime < CMTimeGetSeconds(duration){
                
                let selectedTime: CMTime = CMTimeMake(value: Int64(newTime * 1000 as
                                                                   Float64), timescale: 1000)
                player!.seek(to: selectedTime)
            }
        }
    }
}

extension PlayerController {
    
    func addToFav(idd: String, type: String) -> Void {
        favouriteBtn1.isEnabled = false
        var typeActualName = ""
        if type.lowercased().contains("book") {
            typeActualName = "BOOK"
        } else if type.lowercased().contains("artist") {
            typeActualName = "ARTIST"
        } else if type.lowercased().contains("nashee") {
            typeActualName = "NASHEET"
        } else if type.lowercased().contains("audio") {
            typeActualName = "AUDIO"
        } else {
            typeActualName = "CATEGORY"
        }
        let parameter = ["audio_id": idd,
                         "type": typeActualName
        ]
        let apiManager = ApiManager()
        let url = ApiUrls.addToFavouriteApi
        //self.showSpinner(onView: self.view)
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            self.favouriteBtn1.isEnabled = true
            //   self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                self.playerViewModal?.selectedAudio?.favourites = 1
                    self.selectedAudio?.favourites = 1
                self.setFovuriteIcon()
                self.delegate?.audioFavouriteStatusChanged(isFavourite: true, selectedType: self.selectedType, audioId: self.selectedAudio?.id ?? -1)
            }else{
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
            }
        }) { error in
            //   self.removeSpinner()
            self.favouriteBtn1.isEnabled = true
        }
    }
    
    func removeFromFav(idd: String, type: String) -> Void {
        favouriteBtn1.isEnabled = false
        var typeActualName = ""
        if type.lowercased().contains("book") {
            typeActualName = "BOOK"
        } else if type.lowercased().contains("artist") {
            typeActualName = "ARTIST"
        } else if type.lowercased().contains("nashee") {
            typeActualName = "NASHEET"
        } else if type.lowercased().contains("audio") {
            typeActualName = "AUDIO"
        } else {
            typeActualName = "PLAYLIST"
        }
        let parameter = ["audio_id": idd,
                         "type": typeActualName
        ]
        let apiManager = ApiManager()
        let url = ApiUrls.deleteToFavouriteApi
        // self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            self.favouriteBtn1.isEnabled = true
            //  self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                self.playerViewModal?.selectedAudio?.favourites = 0
                    self.selectedAudio?.favourites = 0
                self.setFovuriteIcon()
                self.delegate?.audioFavouriteStatusChanged(isFavourite: false, selectedType: self.selectedType, audioId: self.selectedAudio?.id ?? -1)
            }else{
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
            }
        }) { error in
            //  self.removeSpinner()
            self.favouriteBtn1.isEnabled = true
        }
    }
    
    @objc func addOrRemoveFromFavourite() -> Void {
        let idd = "\(selectedAudio?.artistID ?? -1)"
        if selectedAudio?.favourites == 1 {
            removeFromFav(idd: idd, type: selectedAudio?.categoryType ?? "")
        }else {
            addToFav(idd: idd, type: selectedAudio?.categoryType ?? "")
        }
    }
    func setFovuriteIcon() -> Void {
        favouriteBtn1.isEnabled = true
        let image = UIImage(named: "heartWhiteEmpty")
        favouriteBtn1.setImage(image, for: .normal)
        if selectedAudio?.favourites == 1 {
            favouriteBtn1.setImage(UIImage(named: "heartWhiteFilled"), for: .normal)
        }
    }
}
