//
//  PlaceSelectionController.swift
//  Rabt
//
//  Created by Asad Khan on 21/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
class PlaceSelectionController: MasterViewController {
    
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeSearchTextField: UITextField!
    @IBOutlet weak var placesTableView: UITableView!
    
    var placesManager : GooglePlacesManager = GooglePlacesManager()
    var lastSelectedPlace : Prediction?
    var locationHandler : LocationHandler = LocationHandler()
    var delegate: googlePlacesCustomDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        placesManager.getRecentSelectedPlaces()
        placesManager.delegate = self
        placesTableView.delegate = self
        placesTableView.dataSource = self
        placeSearchTextField.addTarget(self, action: #selector(searchTextFieldDidChangedText), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBActions
    @IBAction func locateMeBtnTapped(_ sender: Any) {
        locationHandler.delegate = self
        locationHandler.getCurrentLocation()
    }
    @IBAction func cancelBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - TableView methods
extension PlaceSelectionController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int{
        if placeSearchTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            if placesManager.recentlySelectedPlaces?.count ?? 0 > 0{
                return 1
            }
        }else{
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if placeSearchTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            return placesManager.recentlySelectedPlaces?.count ?? 0
            
        }else{
            return placesManager.placesData?.count ?? 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40)
        let headingLabel = UILabel()
        headingLabel.frame = CGRect(x: 15, y: 0, width: headerView.frame.size.width , height: headerView.frame.size.height)
        headingLabel.textAlignment = .left
        headingLabel.font = UIFont.boldSystemFont(ofSize: 14)
        headingLabel.textColor = UIColor.black
        if placeSearchTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            headingLabel.text = "Recent Locations"
        }
        else{
            headingLabel.text = "Select Location"
        }
        headerView.addSubview(headingLabel)
        return headerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil{
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        }
        if placeSearchTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            cell?.textLabel?.text = placesManager.recentlySelectedPlaces?[indexPath.row]?.predictionDescription
        }else{
            cell?.textLabel?.text = placesManager.placesData?[indexPath.row]?.predictionDescription
        }
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell?.textLabel?.textColor = UIColor.darkGray
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if placeSearchTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            lastSelectedPlace = placesManager.recentlySelectedPlaces?[indexPath.row]
            placesManager.getCoordinateFrom(placeId: placesManager.recentlySelectedPlaces?[indexPath.row]?.placeID ?? "")
        }else{
            lastSelectedPlace = placesManager.placesData?[indexPath.row]
            placesManager.getCoordinateFrom(placeId: placesManager.placesData?[indexPath.row]?.placeID ?? "")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       // if placeSearchTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            return 40
       // }
       // return 0
    }
}

//MARK: - textfield delegate methods
extension PlaceSelectionController : UITextFieldDelegate{
    @objc func searchTextFieldDidChangedText(){
        if placeSearchTextField.text?.trimmingCharacters(in: .whitespaces) == ""{
            tableViewTopConstraint.constant = 50
            placesManager.placesData?.removeAll()
            placesManager.getRecentSelectedPlaces()
            placesTableView.reloadData()
        }
        else{
            tableViewTopConstraint.constant = 10
            placesManager.getPlaces(searchString: placeSearchTextField.text ?? "")
        }
    }
}

//MARK: - Google places Manager delegates
extension PlaceSelectionController : googlePlacesCustomDelegate{
    func placeNameFetchingCompleted() {
        delegate?.placeNameFetchingCompleted()
    }
    
    func placesSearchCompleted() {
        placesTableView.reloadData()
    }
    func coordinateFetchingCompleted(){
        let selectedPrayerTimesCoordinate = ["latitude": "\(placesManager.selectedPlaceCoordinates?.lat ?? 0.0)", "longitude": "\(placesManager.selectedPlaceCoordinates?.lng ?? 0.0)"]
        UserDefaults.standard.set(selectedPrayerTimesCoordinate, forKey: "selectedPrayerTimesCoordinate")
        placesManager.cacheSelectedPlace(location: lastSelectedPlace!)
        PrayerTimeManger.shared.prayerTimeCoordinates = selectedPrayerTimesCoordinate
        PrayerTimeManger.shared.shouldRefreshPrayerTimings = true
        PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
        PrayerTimeManger.shared.selectedLocationName = lastSelectedPlace?.predictionDescription
        // update prayer times in app here
        delegate?.coordinateFetchingCompleted()
        delegate?.placeNameFetchingCompleted()
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - location hanler delegates
/// for currect location only
extension PlaceSelectionController : locationHandlerDelegate{
    func locationUpdatedWith(coordinates: CLLocation) {
        let selectedPrayerTimesCoordinate = ["latitude": "\(coordinates.coordinate.latitude)", "longitude": "\(coordinates.coordinate.longitude)"]
        UserDefaults.standard.removeObject(forKey: "selectedPrayerTimesCoordinate")
        PrayerTimeManger.shared.prayerTimeCoordinates = selectedPrayerTimesCoordinate
        PrayerTimeManger.shared.shouldRefreshPrayerTimings = true
        PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
        PrayerTimeManger.shared.selectedLocationName = nil
        // update prayer times in app here
        delegate?.coordinateFetchingCompleted()
       // delegate?.placeNameFetchingCompleted()
        self.dismiss(animated: true, completion: nil)
    }
    
    func locationServicesDisabledError() {
        
    }
    
    func locationUpdatedWith(address: String, city: String) {
        
    }
    
    
}
