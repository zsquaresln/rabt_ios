//
//  ListenFavouritesController.swift
//  Rabt
//
//  Created by Admin on 12/7/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ListenFavouritesController: MasterViewController {
    
    //IBOutlets
    @IBOutlet weak var listenFavouriteTableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    //Properties
    var listenFavouriteArtistCollectionView : UICollectionView?
    var listenFavouriteBooksCollectionView : UICollectionView?
    var adLoader: GADAdLoader = GADAdLoader()
    var nativeAds = [GADNativeAd]()
    var listenViewModal : ListenViewModal?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    //MARK: - Helper Functions
    func reloadSegmentData(){
        if listenFavouriteArtistCollectionView != nil || listenFavouriteBooksCollectionView != nil{
            listenFavouriteArtistCollectionView?.dataSource = nil
            listenFavouriteArtistCollectionView?.delegate  = nil
            listenFavouriteArtistCollectionView = nil
            listenFavouriteBooksCollectionView?.dataSource = nil
            listenFavouriteBooksCollectionView?.delegate  = nil
            listenFavouriteBooksCollectionView = nil
        }
        registerTableViewCells()
        listenFavouriteTableView.delegate = self
        listenFavouriteTableView.dataSource = self
        listenFavouriteTableView.reloadData()
        if listenViewModal?.allFavouriteModal?.artists?.count ?? 0 < 1 && listenViewModal?.allFavouriteModal?.latestAudioBooks?.count ?? 0 < 1{
            bannerView.isHidden = true
        }else{
            bannerView.isHidden = false
        }
    }
    func registerTableViewCells(){
        listenFavouriteTableView.register(UINib(nibName: "recomendedArtistsTableViewCell", bundle: nil), forCellReuseIdentifier: "recomendedArtistsTableViewCell")
        
    }
    func registerCollectionViewCells(collectionView: UICollectionView){
        collectionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        collectionView.register(UINib(nibName: "AudioBookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "audioBookCell")
        collectionView.register(UINib(nibName: "NativeAdsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NativeAdsCollectionCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        if collectionView.tag == 0{
            listenFavouriteArtistCollectionView = collectionView
        }else{
            listenFavouriteBooksCollectionView = collectionView
        }
        collectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.reloadCollectionViewsWithDelay()
        }
    }
    
    func reloadCollectionViewsWithDelay(){
        listenFavouriteArtistCollectionView?.reloadData()
        listenFavouriteBooksCollectionView?.reloadData()
    }
    
    func loadAds() {
      
        if nativeAds.count > 0{
            return
        }
       
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_favorite_banner == true{
            //Load Banner
            bannerView.adUnitID = AdManager().bannerId
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
        }
       
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_favorite_native == true{
            //Load Native
            let options = GADMultipleAdsAdLoaderOptions()
            options.numberOfAds = 1
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ GADSimulatorID ]
            adLoader = GADAdLoader(
                adUnitID: AdManager().nativeadUnitID, rootViewController: self,
                adTypes: [.native], options: [options])
            adLoader.delegate = self
            adLoader.load(GADRequest())
        }
       
    }
    
    //MARK: - IBActions
    @objc func seeAllClicked(_ sender: UIButton){
        if sender.tag == 2{
            // show audio books
            self.show(storyBoard: .ListenNew, controllerIdentifier: "AllBooksNewViewController", navigation: singeltonClass.shared.naviagtionController){
                controller in
                (controller as! AllBooksNewViewController).booksViewModal.latestAudioBooks = listenViewModal?.allListenModal?.listenAll?.latestAudioBooks
                (controller as! AllBooksNewViewController).selectedSegment = .MyFavourite
            }
        } else if sender.tag == 0{
            // show artists
            self.show(storyBoard: .ListenNew, controllerIdentifier: "AllArtistViewController", navigation: singeltonClass.shared.naviagtionController){
                controller in
                (controller as! AllArtistViewController).selectedSegment = .MyFavourite
            }
        }
    }
}
//MARK: - TableViw handling
extension ListenFavouritesController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1{
            if nativeAds.count < 1{
                return 0
            }
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recomendedArtistsTableViewCell", for: indexPath) as! recomendedArtistsTableViewCell
        
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        if indexPath.section == 0 || indexPath.section == 2{
            cell.collectionTopConstraintToContainer.priority = UILayoutPriority(700)
            cell.collectionTopConstraintToSeeView.priority = UILayoutPriority(1000)
            cell.seeAllView.isHidden = false
            cell.seeAllBtn.tag = indexPath.section
            cell.seeAllBtn.addTarget(self, action: #selector(seeAllClicked(_:)), for: .touchUpInside)
        }else{
            cell.collectionTopConstraintToContainer.priority = UILayoutPriority(1000)
            cell.collectionTopConstraintToSeeView.priority = UILayoutPriority(700)
            cell.seeAllView.isHidden = true
        }
        cell.dataCollectionView.tag = indexPath.section
        cell.dataCollectionView.backgroundColor = .clear
        self.registerCollectionViewCells(collectionView: cell.dataCollectionView)
        if indexPath.section == 0{
            cell.titleTL.text = "Content Creators".localized()
        }else if indexPath.section == 2{
            cell.titleTL.text = "Latest Audio Books".localized()
        }else{
            cell.titleTL.text = ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if listenViewModal?.allFavouriteModal?.artists?.count ?? 0 > 0{
                return 250
            }
            else
            {
                return 0
            }
        }else if indexPath.section == 2{
            if listenViewModal?.allFavouriteModal?.latestAudioBooks?.count ?? 0 > 0{
                return 250
            }
            else
            {
                return 0
            }
        }else{
            return 313
        }
    }
}
//MARK: -collection View handling
extension ListenFavouritesController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0{
            return listenViewModal?.allFavouriteModal?.artists?.count ?? 0
        }
        else if collectionView.tag == 2{
            return listenViewModal?.allFavouriteModal?.latestAudioBooks?.count ?? 0
        }else{
            return 1
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 || collectionView.tag == 2{
            return CGSize(width: 160, height: 190)
        }else{
            return CGSize(width: collectionView.frame.size.width, height: 313)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            let currentArtist = listenViewModal?.allFavouriteModal?.artists?[indexPath.row]
            cell.nameLbl.text = currentArtist?.name
            let url_str = currentArtist?.image
            let url = URL(string: url_str ?? "")
            cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            cell.contantView.clipsToBounds = true
            if currentArtist?.disable == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
                cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                // cell.lockImageView.isHidden = true
            } else {
               // cell.lockImageView.isHidden = true
                cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                
            }
            return cell
        }else if collectionView.tag == 2 {
            let cell : AudioBookCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "audioBookCell", for: indexPath) as! AudioBookCollectionViewCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            let currentAudioBook = listenViewModal?.allFavouriteModal?.latestAudioBooks?[indexPath.row]
            cell.nameLbl.text = currentAudioBook?.name
            let url_str = currentAudioBook?.image ?? ""
            let url = URL(string: url_str)
            cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            cell.durationLbl.isHidden = true
            if currentAudioBook?.duration != nil {
                cell.durationLbl.isHidden = false
            }
            let duration = Double(currentAudioBook?.duration ?? "0") ?? 0.0
            cell.durationLbl.text = duration.asString(style: .positional)
            return cell
        }else{
            let cell: NativeAdsCollectionCell! = collectionView.dequeueReusableCell(withReuseIdentifier: "NativeAdsCollectionCell", for: indexPath as IndexPath) as? NativeAdsCollectionCell
            if cell.addView?.subviews.count == 0 && nativeAds.count > 0{
                let adView = Bundle.main.loadNibNamed("NativeAdView", owner: nil, options: nil)
                if let nativeAdView = adView?.first as? GADNativeAdView{
                    
                    let nativeAd = nativeAds[0]
                    /// Set the native ad's rootViewController to the current view controller.
                    nativeAd.rootViewController = self
                    nativeAdView.translatesAutoresizingMaskIntoConstraints = false
                    nativeAdView.tag = 50
                    cell.addView?.addSubview(nativeAdView)
                    let viewDictionary = ["_nativeAdView": nativeAdView]
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "H:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "V:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    setupNativeAdView(nativeAdView: nativeAdView,nativeAd:nativeAd)
                }
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //artist is selected
        if collectionView.tag == 0{
            self.show(storyBoard: .ListenNew, controllerIdentifier: "ArtistDetailController", navigation: singeltonClass.shared.naviagtionController){
                controller in
                //(controller as! ArtistDetailController).artistDetailViewModal.selectedArtist = listenViewModal?.allFavouriteModal?.artists?[indexPath.row]
                //(controller as! ArtistDetailController).selectedSegment = .MyFavourite
            }
        }else if collectionView.tag == 2{
            listenViewModal?.setUpPlayerFor(selectedAudioType: .Book, selectedIndex: indexPath.row)
        }
    }
}
extension ListenFavouritesController: GADNativeAdLoaderDelegate,GADNativeAdDelegate,GADVideoControllerDelegate{
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        nativeAds.append(nativeAd)
        nativeAd.delegate = self
        if listenViewModal?.allFavouriteModal?.artists?.count ?? 0 > 0 || listenViewModal?.allFavouriteModal?.latestAudioBooks?.count ?? 0 > 0{
            reloadSegmentData()
        }
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        
    }
    func setupNativeAdView(nativeAdView : GADNativeAdView,nativeAd:GADNativeAd){
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        
        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
            // By acting as the delegate to the GADVideoController, this ViewController receives messages
            // about events in the video lifecycle.
            mediaContent.videoController.delegate = self
            //  videoStatusLabel.text = "Ad contains a video asset."
        } else {
            //videoStatusLabel.text = "Ad does not contain a video."
        }
        
        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
            let heightConstraint = NSLayoutConstraint(
                item: mediaView,
                attribute: .height,
                relatedBy: .equal,
                toItem: mediaView,
                attribute: .width,
                multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
                constant: 0)
            heightConstraint.isActive = true
        }
        
        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        
        // (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from: nativeAd.starRating)
        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
        
        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
        nativeAdView.storeView?.isHidden = nativeAd.store == nil
        
        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
        nativeAdView.priceView?.isHidden = nativeAd.price == nil
        
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
        
        // In order for the SDK to process touch events properly, user interaction should be disabled.
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
        
        // Associate the native ad view with the native ad object. This is
        // required to make the ad clickable.
        // Note: this should always be done after populating the ad views.
        nativeAdView.nativeAd = nativeAd
    }
    
}

extension Double {
  func asString(style: DateComponentsFormatter.UnitsStyle) -> String {
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.hour, .minute, .second, .nanosecond]
    formatter.unitsStyle = style
    return formatter.string(from: self) ?? ""
  }
}
