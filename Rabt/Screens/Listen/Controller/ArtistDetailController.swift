//
//  ListenArtistController.swift
//  Rabt
//
//  Created by Asad Khan on 23/01/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import StickyTabBarViewController
import FirebaseAnalytics
class ArtistDetailController: MasterViewController {
    //IBOutlets
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var artistOrPlaylistImageView: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var noOfPlaysLabel: UILabel!
    @IBOutlet weak var noOfLikesLabel: UILabel!
    
    @IBOutlet weak var artistPlaylistNameLabel: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet var playPauseLabel: UILabel!
    @IBOutlet weak var playPauseImageView: UIImageView!
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var playAllBtn: UIButton!
    @IBOutlet weak var donateNowView: UIView!
    @IBOutlet weak var donateNowHeightConstraint: NSLayoutConstraint!
    //Properties
 //   var artistDetailViewModal : ArtistDetailViewModal = ArtistDetailViewModal()
    var audioDetailViewModel : ListenViewModal? = ListenViewModal()
  //  var playlistModel : ListeRevampedData?
    var category_type = ""
    var isPlaying = false
    var selectedIndex = -1
   // var selectedSegment : SegmentType!
    var notificationPlayableIndex = -1
    var adManager : AdManager?
    override func viewDidLoad() {
        super.viewDidLoad()
        audioDetailViewModel?.parentController = self
        pauseAudioPlayerIfPlaying()
        showPlaylistData()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(registeredUserSubscribedViaAdListener), name: Notification.Name(rawValue: "rewardAdWatched"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("subscriptionCheck"), object: nil)
        showPlaylistData()
        setPremiumViews()
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "rewardAdWatched"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "subscriptionCheck"), object: nil)
    }
    
    //MARK: - Helper Functions
    func setPremiumViews(){
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            // do nothing
            DispatchQueue.main.async{
                self.donateNowView.isHidden = false
                    self.donateNowHeightConstraint.constant = 50
            }
        }else{
           
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip && isAutoRenewable{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                }
            }else if isVip{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                }
            }
            else{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = false
                        self.donateNowHeightConstraint.constant = 50
                }
            }
        }
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        dataTableView.reloadData()
    }
    
    func pauseAudioPlayerIfPlaying(){
        if isCollapsedShown{
            if let audioPlayerController = tabController?.childViewController as? PlayerController{
                if audioPlayerController.isAudioPlaying{
                    audioPlayerController.changeAudioStatus()
                }
            }
        }
    }
    func registerTableViewCells(){
        dataTableView.register(UINib.init(nibName: "ArtistPlayListTableViewCell", bundle: nil), forCellReuseIdentifier: "ArtistPlayListTableViewCell")
    }
    func showPlaylistData(){
        let url_str = audioDetailViewModel?.playlistDetail?.playlist_image ?? ""
        let url = URL(string: url_str)
        artistOrPlaylistImageView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
        if category_type.lowercased() == "artist" {
            categoryNameLabel.text = audioDetailViewModel?.playlistDetail?.artist_name
            if audioDetailViewModel?.playlistDetail?.artist_name != audioDetailViewModel?.playlistDetail?.playlist_name {
                artistPlaylistNameLabel.text = audioDetailViewModel?.playlistDetail?.playlist_name
            } else {
                artistPlaylistNameLabel.text = ""
            }
            
        } else {
            categoryNameLabel.text = audioDetailViewModel?.playlistDetail?.playlist_name
            artistPlaylistNameLabel.text = ""
        }
        
        noOfPlaysLabel.text = audioDetailViewModel?.playlistDetail?.no_of_plays
        noOfLikesLabel.text = audioDetailViewModel?.playlistDetail?.no_of_likes
        let isFavourite = Int(audioDetailViewModel?.playlistDetail?.favourites ?? "0") ?? 0
        let image = UIImage(named: "heartWhiteEmpty")
        favouriteBtn.setImage(image, for: .normal)
        if isFavourite == 1 {
            favouriteBtn.setImage(UIImage(named: "heartWhiteFilled"), for: .normal)
        }
        registerTableViewCells()
        dataTableView.reloadData()
        let (isEnabled , isSubscriber) = isContentUnlocked()
        if isEnabled {
            if self.isPlaying{
                playPauseLabel.text = "Pause"
                playPauseImageView.image = UIImage(named: "PauseIcon")
            }else{
                playPauseLabel.text = "Play"
                playPauseImageView.image = UIImage(named: "PlayIcon")
            }
        } else {
            if isSubscriber {
                playPauseLabel.text = "Donate Now"
                let image = UIImage(named: "HeartFavourite")
                playPauseImageView.image = image
                //playPauseImageView.tintColor = UIColor.red
            } else {
                playPauseLabel.text = "Play"
                playPauseImageView.image = UIImage(named: "PlayIcon")
            }
            
        }
        
        if notificationPlayableIndex != -1{
            if let trackIndex = audioDetailViewModel?.playlistDetail?.audioData?.firstIndex(where: {$0?.audio_id == notificationPlayableIndex}){
                selectedIndex = trackIndex
                // play notification track
                audioDetailViewModel?.setUpPlayerFor(selectedSegment: .All, selectedIndex: selectedIndex,isPlayAsPlaylist:false, categoryType: category_type)
                notificationPlayableIndex = -1
            }
        }
        
        if isSubscriber == false{
            // load ads in advance
            adManager = AdManager()
            adManager?.loadRewardedAd()
        }
    }
    func isContentUnlocked() -> (Bool, Bool){
//        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
//            removeGuestExpiredUnlockedArtists()
//            if let unlockedArtists = UserDefaults.standard.object(forKey: "unlockedArtists") as? [[String:String]]{
//                // if unlockedArtists.contains(String(artistDetailViewModal.artistDetails?.id ?? -1)){
//                let findArtist = unlockedArtists.filter{$0["id"] == String(audioDetailViewModel?.playlistDetail?.id ?? -1)}
//                if findArtist.count > 0{
//                    return true
//                }else{
//                    return false
//                }
//            }else{
//                return false
//            }
//        }else{
//            // handle registered users
//            if audioDetailViewModel?.playlistDetail?.disable == 1{
//                // content is disabled so check for subsctiption
//                let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
//                if isVip || isAutoRenewable{
//                    return true
//                }
//                return false
//            }
//            return true
//        }
        if audioDetailViewModel?.playlistDetail?.disable == 1{
            // content is disabled so check for subsctiption
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip || isAutoRenewable{
                return (true, true)
            }
            return (false, true)
        } else {
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip || isAutoRenewable{
                return (true, true)
            }
            removeGuestExpiredUnlockedArtists()
            if let unlockedArtists = UserDefaults.standard.object(forKey: "unlockedArtists") as? [[String:String]]{
                // if unlockedArtists.contains(String(artistDetailViewModal.artistDetails?.id ?? -1)){
                let findArtist = unlockedArtists.filter{$0["id"] == String(audioDetailViewModel?.playlistDetail?.id ?? -1)}
                if findArtist.count > 0{
                    return (true, false)
                }else{
                    return (false, false)
                }
            }else{
                return (false, false)
            }
        }
    }
    func removeGuestExpiredUnlockedArtists(){
        if let unlockedArtists = UserDefaults.standard.object(forKey: "unlockedArtists") as? [[String:String]]{
            var tempUnlockedArtists : [[String:String]] = []
            for item in unlockedArtists{

                if PrayerTimeManger().getDatesDifference(firstDate: PrayerTimeManger().getDateWith(format: "dd-MM-yyyy", stringDate: item["date"] ?? ""), secondDate: Date()) < splashSingelton.sharedInstance.adRewardedDays {
                    tempUnlockedArtists.append(item)
                }
            }
            UserDefaults.standard.set(tempUnlockedArtists, forKey: "unlockedArtists")
            UserDefaults.standard.synchronize()
        }
    }
  
    func needSubscriptionToPlayFor(index:Int){
        pauseAudioPlayerIfPlaying()
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.artistId = String(audioDetailViewModel?.playlistDetail?.id ?? -1)
            subsriptionController.selectedAudioIndex = index
            subsriptionController.delegate = self
          //  subsriptionController.adManager = adManager
            subsriptionController.isFromArtistDetail = true
            tabCon.add(subsriptionController)
        }
    }
    
    func needAdWatchToPlayFor(index:Int){
        pauseAudioPlayerIfPlaying()
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let adWatchController = storyBoard.instantiateViewController(withIdentifier: "WatchAdController") as! WatchAdController
            adWatchController.artistId = String(audioDetailViewModel?.playlistDetail?.id ?? -1)
            adWatchController.selectedAudioIndex = index
            adWatchController.delegate = self
            adWatchController.adManager = adManager
            adWatchController.isFromArtistDetail = true
            tabCon.add(adWatchController)
        }
    }
    @objc func registeredUserSubscribedViaAdListener(){
        audioDetailViewModel?.getPlaylistDetailData(playlist_id: audioDetailViewModel?.playlistDetail?.id ?? 0, category_type: category_type, category_id: nil)
    }
    //MARK: - IBActions
    @IBAction func unlockPremiumBtnTapped(_ sender: Any) {
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.delegate = self
            tabCon.add(subsriptionController)
        }
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func favouriteBtnClicked(_ sender: Any) {
        isFavouriteNeedsRefresh = true
        let favourite = audioDetailViewModel?.playlistDetail?.favourites
        if favourite == "1" {
            removeFromFav(idd: "\(audioDetailViewModel?.playlistDetail?.id ?? -1)", type: category_type)
        }else {
            addToFav(idd: "\(audioDetailViewModel?.playlistDetail?.id ?? -1)", type: category_type)
        }
    }
    
    @IBAction func playAllBtnClicked(_ sender: UIButton) {
        if selectedIndex == -1{
            selectedIndex = 0
        }
        let (isEnabled , isSubscriber) = isContentUnlocked()
        if isEnabled{
            // play selected
            audioDetailViewModel?.setUpPlayerFor(selectedSegment: .All, selectedIndex: selectedIndex,isPlayAsPlaylist:true, categoryType: category_type)
        }else{
            // show subscription page
//            if selectedIndex == 0{
//                audioDetailViewModel?.setUpPlayerFor(selectedSegment: .All, selectedIndex: selectedIndex,isPlayAsPlaylist:false)
//            }else{
            if isSubscriber == true {
                needSubscriptionToPlayFor(index:selectedIndex)
            } else {
                needAdWatchToPlayFor(index: selectedIndex)
            }
               
         //   }
        }
    }
//    @objc func addOrRemoveFromFavourite(sender: UIButton) -> Void {
//
//        let favourite = audioDetailViewModel?.playlistDetail?.favourites
//        if favourite == "1" {
//            removeFromFav(idd: "\(audioDetailViewModel?.playlistDetail?.id ?? -1)", type: GlobalMethods.getFavouriteType(app: FavouriteType.Audio))
//        }else {
//            addToFav(idd: "\(audioDetailViewModel?.playlistDetail?.id ?? -1)", type: GlobalMethods.getFavouriteType(app: FavouriteType.Audio))
//        }
//    }
    
    @objc func shareBtnTapped(_ sender: UIButton) {
        if let urlStr = NSURL(string:audioDetailViewModel?.playlistDetail?.audioData?[sender.tag]?.share_link ?? "") {
            let title = audioDetailViewModel?.playlistDetail?.audioData?[sender.tag]?.audio_title ?? ""
            let img  = self.artistOrPlaylistImageView.image ?? UIImage()
            let objectsToShare = [img,title, urlStr] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.setValue("Rabt: Connecting with Allah", forKey: "subject")
            if UIDevice.current.userInterfaceIdiom == .pad {
                if let popup = activityVC.popoverPresentationController {
                    popup.sourceView = self.view
                    popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                }
            }

            self.present(activityVC, animated: true, completion: nil)
        }
    }
    func addToFav(idd: String, type: String) -> Void {
        
        favouriteBtn.isUserInteractionEnabled = false
        var typeActualName = ""
        if type.lowercased().contains("book") {
            typeActualName = "BOOK"
        } else if type.lowercased().contains("artist") {
            typeActualName = "ARTIST"
        } else if type.lowercased().contains("nashee") {
            typeActualName = "NASHEET"
        } else if type.lowercased().contains("audio") {
            typeActualName = "AUDIO"
        } else {
            typeActualName = "CATEGORY"
        }
        let parameter = ["audio_id": idd,
                         "type": typeActualName
        ]
        let apiManager = ApiManager()
        let url = ApiUrls.addToFavouriteApi
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            self.favouriteBtn.isUserInteractionEnabled = true
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                self.audioDetailViewModel?.getPlaylistDetailData(playlist_id: self.audioDetailViewModel?.playlistDetail?.id ?? 0, category_type: self.category_type, category_id: nil)
            }else{
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
            }
        }) { error in
            self.favouriteBtn.isUserInteractionEnabled = true
        }
    }
    
    func removeFromFav(idd: String, type: String) -> Void {
        favouriteBtn.isUserInteractionEnabled = false
        var typeActualName = ""
        if type.lowercased().contains("book") {
            typeActualName = "BOOK"
        } else if type.lowercased().contains("artist") {
            typeActualName = "ARTIST"
        } else if type.lowercased().contains("nashee") {
            typeActualName = "NASHEET"
        } else if type.lowercased().contains("audio") {
            typeActualName = "AUDIO"
        } else {
            typeActualName = "PLAYLIST"
        }
        let parameter = ["audio_id": idd,
                         "type": typeActualName
        ]
        let apiManager = ApiManager()
        let url = ApiUrls.deleteToFavouriteApi
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            self.favouriteBtn.isUserInteractionEnabled = true
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                self.audioDetailViewModel?.getPlaylistDetailData(playlist_id: self.audioDetailViewModel?.playlistDetail?.id ?? 0, category_type: self.category_type, category_id: nil)
            }else{
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
            }
        }) { error in
            self.favouriteBtn.isUserInteractionEnabled = true
        }
    }
    
}
//MARK: - tableview Methods
extension ArtistDetailController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return audioDetailViewModel?.playlistDetail?.audioData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArtistPlayListTableViewCell", for: indexPath) as! ArtistPlayListTableViewCell
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        cell.contantView.backgroundColor = .clear
        cell.counterLabel.text = "\(indexPath.row + 1)."
        let audioTrack = audioDetailViewModel?.playlistDetail?.audioData?[indexPath.row]
        if audioTrack?.arabic_name?.count ?? 0 > 0{
            cell.titleLbl.text = "\(audioTrack?.audio_title ?? "")\n\(audioTrack?.arabic_name ?? "")"//"test\ntest"
        }
        else{
            cell.titleLbl.text = audioTrack?.audio_title//"test\ntest"
        }
        
        let duration = Double(audioTrack?.duration ?? "0") ?? 0.0
        cell.durationLbl.text = self.convertSecondsToHoursMinutesSeconds(seconds: duration, format: ":")
//        let isTrackFavourite =  Int(audioTrack?.favourites ?? "0") ?? 0
//        let image = UIImage(named: "HeartFavourite")?.withRenderingMode(.alwaysTemplate)
//        cell.favouriteBtn.setImage(image, for: .normal)
//        cell.favouriteBtn.tintColor = UIColor.white
//        if isTrackFavourite == 1 {
//            cell.favouriteBtn.setImage(UIImage(named: "HeartFavourite"), for: .normal)
//        }
//        cell.favouriteBtn.tag = indexPath.row
//        cell.favouriteBtn.addTarget(self, action: #selector(addOrRemoveFromFavourite(sender:)), for: .touchUpInside)
        cell.pauseBtn.isEnabled = false
        cell.pauseBtn.isUserInteractionEnabled = false
        if self.isPlaying && selectedIndex == indexPath.row{
            cell.playOrPauseImgView.image = UIImage(named: "PauseRound")
        }else{
            cell.playOrPauseImgView.image = UIImage(named: "PlayRound")
        }
        if indexPath.row == 0 {
            cell.favouriteBtn.isUserInteractionEnabled = true
        } else {
            cell.favouriteBtn.isUserInteractionEnabled = false
        }
        
        cell.favouriteBtn.tag = indexPath.row
        cell.favouriteBtn.addTarget(self, action: #selector(shareBtnTapped(_:)), for: .touchUpInside)
        let (isEnabled , isSubscriber) = isContentUnlocked()
        if isEnabled {
            // play selected
            cell.favouriteBtn.isUserInteractionEnabled = true
            if selectedIndex == indexPath.row && isPlaying{
                cell.titleLbl.textColor = UIColor.white
                cell.durationLbl.textColor = UIColor.white
                let image = UIImage(named: "PauseRound")?.withRenderingMode(.alwaysTemplate)
                cell.playOrPauseImgView.image = image
                cell.playOrPauseImgView.tintColor = UIColor.white
                let shareImage = UIImage(named: "shareWhiteIcon")?.withRenderingMode(.alwaysTemplate)
                cell.favouriteBtn.setImage(shareImage, for: .normal)
                cell.favouriteBtn.tintColor = UIColor.white
               // cell.contantView.backgroundColor = .white
            }else{
                cell.titleLbl.textColor = UIColor.white
                cell.durationLbl.textColor = UIColor.white
                let image = UIImage(named: "PlayRound")?.withRenderingMode(.alwaysTemplate)
                cell.playOrPauseImgView.image = image
                cell.playOrPauseImgView.tintColor = UIColor.white
                let shareImage = UIImage(named: "shareWhiteIcon")?.withRenderingMode(.alwaysTemplate)
                cell.favouriteBtn.setImage(shareImage, for: .normal)
                cell.favouriteBtn.tintColor = UIColor.white
                cell.contantView.backgroundColor = .clear
            }
        //    cell.favouriteBtn.setImage(UIImage(named: "shareWhiteIcon"), for: .normal)
        }else{
            if isSubscriber {
                if indexPath.row == 0{
                    if selectedIndex == indexPath.row && isPlaying{
                        cell.titleLbl.textColor = UIColor.white
                        cell.durationLbl.textColor = UIColor.white
                        let image = UIImage(named: "PauseRound")?.withRenderingMode(.alwaysTemplate)
                        cell.playOrPauseImgView.image = image
                        cell.playOrPauseImgView.tintColor = UIColor.white
                        let shareImage = UIImage(named: "shareWhiteIcon")?.withRenderingMode(.alwaysTemplate)
                        cell.favouriteBtn.setImage(shareImage, for: .normal)
                        cell.favouriteBtn.tintColor = UIColor.white
                       //cell.contantView.backgroundColor = .white
                    }else{
                        cell.titleLbl.textColor = UIColor.white
                        cell.durationLbl.textColor = UIColor.white
                        let image = UIImage(named: "PlayRound")?.withRenderingMode(.alwaysTemplate)
                        cell.playOrPauseImgView.image = image
                        cell.playOrPauseImgView.tintColor = UIColor.white
                        let shareImage = UIImage(named: "shareWhiteIcon")?.withRenderingMode(.alwaysTemplate)
                        cell.favouriteBtn.setImage(shareImage, for: .normal)
                        cell.favouriteBtn.tintColor = UIColor.white
                        cell.contantView.backgroundColor = .clear
                    }
                }else{
                    cell.titleLbl.textColor = UIColor.white
                    cell.durationLbl.textColor = UIColor.white
                    let image = UIImage(named: "PlayRound")?.withRenderingMode(.alwaysTemplate)
                    cell.playOrPauseImgView.image = image
                    cell.playOrPauseImgView.tintColor = UIColor.white
                    cell.contantView.backgroundColor = .clear
                    cell.favouriteBtn.setImage(UIImage(named: "whiteLockIcon"), for: .normal)
                    cell.favouriteBtn.isUserInteractionEnabled = false
                }
            } else {
                cell.favouriteBtn.isUserInteractionEnabled = true
                if selectedIndex == indexPath.row && isPlaying{
                    cell.titleLbl.textColor = UIColor.white
                    cell.durationLbl.textColor = UIColor.white
                    let image = UIImage(named: "PauseRound")?.withRenderingMode(.alwaysTemplate)
                    cell.playOrPauseImgView.image = image
                    cell.playOrPauseImgView.tintColor = UIColor.white
                    let shareImage = UIImage(named: "shareWhiteIcon")?.withRenderingMode(.alwaysTemplate)
                    cell.favouriteBtn.setImage(shareImage, for: .normal)
                    cell.favouriteBtn.tintColor = UIColor.white
                   // cell.contantView.backgroundColor = .white
                }else{
                    cell.titleLbl.textColor = UIColor.white
                    cell.durationLbl.textColor = UIColor.white
                    let image = UIImage(named: "PlayRound")?.withRenderingMode(.alwaysTemplate)
                    cell.playOrPauseImgView.image = image
                    cell.playOrPauseImgView.tintColor = UIColor.white
                    let shareImage = UIImage(named: "shareWhiteIcon")?.withRenderingMode(.alwaysTemplate)
                    cell.favouriteBtn.setImage(shareImage, for: .normal)
                    cell.favouriteBtn.tintColor = UIColor.white
                    cell.contantView.backgroundColor = .clear
                }
                cell.favouriteBtn.setImage(UIImage(named: "shareWhiteIcon"), for: .normal)
            }
            
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            selectedIndex  = indexPath.row
            audioDetailViewModel?.setUpPlayerFor(selectedSegment: .All, selectedIndex: indexPath.row,isPlayAsPlaylist:false, categoryType: category_type)
            return
        }else{
            let (isEnabled , isSubscriber) = isContentUnlocked()
            if isEnabled{
                // play selected
                selectedIndex  = indexPath.row
                audioDetailViewModel?.setUpPlayerFor(selectedSegment: .All, selectedIndex: indexPath.row,isPlayAsPlaylist:false, categoryType: category_type)
            }else{
                // show subscription page
                if isSubscriber == true {
                    needSubscriptionToPlayFor(index:indexPath.row)
                } else {
                    needAdWatchToPlayFor(index: indexPath.row)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return isCollapsedShown ? 50 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if isCollapsedShown {
            let v = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
            v.backgroundColor = .clear
            return v
        }
        return nil
    }
    
}
extension ArtistDetailController: audioPlayerDelegate {
    func didAudioChanged(playingIndex: Int, isPlaying: Bool) {
        if isPlaying{
            // log event
            
            //let userData = UserDefaults.standard.value(forKey: "userinfo") as! [String:Any]
           // AnalyticsManger.logEventWith(name: "artist_played", attributes: ["user_name": (userData["user_name"] as? String ?? "") , "phone_number" : (userData["phone"] as? String ?? ""), "user_location": myLocationPlacemark?.country ?? "","artist_name": (artistDetailViewModal.artistDetails?.name ?? ""),"artist_track_name": (artistDetailViewModal.artistDetails?.artistPlaylist?[playingIndex]?.audioTitle ?? ""),"date_time" : GlobalMethods.convertDateToString(date: Date(), format: "yyyy-MM-dd HH:mm:ss"),"device_type":"ios"])
        }
        self.isPlaying = isPlaying
        let (isEnabled , isSubscriber) = isContentUnlocked()
        if isEnabled {
            if self.isPlaying{
                playPauseLabel.text = "Pause"
                playPauseImageView.image = UIImage(named: "PauseIcon")
            }else{
                playPauseLabel.text = "Play"
                playPauseImageView.image = UIImage(named: "PlayIcon")
            }
        } else {
            if isSubscriber {
                playPauseLabel.text = "Donate Now"
                let image = UIImage(named: "HeartFavourite")
                playPauseImageView.image = image
              //  playPauseImageView.tintColor = UIColor.red
            } else {
                playPauseLabel.text = "Play"
                playPauseImageView.image = UIImage(named: "PlayIcon")
            }
            
        }
       
        selectedIndex = playingIndex
        dataTableView.reloadData()
    }
    func audioPlayerClosed() {
        isCollapsedShown = false
        self.setTabBarCorner()
        self.isPlaying = false
        let (isEnabled , isSubscriber) = isContentUnlocked()
        if isEnabled {
            playPauseLabel.text = "Play"
            playPauseImageView.image = UIImage(named: "PlayIcon")
        } else {
            if isSubscriber {
                playPauseLabel.text = "Donate Now"
                let image = UIImage(named: "HeartFavourite")?.withRenderingMode(.alwaysTemplate)
                playPauseImageView.image = image
            //    playPauseImageView.tintColor = UIColor.red
            } else {
                playPauseLabel.text = "Play"
                playPauseImageView.image = UIImage(named: "PlayIcon")
            }
        }
       
        dataTableView.reloadData()
    }
    func audioDidFinished() {
        let (isEnabled , isSubscriber) = isContentUnlocked()
        if isEnabled {
            playPauseLabel.text = "Play"
            playPauseImageView.image = UIImage(named: "PlayIcon")
        } else {
            if isSubscriber {
                playPauseLabel.text = "Donate Now"
                let image = UIImage(named: "HeartFavourite")
                playPauseImageView.image = image
               // playPauseImageView.tintColor = UIColor.red
            } else {
                playPauseLabel.text = "Play"
                playPauseImageView.image = UIImage(named: "PlayIcon")
            }
        }
       
        self.isPlaying = false
        dataTableView.reloadData()
    }
    func audioFavouriteStatusChanged(isFavourite:Bool ,selectedType: FavouriteType , audioId: Int) {
//        if isFavourite{
//            audioDetailViewModel?.playlistDetail?.audioData?[selectedIndex]?.favourites = "1"
//        }else{
//            audioDetailViewModel?.playlistDetail?.audioData?[selectedIndex]?.favourites = "0"
//        }
        dataTableView.reloadData()
    }
}

extension ArtistDetailController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            dataTableView.reloadData()
        }else{
            audioDetailViewModel?.getPlaylistDetailData(playlist_id: audioDetailViewModel?.playlistDetail?.id ?? 0, category_type: category_type, category_id: nil)
        }
        selectedIndex = playIndex
        // play notification track
        audioDetailViewModel?.setUpPlayerFor(selectedSegment: .All, selectedIndex: selectedIndex,isPlayAsPlaylist:false, categoryType: category_type)
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.showPlaylistData()
                self.setPremiumViews()
            }
        })
    }
    func subscriptionDidCancel(endDate:String){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.showPlaylistData()
                self.setPremiumViews()
            }
        })
    }
}
