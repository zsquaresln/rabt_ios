//
//  AllArtistViewController.swift
//  Rabt
//
//  Created by Asad Khan on 04/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import GoogleMobileAds
class AllArtistViewController: MasterViewController {
    
    @IBOutlet weak var artistCollectionView: UICollectionView!
    var artistViewModal : AllArtistsViewModal = AllArtistsViewModal()
    var selectedSegment : SegmentType!
    var selectedIndex = -1
    var adLoader: GADAdLoader = GADAdLoader()
    var nativeAds = [GADNativeAd]()
//    var heightConstraint: NSLayoutConstraint?
    var artistsAndAdsArray : [Any]? = []
    override func viewDidLoad() {
        super.viewDidLoad()
        artistViewModal.parentController = self
        artistViewModal.getArtistsWith(pageNo: "\(artistViewModal.currentPage)")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if splashSingelton.sharedInstance.splashDataModel?.artist_screen_scroll_native == false{
            nativeAds = []
            showData()
        }
    }
    //MARK: - Helper Functions
    func showData(){
        if let artistArray = artistViewModal.allArtists?.data{
            if artistArray.count > 0{
                artistsAndAdsArray = []
                artistsAndAdsArray?.append(contentsOf: artistArray as [Any])
                registerCollectionViewCells()
            }
        }
        
    }
    
    func registerCollectionViewCells(){
        artistCollectionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        artistCollectionView.register(UINib(nibName: "NativeAdsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NativeAdsCollectionCell")

        reloadCollectionView()
    }
    func reloadCollectionView(){
        addNativeAdsToArtistsData()
        artistCollectionView.delegate = self
        artistCollectionView.dataSource = self
        artistCollectionView.reloadData()
    }
    func addNativeAdsToArtistsData() {
        if nativeAds.count <= 0 {
            loadAds()
            return
        }
        if (artistsAndAdsArray?.count ?? 0) < 5{
            return
        }
        
        // let adInterval = ((booksViewModal.latestAudioBooks?.count ?? 0) / nativeAds.count) + 1
        artistsAndAdsArray = []
        artistsAndAdsArray?.append(contentsOf: artistViewModal.allArtists!.data! as [Any])
        var index = 4
        for nativeAd in nativeAds {
            if index < (artistsAndAdsArray?.count ?? 0) {
                artistsAndAdsArray?.insert(nativeAd, at: index)
                index += 5
            } else {
                break
            }
        }
    }
    
    func loadAds() {
        if splashSingelton.sharedInstance.splashDataModel?.artist_screen_scroll_native == true{
            let options = GADMultipleAdsAdLoaderOptions()
           
            var numberOfAds = (artistViewModal.allArtists?.data?.count ?? 0) / 4
            if numberOfAds > 5{
                numberOfAds = 5
            }
            options.numberOfAds = numberOfAds
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ GADSimulatorID ]
            adLoader = GADAdLoader(
                adUnitID: AdManager().nativeadUnitID, rootViewController: self,
                adTypes: [.native], options: [options])
            adLoader.delegate = self
            adLoader.load(GADRequest())
        }
       
    }
    //MARK: - IBAction
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: -collection View handling
extension AllArtistViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artistsAndAdsArray?.count ?? 0 //artistViewModal.allArtists?.data?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let artist = artistsAndAdsArray![indexPath.row] as? ArtistModal {
        let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        let currentArtist = artist //self.artistViewModal.allArtists?.data?[indexPath.row]
            cell.nameLbl.text = currentArtist.name
        let url_str = currentArtist.image ?? ""
        let url = URL(string: url_str)
        cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            if currentArtist.disable == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
                cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                
//                cell.lockImageView.isHidden = true
            } else {
                cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                
//                cell.lockImageView.isHidden = true
            }
        return cell
        }else{
            
            let cell: NativeAdsCollectionCell! = collectionView.dequeueReusableCell(withReuseIdentifier: "NativeAdsCollectionCell", for: indexPath as IndexPath) as? NativeAdsCollectionCell
            if cell.addView?.subviews.count == 0 && nativeAds.count > 0{
                let adView = Bundle.main.loadNibNamed("NativeAdView", owner: nil, options: nil)
                if let nativeAdView = adView?.first as? GADNativeAdView{
                    let nativeAd = artistsAndAdsArray?[indexPath.row] as! GADNativeAd
                    /// Set the native ad's rootViewController to the current view controller.
                    nativeAd.rootViewController = self
                    nativeAdView.translatesAutoresizingMaskIntoConstraints = false
                    nativeAdView.tag = 50
                    cell.addView?.addSubview(nativeAdView)
                    let viewDictionary = ["_nativeAdView": nativeAdView]
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "H:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "V:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    setupNativeAdView(nativeAdView: nativeAdView,nativeAd:nativeAd)
                }
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if artistViewModal.currentPage < artistViewModal.totalPages && indexPath.row == (artistViewModal.allArtists?.data?.count)! - 1 {
            artistViewModal.currentPage = artistViewModal.currentPage + 1
            artistViewModal.getArtistsWith(pageNo: "\(artistViewModal.currentPage)")
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let artist = artistsAndAdsArray![indexPath.row] as? ArtistModal{
        selectedIndex = indexPath.row
        self.show(storyBoard: .ListenNew, controllerIdentifier: "ArtistDetailController", navigation: self.navigationController){
            controller in
//            (controller as! ArtistDetailController).artistDetailViewModal.selectedArtist = artist
//            (controller as! ArtistDetailController).selectedSegment = selectedSegment
        }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let artist = artistsAndAdsArray![indexPath.row] as? ArtistModal{
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: (self.view.frame.size.width - 52) / 3, height: ((self.view.frame.size.width - 52) / 3) + 30)
        } else {
            return  CGSize(width: (self.view.frame.size.width - 42) / 2, height: ((self.view.frame.size.width - 42) / 2) + 30)
        }
        }else{
            if nativeAds.count > 0{
//                let cell = collectionView.cellForItem(at: IndexPath.init(row: indexPath.row, section: 0)) as? NativeAdsCollectionCell
//                cell?.addView?.subviews
                if let _ = (artistsAndAdsArray?[indexPath.row] as? GADNativeAd)?.mediaContent.hasVideoContent{
                    let width = CGFloat (collectionView.frame.size.width)
                    return CGSize (width: width, height: 313)
                }
              
            }
            let width = CGFloat (collectionView.frame.size.width)
            return CGSize (width: width, height: 120)
        }
    }
}
extension AllArtistViewController: GADNativeAdLoaderDelegate,GADNativeAdDelegate,GADVideoControllerDelegate{
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        nativeAds.append(nativeAd)
        addNativeAdsToArtistsData()
        nativeAd.delegate = self
        reloadCollectionView()
        
        
        //        collectionviewAUction.reloadData()
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        
    }
    func setupNativeAdView(nativeAdView : GADNativeAdView,nativeAd:GADNativeAd){
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        
        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
            // By acting as the delegate to the GADVideoController, this ViewController receives messages
            // about events in the video lifecycle.
            mediaContent.videoController.delegate = self
            //  videoStatusLabel.text = "Ad contains a video asset."
        } else {
            //videoStatusLabel.text = "Ad does not contain a video."
        }
        
        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
                       let heightConstraint = NSLayoutConstraint(
                          item: mediaView,
                          attribute: .height,
                          relatedBy: .equal,
                          toItem: mediaView,
                          attribute: .width,
                          multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
                          constant: 0)
                        heightConstraint.isActive = true
        }
        
        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        
        // (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from: nativeAd.starRating)
        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
        
        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
        nativeAdView.storeView?.isHidden = nativeAd.store == nil
        
        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
        nativeAdView.priceView?.isHidden = nativeAd.price == nil
        
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
        
        // In order for the SDK to process touch events properly, user interaction should be disabled.
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
        
        // Associate the native ad view with the native ad object. This is
        // required to make the ad clickable.
        // Note: this should always be done after populating the ad views.
        nativeAdView.nativeAd = nativeAd
    }
    
}
