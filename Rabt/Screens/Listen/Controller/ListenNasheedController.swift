//
//  ListenNasheedController.swift
//  Rabt
//
//  Created by Admin on 12/7/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ListenNasheedController: MasterViewController {
    
    @IBOutlet weak var listenNasheedTableView: UICollectionView!
    var adLoader: GADAdLoader = GADAdLoader()
    var nativeAds = [GADNativeAd]()
    var nasheetAndAdsArray : [Any]? = []
    var selectedIndex = -1
    var listenViewModal : ListenViewModal?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_books_nasheet_native == false{
            nativeAds = []
            reloadSegmentData()
        }
    }
    //MARK: - Helper Functions
    func reloadSegmentData(){
        registerCollectionViewCells()
        if listenViewModal?.allListenModal?.listenNasheet?.count ?? 0 > 0{
            nasheetAndAdsArray?.append(contentsOf: listenViewModal?.allListenModal?.listenNasheet! as [Any])
            loadAds()
        }
    }
    func registerCollectionViewCells(){
        listenNasheedTableView.register(UINib(nibName: "AudioBookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "audioBookCell")
        listenNasheedTableView.register(UINib(nibName: "NativeAdsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NativeAdsCollectionCell")
        reloadCollectionView()
        
    }
    func reloadCollectionView(){
        addNativeAdsToNasheetData()
        listenNasheedTableView.delegate = self
        listenNasheedTableView.dataSource = self
        listenNasheedTableView.reloadData()
    }
    
    func addNativeAdsToNasheetData() {
        if nativeAds.count <= 0 {
            loadAds()
            return
        }
        if (nasheetAndAdsArray?.count ?? 0) < 5{
            return
        }
        
        // let adInterval = ((booksViewModal.latestAudioBooks?.count ?? 0) / nativeAds.count) + 1
        nasheetAndAdsArray = []
        nasheetAndAdsArray?.append(contentsOf: listenViewModal?.allListenModal?.listenNasheet! as [Any])
        var index = 4
        for nativeAd in nativeAds {
            if index < (nasheetAndAdsArray?.count ?? 0) {
                nasheetAndAdsArray?.insert(nativeAd, at: index)
                index += 5
            } else {
                break
            }
        }
    }
    
    func loadAds() {
        if listenViewModal?.allListenModal?.listenNasheet?.count ?? 0 == 0{
            return
        }
        if nativeAds.count > 0{
            return
        }
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_books_nasheet_native == true{
            let options = GADMultipleAdsAdLoaderOptions()
            
            var numberOfAds = (listenViewModal?.allListenModal?.listenNasheet?.count ?? 0) / 4
            if numberOfAds > 5{
                numberOfAds = 5
            }
            options.numberOfAds = numberOfAds
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ GADSimulatorID ]
            adLoader = GADAdLoader(
                adUnitID: AdManager().nativeadUnitID, rootViewController: self,
                adTypes: [.native], options: [options])
            adLoader.delegate = self
            adLoader.load(GADRequest())
        }
        
    }
    //MARK: - IBActions
    @objc func seeAllClicked(_ sender: UIButton){
        
    }
}
//MARK: -collection View handling
extension ListenNasheedController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nasheetAndAdsArray?.count ?? 0 //listenViewModal?.allListenModal?.listenNasheet?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let nasheet = nasheetAndAdsArray![indexPath.row] as? NasheedModal {
            let cell : AudioBookCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "audioBookCell", for: indexPath) as! AudioBookCollectionViewCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            let nasheetModal = nasheet//listenViewModal?.allListenModal?.listenNasheet?[indexPath.row]
            cell.nameLbl.text = nasheetModal.name
            let url_str = nasheetModal.image ?? ""
            let url = URL(string: url_str)
            cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            cell.durationLbl.isHidden = true
            if nasheetModal.duration != nil {
                cell.durationLbl.isHidden = false
            }
            let duration = Double(nasheetModal.duration ?? "0") ?? 0.0
            cell.durationLbl.text = duration.asString(style: .positional)
            return cell
        }else{
            
            let cell: NativeAdsCollectionCell! = collectionView.dequeueReusableCell(withReuseIdentifier: "NativeAdsCollectionCell", for: indexPath as IndexPath) as? NativeAdsCollectionCell
            if cell.addView?.subviews.count == 0 && nativeAds.count > 0{
                let adView = Bundle.main.loadNibNamed("NativeAdView", owner: nil, options: nil)
                if let nativeAdView = adView?.first as? GADNativeAdView{
                    let nativeAd = nasheetAndAdsArray?[indexPath.row] as! GADNativeAd
                    /// Set the native ad's rootViewController to the current view controller.
                    nativeAd.rootViewController = self
                    nativeAdView.translatesAutoresizingMaskIntoConstraints = false
                    nativeAdView.tag = 50
                    cell.addView?.addSubview(nativeAdView)
                    let viewDictionary = ["_nativeAdView": nativeAdView]
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "H:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "V:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    setupNativeAdView(nativeAdView: nativeAdView,nativeAd:nativeAd)
                }
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let nasheet = nasheetAndAdsArray![indexPath.row] as? NasheedModal{
            if let selectedNasheetIndex = listenViewModal?.allListenModal?.listenNasheet?.firstIndex(where: {$0?.id == nasheet.id}){
                selectedIndex = selectedNasheetIndex
                listenViewModal?.setUpPlayerFor(selectedAudioType: .Nasheet, selectedIndex: selectedIndex)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let _ = nasheetAndAdsArray![indexPath.row] as? NasheedModal{
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: (self.view.frame.size.width - 20) / 3, height: ((self.view.frame.size.width - 20) / 3) + 30)
            } else {
                return  CGSize(width: (self.view.frame.size.width - 10) / 2, height: ((self.view.frame.size.width - 10) / 2) + 30)
            }
        }else{
            if nativeAds.count > 0{
                if let _ = (nasheetAndAdsArray?[indexPath.row] as? GADNativeAd)?.mediaContent.hasVideoContent{
                    let width = CGFloat (collectionView.frame.size.width)
                    return CGSize (width: width, height: 313)
                }
                
            }
            let width = CGFloat (collectionView.frame.size.width)
            return CGSize (width: width, height: 120)
        }
    }
}
extension ListenNasheedController: GADNativeAdLoaderDelegate,GADNativeAdDelegate,GADVideoControllerDelegate{
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        nativeAds.append(nativeAd)
        addNativeAdsToNasheetData()
        nativeAd.delegate = self
        reloadCollectionView()
        
        
        //        collectionviewAUction.reloadData()
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        
    }
    func setupNativeAdView(nativeAdView : GADNativeAdView,nativeAd:GADNativeAd){
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        
        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
            // By acting as the delegate to the GADVideoController, this ViewController receives messages
            // about events in the video lifecycle.
            mediaContent.videoController.delegate = self
            //  videoStatusLabel.text = "Ad contains a video asset."
        } else {
            //videoStatusLabel.text = "Ad does not contain a video."
        }
        
        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
            let heightConstraint = NSLayoutConstraint(
                item: mediaView,
                attribute: .height,
                relatedBy: .equal,
                toItem: mediaView,
                attribute: .width,
                multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
                constant: 0)
            heightConstraint.isActive = true
        }
        
        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        
        // (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from: nativeAd.starRating)
        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
        
        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
        nativeAdView.storeView?.isHidden = nativeAd.store == nil
        
        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
        nativeAdView.priceView?.isHidden = nativeAd.price == nil
        
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
        
        // In order for the SDK to process touch events properly, user interaction should be disabled.
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
        
        // Associate the native ad view with the native ad object. This is
        // required to make the ad clickable.
        // Note: this should always be done after populating the ad views.
        nativeAdView.nativeAd = nativeAd
    }
    
}

