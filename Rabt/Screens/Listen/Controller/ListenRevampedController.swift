//
//  ListenRevampedController.swift
//  Rabt
//
//  Created by Tayyab Faran on 10/09/2023.
//  Copyright © 2023 Ali Sher. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ListenRevampedController: MasterViewController {
    
    @IBOutlet weak var tabsCollectionView: UICollectionView!
    @IBOutlet weak var playlistTableView: UITableView!
    @IBOutlet weak var playslistCollectionView: UICollectionView!
    @IBOutlet weak var donateNowView: UIView!
    @IBOutlet weak var donateNowHeightConstraint: NSLayoutConstraint!
    // Properties
    
    var isSubscriptionDone : Bool = false
    var selectedTabIndex = 1
    var isSearchOpened = false
    var allAudios : [ListeRevampedPlaylistData?]?
    //var searchedText = ""
    var listenViewModel : ListenViewModal?
    
    var adLoader: GADAdLoader = GADAdLoader()
    var nativeAds = [GADNativeAd]()
    var categoryAndAdsArray : [Any]? = []
    var selectedIndex = -1
    var tabFilteredPlaylistData : [ListeRevampedPlaylistData?]?
    var isSearching: Bool = false
    var lastSelectedCategory_type = ""
    
    // Favorites
    var favoriteArtistCollectionView : UICollectionView?
    var favoriteBooksCollectionView : UICollectionView?
    var favoritePlaylistCollectionView : UICollectionView?
    var favoriteNativeAdManager = NativeAdManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        listenViewModel = ListenViewModal()
        listenViewModel?.parentController = self
        listenViewModel?.getListenRevampedData()
        listenViewModel?.getFavouritesData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listenViewModel?.parentController = self
        setPremiumViews()
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_books_scroll_native == false{
            nativeAds = []
            reloadCategoryData()
        }
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_favorite_native == false{
            favoriteNativeAdManager.nativeAds = []
            reloadFavoriteSegmentData()
        }
        
    }
    
    //MARK: - Helper Functions
    func setPremiumViews(){
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            isSubscriptionDone = false
            // do nothing
            DispatchQueue.main.async{
                self.donateNowView.isHidden = false
                self.donateNowHeightConstraint.constant = 50
            }
            
        }else{
            
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip && isAutoRenewable{
                isSubscriptionDone = true
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                    self.donateNowHeightConstraint.constant = 0
                }
            }else if isVip{
                isSubscriptionDone = true
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                    self.donateNowHeightConstraint.constant = 0
                }
            }
            else{
                isSubscriptionDone = false
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = false
                    self.donateNowHeightConstraint.constant = 50
                }
            }
        }
        
        playlistTableView.reloadData()
        playslistCollectionView.delegate = self
        playslistCollectionView.dataSource = self
        playslistCollectionView.reloadData()
    }
    func loadTabs(){
        // registerTableViewCells()
        mergeAllAudiosForAllCategory()
        tabsCollectionView.register(UINib(nibName: "ListenFavCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ListenFavCollectionCell")
        tabsCollectionView.register(UINib(nibName: "ListenSearchCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ListenSearchCollectionCell")
        tabsCollectionView.register(UINib(nibName: "ListenTabsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ListenTabsCollectionCell")
        tabsCollectionView.delegate = self
        tabsCollectionView.dataSource = self
        tabsCollectionView.reloadData()
        registerTableViewCells()
        self.tabsCollectionView.collectionViewLayout.invalidateLayout()
        reloadCategoryData()
    }
    
    func reloadCategoryData(){
        registerPlayslistCollectionCells()
        
        categoryAndAdsArray = []
        if listenViewModel?.listenRevampedModal?.count ?? 0 > 0{
            categoryAndAdsArray?.append(contentsOf: listenViewModel?.listenRevampedModal! as [Any])
            loadAds()
        }
        
        registerTableViewCells()
    }
    
    func reloadFavoriteSegmentData(){
        if selectedTabIndex != 2 {
            return
        }
        if favoriteArtistCollectionView != nil || favoriteBooksCollectionView != nil || favoritePlaylistCollectionView != nil {
            favoriteArtistCollectionView?.dataSource = nil
            favoriteArtistCollectionView?.delegate  = nil
            favoriteArtistCollectionView = nil
            favoriteBooksCollectionView?.dataSource = nil
            favoriteBooksCollectionView?.delegate  = nil
            favoriteBooksCollectionView = nil
            favoritePlaylistCollectionView?.dataSource = nil
            favoritePlaylistCollectionView?.delegate  = nil
            favoritePlaylistCollectionView = nil
        }
        loadFavoriteAds()
        playlistTableView.dataSource = self
        playlistTableView.delegate = self
        playlistTableView.reloadData()
    }
    
    func registerFavoriteCollectionViewCells(collectionView: UICollectionView){
        
        collectionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        collectionView.register(UINib(nibName: "AudioBookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "audioBookCell")
        collectionView.register(UINib(nibName: "NativeAdsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NativeAdsCollectionCell")
        collectionView.register(UINib(nibName: "ListenCategoryCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ListenCategoryCollectionCell")
        collectionView.register(UINib(nibName: "NativeAdsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NativeAdsCollectionCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        if collectionView.tag == 5{
            favoriteArtistCollectionView = collectionView
        } else if collectionView.tag == 7 {
            favoriteBooksCollectionView = collectionView
        } else if collectionView.tag == 9 {
            favoritePlaylistCollectionView = collectionView
        }
        collectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.reloadFavoriteCollectionViewsWithDelay()
        }
    }
    
    func reloadFavoriteCollectionViewsWithDelay(){
        favoriteArtistCollectionView?.reloadData()
        favoriteBooksCollectionView?.reloadData()
        favoritePlaylistCollectionView?.reloadData()
    }
    
    
    
    func registerPlayslistCollectionCells(){
        playslistCollectionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        playslistCollectionView.register(UINib(nibName: "ListenCategoryCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ListenCategoryCollectionCell")
        playslistCollectionView.register(UINib(nibName: "NativeAdsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NativeAdsCollectionCell")
        reloadCategoryView()
    }
    func reloadCategoryView(){
        addNativeAdsToCategoryData()
        playlistTableView.delegate = self
        playlistTableView.dataSource = self
        playlistTableView.reloadData()
    }
    
    func registerTableViewCells(){
        playlistTableView.register(UINib(nibName: "recomendedArtistsTableViewCell", bundle: nil), forCellReuseIdentifier: "recomendedArtistsTableViewCell")
        playlistTableView.register(UINib(nibName: "TableCellWithCollectionView", bundle: nil), forCellReuseIdentifier: "TableCellWithCollectionView")
        playlistTableView.register(UINib(nibName: "DonateNowTableCell", bundle: nil), forCellReuseIdentifier: "DonateNowTableCell")
        playlistTableView.register(UINib(nibName: "ListenPlaylistTableCell", bundle: nil), forCellReuseIdentifier: "ListenPlaylistTableCell")
        playlistTableView.dataSource = self
        playlistTableView.delegate = self
        playlistTableView.reloadData()
    }
    
    func playlistDetailFetched() {
        if listenViewModel?.playlistDetail?.audioData?.count ?? 0 > 0 {
            let audioData = listenViewModel?.playlistDetail?.audioData
            if audioData?.count ?? 0 == 1 {
                // play audio
                listenViewModel?.setUpPlayerFor(selectedAudioType: .Nasheet, selectedIndex: 0, categoryType: lastSelectedCategory_type)
                playslistCollectionView.delegate = self
                playslistCollectionView.dataSource = self
                playslistCollectionView.reloadData()
            } else {
                // show audio listing page
                // audioDetailModel?.playlistDetail = nil
                self.show(storyBoard: .ListenNew, controllerIdentifier: "ArtistDetailController", navigation: self.navigationController){
                    controller in
                    (controller as! ArtistDetailController).audioDetailViewModel = self.listenViewModel
                    if selectedTabIndex > 2 {
                        (controller as! ArtistDetailController).category_type = listenViewModel?.listenRevampedModal?[selectedTabIndex - 3]?.category_type ?? ""
                    } else {
                        (controller as! ArtistDetailController).category_type = lastSelectedCategory_type
                        // All tab is selected so find relevant object and send
                    }
                }
            }
        }
    }
    
    func loadFavoriteAds() {
        
        if favoriteNativeAdManager.nativeAds.count > 0{
            return
        }
        
        //        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_favorite_banner == true{
        //            //Load Banner
        //            bannerView.adUnitID = AdManager().bannerId
        //            bannerView.rootViewController = self
        //            bannerView.load(GADRequest())
        //        }
        
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_favorite_native == true{
            //Load Native
            let options = GADMultipleAdsAdLoaderOptions()
            options.numberOfAds = 2
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ GADSimulatorID ]
            favoriteNativeAdManager.adLoader = GADAdLoader(
                adUnitID: AdManager().nativeadUnitID, rootViewController: self,
                adTypes: [.native], options: [options])
            favoriteNativeAdManager.adLoader.delegate = favoriteNativeAdManager
            favoriteNativeAdManager.adLoader.load(GADRequest())
        }
        
    }
    
    func addNativeAdsToCategoryData() {
        if nativeAds.count <= 0 {
            loadAds()
            return
        }
        if (categoryAndAdsArray?.count ?? 0) < 5{
            return
        }
        
        // let adInterval = ((booksViewModal.latestAudioBooks?.count ?? 0) / nativeAds.count) + 1
        categoryAndAdsArray = []
        categoryAndAdsArray?.append(contentsOf: listenViewModel?.listenRevampedModal! as [Any])
        var index = 4
        for nativeAd in nativeAds {
            if index < (categoryAndAdsArray?.count ?? 0) {
                categoryAndAdsArray?.insert(nativeAd, at: index)
                index += 5
            } else {
                break
            }
        }
    }
    
    func loadAds() {
        if listenViewModel?.listenRevampedModal?.count ?? 0 == 0{
            return
        }
        if nativeAds.count > 0{
            return
        }
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_books_scroll_native == true{
            let options = GADMultipleAdsAdLoaderOptions()
            
            var numberOfAds = (listenViewModel?.listenRevampedModal?.count ?? 0) / 4
            if numberOfAds > 5{
                numberOfAds = 5
            }
            options.numberOfAds = numberOfAds
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ GADSimulatorID ]
            adLoader = GADAdLoader(
                adUnitID: AdManager().nativeadUnitID, rootViewController: self,
                adTypes: [.native], options: [options])
            adLoader.delegate = self
            adLoader.load(GADRequest())
        }
        
    }
    // MARK: - IBActions
    @IBAction func unlockPremiumBtnTapped(_ sender: Any) {
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.delegate = self
            tabCon.add(subsriptionController)
        }
    }
    @objc func searchBtnTapped(_ sender: UIButton) {
        if isSearchOpened {
            isSearchOpened = false
            isSearching = false
            tabsCollectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
            self.tabsCollectionView.collectionViewLayout.invalidateLayout()
            tabChanged()
        } else {
            isSearchOpened = true
            isSearching = false
            tabsCollectionView.reloadItems(at: [IndexPath(row: 0, section: 0)])
            self.tabsCollectionView.collectionViewLayout.invalidateLayout()
        }
    }
}
//MARK: - Type CollectionView Methods
extension ListenRevampedController :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == tabsCollectionView {
            return (listenViewModel?.listenRevampedModal?.count ?? 0 ) + 4
        } else if collectionView == playslistCollectionView {
            return tabFilteredPlaylistData?.count ?? 0
        } else {
            if selectedTabIndex == 2 {
                if collectionView.tag == 5{
                    return listenViewModel?.allFavouriteModal?.artists?.count ?? 0
                }
                else if collectionView.tag == 7{
                    return listenViewModel?.allFavouriteModal?.latestAudioBooks?.count ?? 0
                }else if collectionView.tag == 9{
                    return listenViewModel?.allFavouriteModal?.playlist?.count ?? 0
                }else{
                    return 1
                }
            } else if selectedTabIndex == 1 {
                // handle all category
                if collectionView.tag == 105 {
                    return kAPPDelegate.recentlyPlayed?.count ?? 0
                } else {
                    return categoryAndAdsArray?.count ?? 0
                }
                
            } else {
                return 0
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == tabsCollectionView {
            print(indexPath.row)
            if indexPath.row == 0 {
                // search
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListenSearchCollectionCell", for: indexPath) as! ListenSearchCollectionCell
                DispatchQueue.main.async {
                    if self.isSearchOpened {
                        cell.searchTextField.isHidden = false
                        cell.widthConstraint.constant = 335
                    } else {
                        cell.searchTextField.isHidden = true
                        cell.widthConstraint.constant = 36
                    }
                    if self.isSearching == false {
                        cell.searchTextField.text = ""
                    }
                    cell.searchTextField.addTarget(self, action: #selector(self.searchTextFieldDidChangedText(_ :)), for: .editingChanged)
                }
                cell.searchBtnTapped.addTarget(self, action: #selector(searchBtnTapped(_:)), for: .touchUpInside)
                return cell
            } else if indexPath.row == 1 {
                // All
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListenTabsCollectionCell", for: indexPath) as! ListenTabsCollectionCell
                if selectedTabIndex == 1 {
                    UIView.animate(withDuration: 0.3, animations: {
                        cell.containerView.backgroundColor = .white
                        cell.tabNameLabel.textColor = UIColor.init(hexString: "326962")
                    })
                } else {
                    cell.containerView.backgroundColor = UIColor.init(hexString: "326962")
                    cell.tabNameLabel.textColor = .white
                }
                cell.tabNameLabel.text = "All"
                cell.containerView.borderWidth = 1
                return cell
            } else if indexPath.row == 2 {
                // Favorite
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListenTabsCollectionCell", for: indexPath) as! ListenTabsCollectionCell
                if selectedTabIndex == 2 {
                    UIView.animate(withDuration: 0.3, animations: {
                        cell.containerView.backgroundColor = .white
                        cell.tabNameLabel.textColor = UIColor.init(hexString: "326962")
                    })
                } else {
                    cell.containerView.backgroundColor = UIColor.init(hexString: "326962")
                    cell.tabNameLabel.textColor = .white
                }
                cell.tabNameLabel.text = "Liked"
                cell.containerView.borderWidth = 1
                return cell
            } else {
                // dynamic categories
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListenTabsCollectionCell", for: indexPath) as! ListenTabsCollectionCell
                cell.containerView.borderWidth = 1
                if selectedTabIndex == indexPath.row {
                    UIView.animate(withDuration: 0.3, animations: {
                        cell.containerView.backgroundColor = .white
                        cell.tabNameLabel.textColor = UIColor.init(hexString: "326962")
                    })
                } else {
                    cell.containerView.backgroundColor = UIColor.init(hexString: "326962")
                    cell.tabNameLabel.textColor = .white
                }
                if (indexPath.row - 3) < listenViewModel?.listenRevampedModal?.count ?? 0 {
                    let tabObject = listenViewModel?.listenRevampedModal?[indexPath.row - 3]
                    cell.tabNameLabel.text = tabObject?.category_title
                } else {
                    cell.containerView.backgroundColor = .clear
                    cell.tabNameLabel.text = ""
                    cell.containerView.borderWidth = 0
                }
                
                return cell
            }
        } else {
            if selectedTabIndex == 2 {
                if collectionView.tag == 5 {
                    let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
                    //  cell.lockImageView.isHidden = true
                    cell.imgView.layer.cornerRadius = 10
                    cell.imgView.clipsToBounds = true
                    cell.backgroundColor = .clear
                    cell.contantView.backgroundColor = .clear
                    cell.contentView.backgroundColor = .clear
                    cell.totalPlaysLabel.textColor = .white
                    cell.nameLbl.textColor = .white
                    let currentArtist = listenViewModel?.allFavouriteModal?.artists?[indexPath.row]
                    cell.nameLbl.text = currentArtist?.name
                    let url_str = currentArtist?.image
                    let url = URL(string: url_str ?? "")
                    cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
                    cell.contantView.clipsToBounds = true
                    cell.totalPlaysLabel.text = "\(currentArtist?.playlist_subtext ?? "") - \(currentArtist?.no_of_plays ?? "")"
                    cell.lockImageView.image = UIImage(named: "greenLockIcon")
                    if currentArtist?.disable == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
                        cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                        //  greenLockIcon
                        cell.lockImageView.isHidden = false
                    }
                    return cell
                }else if collectionView.tag == 7 {
                    let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
                    // cell.lockImageView.isHidden = true
                    cell.backgroundColor = .clear
                    cell.contentView.backgroundColor = .clear
                    cell.totalPlaysLabel.textColor = .white
                    cell.nameLbl.textColor = .white
                    let currentAudioBook = listenViewModel?.allFavouriteModal?.latestAudioBooks?[indexPath.row]
                    cell.nameLbl.text = currentAudioBook?.name
                    let url_str = currentAudioBook?.image ?? ""
                    let url = URL(string: url_str)
                    cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
                    //                    cell.durationLbl.isHidden = true
                    //                    if currentAudioBook?.duration != nil {
                    //                        cell.durationLbl.isHidden = false
                    //                    }
                    //                    let duration = Double(currentAudioBook?.duration ?? "0") ?? 0.0
                    //                    cell.durationLbl.text = duration.asString(style: .positional)
                    cell.totalPlaysLabel.text = "\(currentAudioBook?.playlist_subtext ?? "") - \(currentAudioBook?.no_of_plays ?? "")"
                    cell.lockImageView.image = UIImage(named: "greenLockIcon")
                    if currentAudioBook?.disable == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
                        cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                        //  greenLockIcon
                        cell.lockImageView.isHidden = false
                    }
                    return cell
                }else if collectionView.tag == 9 {
                    let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
                    cell.totalPlaysLabel.textColor = .white
                    cell.nameLbl.textColor = .white
                    // cell.lockImageView.isHidden = true
                    cell.backgroundColor = .clear
                    cell.contentView.backgroundColor = .clear
                    let currentPlaylist = listenViewModel?.allFavouriteModal?.playlist?[indexPath.row]
                    cell.nameLbl.text = currentPlaylist?.playlist_title
                    let url_str = currentPlaylist?.playlist_image ?? ""
                    let url = URL(string: url_str)
                    cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
                    // cell.durationLbl.isHidden = true
                    cell.totalPlaysLabel.text = "\(currentPlaylist?.playlist_subtext ?? "") - \(currentPlaylist?.no_of_plays ?? "")"
                    cell.lockImageView.image = UIImage(named: "greenLockIcon")
                    if currentPlaylist?.disable == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
                        cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                        //  greenLockIcon
                        cell.lockImageView.isHidden = false
                    }
                    return cell
                }else{
                    let cell: NativeAdsCollectionCell! = collectionView.dequeueReusableCell(withReuseIdentifier: "NativeAdsCollectionCell", for: indexPath as IndexPath) as? NativeAdsCollectionCell
                    if cell.addView?.subviews.count == 0 && nativeAds.count > 0{
                        let adView = Bundle.main.loadNibNamed("NativeAdView", owner: nil, options: nil)
                        if let nativeAdView = adView?.first as? GADNativeAdView{
                            
                            var nativeAd = nativeAds[0]
                            if indexPath.section == 3 {
                                nativeAd = nativeAds[1]
                            }
                            /// Set the native ad's rootViewController to the current view controller.
                            nativeAd.rootViewController = self
                            nativeAdView.translatesAutoresizingMaskIntoConstraints = false
                            nativeAdView.tag = 50
                            cell.addView?.addSubview(nativeAdView)
                            let viewDictionary = ["_nativeAdView": nativeAdView]
                            cell.addView?.addConstraints(
                                NSLayoutConstraint.constraints(
                                    withVisualFormat: "H:|[_nativeAdView]|",
                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                            )
                            cell.addView?.addConstraints(
                                NSLayoutConstraint.constraints(
                                    withVisualFormat: "V:|[_nativeAdView]|",
                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                            )
                            setupNativeAdView(nativeAdView: nativeAdView,nativeAd:nativeAd)
                        }
                    }
                    return cell
                }
            } else if collectionView == playslistCollectionView {
                let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
                //cell.lockImageView.isHidden = true
                cell.imgView.layer.cornerRadius = 10
                cell.imgView.clipsToBounds = true
                cell.totalPlaysLabel.textColor = .white
                cell.nameLbl.textColor = .white
                cell.contantView.backgroundColor = .clear
                cell.backgroundColor = .clear
                cell.contentView.backgroundColor = .clear
                let playlistData = tabFilteredPlaylistData?[indexPath.row]
                cell.nameLbl.text = playlistData?.playlist_title
                let url_str = playlistData?.playlist_image
                let url = URL(string: url_str ?? "")
                cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
                cell.contantView.clipsToBounds = true
                cell.totalPlaysLabel.text = "\(playlistData?.playlist_subtext ?? "") - \(playlistData?.no_of_plays ?? "")"
                //                if playlistData?.disable == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
                //                    cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                //                  //  greenLockIcon
                //                   // cell.lockImageView.isHidden = true
                //                } else {
                //                    cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                //                   // greenLockIcon
                //                   // cell.lockImageView.isHidden = true
                //                }
                //                if let audioPlayerController = tabController?.childViewController as? PlayerController {
                //                    if audioPlayerController.isAudioPlaying{
                //                        if audioPlayerController.selectedAudio?.playlistId ?? 0 == playlistData?.playlist_id ?? -1 {
                //                            cell.containerView?.backgroundColor  = UIColor.white
                //                        }
                //                    }
                //                }
                cell.lockImageView.image = UIImage(named: "greenLockIcon")
                if playlistData?.disable == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
                    cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                    //  greenLockIcon
                    cell.lockImageView.isHidden = false
                }
                return cell
            } else {
                if collectionView.tag == 105 {
                    let category = kAPPDelegate.recentlyPlayed![indexPath.row] as? [String: Any]
                    let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
                    // cell.lockImageView.isHidden = true
                    cell.imgView.layer.cornerRadius = 10
                    cell.imgView.clipsToBounds = true
                    cell.contantView.backgroundColor = .clear
                    cell.totalPlaysLabel.textColor = .white
                    cell.nameLbl.textColor = .white
                    cell.backgroundColor = .clear
                    cell.contentView.backgroundColor = .clear
                    //let audioBookModal = listenViewModal?.allListenModal?.listenAll?.latestAudioBooks?[indexPath.row]
                    
                    cell.nameLbl.text = category?["name"] as? String
                    let url_str = category?["image"] as? String ?? ""
                    let url = URL(string: url_str)
                    cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
                    cell.totalPlaysLabel.text =  "\(category?["subtext"] as? String ?? "") - \(category?["no_of_plays"] as? String ?? "")"
                    //                cell.durationLbl.isHidden = true
                    //                if audioBookModal.duration != nil {
                    //                    cell.durationLbl.isHidden = false
                    //                }
                    //                let duration = Double(audioBookModal.duration ?? "0") ?? 0.0
                    //                cell.durationLbl.text = duration.asString(style: .positional)
                    cell.lockImageView.image = UIImage(named: "greenLockIcon")
                    if let isDisabled = category?["disable"] as? Int {
                        if isDisabled == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
                            cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                            //  greenLockIcon
                            cell.lockImageView.isHidden = false
                        }
                    }
                    
                    return cell
                    
                } else {
                    if let category = categoryAndAdsArray![indexPath.row] as? ListeRevampedData {
                        let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
                        cell.totalPlaysLabel.textColor = .white
                        cell.nameLbl.textColor = .white
                        cell.lockImageView.isHidden = false
                        cell.imgView.layer.cornerRadius = 10
                        cell.imgView.clipsToBounds = true
                        cell.contantView.backgroundColor = .clear
                        cell.backgroundColor = .clear
                        cell.contentView.backgroundColor = .clear
                        //let audioBookModal = listenViewModal?.allListenModal?.listenAll?.latestAudioBooks?[indexPath.row]
                        let categoryModal = category
                        cell.nameLbl.text = categoryModal.category_title
                        let url_str = categoryModal.category_image ?? ""
                        let url = URL(string: url_str)
                        cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
                        cell.totalPlaysLabel.text = categoryModal.no_of_plays
                        //                cell.durationLbl.isHidden = true
                        //                if audioBookModal.duration != nil {
                        //                    cell.durationLbl.isHidden = false
                        //                }
                        //                let duration = Double(audioBookModal.duration ?? "0") ?? 0.0
                        //                cell.durationLbl.text = duration.asString(style: .positional)
                        cell.lockImageView.image = UIImage(named: "whitePlayIcon")
                        
                        return cell
                    }else{
                        
                        let cell: NativeAdsCollectionCell! = collectionView.dequeueReusableCell(withReuseIdentifier: "NativeAdsCollectionCell", for: indexPath as IndexPath) as? NativeAdsCollectionCell
                        if cell.addView?.subviews.count == 0 && nativeAds.count > 0{
                            let adView = Bundle.main.loadNibNamed("NativeAdView", owner: nil, options: nil)
                            if let nativeAdView = adView?.first as? GADNativeAdView{
                                let nativeAd = categoryAndAdsArray?[indexPath.row] as! GADNativeAd
                                /// Set the native ad's rootViewController to the current view controller.
                                nativeAd.rootViewController = self
                                nativeAdView.translatesAutoresizingMaskIntoConstraints = false
                                nativeAdView.tag = 50
                                cell.addView?.addSubview(nativeAdView)
                                let viewDictionary = ["_nativeAdView": nativeAdView]
                                cell.addView?.addConstraints(
                                    NSLayoutConstraint.constraints(
                                        withVisualFormat: "H:|[_nativeAdView]|",
                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                                )
                                cell.addView?.addConstraints(
                                    NSLayoutConstraint.constraints(
                                        withVisualFormat: "V:|[_nativeAdView]|",
                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                                )
                                setupNativeAdView(nativeAdView: nativeAdView,nativeAd:nativeAd)
                            }
                        }
                        return cell
                    }
                }
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == tabsCollectionView {
            return CGSize(width: 130, height: 36)
        } else {
            if selectedTabIndex == 2 {
                if collectionView.tag == 5 || collectionView.tag == 7 || collectionView.tag == 9 {
                    if UIDevice.current.userInterfaceIdiom == .pad {
                        return CGSize(width: (self.view.frame.size.width - 52) / 3, height: ((self.view.frame.size.width - 52) / 3) + 55)
                    } else {
                        return  CGSize(width: (self.view.frame.size.width - 42) / 2, height: ((self.view.frame.size.width - 42) / 2) + 55)
                    }
                }else{
                    return CGSize(width: collectionView.frame.size.width, height: 313)
                }
            } else if collectionView == playslistCollectionView {
                if UIDevice.current.userInterfaceIdiom == .pad {
                    return CGSize(width: (self.view.frame.size.width - 52) / 3, height: ((self.view.frame.size.width - 52) / 3) + 55)
                } else {
                    return  CGSize(width: (self.view.frame.size.width - 42) / 2, height: ((self.view.frame.size.width - 42) / 2) + 55)
                }
            } else {
                if collectionView.tag == 105 {
                    if UIDevice.current.userInterfaceIdiom == .pad {
                        return CGSize(width: (self.view.frame.size.width - 52) / 3, height: ((self.view.frame.size.width - 52) / 3) + 55)
                    } else {
                        return  CGSize(width: (self.view.frame.size.width - 42) / 2, height: ((self.view.frame.size.width - 42) / 2) + 55)
                    }
                }
                if let _ = categoryAndAdsArray![indexPath.row] as? ListeRevampedData{
                    if UIDevice.current.userInterfaceIdiom == .pad {
                        return CGSize(width: (self.view.frame.size.width - 52) / 3, height: ((self.view.frame.size.width - 52) / 3) + 55)
                    } else {
                        return  CGSize(width: (self.view.frame.size.width - 42) / 2, height: ((self.view.frame.size.width - 42) / 2) + 55)
                    }
                }else{
                    if nativeAds.count > 0{
                        if let _ = (categoryAndAdsArray?[indexPath.row] as? GADNativeAd)?.mediaContent.hasVideoContent{
                            let width = CGFloat (collectionView.frame.size.width)
                            return CGSize (width: width, height: 313)
                        }
                        
                    }
                    let width = CGFloat (collectionView.frame.size.width)
                    return CGSize (width: width, height: 120)
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == tabsCollectionView {
            if indexPath.row == 0 {
                return
            }
            if (indexPath.row - 3) < listenViewModel?.listenRevampedModal?.count ?? 0 {
                selectedTabIndex = indexPath.row
                tabsCollectionView.reloadData()
                tabsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                tabChanged()
            }
            
        } else if collectionView == playslistCollectionView {
            let playlistData = tabFilteredPlaylistData?[indexPath.row]
            var categoryId : Int?
            if selectedTabIndex > 2 {
                lastSelectedCategory_type = listenViewModel?.listenRevampedModal?[selectedTabIndex - 3]?.category_type ?? ""
                listenViewModel?.getPlaylistDetailData(playlist_id: playlistData?.playlist_id ?? -1, category_type: listenViewModel?.listenRevampedModal?[selectedTabIndex - 3]?.category_type ?? "" , category_id: categoryId)
            } else {
                // All tab selected so we need to find audio object and sent its category_type
                lastSelectedCategory_type = playlistData?.category_type ?? ""
                listenViewModel?.getPlaylistDetailData(playlist_id: playlistData?.playlist_id ?? -1, category_type:playlistData?.category_type ?? "" , category_id: categoryId)
            }
        } else {
            if selectedTabIndex == 1 {
                if collectionView.tag == 105 {
                    let audioData = kAPPDelegate.recentlyPlayed?[indexPath.row] as? [String: Any]
                    var categoryId : Int?
                    lastSelectedCategory_type = audioData?["category_type"] as? String ?? ""
                    listenViewModel?.getPlaylistDetailData(playlist_id: audioData?["id"] as? Int ?? -1, category_type: audioData?["category_type"] as? String ?? "" , category_id: categoryId)
                } else {
                    if let category = categoryAndAdsArray![indexPath.row] as? ListeRevampedData{
                        if let selectedCategoryIndex = listenViewModel?.listenRevampedModal?.firstIndex(where: {$0?.category_id == category.category_id}){
                            selectedIndex = selectedCategoryIndex
                            self.show(storyBoard: .ListenNew, controllerIdentifier: "ListenPlaylistController", navigation: self.navigationController){
                                controller in
                                (controller as! ListenPlaylistController).playlistModel = listenViewModel?.listenRevampedModal?[selectedIndex]
                                //                        (controller as! ListenPlaylistController).heading = listenViewModel?.listenRevampedModal?[selectedIndex]?.category_title
                                //                        (controller as! ListenPlaylistController).type = listenViewModel?.listenRevampedModal?[selectedIndex]?.category_type
                                
                            }
                        }
                        
                    }
                }
            } else if selectedTabIndex == 2 {
                if collectionView.tag == 5{
                    let audioData = listenViewModel?.allFavouriteModal?.artists?[indexPath.row]
                    var categoryId : Int?
                    lastSelectedCategory_type = audioData?.category_type ?? ""
                    listenViewModel?.getPlaylistDetailData(playlist_id: audioData?.id ?? -1, category_type: audioData?.category_type ?? "" , category_id: categoryId)
                    
                }else if collectionView.tag == 7{
                    let audioData = listenViewModel?.allFavouriteModal?.latestAudioBooks?[indexPath.row]
                    var categoryId : Int?
                    lastSelectedCategory_type = audioData?.category_type ?? ""
                    listenViewModel?.getPlaylistDetailData(playlist_id: audioData?.id ?? -1, category_type: audioData?.category_type ?? "" , category_id: categoryId)
                } else if collectionView.tag == 9 {
                    let audioData = listenViewModel?.allFavouriteModal?.playlist?[indexPath.row]
                    var categoryId : Int?
                    lastSelectedCategory_type = audioData?.category_type ?? ""
                    listenViewModel?.getPlaylistDetailData(playlist_id: audioData?.playlist_id ?? -1, category_type: audioData?.category_type ?? "" , category_id: categoryId)
                }
            }
        }
    }
}


extension ListenRevampedController: GADNativeAdLoaderDelegate,GADNativeAdDelegate,GADVideoControllerDelegate{
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        nativeAds.append(nativeAd)
        addNativeAdsToCategoryData()
        nativeAd.delegate = self
        reloadCategoryView()
        
        
        //        collectionviewAUction.reloadData()
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        
    }
    func setupNativeAdView(nativeAdView : GADNativeAdView,nativeAd:GADNativeAd){
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        
        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
            // By acting as the delegate to the GADVideoController, this ViewController receives messages
            // about events in the video lifecycle.
            mediaContent.videoController.delegate = self
            //  videoStatusLabel.text = "Ad contains a video asset."
        } else {
            //videoStatusLabel.text = "Ad does not contain a video."
        }
        
        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
            let heightConstraint = NSLayoutConstraint(
                item: mediaView,
                attribute: .height,
                relatedBy: .equal,
                toItem: mediaView,
                attribute: .width,
                multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
                constant: 0)
            heightConstraint.isActive = true
        }
        
        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        
        // (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from: nativeAd.starRating)
        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
        
        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
        nativeAdView.storeView?.isHidden = nativeAd.store == nil
        
        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
        nativeAdView.priceView?.isHidden = nativeAd.price == nil
        
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
        
        // In order for the SDK to process touch events properly, user interaction should be disabled.
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
        
        // Associate the native ad view with the native ad object. This is
        // required to make the ad clickable.
        // Note: this should always be done after populating the ad views.
        nativeAdView.nativeAd = nativeAd
    }
    
}

//MARK: - tap handling in segments containers

extension ListenRevampedController: audioPlayerDelegate {
    func didAudioChanged(playingIndex: Int, isPlaying: Bool) {
    }
    func audioPlayerClosed() {
        isCollapsedShown = false
        self.setTabBarCorner()
        playlistTableView.reloadData()
    }
    func audioDidFinished() {
        playlistTableView.reloadData()
    }
    func audioFavouriteStatusChanged(isFavourite:Bool ,selectedType: FavouriteType , audioId: Int) {
        listenViewModel?.setFavourite(isFavourite: isFavourite, selectedType: selectedType, audioId: audioId)
        listenViewModel?.getFavouritesData()
    }
}

// MARK: - Searching methods

extension ListenRevampedController: UITextFieldDelegate{
    @objc func searchTextFieldDidChangedText(_ sender: UITextField){
        if sender.text == ""{
            isSearching = false
            tabChanged()
        }else{
            isSearching = true
            playslistCollectionView.isHidden = false
            playlistTableView.isHidden = true
            if selectedTabIndex > 2 {
                
                tabFilteredPlaylistData = listenViewModel?.listenRevampedModal?[selectedTabIndex - 3]?.playlist?.filter{$0?.playlist_title?.lowercased().contains(sender.text?.lowercased() ?? "") == true}
                if tabFilteredPlaylistData?.count ?? 0 == 0 {
                    let completePlaylist = listenViewModel?.listenRevampedModal?[selectedTabIndex - 3]?.playlist
                    if completePlaylist?.count ?? 0 < 9 {
                        tabFilteredPlaylistData = completePlaylist
                    } else {
                        // show top 8 content
                        tabFilteredPlaylistData = []
                        for indexItem in 0...7 {
                            tabFilteredPlaylistData?.append(completePlaylist![indexItem])
                        }
                    }
                }
            } else {
                // All is selected so combine search from every tab
                
                tabFilteredPlaylistData = allAudios?.filter{$0?.playlist_title?.lowercased().contains(sender.text?.lowercased() ?? "") == true}
                if tabFilteredPlaylistData?.count ?? 0 == 0 {
                    let completePlaylist = allAudios
                    if completePlaylist?.count ?? 0 < 9 {
                        tabFilteredPlaylistData = completePlaylist
                    } else {
                        // show top 8 content
                        tabFilteredPlaylistData = []
                        for indexItem in 0...7 {
                            tabFilteredPlaylistData?.append(completePlaylist![indexItem])
                        }
                    }
                }
            }
        }
        playslistCollectionView.delegate = self
        playslistCollectionView.dataSource = self
        playslistCollectionView.reloadData()
    }
}

// MARK: - Dynamic categories handling

extension ListenRevampedController {
    
    func tabChanged() {
        if selectedTabIndex == 2 {
            playlistTableView.isHidden = false
            playslistCollectionView.isHidden = true
            reloadFavoriteSegmentData()
            listenViewModel?.getFavouritesData()
        } else if selectedTabIndex > 2{
            tabFilteredPlaylistData = listenViewModel?.listenRevampedModal?[selectedTabIndex - 3]?.playlist
            playslistCollectionView.isHidden = false
            playlistTableView.isHidden = true
            playslistCollectionView.delegate = self
            playslistCollectionView.dataSource = self
            playslistCollectionView.reloadData()
        } else {
            
            if selectedTabIndex == 1 {
                playlistTableView.isHidden = false
                playslistCollectionView.isHidden = true
                playlistTableView.reloadData()
                tabFilteredPlaylistData = allAudios
            } else {
                playlistTableView.isHidden = true
                playslistCollectionView.isHidden = false
                playslistCollectionView.delegate = self
                playslistCollectionView.dataSource = self
                playslistCollectionView.reloadData()
            }
        }
    }
    
    func mergeAllAudiosForAllCategory() {
        if let categoryArray = listenViewModel?.listenRevampedModal {
            for categoryItem in categoryArray {
                if let playlistArray = categoryItem?.playlist {
                    if allAudios == nil {
                        allAudios = playlistArray
                    } else {
                        allAudios?.append(contentsOf: playlistArray)
                    }
                }
            }
        }
    }
    
}

extension ListenRevampedController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedTabIndex == 2 {
            if listenViewModel?.allFavouriteModal?.artists?.count ?? 0 == 0 && listenViewModel?.allFavouriteModal?.latestAudioBooks?.count ?? 0 == 0 && listenViewModel?.allFavouriteModal?.playlist?.count ?? 0 == 0 {
                return 0
            }
            return 5
        } else if selectedTabIndex == 1 {
            return 3
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedTabIndex == 2 {
            
            if section == 1{
                if listenViewModel?.allFavouriteModal?.artists?.count ?? 0 > 0 {
                    if nativeAds.count < 1{
                        return 0
                    }
                } else {
                    return 0
                }
            }
            
            
            if section == 3{
                if listenViewModel?.allFavouriteModal?.latestAudioBooks?.count ?? 0 > 0 {
                    if nativeAds.count < 2{
                        return 0
                    }
                } else {
                    return 0
                }
            }
            
            return 1
        }
        // return tabFilteredPlaylistData?.count ?? 0
        if selectedTabIndex == 1 {
            //            if section == 0 {
            //                return kAPPDelegate.recentlyPlayed?.count ?? 0
            //            } else if section == 1 {
            //                return 1
            //            } else {
            //                return categoryAndAdsArray?.count ?? 0
            //            }
            if section == 0 {
                if kAPPDelegate.recentlyPlayed?.count ?? 0 > 0 {
                    return 1
                }
            } else if section == 1 {
                if isSubscriptionDone == false {
                    return 1
                } else {
                    return 0
                }
            } else {
                if categoryAndAdsArray?.count ?? 0 > 0 {
                    return 1
                }
            }
            return 0
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedTabIndex == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "recomendedArtistsTableViewCell", for: indexPath) as! recomendedArtistsTableViewCell
            
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = .clear
            cell.backgroundColor = .clear
            if indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 4{
                cell.collectionTopConstraintToContainer.priority = UILayoutPriority(700)
                cell.collectionTopConstraintToSeeView.priority = UILayoutPriority(1000)
                cell.seeAllView.isHidden = true
            }else{
                cell.collectionTopConstraintToContainer.priority = UILayoutPriority(1000)
                cell.collectionTopConstraintToSeeView.priority = UILayoutPriority(700)
                cell.seeAllView.isHidden = true
            }
            if let layout =  cell.dataCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal  // .horizontal
            }
            cell.dataCollectionView.isScrollEnabled = true
            cell.dataCollectionView.tag = indexPath.section + 5
            cell.dataCollectionView.backgroundColor = .clear
            self.registerFavoriteCollectionViewCells(collectionView: cell.dataCollectionView)
            if indexPath.section == 0{
                cell.titleTL.text = "Content Creators".localized()
            }else if indexPath.section == 2{
                cell.titleTL.text = "Latest Audio Books".localized()
            }else{
                cell.titleTL.text = "Playlists"
            }
            return cell
        } else if selectedTabIndex == 1 {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "recomendedArtistsTableViewCell", for: indexPath) as! recomendedArtistsTableViewCell
                
                cell.selectionStyle = .none
                cell.contentView.backgroundColor = .clear
                cell.backgroundColor = .clear
                
                cell.collectionTopConstraintToContainer.priority = UILayoutPriority(700)
                cell.collectionTopConstraintToSeeView.priority = UILayoutPriority(1000)
                cell.seeAllView.isHidden = true
                
                if let layout =  cell.dataCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                    layout.scrollDirection = .horizontal  // .horizontal
                }
                cell.dataCollectionView.isScrollEnabled = true
                cell.dataCollectionView.tag = indexPath.section + 105
                cell.dataCollectionView.backgroundColor = .clear
                self.registerFavoriteCollectionViewCells(collectionView: cell.dataCollectionView)
                cell.titleTL.text = "Recently played".localized()
                return cell
            } else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DonateNowTableCell", for: indexPath) as! DonateNowTableCell
                if let url = URL(string: splashSingelton.sharedInstance.listen_charity_image){
                    cell.donateNowImageView.sd_setImage(with: url , placeholderImage: UIImage(named: "donateNowNewIcon"))
                }
                cell.donateNowBtn.addTarget(self, action: #selector(unlockPremiumBtnTapped(_:)), for: .touchUpInside)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "recomendedArtistsTableViewCell", for: indexPath) as! recomendedArtistsTableViewCell
                
                cell.selectionStyle = .none
                cell.contentView.backgroundColor = .clear
                cell.backgroundColor = .clear
                
                cell.collectionTopConstraintToContainer.priority = UILayoutPriority(700)
                cell.collectionTopConstraintToSeeView.priority = UILayoutPriority(1000)
                cell.seeAllView.isHidden = true
                
                if let layout =  cell.dataCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                    layout.scrollDirection = .vertical  // .horizontal
                }
                cell.dataCollectionView.isScrollEnabled = false
                cell.dataCollectionView.tag = indexPath.section + 105
                cell.dataCollectionView.backgroundColor = .clear
                self.registerFavoriteCollectionViewCells(collectionView: cell.dataCollectionView)
                cell.titleTL.text = "Categories".localized()
                return cell
            }
        }
        return UITableViewCell()
        //else {
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "ListenPlaylistTableCell", for: indexPath) as! ListenPlaylistTableCell
        //            let playlistData = tabFilteredPlaylistData?[indexPath.row]
        //            let url_str = playlistData?.playlist_image ?? ""
        //            let url = URL(string: url_str)
        //            cell.iconView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
        //            cell.nameLabel.text = playlistData?.playlist_title
        //            if selectedTabIndex > 2 {
        //                cell.typeLabel.text = playlistData?.playlist_subtext ?? playlistData?.category_type
        //            } else {
        //                cell.typeLabel.text = playlistData?.playlist_subtext ?? playlistData?.category_type
        //            }
        //
        //            cell.arrowIcon.isHidden = true
        //            if playlistData?.is_playlist == "1" {
        //                cell.arrowIcon.isHidden = false
        //            }
        //            cell.containerView?.backgroundColor = UIColor.clear
        //            if let audioPlayerController = tabController?.childViewController as? PlayerController {
        //                if audioPlayerController.isAudioPlaying{
        //                    if audioPlayerController.selectedAudio?.playlistId ?? 0 == playlistData?.playlist_id ?? -1 {
        //                        cell.containerView?.backgroundColor  = UIColor.white
        //                    }
        //                }
        //            }
        //            return cell
        //      }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedTabIndex == 2 {
            if indexPath.section == 0{
                if listenViewModel?.allFavouriteModal?.artists?.count ?? 0 > 0{
                    return 290
                }
                else
                {
                    return 0
                }
            }else if indexPath.section == 2{
                if listenViewModel?.allFavouriteModal?.latestAudioBooks?.count ?? 0 > 0{
                    return 290
                }
                else
                {
                    return 0
                }
            }else if indexPath.section == 4{
                if listenViewModel?.allFavouriteModal?.playlist?.count ?? 0 > 0{
                    return 290
                }
                else
                {
                    return 0
                }
            }else{
                return 313
            }
        } else  if selectedTabIndex == 1 {
            if indexPath.section == 0 {
                if kAPPDelegate.recentlyPlayed?.count ?? 0 > 0 {
                    return 290
                } else {
                    return 0
                }
            } else if indexPath.section == 1 {
                return 145
            } else {
                if categoryAndAdsArray?.count ?? 0 > 0 {
                    let addsCount = (categoryAndAdsArray?.count ?? 0) - (listenViewModel?.listenRevampedModal?.count ?? 0)
                    var heightOfCell = (((listenViewModel?.listenRevampedModal?.count ?? 0)/2) * 260) + (addsCount * 313)
                    if ((listenViewModel?.listenRevampedModal?.count ?? 0) % 2)  == 1 {
                        heightOfCell += 260
                    }
                    return CGFloat(heightOfCell)
                } else {
                    return 0
                }
            }
            
        }
        
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            unlockPremiumBtnTapped(UIButton())
        }
        //        let playlistData = tabFilteredPlaylistData?[indexPath.row]
        //        var categoryId : Int?
        //        if selectedTabIndex > 2 {
        //            lastSelectedCategory_type = listenViewModel?.listenRevampedModal?[selectedTabIndex - 3]?.category_type ?? ""
        //            listenViewModel?.getPlaylistDetailData(playlist_id: playlistData?.playlist_id ?? -1, category_type: listenViewModel?.listenRevampedModal?[selectedTabIndex - 3]?.category_type ?? "" , category_id: categoryId)
        //        } else {
        //            // All tab selected so we need to find audio object and sent its category_type
        //            lastSelectedCategory_type = playlistData?.category_type ?? ""
        //            listenViewModel?.getPlaylistDetailData(playlist_id: playlistData?.playlist_id ?? -1, category_type:playlistData?.category_type ?? "" , category_id: categoryId)
        //        }
        
    }
}

extension ListenRevampedController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
        
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.setPremiumViews()
            }
        })
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
            tabCon.add(subsriptionController)
        }
    }
    func subscriptionDidCancel(endDate: String){
        DispatchQueue.main.async {
            self.setPremiumViews()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let tabCon = self.tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
                subsriptionController.endDate = endDate
                tabCon.add(subsriptionController)
            }
        }
        
    }
}
