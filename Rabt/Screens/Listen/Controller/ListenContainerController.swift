//
//  ListenContainerController.swift
//  Rabt
//
//  Created by Admin on 12/2/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import StickyTabBarViewController
enum SegmentType{
    case All
    case MyFavourite
    case Recommended
    case AudioBooks
    case Nasheed
    case None
    func getTypeFrom(stringValue : String) -> SegmentType{
        switch stringValue{
        case "All":
            return .All
        case "My Favourite":
            return .MyFavourite
        case "Recommended":
            return .Recommended
        case "Audio Books":
            return .AudioBooks
        case "Nasheed":
            return .Nasheed
        default:
            return .None
        }
    }
}
class ListenContainerController: MasterViewController {
    
    //IBOutlets
    @IBOutlet weak var typesCollectionView: UICollectionView!
    @IBOutlet weak var loadingView: LoadingOrNoRecordView!
    @IBOutlet var containerViewsArray: [UIView]!
    //Properties
    var typeOptionsArray : [String] = ["All", "My Favourite", "Recommended", "Audio Books", "Nasheed"]
    var listenViewModal : ListenViewModal = ListenViewModal()
    //they will get refference from segue method of this controller as segue get called when there are containers in parent controller
    var listenAllController : ListenAllController?
    var listenFavouriteController : ListenFavouritesController?
    var listenRecommendedController : ListenRecommendedController?
    var listenAudioBookController : ListenAudioBooksController?
    var listenNasheedController : ListenNasheedController?
//    var tabController: StickyViewControllerSupportingTabBarController? {
//        if let tabBarController = tabBarController as? StickyViewControllerSupportingTabBarController {
//            return tabBarController
//        }
//        return nil
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        singeltonClass.shared.naviagtionController = self.navigationController
        listenViewModal.parentController = self
        registerCollectionCell()
        listenViewModal.getListenData()
    }
    override func viewWillAppear(_ animated: Bool) {
            let userData = UserDefaults.standard.value(forKey: "userinfo") as? [String:Any]
            AnalyticsManger.logEventWith(name: "track_listen", attributes: ["username": userData?["user_name"] ?? "Guest" , "devicetype" : "iOS", "email": userData?["email"] ?? "Guest", "phone": userData?["phone"] ?? "Guest","location":PrayerTimeManger.shared.selectedLocationName ?? ""])
        singeltonClass.shared.naviagtionController = self.navigationController
        if isFavouriteNeedsRefresh == true{
            isFavouriteNeedsRefresh = false
            listenViewModal.getFavouritesData()
        }
        if isAllDataNeedsRefresh == true{
            isAllDataNeedsRefresh = false
            listenViewModal.getListenData()
        }
    }
    //MARK: - Helper Functions
    func registerCollectionCell(){
        typesCollectionView.register(UINib(nibName: "TypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "typeCell")
        typesCollectionView.delegate = self
        typesCollectionView.dataSource = self
        typesCollectionView.reloadData()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.destination {
            
        case let listenAllController as ListenAllController:
            self.listenAllController = listenAllController
            if listenViewModal.childSegmentController == nil{
                listenViewModal.childSegmentController = self.listenAllController
            }
        case let listenFavouriteController as ListenFavouritesController:
            self.listenFavouriteController = listenFavouriteController
            
        case let listenRecommendedController as ListenRecommendedController:
            self.listenRecommendedController = listenRecommendedController
        case let listenAudioBookController as ListenAudioBooksController:
            self.listenAudioBookController = listenAudioBookController
        case let listenNasheedController as ListenNasheedController:
            self.listenNasheedController = listenNasheedController
            
        default:
            break
        }
    }
}
//MARK: - Type CollectionView Methods
extension ListenContainerController :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return typeOptionsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "typeCell", for: indexPath) as! TypeCollectionViewCell
        cell.typeLbl.text = typeOptionsArray[indexPath.row]
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        cell.typeLbl.textColor = .white
        cell.contantView.backgroundColor = UIColor(white: 0.0/255, alpha: 0.2)
        if typeOptionsArray[indexPath.row] == listenViewModal.selectedType {
            UIView.animate(withDuration: 0.3, animations: {
                cell.typeLbl.textColor = .black
                cell.contantView.backgroundColor = .white
            })
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 32)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if listenViewModal.selectedType != typeOptionsArray[indexPath.row]{
            listenViewModal.selectedType = typeOptionsArray[indexPath.row]
            typesCollectionView.reloadData()
            listenViewModal.loadSelectedSegment(from: containerViewsArray)
        }
        typesCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}

//MARK: - tap handling in segments containers

extension ListenContainerController: audioPlayerDelegate {
    func didAudioChanged(playingIndex: Int, isPlaying: Bool) {
    }
    func audioPlayerClosed() {
        isCollapsedShown = false
        self.setTabBarCorner()
    }
    func audioDidFinished() {
    }
    func audioFavouriteStatusChanged(isFavourite:Bool ,selectedType: FavouriteType , audioId: Int) {
        listenViewModal.setFavourite(isFavourite: isFavourite, selectedType: selectedType, audioId: audioId)
        listenViewModal.getFavouritesData()
    }
}

