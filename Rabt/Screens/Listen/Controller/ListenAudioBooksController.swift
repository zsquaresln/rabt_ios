//
//  ListenAudioBooksController.swift
//  Rabt
//
//  Created by Admin on 12/7/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import GoogleMobileAds
class ListenAudioBooksController: MasterViewController {
    
    @IBOutlet weak var listenAudioBooksCollectionView: UICollectionView!
    var adLoader: GADAdLoader = GADAdLoader()
    var nativeAds = [GADNativeAd]()
    var listenViewModal : ListenViewModal?
    var booksAndAdsArray : [Any]? = []
    var selectedIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_books_scroll_native == false{
            nativeAds = []
            reloadSegmentData()
        }
    }
    //MARK: - Helper Functions
    func reloadSegmentData(){
        registerCollectionViewCells()
        if listenViewModal?.allListenModal?.listenAll?.latestAudioBooks?.count ?? 0 > 0{
            booksAndAdsArray?.append(contentsOf: listenViewModal?.allListenModal?.listenAll?.latestAudioBooks! as [Any])
            loadAds()
        }
       
        
    }
    func registerCollectionViewCells(){
        listenAudioBooksCollectionView.register(UINib(nibName: "AudioBookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "audioBookCell")
        listenAudioBooksCollectionView.register(UINib(nibName: "NativeAdsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NativeAdsCollectionCell")
        reloadCollectionView()
    }
    func reloadCollectionView(){
        addNativeAdsToBooksData()
        listenAudioBooksCollectionView.delegate = self
        listenAudioBooksCollectionView.dataSource = self
        listenAudioBooksCollectionView.reloadData()
    }
    
    func addNativeAdsToBooksData() {
        if nativeAds.count <= 0 {
            loadAds()
            return
        }
        if (booksAndAdsArray?.count ?? 0) < 5{
            return
        }
        
        // let adInterval = ((booksViewModal.latestAudioBooks?.count ?? 0) / nativeAds.count) + 1
        booksAndAdsArray = []
        booksAndAdsArray?.append(contentsOf: listenViewModal?.allListenModal?.listenAll?.latestAudioBooks! as [Any])
        var index = 4
        for nativeAd in nativeAds {
            if index < (booksAndAdsArray?.count ?? 0) {
                booksAndAdsArray?.insert(nativeAd, at: index)
                index += 5
            } else {
                break
            }
        }
    }
    
    func loadAds() {
        if listenViewModal?.allListenModal?.listenAll?.latestAudioBooks?.count ?? 0 == 0{
            return
        }
        if nativeAds.count > 0{
            return
        }
        if splashSingelton.sharedInstance.splashDataModel?.listen_screen_books_scroll_native == true{
            let options = GADMultipleAdsAdLoaderOptions()
            
            var numberOfAds = (listenViewModal?.allListenModal?.listenAll?.latestAudioBooks?.count ?? 0) / 4
            if numberOfAds > 5{
                numberOfAds = 5
            }
            options.numberOfAds = numberOfAds
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ GADSimulatorID ]
            adLoader = GADAdLoader(
                adUnitID: AdManager().nativeadUnitID, rootViewController: self,
                adTypes: [.native], options: [options])
            adLoader.delegate = self
            adLoader.load(GADRequest())
        }
       
    }
    
    //MARK: - IBActions
    @objc func seeAllClicked(_ sender: UIButton){
        
    }
}
//MARK: -collection View handling
extension ListenAudioBooksController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return booksAndAdsArray?.count ?? 0 //listenViewModal?.allListenModal?.listenAll?.latestAudioBooks?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let book = booksAndAdsArray![indexPath.row] as? AudioBookModal {
            let cell : AudioBookCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "audioBookCell", for: indexPath) as! AudioBookCollectionViewCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            //let audioBookModal = listenViewModal?.allListenModal?.listenAll?.latestAudioBooks?[indexPath.row]
            let audioBookModal = book
            cell.nameLbl.text = audioBookModal.name
            let url_str = audioBookModal.image ?? ""
            let url = URL(string: url_str)
            cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            cell.durationLbl.isHidden = true
            if audioBookModal.duration != nil {
                cell.durationLbl.isHidden = false
            }
            let duration = Double(audioBookModal.duration ?? "0") ?? 0.0
            cell.durationLbl.text = duration.asString(style: .positional)
            return cell
        }else{
            
            let cell: NativeAdsCollectionCell! = collectionView.dequeueReusableCell(withReuseIdentifier: "NativeAdsCollectionCell", for: indexPath as IndexPath) as? NativeAdsCollectionCell
            if cell.addView?.subviews.count == 0 && nativeAds.count > 0{
                let adView = Bundle.main.loadNibNamed("NativeAdView", owner: nil, options: nil)
                if let nativeAdView = adView?.first as? GADNativeAdView{
                    let nativeAd = booksAndAdsArray?[indexPath.row] as! GADNativeAd
                    /// Set the native ad's rootViewController to the current view controller.
                    nativeAd.rootViewController = self
                    nativeAdView.translatesAutoresizingMaskIntoConstraints = false
                    nativeAdView.tag = 50
                    cell.addView?.addSubview(nativeAdView)
                    let viewDictionary = ["_nativeAdView": nativeAdView]
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "H:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "V:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    setupNativeAdView(nativeAdView: nativeAdView,nativeAd:nativeAd)
                }
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // listenViewModal?.setUpPlayerFor(selectedAudioType: .Book, selectedIndex: indexPath.row)
        if let book = booksAndAdsArray![indexPath.row] as? AudioBookModal{
            if let selectedBookIndex = listenViewModal?.allListenModal?.listenAll?.latestAudioBooks?.firstIndex(where: {$0?.id == book.id}){
                selectedIndex = selectedBookIndex
                listenViewModal?.setUpPlayerFor(selectedAudioType: .Book, selectedIndex: selectedIndex)
            }
            
            //listenViewModal.setUpPlayerFor(selectedSegment: selectedSegment, selectedIndex: indexPath.row,selectedAudioBook:book)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let _ = booksAndAdsArray![indexPath.row] as? AudioBookModal{
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: (self.view.frame.size.width - 20) / 3, height: ((self.view.frame.size.width - 20) / 3) + 30)
            } else {
                return  CGSize(width: (self.view.frame.size.width - 10) / 2, height: ((self.view.frame.size.width - 10) / 2) + 30)
            }
        }else{
            if nativeAds.count > 0{
                if let _ = (booksAndAdsArray?[indexPath.row] as? GADNativeAd)?.mediaContent.hasVideoContent{
                    let width = CGFloat (collectionView.frame.size.width)
                    return CGSize (width: width, height: 313)
                }
                
            }
            let width = CGFloat (collectionView.frame.size.width)
            return CGSize (width: width, height: 120)
        }
    }
}
extension ListenAudioBooksController: GADNativeAdLoaderDelegate,GADNativeAdDelegate,GADVideoControllerDelegate{
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        nativeAds.append(nativeAd)
        addNativeAdsToBooksData()
        nativeAd.delegate = self
        reloadCollectionView()
        
        
        //        collectionviewAUction.reloadData()
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        
    }
    func setupNativeAdView(nativeAdView : GADNativeAdView,nativeAd:GADNativeAd){
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        
        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
            // By acting as the delegate to the GADVideoController, this ViewController receives messages
            // about events in the video lifecycle.
            mediaContent.videoController.delegate = self
            //  videoStatusLabel.text = "Ad contains a video asset."
        } else {
            //videoStatusLabel.text = "Ad does not contain a video."
        }
        
        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
            let heightConstraint = NSLayoutConstraint(
                item: mediaView,
                attribute: .height,
                relatedBy: .equal,
                toItem: mediaView,
                attribute: .width,
                multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
                constant: 0)
            heightConstraint.isActive = true
        }
        
        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        
        // (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from: nativeAd.starRating)
        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
        
        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
        nativeAdView.storeView?.isHidden = nativeAd.store == nil
        
        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
        nativeAdView.priceView?.isHidden = nativeAd.price == nil
        
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
        
        // In order for the SDK to process touch events properly, user interaction should be disabled.
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
        
        // Associate the native ad view with the native ad object. This is
        // required to make the ad clickable.
        // Note: this should always be done after populating the ad views.
        nativeAdView.nativeAd = nativeAd
    }
    
}

