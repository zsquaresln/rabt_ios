//
//  ListenPlaylistController.swift
//  Rabt
//
//  Created by Tayyab Faran on 10/09/2023.
//  Copyright © 2023 Ali Sher. All rights reserved.
//

import UIKit
import IBAnimatable
class ListenPlaylistController: MasterViewController {

    // IBOutlets
   // @IBOutlet weak var playlistTableView: UITableView!
    @IBOutlet weak var playlistCollectionView: UICollectionView!
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var searchView: AnimatableView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var donateNowView: UIView!
    @IBOutlet weak var donateNowHeightConstraint: NSLayoutConstraint!
    // Properties
    var playlistModel : ListeRevampedData?
    var audioDetailModel : ListenViewModal?
    var filteredPlaylist: [ListeRevampedPlaylistData?]?
    var isSearching: Bool = false
//    var heading : String? = ""
//    var type : String? = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        audioDetailModel = ListenViewModal()
        audioDetailModel?.parentController = self
        setupData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        audioDetailModel?.parentController = self
        playlistCollectionView.reloadData()
        setPremiumViews()
    }

    // MARK: - Helper functions
    func setPremiumViews(){
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            // do nothing
            DispatchQueue.main.async{
                self.donateNowView.isHidden = false
                    self.donateNowHeightConstraint.constant = 50
            }
        }else{
           
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip && isAutoRenewable{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                }
            }else if isVip{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                }
            }
            else{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = false
                        self.donateNowHeightConstraint.constant = 50
                }
            }
        }
    }
    func setupData() {
        headingLabel.text = playlistModel?.category_title
        filteredPlaylist = playlistModel?.playlist
        registerCollectionViewCells()
    }
    func playlistDetailFetched() {
        if audioDetailModel?.playlistDetail?.audioData?.count ?? 0 > 0 {
            let audioData = audioDetailModel?.playlistDetail?.audioData
            if audioData?.count ?? 0 == 1 {
                // play audio
                audioDetailModel?.setUpPlayerFor(selectedAudioType: .Nasheet, selectedIndex: 0, categoryType: self.playlistModel?.category_type ?? "")
                playlistCollectionView.reloadData()
            } else {
                // show audio listing page
               // audioDetailModel?.playlistDetail = nil
                self.show(storyBoard: .ListenNew, controllerIdentifier: "ArtistDetailController", navigation: self.navigationController){
                    controller in
                    (controller as! ArtistDetailController).audioDetailViewModel = self.audioDetailModel
                    (controller as! ArtistDetailController).category_type = self.playlistModel?.category_type ?? ""
                    
                }
            }
        }
    }
//    func registerTableViewCells(){
//        playlistTableView.register(UINib(nibName: "ListenPlaylistTableCell", bundle: nil), forCellReuseIdentifier: "ListenPlaylistTableCell")
//        playlistTableView.dataSource = self
//        playlistTableView.delegate = self
//        playlistTableView.reloadData()
//    }
    func registerCollectionViewCells(){
        playlistCollectionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        playlistCollectionView.dataSource = self
        playlistCollectionView.delegate = self
        playlistCollectionView.reloadData()
    }
    
   // MARK: - IBActions
    @IBAction func unlockPremiumBtnTapped(_ sender: Any) {
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.delegate = self
            tabCon.add(subsriptionController)
        }
    }
    @IBAction func searchValueChanged(_ sender: Any) {
        if (sender as? UITextField)?.text == ""{
            isSearching = false
            filteredPlaylist = playlistModel?.playlist
        }else{
            isSearching = true
            filteredPlaylist = playlistModel?.playlist?.filter{$0?.playlist_title?.lowercased().contains((sender as? UITextField)?.text?.lowercased() ?? "") == true}
            if filteredPlaylist?.count ?? 0 == 0 {
                let completePlaylist = playlistModel?.playlist
                if completePlaylist?.count ?? 0 < 9 {
                    filteredPlaylist = completePlaylist
                } else {
                    // show top 8 content
                    filteredPlaylist = []
                    for indexItem in 0...7 {
                        filteredPlaylist?.append(completePlaylist![indexItem])
                    }
                }
            }
        }
        playlistCollectionView.reloadData()
    }
    @IBAction func searchBtnTapped(_ sender: Any) {
        if searchBtn.tag == 0 {
            searchBtn.tag = 1
            searchView.isHidden = false
        } else {
            searchBtn.tag = 0
            isSearching = false
            searchTextField.text = ""
            searchView.isHidden = true
            filteredPlaylist = playlistModel?.playlist
            playlistCollectionView.reloadData()
        }
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//extension ListenPlaylistController : UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return filteredPlaylist?.count ?? 0
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "ListenPlaylistTableCell", for: indexPath) as! ListenPlaylistTableCell
//        let playlistData = filteredPlaylist?[indexPath.row]
//        let url_str = playlistData?.playlist_image ?? ""
//        let url = URL(string: url_str)
//        cell.iconView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
//        cell.nameLabel.text = playlistData?.playlist_title
//        cell.typeLabel.text = playlistModel?.category_type
//        cell.arrowIcon.isHidden = true
//        if playlistData?.is_playlist == "1" {
//            cell.arrowIcon.isHidden = false
//        }
//        cell.containerView.backgroundColor = UIColor.clear
//        if let audioPlayerController = tabController?.childViewController as? PlayerController {
//            if audioPlayerController.isAudioPlaying{
//                if audioPlayerController.selectedAudio?.playlistId ?? 0 == playlistData?.playlist_id ?? -1 {
//                    cell.containerView.backgroundColor = UIColor.white
//                }
//            }
//        }
//       
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 78
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let playlistData = filteredPlaylist?[indexPath.row]
//        var categoryId : Int?
////        if playlistModel?.category_type?.lowercased() == "artist" {
////            categoryId = playlistModel?.category_id
////        }
//        audioDetailModel?.getPlaylistDetailData(playlist_id: playlistData?.playlist_id ?? -1, category_type: playlistModel?.category_type ?? "" , category_id: categoryId)
//       // audioDetailModel?.getPlaylistDetailData(playlist_id: 9, category_type: "CATEGORY" , category_id: 7)
//    }
//}

//MARK: - tap handling in segments containers

extension ListenPlaylistController: audioPlayerDelegate {
    func didAudioChanged(playingIndex: Int, isPlaying: Bool) {
    }
    func audioPlayerClosed() {
        isCollapsedShown = false
        self.setTabBarCorner()
        playlistCollectionView.reloadData()
//        DispatchQueue.main.async {
//            self.playlistTableView.reloadData()
//        }
       
    }
    func audioDidFinished() {
//        DispatchQueue.main.async {
//            self.playlistTableView.reloadData()
//        }
    }
    func audioFavouriteStatusChanged(isFavourite:Bool ,selectedType: FavouriteType , audioId: Int) {
        audioDetailModel?.setFavourite(isFavourite: isFavourite, selectedType: selectedType, audioId: audioId)
        audioDetailModel?.getFavouritesData()
    }
}
//MARK: - Type CollectionView Methods
extension ListenPlaylistController :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredPlaylist?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
        
        let playlistData = filteredPlaylist?[indexPath.row]
        let url_str = playlistData?.playlist_image ?? ""
        let url = URL(string: url_str)
        cell.nameLbl.text = playlistData?.playlist_title
        cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
        cell.contantView.clipsToBounds = true
       // cell.lockImageView.isHidden = true
        cell.totalPlaysLabel.text = "\(playlistData?.playlist_subtext ?? "") - \(playlistData?.no_of_plays ?? "")"
        cell.lockImageView.image = UIImage(named: "greenLockIcon")
        if playlistData?.disable == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
            cell.lockImageView.image = UIImage(named: "whitePlayIcon")
          //  greenLockIcon
            cell.lockImageView.isHidden = false
        }
//        if playlistData?.disable == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true {
//            cell.lockImageView.image = UIImage(named: "whitePlayIcon")
//            cell.lockImageView.image = UIImage(named: "whitePlayIcon")
//            if let audioPlayerController = tabController?.childViewController as? PlayerController {
//                if audioPlayerController.isAudioPlaying{
//                    if audioPlayerController.selectedAudio?.playlistId ?? 0 == playlistData?.playlist_id ?? -1 {
//                        cell.lockImageView.image = UIImage(named: "whitePauseIcon")
//                    }
//                }
//            }
//        } else {
//            cell.lockImageView.image = UIImage(named: "whitePlayIcon")
//            cell.lockImageView.image = UIImage(named: "whitePlayIcon")
//            if let audioPlayerController = tabController?.childViewController as? PlayerController {
//                if audioPlayerController.isAudioPlaying{
//                    if audioPlayerController.selectedAudio?.playlistId ?? 0 == playlistData?.playlist_id ?? -1 {
//                        cell.lockImageView.image = UIImage(named: "whitePauseIcon")
//                    }
//                }
//            }
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: (self.view.frame.size.width - 52) / 3, height: ((self.view.frame.size.width - 52) / 3) + 55)
        } else {
            return  CGSize(width: (self.view.frame.size.width - 42) / 2, height: ((self.view.frame.size.width - 42) / 2) + 55)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let playlistData = filteredPlaylist?[indexPath.row]
        var categoryId : Int?
        audioDetailModel?.getPlaylistDetailData(playlist_id: playlistData?.playlist_id ?? -1, category_type: playlistModel?.category_type ?? "" , category_id: categoryId)
    }
}
extension ListenPlaylistController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
        
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.setPremiumViews()
            }
        })
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
            tabCon.add(subsriptionController)
        }
    }
    func subscriptionDidCancel(endDate: String){
        DispatchQueue.main.async {
            self.setPremiumViews()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let tabCon = self.tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
                subsriptionController.endDate = endDate
                tabCon.add(subsriptionController)
            }
        }
        
    }
}
//let audioPlayerController = tabController?.childViewController as! PlayerController
//if audioPlayerController.isAudioPlaying{
//    audioPlayerController.changeAudioStatus()
//}
