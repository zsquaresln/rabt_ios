//
//  AllArtistsModal.swift
//  Rabt
//
//  Created by Asad Khan on 04/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class AllArtistsModal: NetworkCommonResponseDataModel {
    
    let data: AllArtistData?
    enum CodingKeys: String, CodingKey {
        case data
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data    = try values.decodeIfPresent(AllArtistData.self, forKey: .data)
        try super.init(from: decoder)
    }
}
// MARK: - DataClass
struct AllArtistData: Codable {
    var data: [ArtistModal]?
    var current_page : Int?
    var first_page_url : String?
    var from : Int?
    var last_page : Int?
    var last_page_url : String?
    var next_page_url  : String?
    var path : String?
    var per_page : Int?
    var prev_page_url : String?
    var to : Int?
    var total : Int?
    enum CodingKeys: String, CodingKey {
        case data
        case current_page
        case first_page_url
        case from
        case last_page
        case last_page_url
        case next_page_url
        case path
        case per_page
        case prev_page_url
        case to
        case total
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data    = try values.decodeIfPresent([ArtistModal].self, forKey: .data)
        current_page    = try values.decodeIfPresent(Int.self, forKey: .current_page)
        first_page_url    = try values.decodeIfPresent(String.self, forKey: .first_page_url)
        from    = try values.decodeIfPresent(Int.self, forKey: .from)
        last_page    = try values.decodeIfPresent(Int.self, forKey: .last_page)
        last_page_url    = try values.decodeIfPresent(String.self, forKey: .last_page_url)
        next_page_url    = try values.decodeIfPresent(String.self, forKey: .next_page_url)
        path    = try values.decodeIfPresent(String.self, forKey: .path)
        per_page    = try values.decodeIfPresent(Int.self, forKey: .per_page)
        prev_page_url    = try values.decodeIfPresent(String.self, forKey: .prev_page_url)
        to    = try values.decodeIfPresent(Int.self, forKey: .to)
        total    = try values.decodeIfPresent(Int.self, forKey: .total)
        
    }
}
