//
//  ListenRevampedModal.swift
//  Rabt
//
//  Created by Tayyab Faran on 10/09/2023.
//  Copyright © 2023 Ali Sher. All rights reserved.
//


import UIKit

class ListenRevampedModal: NetworkCommonResponseDataModel {
  
    let data: [ListeRevampedData?]?
    enum CodingKeys: String, CodingKey {
        case data
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data    = try values.decodeIfPresent([ListeRevampedData].self, forKey: .data)
        
        try super.init(from: decoder)
    }
}
struct ListeRevampedData: Codable {
    var category_id: Int?
    var category_title, category_image, category_type,no_of_plays : String?
       var playlist: [ListeRevampedPlaylistData?]?

       enum CodingKeys: String, CodingKey {
           case category_id = "category_id"
           case category_title = "category_title"
           case category_image = "category_image"
           case category_type = "category_type"
           case playlist = "playlist"
           case no_of_plays = "no_of_plays"
       }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        category_id = try values.decodeIfPresent(Int.self, forKey: .category_id)
        category_title = try values.decodeIfPresent(String.self, forKey: .category_title)
        category_image = try values.decodeIfPresent(String.self, forKey: .category_image)
        category_type = try values.decodeIfPresent(String.self, forKey: .category_type)
        playlist = try values.decodeIfPresent([ListeRevampedPlaylistData].self, forKey: .playlist)
        no_of_plays = try values.decodeIfPresent(String.self, forKey: .no_of_plays)
        
   }
}

struct ListeRevampedPlaylistData: Codable {
    var playlist_id, disable: Int?
    var playlist_title, playlist_image, is_playlist,category_type, playlist_subtext, no_of_plays : String?

       enum CodingKeys: String, CodingKey {
           case playlist_id = "playlist_id"
           case playlist_title = "playlist_title"
           case playlist_subtext = "playlist_subtext"
           case playlist_image = "playlist_image"
           case is_playlist = "is_playlist"
           case category_type = "category_type"
           case no_of_plays = "no_of_plays"
           case disable = "disable"
       }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        playlist_id = try values.decodeIfPresent(Int.self, forKey: .playlist_id)
        playlist_title = try values.decodeIfPresent(String.self, forKey: .playlist_title)
        playlist_subtext = try values.decodeIfPresent(String.self, forKey: .playlist_subtext)
        playlist_image = try values.decodeIfPresent(String.self, forKey: .playlist_image)
        is_playlist = try values.decodeIfPresent(String.self, forKey: .is_playlist)
        category_type = try values.decodeIfPresent(String.self, forKey: .category_type)
        no_of_plays = try values.decodeIfPresent(String.self, forKey: .no_of_plays)
        disable = try values.decodeIfPresent(Int.self, forKey: .disable)
        
   }
}
