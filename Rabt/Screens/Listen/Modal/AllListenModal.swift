//
//  AllListenModal.swift
//  Rabt
//
//  Created by Admin on 12/9/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class AllListenModal: NetworkCommonResponseDataModel {
  
    let data: ListenModal?
    enum CodingKeys: String, CodingKey {
        case data
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data    = try values.decodeIfPresent(ListenModal.self, forKey: .data)
        
        try super.init(from: decoder)
    }
}
struct ListenModal: Codable {
    var listenAll: ListenAll?
       var listenRecommended: ListenRecommended?
       var listenNasheet: [NasheedModal?]?

       enum CodingKeys: String, CodingKey {
           case listenAll = "all"
           case listenRecommended = "Recommended"
           case listenNasheet = "Nasheet"
       }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        listenAll = try values.decodeIfPresent(ListenAll.self, forKey: .listenAll)
        listenRecommended = try values.decodeIfPresent(ListenRecommended.self, forKey: .listenRecommended)
        listenNasheet = try values.decodeIfPresent([NasheedModal].self, forKey: .listenNasheet)
   }
}
struct ListenAll: Codable {
    let artists: [ArtistModal?]?
    var latestAudioBooks: [AudioBookModal?]?
    let playlist:  [PlaylistData?]?

        enum CodingKeys: String, CodingKey {
            case artists = "Artist"
            case latestAudioBooks = "Latest Audio Book"
            case playlist = "Playlist"
        }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        artists = try values.decodeIfPresent([ArtistModal].self, forKey: .artists)
        latestAudioBooks = try values.decodeIfPresent([AudioBookModal].self, forKey: .latestAudioBooks)
        playlist = try values.decodeIfPresent([PlaylistData].self, forKey: .playlist)
   }
}
struct ListenRecommended: Codable {
    let artists: [ArtistModal?]?
    var latestAudioBooks: [AudioBookModal?]?

    enum CodingKeys: String, CodingKey {
        case artists = "Artist"
        case latestAudioBooks = "Latest Audio Book"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        artists = try values.decodeIfPresent([ArtistModal].self, forKey: .artists)
        latestAudioBooks = try values.decodeIfPresent([AudioBookModal].self, forKey: .latestAudioBooks)
   }
}

struct PlaylistData: Codable {
    let playlist_id, category_id, disable: Int?
    var playlist_title, playlist_image, category_type , no_of_plays, playlist_subtext: String?

    enum CodingKeys: String, CodingKey {
        case playlist_id = "playlist_id"
        case playlist_title = "playlist_title"
        case playlist_image = "playlist_image"
        case category_id = "category_id"
        case category_type = "category_type"
        case no_of_plays = "no_of_plays"
        case playlist_subtext = "playlist_subtext"
        case disable = "disable"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        playlist_id = try values.decodeIfPresent(Int.self, forKey: .playlist_id)
        playlist_title = try values.decodeIfPresent(String.self, forKey: .playlist_title)
        playlist_image = try values.decodeIfPresent(String.self, forKey: .playlist_image)
        category_id = try values.decodeIfPresent(Int.self, forKey: .category_id)
        category_type = try values.decodeIfPresent(String.self, forKey: .category_type)
        no_of_plays = try values.decodeIfPresent(String.self, forKey: .no_of_plays)
        playlist_subtext = try values.decodeIfPresent(String.self, forKey: .playlist_subtext)
        disable = try values.decodeIfPresent(Int.self, forKey: .disable)
   }
}

