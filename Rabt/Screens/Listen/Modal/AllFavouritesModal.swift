//
//  AllFavouritesModal.swift
//  Rabt
//
//  Created by Admin on 12/10/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit


class AllFavouritesModal: NetworkCommonResponseDataModel {
  
    let data: ListenAll?
    enum CodingKeys: String, CodingKey {
        case data
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data    = try values.decodeIfPresent(ListenAll.self, forKey: .data)
        
        try super.init(from: decoder)
    }
}
