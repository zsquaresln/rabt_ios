//
//  AudioBookModal.swift
//  Rabt
//
//  Created by Admin on 12/8/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit


struct AudioBookModal: Codable {
    let id, disable: Int?
    let name, arabicName, des, author, category_type: String?
    let image, audio, duration, no_of_plays, playlist_subtext: String?
    var favourites: Int?

    enum CodingKeys: String, CodingKey {
        case id, name, disable
        case arabicName = "arabic_name"
        case des, author, image, audio, duration, favourites, category_type, no_of_plays, playlist_subtext
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        disable = try values.decodeIfPresent(Int.self, forKey: .disable)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        category_type = try values.decodeIfPresent(String.self, forKey: .category_type)
        arabicName = try values.decodeIfPresent(String.self, forKey: .arabicName)
        des = try values.decodeIfPresent(String.self, forKey: .des)
        author = try values.decodeIfPresent(String.self, forKey: .author)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        audio = try values.decodeIfPresent(String.self, forKey: .audio)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        no_of_plays = try values.decodeIfPresent(String.self, forKey: .duration)
        playlist_subtext = try values.decodeIfPresent(String.self, forKey: .duration)
        do{
            favourites = try values.decodeIfPresent(Int.self, forKey: .favourites)
        }catch{
            favourites =  try Int(values.decodeIfPresent(String.self, forKey: .favourites)!)
        }
        
   }
}
