//
//  NasheedModal.swift
//  Rabt
//
//  Created by Admin on 12/8/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

struct NasheedModal: Codable {
    let id, artistID: Int?
    let name, arabicName, image, audio: String?
    let duration, createdAt, updatedAt: String?
    var favourites: String?

    enum CodingKeys: String, CodingKey {
        case id
        case artistID = "artist_id"
        case name
        case arabicName = "arabic_name"
        case image, audio, duration
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case favourites
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        artistID = try values.decodeIfPresent(Int.self, forKey: .artistID)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        arabicName = try values.decodeIfPresent(String.self, forKey: .arabicName)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        audio = try values.decodeIfPresent(String.self, forKey: .audio)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
        favourites = try values.decodeIfPresent(String.self, forKey: .favourites)
   }
}
