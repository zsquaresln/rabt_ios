//
//  ArtistModal.swift
//  Rabt
//
//  Created by Admin on 12/8/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit


struct ArtistModal: Codable {
    var id: Int?
    var name, des, image, category_type, no_of_plays, playlist_subtext: String?
    var approved, disable: Int?
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        des = try values.decodeIfPresent(String.self, forKey: .des)
        category_type = try values.decodeIfPresent(String.self, forKey: .category_type)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        approved = try values.decodeIfPresent(Int.self, forKey: .approved)
        disable = try values.decodeIfPresent(Int.self, forKey: .disable)
        no_of_plays = try values.decodeIfPresent(String.self, forKey: .no_of_plays)
        playlist_subtext = try values.decodeIfPresent(String.self, forKey: .playlist_subtext)
   }
    init(from : [String:Any]){
        self.id = from["id"] as? Int ?? Int(from["id"] as? String ?? "-1")
        self.name = from["name"] as? String
        self.des = from["des"] as? String
        self.category_type = from["category_type"] as? String
        self.image = from["image"] as? String
        self.approved = from["approved"] as? Int ?? Int(from["approved"] as? String ?? "-1")
        self.disable = from["disable"] as? Int ?? Int(from["disable"] as? String ?? "-1")
        self.no_of_plays = from["image"] as? String
        self.playlist_subtext = from["image"] as? String
    }
}
