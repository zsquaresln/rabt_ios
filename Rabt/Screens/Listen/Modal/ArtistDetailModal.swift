//
//  ArtistDetailModal.swift
//  Rabt
//
//  Created by Asad Khan on 23/01/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class ArtistDetailModal: NetworkCommonResponseDataModel {
    let data: ArtistDetails?
    enum CodingKeys: String, CodingKey {
        case data
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data    = try values.decodeIfPresent(ArtistDetails.self, forKey: .data)
        
        try super.init(from: decoder)
    }
}
// MARK: - DataClass
struct ArtistDetails: Codable {
    let id: Int?
    let image, des, name : String?
    var favourites: String?
    var isContentDisabled : Int?
    var artistPlaylist: [ArtistPlaylist?]?
    enum CodingKeys: String, CodingKey {
        case id
        case image
        case des
        case name
        case favourites
        case isContentDisabled = "disable"
        case artistPlaylist = "details"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        des = try values.decodeIfPresent(String.self, forKey: .des)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        favourites = try values.decodeIfPresent(String.self, forKey: .favourites)
        isContentDisabled = try values.decodeIfPresent(Int.self, forKey: .isContentDisabled)
        artistPlaylist = try values.decodeIfPresent([ArtistPlaylist].self, forKey: .artistPlaylist)
   }
}

// MARK: - Detail
struct ArtistPlaylist: Codable {
    let id, artistID: Int?
    let audioTitle, arabicName, audio, duration: String?
    var favourites: String?

    enum CodingKeys: String, CodingKey {
        case id
        case artistID = "artist_id"
        case audioTitle = "audio_title"
        case arabicName = "arabic_name"
        case audio, duration, favourites
    }
   
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        artistID = try values.decodeIfPresent(Int.self, forKey: .artistID)
        audioTitle = try values.decodeIfPresent(String.self, forKey: .audioTitle)
        arabicName = try values.decodeIfPresent(String.self, forKey: .arabicName)
        audio = try values.decodeIfPresent(String.self, forKey: .audio)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        favourites = try values.decodeIfPresent(String.self, forKey: .favourites)
       
   }
}
