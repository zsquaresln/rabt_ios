//
//  PlaylistDetailModel.swift
//  Rabt
//
//  Created by Tayyab Faran on 11/09/2023.
//  Copyright © 2023 Ali Sher. All rights reserved.
//


import UIKit

class PlaylistDetailModel: NetworkCommonResponseDataModel {
  
    let data: PlaylistDetailData?
    enum CodingKeys: String, CodingKey {
        case data
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data    = try values.decodeIfPresent(PlaylistDetailData.self, forKey: .data)
        
        try super.init(from: decoder)
    }
}
struct PlaylistDetailData: Codable {
    var id, disable: Int?
    var playlist_name, playlist_image, no_of_plays, no_of_likes, artist_name, favourites : String?
       var audioData: [PlaylistAudioDetail?]?

       enum CodingKeys: String, CodingKey {
           case id = "id"
           case disable = "disable"
           case playlist_name = "playlist_name"
           case playlist_image = "playlist_image"
           case no_of_plays = "no_of_plays"
           case no_of_likes = "no_of_likes"
           case artist_name = "artist_name"
           case favourites = "favourites"
           case audioData = "AudioDetail"
       }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        disable = try values.decodeIfPresent(Int.self, forKey: .disable)
        playlist_name = try values.decodeIfPresent(String.self, forKey: .playlist_name)
        playlist_image = try values.decodeIfPresent(String.self, forKey: .playlist_image)
        no_of_plays = try values.decodeIfPresent(String.self, forKey: .no_of_plays)
        no_of_likes = try values.decodeIfPresent(String.self, forKey: .no_of_likes)
        artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
        favourites = try values.decodeIfPresent(String.self, forKey: .favourites)
        audioData = try values.decodeIfPresent([PlaylistAudioDetail].self, forKey: .audioData)
        
   }
}

struct PlaylistAudioDetail: Codable {
    var audio_id, playlist_id: Int?
    var audio_title, arabic_name, audio, duration, share_link : String?

       enum CodingKeys: String, CodingKey {
           case audio_id = "audio_id"
           case playlist_id = "playlist_id"
           case audio_title = "audio_title"
           case arabic_name = "arabic_name"
           case audio = "audio"
           case duration = "duration"
           case share_link = "share_link"
       }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        audio_id = try values.decodeIfPresent(Int.self, forKey: .audio_id)
        do {
            playlist_id = try values.decodeIfPresent(Int.self, forKey: .playlist_id)
        } catch {
            playlist_id = Int(try values.decodeIfPresent(String.self, forKey: .playlist_id)!)
        }
        
        audio_title = try values.decodeIfPresent(String.self, forKey: .audio_title)
        arabic_name = try values.decodeIfPresent(String.self, forKey: .arabic_name)
        audio = try values.decodeIfPresent(String.self, forKey: .audio)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        share_link = try values.decodeIfPresent(String.self, forKey: .share_link)
        
   }
}
