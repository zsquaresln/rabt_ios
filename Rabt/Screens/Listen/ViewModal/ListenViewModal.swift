//
//  ListenViewModal.swift
//  Rabt
//
//  Created by Admin on 12/2/21.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import StickyTabBarViewController
enum listenTypes : String{
    case artists = "Artists"
    case audioBooks = "Audio Books"
    case nasheed = "Nasheed"
}
class ListenViewModal: NSObject {
    var selectedType = "All"
    var parentController : MasterViewController?
    var childSegmentController : MasterViewController?
    var allListenModal : ListenModal?
    var allFavouriteModal : ListenAll?
    var listenRevampedModal : [ListeRevampedData?]?
    var playlistDetail : PlaylistDetailData?
}
//MARK: - Api services
extension ListenViewModal{
    func updateAudioCount(audioId: Int) -> Void {
        
        let apiManager = ApiManager()
        let url = ApiUrls.audioPlayCount
       // (self.parentController as? ListenContainerController)?.loadingView.showLoading()
        //self.showSpinner(onView: self.view)
        apiManager.WebService(url: url, parameter: ["audio_id": audioId], method: .post, encoding: .queryString, {
            (dataModel: AllListenModal?) in
           // (self.parentController as? ListenContainerController)?.loadingView.hideLoading()
           // self.allListenModal = dataModel?.data
           // self.reloadSegmentdata()
        }){ error in
            print(error.localizedDescription)
        }
    }
    func getListenData() -> Void {
        
        let apiManager = ApiManager()
        let url = ApiUrls.listenServicesApi
        (self.parentController as? ListenContainerController)?.loadingView.showLoading()
        //self.showSpinner(onView: self.view)
        apiManager.WebService(url: url, parameter: nil, method: .get, encoding: .queryString, {
            (dataModel: AllListenModal?) in
            (self.parentController as? ListenContainerController)?.loadingView.hideLoading()
            self.allListenModal = dataModel?.data
            self.reloadSegmentdata()
        }){ error in
            print(error.localizedDescription)
        }
    }
    func getFavouritesData() -> Void{
        let apiManager = ApiManager()
        let url = ApiUrls.listenFavouriteListApi
        (self.parentController as? ListenContainerController)?.loadingView.showLoading()
        //self.showSpinner(onView: self.view)
        apiManager.WebService(url: url, parameter: nil, method: .get, encoding: .queryString, {
            (dataModel: AllFavouritesModal?) in
            (self.parentController as? ListenContainerController)?.loadingView.hideLoading()
            self.allFavouriteModal = dataModel?.data
           // self.reloadSegmentdata()
            (self.parentController as? ListenRevampedController)?.reloadFavoriteSegmentData()
        }){ error in
            print(error.localizedDescription)
        }
    }
    
    func getListenRevampedData() {
        let apiManager = ApiManager()
        let url = ApiUrls.listenRevampedServicesApi
       // (self.parentController as? ListenContainerController)?.loadingView.showLoading()
        //self.showSpinner(onView: self.view)
        apiManager.WebService(url: url, parameter: nil, method: .get, encoding: .queryString, {
            (dataModel: ListenRevampedModal?) in
            //(self.parentController as? ListenContainerController)?.loadingView.hideLoading()
            self.listenRevampedModal = dataModel?.data
            (self.parentController as? ListenRevampedController)?.loadTabs()
        }){ error in
            print(error.localizedDescription)
        }
    }
    
    func getPlaylistDetailData(playlist_id: Int, category_type:String ,category_id:Int?) {
        let apiManager = ApiManager()
        let url = ApiUrls.playlistDetailServicesApi
       // (self.parentController as? ListenContainerController)?.loadingView.showLoading()
        //self.showSpinner(onView: self.view)
        var params = ["playlist_id":playlist_id, "category_type":category_type] as [String : Any]
        if category_id != nil {
             params["category_id"] = category_id
        }
        apiManager.WebService(url: url, parameter: params, method: .get, encoding: .queryString, {
            (dataModel: PlaylistDetailModel?) in
            //(self.parentController as? ListenContainerController)?.loadingView.hideLoading()
            self.playlistDetail = dataModel?.data
            (self.parentController as? ListenPlaylistController)?.playlistDetailFetched()
            (self.parentController as? ListenRevampedController)?.playlistDetailFetched()
            (self.parentController as? HomeViewController)?.playlistDetailFetched()
            (self.parentController as? ArtistDetailController)?.showPlaylistData()
        }){ error in
            print(error.localizedDescription)
        }
    }
}
//MARK: - Navigation Hanlding
extension ListenViewModal{
    func reloadSegmentdata(){
        (self.parentController as? ListenContainerController)?.loadingView.hideLoading()
        if selectedType == "All"{
            (self.childSegmentController as? ListenAllController)?.listenViewModal = self
            (self.childSegmentController as? ListenAllController)?.reloadSegmentData()
        }else if selectedType == "My Favourite"{
            (self.childSegmentController as? ListenFavouritesController)?.listenViewModal = self
            (self.childSegmentController as? ListenFavouritesController)?.reloadSegmentData()
            (self.childSegmentController as? ListenFavouritesController)?.loadAds()
            if self.allFavouriteModal?.artists?.count ?? 0 == 0 && self.allFavouriteModal?.latestAudioBooks?.count ?? 0 == 0{
                (self.parentController as? ListenContainerController)?.loadingView.showLoadingWithIconAnd(message: "No Favourites Yet!\nBuild your own Favourite list by clicking Heart button next to each item.")
            }
        }
        else if selectedType == "Recommended"{
            (self.childSegmentController as? ListenRecommendedController)?.listenViewModal = self
            (self.childSegmentController as? ListenRecommendedController)?.reloadSegmentData()
            (self.childSegmentController as? ListenRecommendedController)?.loadAds()
        }
        else if selectedType == "Audio Books"{
            (self.childSegmentController as? ListenAudioBooksController)?.listenViewModal = self
            (self.childSegmentController as? ListenAudioBooksController)?.reloadSegmentData()
            (self.childSegmentController as? ListenAudioBooksController)?.loadAds()
        }
        else{
            (self.childSegmentController as? ListenNasheedController)?.listenViewModal = self
            (self.childSegmentController as? ListenNasheedController)?.reloadSegmentData()
        }
    }
    //This will show selected segments
    func loadSelectedSegment(from arrayOfSegments: [UIView]){
        (self.parentController as? ListenContainerController)?.loadingView.hideLoading()
        hideAllSegments(arrayOfSegments: arrayOfSegments)
        if selectedType == "All" {
          let listenAllContainer = arrayOfSegments[0]
            listenAllContainer.isHidden = false
            self.childSegmentController = (self.parentController as? ListenContainerController)?.listenAllController
            if allListenModal == nil{
                self.getListenData()
            }
            self.reloadSegmentdata()
        }else if selectedType == "My Favourite"{
            let listenFavContainer = arrayOfSegments[1]
            listenFavContainer.isHidden = false
            self.childSegmentController = (self.parentController as? ListenContainerController)?.listenFavouriteController
            if allFavouriteModal ==  nil || isFavouriteNeedsRefresh == true{
                isFavouriteNeedsRefresh = false
                self.getFavouritesData()
            }else{
                
                if self.allFavouriteModal?.artists?.count ?? 0 == 0 && self.allFavouriteModal?.latestAudioBooks?.count ?? 0 == 0{
                    (self.parentController as? ListenContainerController)?.loadingView.showLoadingWithIconAnd(message: "No Favourites Yet!\nBuild your own Favourite list by clicking Heart button next to each item.")
                }
                self.reloadSegmentdata()
            }
        }
        else if selectedType == "Recommended"{
            let listenRecommendedContainer = arrayOfSegments[2]
            listenRecommendedContainer.isHidden = false
            self.childSegmentController = (self.parentController as? ListenContainerController)?.listenRecommendedController
            
            self.reloadSegmentdata()
        }
        else if selectedType == "Audio Books"{
            let listenAudioBooksContainer = arrayOfSegments[3]
            listenAudioBooksContainer.isHidden = false
            self.childSegmentController = (self.parentController as? ListenContainerController)?.listenAudioBookController
            self.reloadSegmentdata()
        }
        else{
            let listenNasheedContainer = arrayOfSegments[4]
            listenNasheedContainer.isHidden = false
            self.childSegmentController = (self.parentController as? ListenContainerController)?.listenNasheedController
            self.reloadSegmentdata()
        }
    }
    
    private func hideAllSegments(arrayOfSegments: [UIView]){
        for containerSegmentView in arrayOfSegments{
            containerSegmentView.isHidden = true
        }
    }
}
//MARK: - Helper Functions
extension ListenViewModal{
    func setFavourite(isFavourite:Bool ,selectedType: FavouriteType , audioId: Int){
        // to avoid api loading, find and replace favourite property against selected type in all sections
        if selectedType == .Book{
            // check in all section
            let allAudioBookIndex = self.allListenModal?.listenAll?.latestAudioBooks?.firstIndex{$0?.id == audioId}
            if allAudioBookIndex == nil{
                return
            }
            var allFoundBook = self.allListenModal?.listenAll?.latestAudioBooks?[allAudioBookIndex!]
            if isFavourite{
                allFoundBook?.favourites = 1
            }
            else{
                allFoundBook?.favourites = 0
            }
            self.allListenModal?.listenAll?.latestAudioBooks?[allAudioBookIndex!] = allFoundBook
            // check in recommended section
            let allRecomendedBookIndex = self.allListenModal?.listenRecommended?.latestAudioBooks?.firstIndex{$0?.id == audioId}
            if allRecomendedBookIndex == nil{
                return
            }
            var recomendedFoundBook = self.allListenModal?.listenRecommended?.latestAudioBooks?[allRecomendedBookIndex!]
            if isFavourite{
                recomendedFoundBook?.favourites = 1
            }
            else{
                recomendedFoundBook?.favourites = 0
            }
            self.allListenModal?.listenRecommended?.latestAudioBooks?[allRecomendedBookIndex!] = recomendedFoundBook
            
            
        }else if selectedType == .Nasheet{
            let allNasheedIndex = self.allListenModal?.listenNasheet?.firstIndex{$0?.id == audioId}
            var foundNasheed = self.allListenModal?.listenNasheet?[allNasheedIndex!]
            if isFavourite{
                foundNasheed?.favourites = "1"
            }
            else{
                foundNasheed?.favourites = "0"
            }
            self.allListenModal?.listenNasheet?[allNasheedIndex!] = foundNasheed
        }
        self.reloadSegmentdata()
    }
}
//MARK: - Handle AudioBooks & Nasheed playing
extension ListenViewModal{
    func setUpPlayerFor(selectedAudioType : FavouriteType , selectedIndex: Int, categoryType:String = ""){
        var selectedAudio : PlayerModal?
        if selectedAudioType == .Book{
//            if selectedType == "All"{
//                selectedAudio = PlayerModal.init(selectedAudioBook: (self.allListenModal?.listenAll?.latestAudioBooks?[selectedIndex])!)
//            }else if selectedType == "My Favourite"{
//                selectedAudio = PlayerModal.init(selectedAudioBook: (self.allFavouriteModal?.latestAudioBooks![selectedIndex])!)
//            }else if selectedType == "Recommended"{
//                selectedAudio = PlayerModal.init(selectedAudioBook: (self.allListenModal?.listenRecommended?.latestAudioBooks?[selectedIndex])!)
//            }else{
//                //audio books section
//                selectedAudio = PlayerModal.init(selectedAudioBook: (self.allListenModal?.listenAll?.latestAudioBooks?[selectedIndex])!)
//            }
        }else{
            
            //\selectedAudio = PlayerModal.init(selectedNasheed: (playlistDetail?.audioData?[selectedIndex])!)
            selectedAudio = PlayerModal.init(selectedAudio: (playlistDetail?.audioData?[selectedIndex])!, playlistImage: playlistDetail?.playlist_image ?? "", artistName: playlistDetail?.artist_name ?? "", artistId: playlistDetail?.id ?? 0, favourite: playlistDetail?.favourites, category: categoryType)
        }
        self.updateAudioCount(audioId: self.playlistDetail?.audioData?[selectedIndex]?.audio_id ?? -1)
        let storyboard = UIStoryboard.init(name: "Player", bundle: nil)
        var audioPlayerController = storyboard.instantiateViewController(withIdentifier: "PlayerController") as! PlayerController

        if isCollapsedShown{
            audioPlayerController = tabController?.childViewController as! PlayerController
            if (audioPlayerController.playerViewModal?.selectedAudio?.id == selectedAudio?.id) && (audioPlayerController.selectedType == selectedAudioType) && (audioPlayerController.selectedSegment == .All.getTypeFrom(stringValue: selectedType)){
                return
            }
         }
        audioPlayerController.selectedSegment = .All.getTypeFrom(stringValue: selectedType)
        audioPlayerController.selectedType = selectedAudioType
       // audioPlayerController.navController = ((self.parentController as? ListenContainerController)?.navigationController!)!
        if let listenDelegate = (self.parentController as? ListenRevampedController) {
            audioPlayerController.delegate = listenDelegate
        } else if let playlistDelegate = (self.parentController as? ListenPlaylistController){
            audioPlayerController.delegate = playlistDelegate
        } else {
            audioPlayerController.delegate = (self.parentController as? HomeViewController)
        }
        
        audioPlayerController.playerViewModal = PlayerViewModal()
        audioPlayerController.playerViewModal?.selectedAudio = selectedAudio
        audioPlayerController.isAudioPlaying = false
        if !isCollapsedShown {
            isCollapsedShown = true
            let viewControllerToStick = audioPlayerController
           tabController?.configureCollapsableChild(viewControllerToStick, isFullScreenOnFirstAppearance: false)
            //tabController?.tabBar.isHidden = false
        }
        else{
            audioPlayerController.playNew()
        }
       // isPlaying = false
        (self.parentController as? ListenRevampedController)?.setTabBarCorner()
    }
}

//MARK: - Audio player handling
extension ListenViewModal{
    func setUpPlayerFor(selectedSegment : SegmentType , selectedIndex: Int ,isPlayAsPlaylist: Bool, categoryType: String = ""){
    var selectedAudio : PlayerModal?
    // must have atleast one audio
        if self.playlistDetail?.audioData == nil || self.playlistDetail?.audioData?.count == 0{
        return
    }
        self.updateAudioCount(audioId: self.playlistDetail?.audioData?[selectedIndex]?.audio_id ?? -1)
    selectedAudio = PlayerModal.init(selectedPlaylist: self.playlistDetail!, category: categoryType)
     let storyboard = UIStoryboard.init(name: "Player", bundle: nil)
    var audioPlayerController = storyboard.instantiateViewController(withIdentifier: "PlayerController") as! PlayerController

    if isCollapsedShown{
        if let child = tabController?.childViewController as? PlayerController {
            audioPlayerController = child
            if (audioPlayerController.playerViewModal?.selectedAudio?.id == selectedAudio?.id) && (audioPlayerController.selectedType == .Artist) && (audioPlayerController.currentAudioIndex == selectedIndex) && (audioPlayerController.selectedSegment == selectedSegment){
                // in case user goes back and came to same controller, this will update delegate to new controller
                audioPlayerController.delegate = (self.parentController as? ArtistDetailController)
                // here pause or play the already selected audio
                audioPlayerController.changeAudioStatus()
                return
            }
        }
        
     }
    audioPlayerController.currentAudioIndex = selectedIndex
    audioPlayerController.selectedType = .Artist
    audioPlayerController.selectedSegment = selectedSegment
   // audioPlayerController.navController = ((self.parentController as? ArtistDetailController)?.navigationController!)!
    audioPlayerController.delegate = (self.parentController as? ArtistDetailController)
    audioPlayerController.playerViewModal = PlayerViewModal()
    audioPlayerController.playerViewModal?.selectedAudio = selectedAudio
    // set it as false so player should knows to start playing the audio
    audioPlayerController.isAudioPlaying = false
    audioPlayerController.isPlayAsPlaylist = isPlayAsPlaylist
  //  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playNew"), object: nil, userInfo: dict)
    //isPlaying = false
    if !isCollapsedShown {
        isCollapsedShown = true
        let viewControllerToStick = audioPlayerController
        tabController?.configureCollapsableChild(viewControllerToStick, isFullScreenOnFirstAppearance: false)
        //tabController?.tabBar.isHidden = false
    }
    else{
        audioPlayerController.playNew()
    }
   
    (self.parentController as? ArtistDetailController)?.setTabBarCorner()
}
}
