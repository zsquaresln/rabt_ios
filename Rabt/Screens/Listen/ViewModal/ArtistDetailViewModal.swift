//
//  ArtistDetailViewModal.swift
//  Rabt
//
//  Created by Asad Khan on 23/01/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class ArtistDetailViewModal: MasterViewController {
    var parentController : MasterViewController?
    var selectedArtist : ArtistModal?
    var artistDetails : ArtistDetails?
 
}
//MARK: - Api Calling
extension ArtistDetailViewModal{
    func getArtistDetailData() -> Void{
        let apiManager = ApiManager()
        let url = ApiUrls.artistDetailsApi
        //self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: ["id":String(selectedArtist?.id ?? -1) ], method: .get, encoding: .queryString, {
            (dataModel: ArtistDetailModal?) in
            self.artistDetails = dataModel?.data
            (self.parentController as? ArtistDetailController)?.showPlaylistData()
        }){ error in
            print(error.localizedDescription)
        }
    }
}

//MARK: - Audio player handling
extension ArtistDetailViewModal{
    func setUpPlayerFor(selectedSegment : SegmentType , selectedIndex: Int ,isPlayAsPlaylist: Bool){
    var selectedAudio : PlayerModal?
    // must have atleast one audio
    if self.artistDetails == nil || self.artistDetails?.artistPlaylist?.count == 0{
        return
    }
    
    selectedAudio = PlayerModal.init(selectedArtist: self.artistDetails!)
     let storyboard = UIStoryboard.init(name: "Player", bundle: nil)
    var audioPlayerController = storyboard.instantiateViewController(withIdentifier: "PlayerController") as! PlayerController

    if isCollapsedShown{
        audioPlayerController = tabController?.childViewController as! PlayerController
        if (audioPlayerController.playerViewModal?.selectedAudio?.id == selectedAudio?.id) && (audioPlayerController.selectedType == .Artist) && (audioPlayerController.currentAudioIndex == selectedIndex) && (audioPlayerController.selectedSegment == selectedSegment){
            // in case user goes back and came to same controller, this will update delegate to new controller
            audioPlayerController.delegate = (self.parentController as? ArtistDetailController)
            // here pause or play the already selected audio
            audioPlayerController.changeAudioStatus()
            return
        }
     }
    audioPlayerController.currentAudioIndex = selectedIndex
    audioPlayerController.selectedType = .Artist
    audioPlayerController.selectedSegment = selectedSegment
   // audioPlayerController.navController = ((self.parentController as? ArtistDetailController)?.navigationController!)!
    audioPlayerController.delegate = (self.parentController as? ArtistDetailController)
    audioPlayerController.playerViewModal = PlayerViewModal()
    audioPlayerController.playerViewModal?.selectedAudio = selectedAudio
    // set it as false so player should knows to start playing the audio
    audioPlayerController.isAudioPlaying = false
    audioPlayerController.isPlayAsPlaylist = isPlayAsPlaylist
  //  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playNew"), object: nil, userInfo: dict)
    //isPlaying = false
    if !isCollapsedShown {
        isCollapsedShown = true
        let viewControllerToStick = audioPlayerController
        tabController?.configureCollapsableChild(viewControllerToStick, isFullScreenOnFirstAppearance: false)
        //tabController?.tabBar.isHidden = false
    }
    else{
        audioPlayerController.playNew()
    }
   
    (self.parentController as? ArtistDetailController)?.setTabBarCorner()
}
}
