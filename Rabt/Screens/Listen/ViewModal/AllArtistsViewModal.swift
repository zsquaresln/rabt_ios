//
//  AllArtistsViewModal.swift
//  Rabt
//
//  Created by Asad Khan on 04/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class AllArtistsViewModal: NSObject {
    
    var parentController : MasterViewController?
    var allArtists: AllArtistData?
    var currentPage = 1
    var totalPages = 2
}
//MARK: - Audio player handling
extension AllArtistsViewModal{
    func getArtistsWith(pageNo: String){
        
        let apiManager = ApiManager()
        let url = ApiUrls.allArtistApi
        //self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: ["page": pageNo], method: .get, encoding: .queryString, {
            (dataModel: AllArtistsModal?) in
            
            if self.allArtists == nil{
                self.allArtists = dataModel?.data
            }
            else{
                if dataModel?.data?.data?.count ?? 0 > 0{
                    self.allArtists?.data?.append(contentsOf: dataModel!.data!.data!)
                }
            }
            self.totalPages = self.allArtists?.last_page ?? 1
            (self.parentController as? AllArtistViewController)?.showData()
        }){ error in
            print(error.localizedDescription)
        }
    }
}

