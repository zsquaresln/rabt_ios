//
//  AllBooksViewModal.swift
//  Rabt
//
//  Created by Asad Khan on 04/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class AllBooksViewModal: NSObject {
    var parentController : MasterViewController?
    var latestAudioBooks: [AudioBookModal?]?
}
//MARK: - Audio player handling
extension AllBooksViewModal{
    func setUpPlayerFor(selectedSegment : SegmentType , selectedIndex: Int,selectedAudioBook:AudioBookModal){
    var selectedAudio : PlayerModal?
    // must have atleast one audio
    if self.latestAudioBooks == nil || self.latestAudioBooks?.count == 0{
        return
    }
        selectedAudio = PlayerModal.init(selectedAudioBook: selectedAudioBook)
     let storyboard = UIStoryboard.init(name: "Player", bundle: nil)
    var audioPlayerController = storyboard.instantiateViewController(withIdentifier: "PlayerController") as! PlayerController

    if isCollapsedShown{
        audioPlayerController = tabController?.childViewController as! PlayerController
        if (audioPlayerController.playerViewModal?.selectedAudio?.id == selectedAudio?.id) && (audioPlayerController.selectedType == .Book) && (audioPlayerController.currentAudioIndex == selectedIndex) && (audioPlayerController.selectedSegment == selectedSegment){
            // in case user goes back and came to same controller, this will update delegate to new controller
            audioPlayerController.delegate = (self.parentController as? AllBooksNewViewController)
            // here pause or play the already selected audio
            audioPlayerController.changeAudioStatus()
            return
        }
     }
    audioPlayerController.currentAudioIndex = selectedIndex
    audioPlayerController.selectedType = .Book
    audioPlayerController.selectedSegment = selectedSegment
  //  audioPlayerController.navController = ((self.parentController as? AllBooksNewViewController)?.navigationController!)!
    audioPlayerController.delegate = (self.parentController as? AllBooksNewViewController)
    audioPlayerController.playerViewModal = PlayerViewModal()
    audioPlayerController.playerViewModal?.selectedAudio = selectedAudio
    // set it as false so player should knows to start playing the audio
    audioPlayerController.isAudioPlaying = false
   
  //  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "playNew"), object: nil, userInfo: dict)
    //isPlaying = false
    if !isCollapsedShown {
        isCollapsedShown = true
        let viewControllerToStick = audioPlayerController
        tabController?.configureCollapsableChild(viewControllerToStick, isFullScreenOnFirstAppearance: false)
        //tabController?.tabBar.isHidden = false
    }
    else{
        audioPlayerController.playNew()
    }
   
    (self.parentController as? AllBooksNewViewController)?.setTabBarCorner()
}
}
