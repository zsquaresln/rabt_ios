//
//  NewProfileController.swift
//  Rabt
//
//  Created by Asad Khan on 21/08/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import IBAnimatable
class NewProfileController:  MasterViewController {
    
    @IBOutlet weak var profileBtn: AnimatableButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var profileInfoLabel: UILabel!
    @IBOutlet weak var contentEnabledHeadingLabel: UILabel!
    @IBOutlet weak var notificationSwitchBtn: UISwitch!
    @IBOutlet weak var contentEnablingLabel: UILabel!
    @IBOutlet weak var contentEnabledDescriptionLabel: UILabel!
    @IBOutlet weak var premiumIcon: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
       // populateData()
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("subscriptionCheck"), object: nil)
        populateData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "subscriptionCheck"), object: nil)
    }
    //MARK: - Helper Functions
    @objc func methodOfReceivedNotification(notification: Notification) {
        populateData()
    }
    func populateData(){
        if UserDefaults.standard.value(forKey: "pauseNotifications") as? Bool == true{
            notificationSwitchBtn.setOn(true, animated: false)
        }
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            premiumIcon.isHidden = true
            contentEnablingLabel.text = "Unlock Rabt Premium"
            profileInfoLabel.text = "Enter your profile information here and limit to three (3) lines maximum to make decent length."
        }else{
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
                if isVip && isAutoRenewable{
                    DispatchQueue.main.async{
                        self.premiumIcon.isHidden = false
                        self.contentEnabledHeadingLabel.isHidden = false
                        self.contentEnabledDescriptionLabel.isHidden = false
                      //  self.contentEnablingLabel.text = "Unsubscribe Premium"
                        self.contentEnablingLabel.text = "Premium"
                    }
                }else if isVip{
                    DispatchQueue.main.async{
                        self.premiumIcon.isHidden = false
                        self.contentEnabledHeadingLabel.isHidden = false
                        self.contentEnabledDescriptionLabel.isHidden = false
                      //  self.contentEnablingLabel.text = "Unlock Rabt Premium"
                        self.contentEnablingLabel.text = "Premium"
                    }
                }
                    else{
                    DispatchQueue.main.async{
                        self.premiumIcon.isHidden = true
                        self.contentEnabledHeadingLabel.isHidden = true
                        self.contentEnabledDescriptionLabel.isHidden = true
                        self.contentEnablingLabel.text = "Unlock Rabt Premium"
                    }
                }
            let bio = userInfo["bio"] as? String ?? ""
            if bio != "" {
                profileInfoLabel.text = bio
            }else{
                profileInfoLabel.text = "Enter your profile information here and limit to three (3) lines maximum to make decent length."
            }
        }
        nameLabel.text = userInfo["full_name"] as? String
        profileLabel.text = userInfo["user_name"] as? String ?? "----"
        
        let url_str = userInfo["picture"] as? String ?? ""
        let url = URL(string: url_str)
        profileBtn.sd_setBackgroundImage(with: url, for:
                                            UIControl.State.normal, placeholderImage: UIImage(named:
                                                                                                "PlaceHolder1.jpg"), options: SDWebImageOptions(rawValue: 0)) { (image,
                                                                                                                                                            error, cache, url) in
        }
    }
    //MARK: -IBActions
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func settingsBtnTapped(_ sender: Any) {
        let settingsViewController = self.storyboard!.instantiateViewController(withIdentifier: "NewSettingsController") as! NewSettingsController
        self.navigationController?.pushViewController(settingsViewController, animated: true)
    }
    @IBAction func notificationSwitchValueChanged(_ sender: Any) {
        if notificationSwitchBtn.isOn == false{
            UserDefaults.standard.setValue(false, forKey: "pauseNotifications")
            UserDefaults.standard.synchronize()
            PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
            NotificationsHandler().scheduleAllEnabledNotifications()
        }
        else{
            UserDefaults.standard.setValue(true, forKey: "pauseNotifications")
            UserDefaults.standard.synchronize()
            PrayerTimeManger.shared.shouldRefreshPrayerNotifications = false
            NotificationsHandler().pauseAllNotifications()
        }
    }
    @IBAction func lockUnlockContentBtnTapped(_ sender: Any) {
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            let alert = UIAlertController(title: "Alert", message: "Please login to Unlock Rabt.".localized(), preferredStyle: .alert)
            let ok = UIAlertAction(title: "Login".localized(), style: .default, handler: { action in
                DispatchQueue.main.async{
                    self.logoutGuest()
                }
            })
            let cancel = UIAlertAction(title: "Cancel".localized(), style: .default, handler: { action in
               
            })
            alert.addAction(cancel)
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }else{
            // show subscription screen
            if let tabCon = tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
                subsriptionController.delegate = self
                tabCon.add(subsriptionController)
            }
        }
//        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
//            let alert = UIAlertController(title: "Alert", message: "Please login to Unlock Rabt.".localized(), preferredStyle: .alert)
//            let ok = UIAlertAction(title: "Login".localized(), style: .default, handler: { action in
//                DispatchQueue.main.async{
//                    self.logoutGuest()
//                }
//            })
//            let cancel = UIAlertAction(title: "Cancel".localized(), style: .default, handler: { action in
//               
//            })
//            alert.addAction(cancel)
//            alert.addAction(ok)
//            DispatchQueue.main.async(execute: {
//                self.present(alert, animated: true)
//            })
//        }else{
//            // show subscription screen
//            if let tabCon = tabBarController?.parent{
//                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
//                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
//                subsriptionController.delegate = self
//                tabCon.add(subsriptionController)
//            }
//        }
    }
}
extension NewProfileController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
    
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.populateData()
            }
        })
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
            tabCon.add(subsriptionController)
        }
    }
    func subscriptionDidCancel(endDate: String){
        DispatchQueue.main.async {
            self.populateData()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let tabCon = self.tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
                subsriptionController.endDate = endDate
                tabCon.add(subsriptionController)
            }
        }
//        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
//            DispatchQueue.main.async {
//                self.populateData()
//            }
//        })
//        if let tabCon = tabBarController?.parent{
//            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
//            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
//            tabCon.add(subsriptionController)
//        }
    }
  //  func subscriptionDidCancel(endDate: String){
//        DispatchQueue.main.async {
//            self.populateData()
//        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            if let tabCon = self.tabBarController?.parent{
//                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
//                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
//                subsriptionController.endDate = endDate
//                tabCon.add(subsriptionController)
//            }
//        }
//        
  //  }
}
