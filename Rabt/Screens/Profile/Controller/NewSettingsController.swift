//
//  NewSettingsController.swift
//  Rabt
//
//  Created by Asad Khan on 21/08/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import IBAnimatable
import AVFoundation
import MediaPlayer
class NewSettingsController: UIViewController {
    
    @IBOutlet weak var profileBtn: AnimatableButton!
    @IBOutlet weak var nameLabel: UILabel!
  //  @IBOutlet weak var proflleLabel: UILabel!
    @IBOutlet weak var profileInfoLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var logoutBtn: AnimatableButton!
  //  @IBOutlet weak var profileEditIcon: AnimatableImageView!
    @IBOutlet weak var nameEditIcon: UIImageView!
    @IBOutlet weak var bioEditIcon: UIImageView!

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailEditIcon: UIImageView!
    
    @IBOutlet weak var notificationSwitchBtn: UISwitch!
    @IBOutlet weak var contentEnablingLabel: UILabel!
    @IBOutlet weak var premiumIcon: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        populateData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("subscriptionCheck"), object: nil)
        populateData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
       
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "subscriptionCheck"), object: nil)
    }
    //MARK: - Helper Functions
    @objc func methodOfReceivedNotification(notification: Notification) {
        populateData()
    }
    
    func populateData(){
        locationNameLabel.text = PrayerTimeManger.shared.selectedLocationName
        if UserDefaults.standard.value(forKey: "pauseNotifications") as? Bool == true{
            notificationSwitchBtn.setOn(true, animated: false)
        }

        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            profileInfoLabel.text = "Enter your profile information here and limit to three (3) lines maximum to make decent length."
          //  profileEditIcon.isHidden = true
            nameEditIcon.isHidden = true
            bioEditIcon.isHidden = true
            emailEditIcon.isHidden = true
            logoutBtn.setTitle("Login", for: .normal)
            premiumIcon.isHidden = true
            contentEnablingLabel.text = "Unlock Rabt Premium"
        }else{
            if  (userInfo["email"] as? String ?? "").trimmingCharacters(in: .whitespaces) == ""{
                emailEditIcon.isHidden = false
                emailLabel.text = "Add Email"
            }else{
                emailEditIcon.isHidden = true
                emailLabel.text = userInfo["email"] as? String
            }
            let bio = userInfo["bio"] as? String ?? ""
            if bio != "" {
                profileInfoLabel.text = bio
            }else{
                profileInfoLabel.text = "Enter your profile information here and limit to three (3) lines maximum to make decent length."
            }
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
                if isVip && isAutoRenewable{
                    DispatchQueue.main.async{
                        self.premiumIcon.isHidden = false
//                        self.contentEnabledHeadingLabel.isHidden = false
//                        self.contentEnabledDescriptionLabel.isHidden = false
                      //  self.contentEnablingLabel.text = "Unsubscribe Premium"
                        self.contentEnablingLabel.text = "Premium"
                    }
                }else if isVip{
                    DispatchQueue.main.async{
                        self.premiumIcon.isHidden = false
//                        self.contentEnabledHeadingLabel.isHidden = false
//                        self.contentEnabledDescriptionLabel.isHidden = false
                      //  self.contentEnablingLabel.text = "Unlock Rabt Premium"
                        self.contentEnablingLabel.text = "Premium"
                    }
                }
                    else{
                    DispatchQueue.main.async{
                        self.premiumIcon.isHidden = true
//                        self.contentEnabledHeadingLabel.isHidden = true
//                        self.contentEnabledDescriptionLabel.isHidden = true
                        self.contentEnablingLabel.text = "Unlock Rabt Premium"
                    }
                }
        }
        nameLabel.text = userInfo["full_name"] as? String
     //   proflleLabel.text = userInfo["user_name"] as? String ?? "----"
        
        let url_str = userInfo["picture"] as? String ?? ""
        let url = URL(string: url_str)
        profileBtn.sd_setBackgroundImage(with: url, for:
                                            UIControl.State.normal, placeholderImage: UIImage(named:
                                                                                                "PlaceHolder1.jpg"), options: SDWebImageOptions(rawValue: 0)) { (image,
                                                                                                                                                                error, cache, url) in
        }
    }
    func MoveToLogout() -> Void {

        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout".localized(), preferredStyle: .alert)
        let ok = UIAlertAction(title: "Yes".localized(), style: .default, handler: { action in
            DispatchQueue.main.async {
                self.clearDataAndLogout()
            }
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "No".localized(), style: .default, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    func clearDataAndLogout(){
        NotificationsHandler().removeAllNotificationsData()
        PrayerTimeManger.shared.shouldRefreshPrayerTimings = true
        PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
        PrayerTimeManger.shared.prayerTimeCoordinates = nil
        PrayerTimeManger.shared.prayerData = nil
        PrayerTimeManger.shared.selectedLocationName = nil
        UserDefaults.standard.removeObject(forKey: "access_token")
        UserDefaults.standard.synchronize()
        GooglePlacesManager().clearRecentPlacesCache()
        
        let dict = ["status": isPlaying ? "1" : "0", "artist": isSameArtist ? "1" : "0", "already": isAlreadyPlayAllClcked ? "1" : "0"]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "playOrPaused"), object: nil, userInfo: dict)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "closeListen"), object: nil, userInfo: nil)
        self.dismiss(animated: false, completion: nil)
        UserDefaults.standard.removeObject(forKey: "favourites")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.removeObject(forKey: "userinfo")
        UserDefaults.standard.synchronize()
        
        self.tabBarController?.tabBar.isHidden = true
       
        try? AVAudioSession.sharedInstance().setActive(false, options: .notifyOthersOnDeactivation) // Where self.session is AVAudioSession. You should do it with do-catch to good error catching.
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [:]
        let storyboard = AuthenticationStoryBoard
        let VC = storyboard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    //MARK: - IBActions
    @IBAction func backBtnTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func profileBtnTapped(_ sender: Any) {
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            return
        }else{
            let storyBoard = ProfileStoryBoard
            let VC = storyBoard.instantiateViewController(withIdentifier: "UpdatePictureViewController") as? UpdatePictureViewController
            VC?.delegate = self
            self.modalPresentationStyle = .overFullScreen
            self.present(VC!, animated: true)
        }
    }
    @IBAction func editNameBtnTapped(_ sender: Any) {
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            return
        }
        else{
            let storyBoard = ProfileStoryBoard
            let vc = storyBoard.instantiateViewController(withIdentifier: "UpdateNameViewController") as! UpdateNameViewController
            vc.delegate = self
            self.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
        }
    }

    @IBAction func editEmailBtnTapped(_ sender: Any) {
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            return
        }
        else{
           
            if emailEditIcon.isHidden == false{
                let storyBoard = ProfileStoryBoard
                let vc = storyBoard.instantiateViewController(withIdentifier: "UpdateNameViewController") as! UpdateNameViewController
                vc.delegate = self
                self.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: true)
            }
            
        }
    }
    @IBAction func profileInforBtnTapped(_ sender: Any) {
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            return
        }
        else{
            let storyBoard = ProfileStoryBoard
            let obj = storyBoard.instantiateViewController(withIdentifier: "AddBioViewController") as? AddBioViewController
            obj?.bioText = profileInfoLabel.text ?? ""
            obj?.delegate = self
            addChild(obj!)
            view.addSubview(obj!.view)
            obj!.didMove(toParent: self)
        }
    }
    @IBAction func contactUsBtnTapped(_ sender: Any) {
        let path = "https://www.rabt.co.uk"
        guard let url = URL(string:path) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    @IBAction func termsOfServiceBtnTapped(_ sender: Any) {
        let path = "https://rabt.co.uk/tos/"
        guard let url = URL(string:path) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    @IBAction func logoutBtnTapped(_ sender: Any) {
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            clearDataAndLogout()
        }
        else{
            MoveToLogout()
        }
    }
    @IBAction func notificationSwitchValueChanged(_ sender: Any) {
        if notificationSwitchBtn.isOn == false{
            UserDefaults.standard.setValue(false, forKey: "pauseNotifications")
            UserDefaults.standard.synchronize()
            PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
            NotificationsHandler().scheduleAllEnabledNotifications()
        }
        else{
            UserDefaults.standard.setValue(true, forKey: "pauseNotifications")
            UserDefaults.standard.synchronize()
            PrayerTimeManger.shared.shouldRefreshPrayerNotifications = false
            NotificationsHandler().pauseAllNotifications()
        }
    }

    @IBAction func lockUnlockContentBtnTapped(_ sender: Any) {
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            let alert = UIAlertController(title: "Alert", message: "Please login to Unlock Rabt.".localized(), preferredStyle: .alert)
            let ok = UIAlertAction(title: "Login".localized(), style: .default, handler: { action in
                DispatchQueue.main.async{
                    self.logoutGuest()
                }
            })
            let cancel = UIAlertAction(title: "Cancel".localized(), style: .default, handler: { action in
               
            })
            alert.addAction(cancel)
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }else{
            // show subscription screen
            if let tabCon = tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
                subsriptionController.delegate = self
                tabCon.add(subsriptionController)
            }
        }
    }
    func logoutGuest()  {
          NotificationsHandler().removeAllNotificationsData()
          PrayerTimeManger.shared.shouldRefreshPrayerTimings = true
          PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
          PrayerTimeManger.shared.prayerTimeCoordinates = nil
          PrayerTimeManger.shared.prayerData = nil
          PrayerTimeManger.shared.selectedLocationName = nil
          UserDefaults.standard.removeObject(forKey: "access_token")
          UserDefaults.standard.synchronize()
          GooglePlacesManager().clearRecentPlacesCache()
          clearDataAndLogout()
          
      }
}
extension NewSettingsController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
    
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.populateData()
            }
        })
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
            tabCon.add(subsriptionController)
        }
    }
    func subscriptionDidCancel(endDate: String){
        DispatchQueue.main.async {
            self.populateData()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let tabCon = self.tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
                subsriptionController.endDate = endDate
                tabCon.add(subsriptionController)
            }
        }
    }
}

extension NewSettingsController: bioDataDelegate {
    func didEditBioData(str: String) {
        profileInfoLabel.text = str
    }
}

extension NewSettingsController: profileImageDelegate {
    func didChangeImage(img: UIImage) {
        profileBtn.setBackgroundImage(img, for: .normal)
    }
}
extension NewSettingsController: updateNameDelegate {
    func updateName() {
        populateData()
    }
}
