//
//  SubscriptionViewModel.swift
//  Rabt
//
//  Created by Asad Khan on 15/06/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class SubscriptionViewModel: NSObject {
    var parentController : MasterViewController?
    var artistId: String?
    var trackId: String?
}
//MARK: - Api services
extension SubscriptionViewModel{
    func artistSubscribedViaAd() -> Void {
        
        let apiManager = ApiManager()
        let url = ApiUrls.artistRewarded
        //self.showSpinner(onView: self.view)
        apiManager.WebService(url: url, parameter: ["id":artistId ?? "" ,"track_id":trackId ?? ""], method: .get, encoding: .queryString, {
            (dataModel: SubsriptionAdModal?) in
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "rewardAdWatched"), object: nil, userInfo: nil)
        }){ error in
            print(error.localizedDescription)
        }
    }

    func premiumContentPurchased(status: Int = 1, referalCode: String = "") -> Void {
        
        let apiManager = ApiManager()
        let url = ApiUrls.premiumPurchase
        //self.showSpinner(onView: self.view)
        apiManager.WebService(url: url, parameter: ["status":status,"referal_code":referalCode], method: .get, encoding: .queryString, {
            (dataModel: AllListenModal?) in
//            (self.parentController as? SubsriptionOptionController)?.loadingView.hideLoading()
//            self.allListenModal = dataModel?.data
//            self.reloadSegmentdata()
        }){ error in
            print(error.localizedDescription)
        }
    }
}


