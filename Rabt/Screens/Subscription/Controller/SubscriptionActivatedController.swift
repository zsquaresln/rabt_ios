//
//  SubscriptionActivatedController.swift
//  Rabt
//
//  Created by Asad Khan on 21/08/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class SubscriptionActivatedController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //MARK: - IBActions
    
    @IBAction func crossBtnTapped(_ sender: Any) {
        self.remove()
    }
}
