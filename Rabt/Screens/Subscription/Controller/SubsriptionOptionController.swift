//
//  SubsriptionOptionController.swift
//  Rabt
//
//  Created by Asad Khan on 09/06/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import StoreKit
//import GoogleMobileAds
import IBAnimatable

import TPInAppReceipt
import ProgressHUD
//import AppLovinSDK

protocol subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int)
    func premiumContentPurchased()
    func subscriptionDidCancel(endDate:String)
}
class SubsriptionOptionController: MasterViewController {
    //    @IBOutlet weak var watchAdBtn: AnimatableButton!
    //    @IBOutlet weak var descriptionLabel: UILabel!
    //    @IBOutlet weak var loadingLabel: UILabel!
    
    @IBOutlet weak var subscribeView: UIView!
    @IBOutlet weak var subscribeHeading: UILabel!
    @IBOutlet weak var subscribeSubheading: UILabel!
    @IBOutlet weak var subscribeTableView: UITableView!
    
   // @IBOutlet weak var subscriptionLabel: UILabel!
    @IBOutlet weak var subscriptionAmountLabel: UILabel!
   // @IBOutlet weak var adWatchContainerView: UIView!
   // @IBOutlet weak var adDisabledView: UIView!
    
    
    @IBOutlet weak var unsbscribeView: UIView!
    @IBOutlet weak var unsubscribeHeading: UILabel!
    @IBOutlet weak var unsubscribeSubHeading: UILabel!
    @IBOutlet weak var unsubscribeTableView: UITableView!
    @IBOutlet weak var continueSubscriptionBtn: UIButton!
    @IBOutlet weak var freemiumBtn: AnimatableButton!
    @IBOutlet weak var refferalTextField: UITextField!
  //  @IBOutlet weak var activatedSubscriptionLabel: UILabel!
    
    
    // var retryAttempt = 0.0
    
   // var rewardedAd: GADRewardedAd?

    //var appLovinRewardedAd: MARewardedAd!


    var delegate : subscriptionDelegate?
  //  private var adWatched : Bool?
    var artistId: String?
    var trackiId: String?
    var viewModel : SubscriptionViewModel?
  //  var interstitial: GADInterstitialAd?
    //  var fbInterstitial: FBInterstitialAd?
    var selectedAudioIndex: Int?
    
  //  var adManager: AdManager?
    //   var productModal = [SKProduct]()
    var products: [SKProduct] = []
    var isFromArtistDetail : Bool = false
    var isAlreadySubscribed = false
    var isPurchaseTapped = false
    var isCancelSubscriptionTapped = false
    var shouldShowCancelConfirmation = false
    override func viewDidLoad() {
        super.viewDidLoad()
        refferalTextField.attributedPlaceholder = NSAttributedString(
            string: "Enter refferal code (optional)",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "DBDBDB")]
        )
        unsbscribeView.isHidden = true
        subscribeView .isHidden = true
        viewModel = SubscriptionViewModel()
        viewModel?.parentController = self
        viewModel?.artistId = artistId
        viewModel?.trackId = trackiId
        NotificationCenter.default.addObserver(self, selector: #selector(handlePurchaseNotification(_:)),
                                               name: .IAPHelperPurchaseNotification,
                                               object: nil)
        reload()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        isCancelSubscriptionTapped = false
        shouldShowCancelConfirmation = false
        NotificationCenter.default.post(name: Notification.Name("subscriptionCheck"), object: nil, userInfo: nil)
    }
    //MARK: - Helper Functions
    @objc func reload() {
        products = []
        
        //tableView.reloadData()
        RabtPremiumProduct.store.requestProducts{ [weak self] success, products in
            guard let self = self else { return }
            if success {
                self.products = products!
                if self.products.count > 0{
                    // let product = self.products.first!
                    //  IAPRecieptValidator.isVIPuser(identifier: RabtPremiumProduct.SwiftShopping, completionHandler: { status in
                    self.validateSubscription()
                    // })
                }
                
                // self.tableView.reloadData()
            }
            
            // self.refreshControl?.endRefreshing()
        }
    }
    func validateSubscription(){
        let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
        if isVip && isAutoRenewable{
            self.isAlreadySubscribed = true
            DispatchQueue.main.async{
                self.loadData()
            }
        }else if isVip && isAutoRenewable == false{
            if let receiptExpiryDateString = IAPRecieptValidator.sharedInstance.getExpirationDateOfReceipt(){
                // show expiry data
                DispatchQueue.main.async{
              //      self.activatedSubscriptionLabel.text = "You have an active subscription which will get expired on \(receiptExpiryDateString)"
                  //  self.activatedSubscriptionLabel.isHidden = false
                    if self.shouldShowCancelConfirmation == true{
                        self.delegate?.subscriptionDidCancel(endDate: receiptExpiryDateString)
                        self.shouldShowCancelConfirmation = false
                        self.remove()
                    }
                    
                }
            } else {
                if isCancelSubscriptionTapped{
                    viewModel?.premiumContentPurchased(status: 0, referalCode: self.refferalTextField.text ?? "")
                }
            }
            self.isAlreadySubscribed = false
            self.isCancelSubscriptionTapped = false
            DispatchQueue.main.async{
                self.loadData()
            }
        }else{
            self.isAlreadySubscribed = false
            self.isCancelSubscriptionTapped = false
            DispatchQueue.main.async{
                self.loadData()
            }
        }
    }
    func loadData(){
        
        registerTableViewCells()
        
        if isAlreadySubscribed && ((userInfo["guest_user"] as? Int) == 1 || (userInfo["guest_user"] as? String) == "1"){
            //load fremium data
            subscribeView.isHidden = true
            unsubscribeTableView.delegate = self
            unsubscribeTableView.dataSource = self
            unsubscribeTableView.reloadData()
            unsubscribeHeading.text = splashSingelton.sharedInstance.freemiumConfig["Title"] as? String
            unsubscribeSubHeading.text = splashSingelton.sharedInstance.freemiumConfig["Subtitle"] as? String
            continueSubscriptionBtn.setTitle(splashSingelton.sharedInstance.freemiumConfig["fremium_btn_text"] as? String ?? "Continue using Rabt Premium", for: .normal)
            
            freemiumBtn.setTitle(splashSingelton.sharedInstance.freemiumConfig["fremium_sub_text"] as? String ?? "", for: .normal)
            unsbscribeView.isHidden = false
            
            
        }else{
            unsbscribeView.isHidden = true
            subscribeTableView.delegate = self
            subscribeTableView.dataSource = self
            subscribeTableView.reloadData()
//            if !isFromArtistDetail{
//                adWatchContainerView.isHidden = true
//            }else{
//                adManager?.parentController = self
//                adManager?.updateAdsStatus()
//                adWatchContainerView.isHidden = false
//            }
            subscribeHeading.text = splashSingelton.sharedInstance.premiumConfig["Title"] as? String
            subscribeSubheading.text = splashSingelton.sharedInstance.premiumConfig["Subtitle"] as? String
//            subscriptionLabel.text = splashSingelton.sharedInstance.premiumConfig["premium_btn_text"] as? String
            subscriptionAmountLabel.text = splashSingelton.sharedInstance.premiumConfig["premium_sub_text"] as? String
            subscribeView.isHidden = false
        }
        
        
    }
    func registerTableViewCells(){
        subscribeTableView.register(UINib(nibName: "SubscriptionCell", bundle: nil), forCellReuseIdentifier: "SubscriptionCell")
        unsubscribeTableView.register(UINib(nibName: "SubscriptionCell", bundle: nil), forCellReuseIdentifier: "SubscriptionCell")
    }
//    func rewardAdLoaded(){
//        // self.watchAdBtn.isHidden = false
//        //self.loadingLabel.isHidden = true
//        //self.descriptionLabel.isHidden = false
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
//    }


//    func interestitialLoaded(){
//        //self.watchAdBtn.isHidden = false
//        //self.loadingLabel.isHidden = true
//        // self.descriptionLabel.isHidden = false
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
//    }
//    func fbInterestitialLoaded(){
//        //self.watchAdBtn.isHidden = false
//        //self.loadingLabel.isHidden = true
//        // self.descriptionLabel.isHidden = false
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
//    }
//    func noAdAvailable(){
//        // self.watchAdBtn.isHidden = false
//        // self.loadingLabel.isHidden = true
//        //self.descriptionLabel.isHidden = false
//        // load fb ads and disable below lines
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
//    }
//    func enableContentAfterRewardClosed(){
//        if adWatched == true{
//            delegate?.rewardAdDidWatchedSuccessfully(playIndex: selectedAudioIndex ?? -1)
//        }
//        self.remove()
//    }
    
    func unlockContent(){
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            // hanlde guest mode
            if var unlockedArtists = UserDefaults.standard.object(forKey: "unlockedArtists") as? [[String:Any]]{
                let findArtist = unlockedArtists.filter{$0["id"] as? String == artistId}
                var tempTracks : [[String:String]] = []
                if findArtist.count > 0{
                    if  let tracks = findArtist.first!["tracks"] as? [[String:String]]{
                        for trackItem in tracks{
                            if trackItem["id"] != trackiId{
                                tempTracks.append(trackItem)
                            }
                        }
                    }
                }
                tempTracks.append(["id":trackiId ??  "","date":(PrayerTimeManger().getStringDateWith(format: "dd-MM-yyyy", date: Date()))])
                unlockedArtists.append(["id":(self.artistId ?? "-1"),"tracks":tempTracks])
                UserDefaults.standard.set(unlockedArtists, forKey: "unlockedArtists")
                UserDefaults.standard.synchronize()
                
            }
            else{
                let unlockedArtists = [["id":(self.artistId ?? "-1"),"tracks":[["id":trackiId,"date":(PrayerTimeManger().getStringDateWith(format: "dd-MM-yyyy", date: Date()))]]]]
                UserDefaults.standard.set(unlockedArtists, forKey: "unlockedArtists")
                UserDefaults.standard.synchronize()
            }
        }else{
            //call api to unlock artist
            self.viewModel?.artistSubscribedViaAd()
        }
    }
    //MARK: - IBActions
    @IBAction func subscribeBtnTapped(_ sender: Any) {
        isCancelSubscriptionTapped = false
        shouldShowCancelConfirmation = false
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            let alert = UIAlertController(title: "Alert", message: "Please login to Unlock Rabt.".localized(), preferredStyle: .alert)
            let ok = UIAlertAction(title: "Login".localized(), style: .default, handler: { action in
                DispatchQueue.main.async{
                    
                    self.logoutGuest()
                    self.remove()
                }
            })
            let cancel = UIAlertAction(title: "Cancel".localized(), style: .default, handler: { action in
               
            })
            alert.addAction(cancel)
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }else{
            ProgressHUD.show("Processing", interaction: false)
            isPurchaseTapped = true
            if products.count > 0{
                let product = products.first!
                RabtPremiumProduct.store.buyProduct(product)
            }
            
        }
        
        
    }
    @IBAction func watchAddBtnTapped(_ sender: Any) {
//        isCancelSubscriptionTapped = false
//        shouldShowCancelConfirmation = false
//        if let ad = rewardedAd {
//            ad.present(fromRootViewController: self) {
//                let reward = ad.adReward
//                print("Reward received with currency \(reward.amount), amount \(reward.amount.doubleValue)")
//                // enable all content of current reciter
//                /// for guest save locally
//                /// for registered users, call api here
//                self.unlockContent()
//                self.adWatched = true
//            }
//        }
//        else if appLovinRewardedAd != nil {
//            if appLovinRewardedAd.isReady
//            {
//                appLovinRewardedAd.delegate = self
//                appLovinRewardedAd.show()
//            }
//        }
//        else if interstitial != nil {
//            interstitial?.present(fromRootViewController: self)
//            self.unlockContent()
//            self.adWatched = true
//        }
//        else if appLovinRewardedAd != nil {
//            if appLovinRewardedAd.isReady
//            {
//                appLovinRewardedAd.delegate = self
//                appLovinRewardedAd.show()
//            }
//        }
        //        else if fbInterstitial != nil {
        //            guard fbInterstitial!.isAdValid else {
        //                return
        //            }
        //            print("Ad is loaded and ready to be displayed")
        //            fbInterstitial!.delegate = self
        //            fbInterstitial?.show(fromRootViewController: self)
        //        }
//        else {
//            //unlock content anyway
//            self.adWatched = true
//            self.unlockContent()
//            delegate?.rewardAdDidWatchedSuccessfully(playIndex: selectedAudioIndex ?? -1)
//            self.remove()
//        }
    }
    @IBAction func contineSubscriptionBtnTapped(_ sender: Any) {
        isCancelSubscriptionTapped = false
        shouldShowCancelConfirmation = false
        self.remove()
    }
    @IBAction func freemiumBtnTapped(_ sender: Any) {
        freemiumBtn.isEnabled = false
        if products.count > 0{
            let product = products.first!
            RabtPremiumProduct.store.buyProduct(product)
            
        }
        isCancelSubscriptionTapped = true
        shouldShowCancelConfirmation = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.checkForSubscriptionCancelation()
        }
        //UIApplication.shared.open(URL(string: "https://apps.apple.com/account/subscriptions")!)
    }
    
    @IBAction func crossBtnTapped(_ sender: Any) {
//        isCancelSubscriptionTapped = false
//        shouldShowCancelConfirmation = false
//        if adWatched == true{
//            delegate?.rewardAdDidWatchedSuccessfully(playIndex: selectedAudioIndex ?? -1)
//        }
        self.remove()
    }
}
extension SubsriptionOptionController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0{
            return (splashSingelton.sharedInstance.premiumConfig["premium_text"] as? [[String:String]])?.count ?? 0
        }else{
            return (splashSingelton.sharedInstance.freemiumConfig["fremium_text"] as? [[String:String]])?.count ?? 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionCell", for: indexPath) as? SubscriptionCell
        var config : [[String:String]] = []
        if tableView.tag == 0{
            config = splashSingelton.sharedInstance.premiumConfig["premium_text"] as? [[String:String]] ?? []
        }else{
            config = splashSingelton.sharedInstance.freemiumConfig["fremium_text"] as? [[String:String]] ?? []
        }
        let textDict = config[indexPath.row]
        cell?.subscriptionText.text = textDict["text"] ?? ""
        let url_str = textDict["icon"] ?? ""
        if let url = URL(string: url_str) {
            cell?.iconView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension SubsriptionOptionController {
    @objc func handlePurchaseNotification(_ notification: Notification) {
        ProgressHUD.dismiss()
        guard
            let productID = notification.object as? String,
            let index = products.index(where: { product -> Bool in
                product.productIdentifier == productID
            })
        else {
           
            return
        }
        
        if isPurchaseTapped{
            viewModel?.premiumContentPurchased(status: 1, referalCode: self.refferalTextField.text ?? "")
            delegate?.premiumContentPurchased()
            self.remove()
        }
        
        
        print("call api here")
    }
}
//extension SubsriptionOptionController:FBRewardedVideoAdDelegate{
//    func rewardedVideoAdWillClose(_ rewardedVideoAd: FBRewardedVideoAd) {
//        print("The user clicked on the close button, the ad is just about to close")
//        self.unlockContent()
//        self.adWatched = true
//    }
//
//    func rewardedVideoAdWillLogImpression(_ rewardedVideoAd: FBRewardedVideoAd) {
//        print("Rewarded Video impression is being captured")
//    }
//}
//extension SubsriptionOptionController:FBInterstitialAdDelegate{
//    func interstitialAdDidClose(_ interstitialAd: FBInterstitialAd) {
//        self.unlockContent()
//        self.adWatched = true
//    }
//}
//extension SubsriptionOptionController : MARewardedAdDelegate{
//    //    func createRewardedAd()
//    //       {
//    //           appLovinRewardedAd = MARewardedAd.shared(withAdUnitIdentifier: "6edb3c3a048aba8e")
//    //           appLovinRewardedAd.delegate = self
//    //
//    //           // Load the first ad
//    //           appLovinRewardedAd.load()
//    //       }
//
//    // MARK: MAAdDelegate Protocol
//
//    func didLoad(_ ad: MAAd)
//    {
//        // Rewarded ad is ready to be shown. '[self.rewardedAd isReady]' will now return 'YES'
//
//        // Reset retry attempt
//        //           retryAttempt = 0
//        //           if appLovinRewardedAd.isReady
//        //           {
//        //               appLovinRewardedAd.show()
//        //           }
//    }
//
//    func didFailToLoadAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError)
//    {
//        // Rewarded ad failed to load
//        // We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds)
//
//        //           retryAttempt += 1
//        //           let delaySec = pow(2.0, min(6.0, retryAttempt))
//        //
//        //           DispatchQueue.main.asyncAfter(deadline: .now() + delaySec) {
//        //               self.appLovinRewardedAd.load()
//        //           }
//    }
//
//    func didDisplay(_ ad: MAAd) {}
//
//    func didClick(_ ad: MAAd) {}
//
//    func didHide(_ ad: MAAd)
//    {
//        // Rewarded ad is hidden. Pre-load the next ad
//        // appLovinRewardedAd.load()
//    }
//
//    func didFail(toDisplay ad: MAAd, withError error: MAError)
//    {
//        // Rewarded ad failed to display. We recommend loading the next ad
//        //  appLovinRewardedAd.load()
//    }
//
//    // MARK: MARewardedAdDelegate Protocol
//
//    func didStartRewardedVideo(for ad: MAAd) {}
//
//    func didCompleteRewardedVideo(for ad: MAAd) {}
//
//    func didRewardUser(for ad: MAAd, with reward: MAReward)
//    {
//        // Rewarded ad was displayed and user should receive the reward
//        //print("Rewarded user: \(reward.amount) \(reward.label)")
//        self.unlockContent()
//        self.adWatched = true
//        delegate?.rewardAdDidWatchedSuccessfully(playIndex: selectedAudioIndex ?? -1)
//        self.remove()
//    }
//
//}
extension SubsriptionOptionController{
    func checkForSubscriptionCancelation(){
        freemiumBtn.isEnabled = true
        if isCancelSubscriptionTapped{
            InAppReceipt.refresh { (error) in
                if let _ = error
                {
                   return
                }else{
                    IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
                        DispatchQueue.main.async {
                            self.validateSubscription()
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            self.checkForSubscriptionCancelation()
                        }
                    })
                }
            }
          
        }
    }
}
////MARK: - Reward Ads delegates
//extension SubsriptionOptionController :GADFullScreenContentDelegate {
//    /// Tells the delegate that the ad failed to present full screen content.
//    func adWillDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//        print("Rewarded ad will be dismissed.")
//    }
//    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//        print("Ad presented")
//    }
//    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//        print("Rewarded ad dismissed.")
//        if adWatched == true{
//            delegate?.rewardAdDidWatchedSuccessfully(playIndex: selectedAudioIndex ?? -1)
//        }
//        self.remove()
//    }
//    
//    func ad(
//        _ ad: GADFullScreenPresentingAd,
//        didFailToPresentFullScreenContentWithError error: Error
//    ) {
//        print("Rewarded ad failed to present with error: \(error.localizedDescription).")
//        let alert = UIAlertController(
//            title: "Rewarded ad failed to present",
//            message: "The reward ad could not be presented.",
//            preferredStyle: .alert)
//        let alertAction = UIAlertAction(
//            title: "Ok",
//            style: .cancel,
//            handler: { [weak self] action in
//                
//            })
//        alert.addAction(alertAction)
//        self.present(alert, animated: true, completion: nil)
//    }
//}
////MARK: - Interestitial delegates
//extension SubsriptionOptionController{
//    /// Tells the delegate that the ad will present full screen content.
//    func adWillPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
//        print("Ad will present full screen content.")
//    }
//}
//extension SubsriptionOptionController : MARewardedAdDelegate{
//    //    func createRewardedAd()
//    //       {
//    //           appLovinRewardedAd = MARewardedAd.shared(withAdUnitIdentifier: "6edb3c3a048aba8e")
//    //           appLovinRewardedAd.delegate = self
//    //
//    //           // Load the first ad
//    //           appLovinRewardedAd.load()
//    //       }
//
//    // MARK: MAAdDelegate Protocol
//
//    func didLoad(_ ad: MAAd)
//    {
//        // Rewarded ad is ready to be shown. '[self.rewardedAd isReady]' will now return 'YES'
//
//        // Reset retry attempt
//        //           retryAttempt = 0
//        //           if appLovinRewardedAd.isReady
//        //           {
//        //               appLovinRewardedAd.show()
//        //           }
//    }
//
//    func didFailToLoadAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError)
//    {
//        // Rewarded ad failed to load
//        // We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds)
//
//        //           retryAttempt += 1
//        //           let delaySec = pow(2.0, min(6.0, retryAttempt))
//        //
//        //           DispatchQueue.main.asyncAfter(deadline: .now() + delaySec) {
//        //               self.appLovinRewardedAd.load()
//        //           }
//    }
//
//    func didDisplay(_ ad: MAAd) {}
//
//    func didClick(_ ad: MAAd) {}
//
//    func didHide(_ ad: MAAd)
//    {
//        // Rewarded ad is hidden. Pre-load the next ad
//        // appLovinRewardedAd.load()
//    }
//
//    func didFail(toDisplay ad: MAAd, withError error: MAError)
//    {
//        // Rewarded ad failed to display. We recommend loading the next ad
//        //  appLovinRewardedAd.load()
//    }
//
//    // MARK: MARewardedAdDelegate Protocol
//
//    func didStartRewardedVideo(for ad: MAAd) {}
//
//    func didCompleteRewardedVideo(for ad: MAAd) {}
//
//    func didRewardUser(for ad: MAAd, with reward: MAReward)
//    {
//        // Rewarded ad was displayed and user should receive the reward
//        //print("Rewarded user: \(reward.amount) \(reward.label)")
//        self.unlockContent()
//        self.adWatched = true
//        delegate?.rewardAdDidWatchedSuccessfully(playIndex: selectedAudioIndex ?? -1)
//        self.remove()
//    }
//
//}
