//
//  WatchAdController.swift
//  Rabt
//
//  Created by Tayyab Faran on 19/09/2023.
//  Copyright © 2023 Ali Sher. All rights reserved.
//

import UIKit
import StoreKit
import GoogleMobileAds
import IBAnimatable

import TPInAppReceipt
import ProgressHUD
//import AppLovinSDK


class WatchAdController: MasterViewController {
    //    @IBOutlet weak var watchAdBtn: AnimatableButton!
    //    @IBOutlet weak var descriptionLabel: UILabel!
    //    @IBOutlet weak var loadingLabel: UILabel!
    
  
    @IBOutlet weak var refferalTextField: UITextField!
    @IBOutlet weak var subscriptionAmountLabel: UILabel!
    
    // var retryAttempt = 0.0
    
    var rewardedAd: GADRewardedAd?

    //var appLovinRewardedAd: MARewardedAd!


    var delegate : subscriptionDelegate?
    private var adWatched : Bool?
    var artistId: String?
   // var trackiId: String?
    var viewModel : SubscriptionViewModel?
    var interstitial: GADInterstitialAd?
    //  var fbInterstitial: FBInterstitialAd?
    var selectedAudioIndex: Int?
    
    var adManager: AdManager?
    //   var productModal = [SKProduct]()
    var products: [SKProduct] = []
    var isFromArtistDetail : Bool = false
    var isPurchaseTapped = false
    override func viewDidLoad() {
        super.viewDidLoad()
        refferalTextField.attributedPlaceholder = NSAttributedString(
            string: "Enter refferal code (optional)",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "DBDBDB")]
        )
        viewModel = SubscriptionViewModel()
        viewModel?.parentController = self
        viewModel?.artistId = artistId
      //  viewModel?.trackId = trackiId
        NotificationCenter.default.addObserver(self, selector: #selector(handlePurchaseNotification(_:)),
                                               name: .IAPHelperPurchaseNotification,
                                               object: nil)
        adManager?.parentController = self
        adManager?.updateAdsStatus()
        reload()
    }
    override func viewWillAppear(_ animated: Bool) {
    subscriptionAmountLabel.text = splashSingelton.sharedInstance.premiumConfig["premium_sub_text"] as? String
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name("subscriptionCheck"), object: nil, userInfo: nil)
    }
    //MARK: - Helper Functions
    @objc func reload() {
        products = []

        //tableView.reloadData()
        RabtPremiumProduct.store.requestProducts{ [weak self] success, products in
            guard let self = self else { return }
            if success {
                self.products = products!
            }
        }
    }

//    func loadData(){
//
//      //  registerTableViewCells()
//
////        if isAlreadySubscribed && ((userInfo["guest_user"] as? Int) == 1 || (userInfo["guest_user"] as? String) == "1"){
////            //load fremium data
////            subscribeView.isHidden = true
////            unsubscribeTableView.delegate = self
////            unsubscribeTableView.dataSource = self
////            unsubscribeTableView.reloadData()
////            unsubscribeHeading.text = splashSingelton.sharedInstance.freemiumConfig["Title"] as? String
////            unsubscribeSubHeading.text = splashSingelton.sharedInstance.freemiumConfig["Subtitle"] as? String
////            continueSubscriptionLabel.text = splashSingelton.sharedInstance.freemiumConfig["fremium_btn_text"] as? String
////            freemiumBtn.setTitle(splashSingelton.sharedInstance.freemiumConfig["fremium_sub_text"] as? String ?? "", for: .normal)
////            unsbscribeView.isHidden = false
////
////
////        }else{
////            unsbscribeView.isHidden = true
////            subscribeTableView.delegate = self
////            subscribeTableView.dataSource = self
////            subscribeTableView.reloadData()
////            if !isFromArtistDetail{
////                adWatchContainerView.isHidden = true
////            }else{
////                adManager?.parentController = self
////                adManager?.updateAdsStatus()
////                adWatchContainerView.isHidden = false
////            }
////            subscribeHeading.text = splashSingelton.sharedInstance.premiumConfig["Title"] as? String
////            subscribeSubheading.text = splashSingelton.sharedInstance.premiumConfig["Subtitle"] as? String
////            subscriptionLabel.text = splashSingelton.sharedInstance.premiumConfig["premium_btn_text"] as? String
////            subscriptionAmountLabel.text = splashSingelton.sharedInstance.premiumConfig["premium_sub_text"] as? String
////            subscribeView.isHidden = false
////        }
//
//
//    }
//    func registerTableViewCells(){
//        subscribeTableView.register(UINib(nibName: "SubscriptionCell", bundle: nil), forCellReuseIdentifier: "SubscriptionCell")
//        unsubscribeTableView.register(UINib(nibName: "SubscriptionCell", bundle: nil), forCellReuseIdentifier: "SubscriptionCell")
//    }
    func rewardAdLoaded(){
        // self.watchAdBtn.isHidden = false
        //self.loadingLabel.isHidden = true
        //self.descriptionLabel.isHidden = false
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
    }
//    func applovinRewardAdLoaded(){
//        // self.watchAdBtn.isHidden = false
//        //self.loadingLabel.isHidden = true
//        //self.descriptionLabel.isHidden = false
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
//    }
//    func fbRewardAdLoaded(){
//        // self.watchAdBtn.isHidden = false
//        //self.loadingLabel.isHidden = true
//        //self.descriptionLabel.isHidden = false
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
//    }
//    func applovinRewardAdLoaded(){
//         self.watchAdBtn.isHidden = false
//        self.loadingLabel.isHidden = true
//        self.descriptionLabel.isHidden = false
//
//    }

    func interestitialLoaded(){
        //self.watchAdBtn.isHidden = false
        //self.loadingLabel.isHidden = true
        // self.descriptionLabel.isHidden = false
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
    }
//    func fbInterestitialLoaded(){
//        //self.watchAdBtn.isHidden = false
//        //self.loadingLabel.isHidden = true
//        // self.descriptionLabel.isHidden = false
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
//    }
    func noAdAvailable(){
        // self.watchAdBtn.isHidden = false
        // self.loadingLabel.isHidden = true
        //self.descriptionLabel.isHidden = false
        // load fb ads and disable below lines
//        adDisabledView.isHidden = true
//        adDisabledView.isUserInteractionEnabled = false
    }
    func enableContentAfterRewardClosed(){
        if adWatched == true{
            delegate?.rewardAdDidWatchedSuccessfully(playIndex: selectedAudioIndex ?? -1)
        }
        self.remove()
    }
    
    func unlockContent(){
      //  if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            // hanlde guest mode
            if var unlockedArtists = UserDefaults.standard.object(forKey: "unlockedArtists") as? [[String:Any]]{
//                let findArtist = unlockedArtists.filter{$0["id"] as? String == artistId}
//                var tempTracks : [[String:String]] = []
//                if findArtist.count > 0{
//                    if  let tracks = findArtist.first!["tracks"] as? [[String:String]]{
//                        for trackItem in tracks{
//                            if trackItem["id"] != trackiId{
//                                tempTracks.append(trackItem)
//                            }
//                        }
//                    }
//                }
                //tempTracks.append(["id":trackiId ??  "","date":(PrayerTimeManger().getStringDateWith(format: "dd-MM-yyyy", date: Date()))])
                unlockedArtists.append(["id":(self.artistId ?? "-1"),"date":(PrayerTimeManger().getStringDateWith(format: "dd-MM-yyyy", date: Date()))])
                UserDefaults.standard.set(unlockedArtists, forKey: "unlockedArtists")
                UserDefaults.standard.synchronize()
                
            }
            else{
                let unlockedArtists = [["id":(self.artistId ?? "-1"),"date":(PrayerTimeManger().getStringDateWith(format: "dd-MM-yyyy", date: Date()))]]
//                let unlockedArtists = [["id":(self.artistId ?? "-1"),"tracks":[["id":trackiId,"date":(PrayerTimeManger().getStringDateWith(format: "dd-MM-yyyy", date: Date()))]]]]
                UserDefaults.standard.set(unlockedArtists, forKey: "unlockedArtists")
                UserDefaults.standard.synchronize()
            }
//        }else{
//            //call api to unlock artist
//            self.viewModel?.artistSubscribedViaAd()
//        }
    }
    //MARK: - IBActions
    @IBAction func subscribeBtnTapped(_ sender: Any) {
      //  isCancelSubscriptionTapped = false
      //  shouldShowCancelConfirmation = false
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {
            let alert = UIAlertController(title: "Alert", message: "Please login to Unlock Rabt.".localized(), preferredStyle: .alert)
            let ok = UIAlertAction(title: "Login".localized(), style: .default, handler: { action in
                DispatchQueue.main.async{
                    
                    self.logoutGuest()
                    self.remove()
                }
            })
            let cancel = UIAlertAction(title: "Cancel".localized(), style: .default, handler: { action in
               
            })
            alert.addAction(cancel)
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }else{
            ProgressHUD.show("Processing", interaction: false)
            isPurchaseTapped = true
            if products.count > 0{
                let product = products.first!
                RabtPremiumProduct.store.buyProduct(product)
            }
            
        }
        
        
    }
    @IBAction func watchAddBtnTapped(_ sender: Any) {
      //  isCancelSubscriptionTapped = false
      //  shouldShowCancelConfirmation = false
        if let ad = rewardedAd {
            ad.present(fromRootViewController: self) {
                let reward = ad.adReward
                print("Reward received with currency \(reward.amount), amount \(reward.amount.doubleValue)")
                // enable all content of current reciter
                /// for guest save locally
                /// for registered users, call api here
                self.unlockContent()
                self.adWatched = true
            }
        }
//        else if appLovinRewardedAd != nil {
//            if appLovinRewardedAd.isReady
//            {
//                appLovinRewardedAd.delegate = self
//                appLovinRewardedAd.show()
//            }
//        }
        else if interstitial != nil {
            interstitial?.present(fromRootViewController: self)
            self.unlockContent()
            self.adWatched = true
        }
//        else if appLovinRewardedAd != nil {
//            if appLovinRewardedAd.isReady
//            {
//                appLovinRewardedAd.delegate = self
//                appLovinRewardedAd.show()
//            }
//        }
        //        else if fbInterstitial != nil {
        //            guard fbInterstitial!.isAdValid else {
        //                return
        //            }
        //            print("Ad is loaded and ready to be displayed")
        //            fbInterstitial!.delegate = self
        //            fbInterstitial?.show(fromRootViewController: self)
        //        }
        else {
            //unlock content anyway
            self.adWatched = true
            self.unlockContent()
            delegate?.rewardAdDidWatchedSuccessfully(playIndex: selectedAudioIndex ?? -1)
            self.remove()
        }
    }
//    @IBAction func contineSubscriptionBtnTapped(_ sender: Any) {
//     //   isCancelSubscriptionTapped = false
//     //   shouldShowCancelConfirmation = false
//        self.remove()
//    }
//    @IBAction func freemiumBtnTapped(_ sender: Any) {
//        freemiumBtn.isEnabled = false
//        if products.count > 0{
//            let product = products.first!
//            RabtPremiumProduct.store.buyProduct(product)
//
//        }
//        isCancelSubscriptionTapped = true
//        shouldShowCancelConfirmation = true
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//            self.checkForSubscriptionCancelation()
//        }
//        //UIApplication.shared.open(URL(string: "https://apps.apple.com/account/subscriptions")!)
//    }
    
    @IBAction func crossBtnTapped(_ sender: Any) {
//        isCancelSubscriptionTapped = false
//        shouldShowCancelConfirmation = false
        if adWatched == true{
            delegate?.rewardAdDidWatchedSuccessfully(playIndex: selectedAudioIndex ?? -1)
        }
        self.remove()
    }
}
//extension WatchAdController : UITableViewDelegate, UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView.tag == 0{
//            return (splashSingelton.sharedInstance.premiumConfig["premium_text"] as? [[String:String]])?.count ?? 0
//        }else{
//            return (splashSingelton.sharedInstance.freemiumConfig["fremium_text"] as? [[String:String]])?.count ?? 0
//        }
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionCell", for: indexPath) as? SubscriptionCell
//        var config : [[String:String]] = []
//        if tableView.tag == 0{
//            config = splashSingelton.sharedInstance.premiumConfig["premium_text"] as? [[String:String]] ?? []
//        }else{
//            config = splashSingelton.sharedInstance.freemiumConfig["fremium_text"] as? [[String:String]] ?? []
//        }
//        let textDict = config[indexPath.row]
//        cell?.subscriptionText.text = textDict[textDict.keys.first ?? ""]
//        return cell!
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
//}

extension WatchAdController {
    @objc func handlePurchaseNotification(_ notification: Notification) {
        ProgressHUD.dismiss()
        guard
            let productID = notification.object as? String,
            let index = products.index(where: { product -> Bool in
                product.productIdentifier == productID
            })
        else {
           
            return
        }
        
        if isPurchaseTapped{
            delegate?.premiumContentPurchased()
            viewModel?.premiumContentPurchased(status: 1, referalCode: self.refferalTextField.text ?? "")
            self.remove()
        }
        
        
        print("call api here")
    }
}

//extension WatchAdController{
//    func checkForSubscriptionCancelation(){
//        freemiumBtn.isEnabled = true
//        if isCancelSubscriptionTapped{
//            InAppReceipt.refresh { (error) in
//                if let _ = error
//                {
//                   return
//                }else{
//                    IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
//                        DispatchQueue.main.async {
//                            self.validateSubscription()
//                        }
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                            self.checkForSubscriptionCancelation()
//                        }
//                    })
//                }
//            }
//
//        }
//    }
//}

