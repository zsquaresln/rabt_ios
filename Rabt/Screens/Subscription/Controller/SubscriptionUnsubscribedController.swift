//
//  SubscriptionUnsubscribedController.swift
//  Rabt
//
//  Created by Asad Khan on 08/09/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class SubscriptionUnsubscribedController: UIViewController {

    @IBOutlet weak var endLabel: UILabel!
    var endDate : String?
    override func viewDidLoad() {
        super.viewDidLoad()
       // endLabel.text = "You have Unsubscribed Successfully. Your Subscription will end on \(endDate ?? "Month end")"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func crossBtnTapped(_ sender: Any) {
        self.remove()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
