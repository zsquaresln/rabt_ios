//
//  SubsriptionAdModal.swift
//  Rabt
//
//  Created by Asad Khan on 15/06/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit


class SubsriptionAdModal: NetworkCommonResponseDataModel {
  
    let data: SubscriptionAd?
    enum CodingKeys: String, CodingKey {
        case data
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data    = try values.decodeIfPresent(SubscriptionAd.self, forKey: .data)
        
        try super.init(from: decoder)
    }
}
struct SubscriptionAd: Codable {
    var artist_id: String?
       
       enum CodingKeys: String, CodingKey {
           case artist_id = "artist_id"
       }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        artist_id = try values.decodeIfPresent(String.self, forKey: .artist_id)
   }
}
