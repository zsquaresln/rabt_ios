//
//  LiveTVController.swift
//  Rabt
//
//  Created by Tayyab Faran on 23/07/2023.
//  Copyright © 2023 Ali Sher. All rights reserved.
//

import UIKit
import youtube_ios_player_helper
class LiveTVController: UIViewController,YTPlayerViewDelegate {

    @IBOutlet weak var ytPlayer: YTPlayerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        ytPlayer.delegate = self
        // Do any additional setup after loading the view.
    }
    override var shouldAutorotate: Bool {
        return false
    }
    override func viewWillAppear(_ animated: Bool) {
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        super.viewWillAppear(animated)
        let playerVars = [
                             "playsinline" : 0,
                             "showinfo" : 0,
                             "rel" : 0,
                             "modestbranding" : 0,
                             "controls" : 0
                         ]
       // ["controls":0,"playsinline":0, "showinfo":0]
        ytPlayer.load(withVideoId:  splashSingelton.sharedInstance.url_live_stream, playerVars: playerVars)
       // ytPlayer.load(withVideoId: "gUC3TjCrwRw")
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ytPlayer.stopVideo()
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        ytPlayer.playVideo()
    }

}
