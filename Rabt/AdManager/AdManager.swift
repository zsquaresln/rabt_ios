//
//  AdManager.swift
//  Rabt
//
//  Created by Asad Khan on 10/06/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import GoogleMobileAds

//import AppLovinSDK
class AdManager: NSObject {
    var rewardedAd: GADRewardedAd?
    //  var appLovinRewardedAd: MARewardedAd!
    //   private var fbRewardedVideoAd: FBRewardedVideoAd?
    var interstitial: GADInterstitialAd?
    //  private var fbInterstitialAd: FBInterstitialAd?
    var parentController : WatchAdController?
    private var adFailedToLoad: Bool = false

    
    
    // testing ads ids
//    let testInterstitialAdId = "ca-app-pub-9017718178049269/6984737172"
//    let nativeadUnitID = "ca-app-pub-3940256099942544/3986624511"
//    let bannerId = "ca-app-pub-3940256099942544/2934735716"
//    let testRewardAdId = "ca-app-pub-3940256099942544/1712485313"
    // live ads ids
     let liveRewardAdId = "ca-app-pub-5106240261809786/1933037697"
     let liveInterestitialAdId = "ca-app-pub-5106240261809786/1249445759"
    let nativeadUnitID = "ca-app-pub-5106240261809786/1862531372"
    let bannerId = "ca-app-pub-5106240261809786/2760362841"
    func loadRewardedAd() {
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "735bb235a7ede15a5a8fefdb3239ffab" ]
        
        GADRewardedAd.load(
            withAdUnitID: liveRewardAdId, request: GADRequest()
        ) { (ad, error) in
            // self.watchAdBtn.isHidden = false
            if let error = error {
                // self.adFailedToLoad = true
                self.loadInterestitialAd()
                // self.parentController?.noAdAvailable()
                print("Rewarded ad failed to load with error: \(error.localizedDescription)")
                return
            }
            print("Loading Succeeded")
            // self.isRewardAvailable = true
            self.adFailedToLoad = false
            self.rewardedAd = ad
            self.parentController?.rewardedAd = ad
            self.parentController?.rewardAdLoaded()
            self.rewardedAd?.fullScreenContentDelegate = self
        }
    }
    func loadInterestitialAd(){
        let request = GADRequest()
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "735bb235a7ede15a5a8fefdb3239ffab" ]
        
        GADInterstitialAd.load(withAdUnitID:liveInterestitialAdId,
                               request: request,
                               completionHandler: { [self] ad, error in
            if let error = error {
                self.adFailedToLoad = true
                self.parentController?.noAdAvailable()
                // self.loadFacebookRewardedAd()
                //self.loadApplovinRewardedAd()
                print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                return
            }
            self.adFailedToLoad = false
            interstitial = ad
            self.parentController?.interstitial = ad
            self.parentController?.interestitialLoaded()
            interstitial?.fullScreenContentDelegate = self
        }
        )
    }
    
    //    func loadApplovinRewardedAd()
    //       {
    //           appLovinRewardedAd = MARewardedAd.shared(withAdUnitIdentifier: "6edb3c3a048aba8e")
    //           appLovinRewardedAd.delegate = self
    //
    //           // Load the first ad
    //           appLovinRewardedAd.load()
    //       }
    //    func loadFacebookRewardedAd(){
    //        let rewardedVideoAd = FBRewardedVideoAd(placementID: "YOUR_PLACEMENT_ID")
    //         rewardedVideoAd.delegate = self
    //
    //         // For auto play video ads, it's recommended to load the ad at least 30 seconds before it is shown
    //         rewardedVideoAd.load()
    //
    //         self.fbRewardedVideoAd = rewardedVideoAd
    //    }
    //    func loadFacebookInterestitialAd(){
    //        let interstitialAd = FBInterstitialAd(placementID: "YOUR_PLACEMENT_ID")
    //        interstitialAd.delegate = self
    //
    //        // For auto play video ads, it's recommended to load the ad at least 30 seconds before it is shown
    //        interstitialAd.load()
    //
    //        self.fbInterstitialAd = interstitialAd
    //    }
    func updateAdsStatus(){
        if let rewardedAd = rewardedAd {
            self.parentController?.rewardedAd = rewardedAd
            self.parentController?.rewardAdLoaded()
        }
        if let interstitial = interstitial {
            self.parentController?.interstitial = interstitial
            self.parentController?.interestitialLoaded()
        }
        
        //        if let applovinRewaredAd = appLovinRewardedAd {
        //            self.parentController?.appLovinRewardedAd = applovinRewaredAd
        //            self.parentController?.applovinRewardAdLoaded()
        //        }
        //        if let fbInterestitial = fbInterstitialAd {
        //            self.parentController?.fbInterstitial = fbInterestitial
        //            self.parentController?.fbInterestitialLoaded()
        //        }
        if adFailedToLoad{
            self.parentController?.noAdAvailable()
        }
    }
}
//MARK: - Reward Ads delegates
extension AdManager :GADFullScreenContentDelegate {
    /// Tells the delegate that the ad failed to present full screen content.
    func adWillDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Rewarded ad will be dismissed.")
        self.rewardedAd = nil
        self.interstitial = nil
        //   self.parentController = nil
        self.adFailedToLoad = false
        self.loadRewardedAd()
    }
    //    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
    //        print("Ad presented")
    //    }
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Rewarded ad dismissed.")
        self.parentController?.enableContentAfterRewardClosed()
    }
    
    func ad(
        _ ad: GADFullScreenPresentingAd,
        didFailToPresentFullScreenContentWithError error: Error
    ) {
        
        if (error as NSError).code == 18{
            // ad used, fetch new ad
            self.adFailedToLoad = false
            
            self.loadRewardedAd()
        }else{
            self.adFailedToLoad = true
            self.parentController?.noAdAvailable()
        }
        print("Rewarded ad failed to present with error: \(error.localizedDescription).")
    }
}
//MARK: - Interestitial delegates
extension AdManager{
    /// Tells the delegate that the ad will present full screen content.
    func adWillPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        print("Ad will present full screen content.")
    }
}
////MARK: - Facebook reward ads delegate
//extension AdManager :FBRewardedVideoAdDelegate{
//    func rewardedVideoAdDidLoad(_ rewardedVideoAd: FBRewardedVideoAd) {
//      print("Video ad is loaded and ready to be displayed")
//        self.adFailedToLoad = false
//        self.fbRewardedVideoAd = rewardedVideoAd
//        self.parentController?.fbRewardedAd = rewardedVideoAd
//        self.parentController?.rewardAdLoaded()
//        //self.fbRewardedVideoAd?.fullScreenContentDelegate = self
//    }
//
//    func rewardedVideoAd(_ rewardedVideoAd: FBRewardedVideoAd, didFailWithError error: Error) {
//      print("Rewarded video ad failed to load")
//        self.loadFacebookInterestitialAd()
//    }
//
//    func rewardedVideoAdDidClick(_ rewardedVideoAd: FBRewardedVideoAd) {
//      print("Video ad clicked")
//    }
//
//    func rewardedVideoAdDidClose(_ rewardedVideoAd: FBRewardedVideoAd) {
//      print("Rewarded Video ad closed - this can be triggered by closing the application, or closing the video end card")
//    }
//
//    func rewardedVideoAdVideoComplete(_ rewardedVideoAd: FBRewardedVideoAd) {
//      print("Rewarded Video ad video completed - this is called after a full video view, before the ad end card is shown. You can use this event to initialize your reward")
//    }
//}
////MARK: - Facebook interestitial ads delegate
//extension AdManager :FBInterstitialAdDelegate{
//    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
//      print("Interstitial ad failed to load with error: \(error.localizedDescription)")
//         self.adFailedToLoad = true
//         self.parentController?.noAdAvailable()
//    }
//    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd) {
//        self.adFailedToLoad = false
//        self.fbInterstitialAd = interstitialAd
//        self.parentController?.fbInterstitial = interstitialAd
//        self.parentController?.fbInterestitialLoaded()
//    }
//}

//extension AdManager : MARewardedAdDelegate{
//    func didStartRewardedVideo(for ad: MAAd) {
//
//    }
//
//    func didCompleteRewardedVideo(for ad: MAAd) {
//
//    }
//
//    func didRewardUser(for ad: MAAd, with reward: MAReward) {
//
//    }
//
//    func didFailToLoadAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError) {
//        self.adFailedToLoad = true
//        self.parentController?.noAdAvailable()
//    }
//
//    func didDisplay(_ ad: MAAd) {
//
//    }
//
//    func didHide(_ ad: MAAd) {
//
//    }
//
//    func didClick(_ ad: MAAd) {
//
//    }
//
//    func didFail(toDisplay ad: MAAd, withError error: MAError) {
//
//    }
//
//
//
//       // MARK: MAAdDelegate Protocol
//
//       func didLoad(_ ad: MAAd)
//       {
//           // Rewarded ad is ready to be shown. '[self.rewardedAd isReady]' will now return 'YES'
//
//           // Reset retry attempt
//          // retryAttempt = 0
//           self.adFailedToLoad = false
//         //  self.fbRewardedVideoAd = rewardedVideoAd
//           self.parentController?.appLovinRewardedAd = appLovinRewardedAd
//           self.parentController?.applovinRewardAdLoaded()
////           if appLovinRewardedAd.isReady
////           {
////               appLovinRewardedAd.show()
////           }
//       }
//
////       func didFailToLoadAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError)
////       {
////           // Rewarded ad failed to load
////           // We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds)
////
////           retryAttempt += 1
////           let delaySec = pow(2.0, min(6.0, retryAttempt))
////
////           DispatchQueue.main.asyncAfter(deadline: .now() + delaySec) {
////               self.appLovinRewardedAd.load()
////           }
////       }
//
////       func didDisplay(_ ad: MAAd) {}
////
////       func didClick(_ ad: MAAd) {}
////
////       func didHide(_ ad: MAAd)
////       {
////           // Rewarded ad is hidden. Pre-load the next ad
////           appLovinRewardedAd.load()
////       }
//
////       func didFail(toDisplay ad: MAAd, withError error: MAError)
////       {
////           // Rewarded ad failed to display. We recommend loading the next ad
////           appLovinRewardedAd.load()
////       }
//
//       // MARK: MARewardedAdDelegate Protocol
//
////       func didStartRewardedVideo(for ad: MAAd) {}
////
////       func didCompleteRewardedVideo(for ad: MAAd) {}
////
////       func didRewardUser(for ad: MAAd, with reward: MAReward)
////       {
////           // Rewarded ad was displayed and user should receive the reward
////           print("Rewarded user: \(reward.amount) \(reward.label)")
////       }
//
//}


