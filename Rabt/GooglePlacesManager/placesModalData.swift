//
//  placesModalData.swift
//  Rabt
//
//  Created by Asad Khan on 21/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//


import Foundation

// MARK: - Welcome
class placesModalData: NetworkCommonResponseDataModel {
  
    let predictions: [Prediction?]?
    enum CodingKeys: String, CodingKey {
        case predictions
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        predictions    = try values.decodeIfPresent([Prediction].self, forKey: .predictions)
        
        try super.init(from: decoder)
    }
}


// MARK: - Prediction
struct Prediction: Codable {
    let predictionDescription: String?
    let placeID: String?
    enum CodingKeys: String, CodingKey {
        case predictionDescription = "description"
        case placeID = "place_id"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        predictionDescription = try values.decodeIfPresent(String.self, forKey: .predictionDescription)
        placeID = try values.decodeIfPresent(String.self, forKey: .placeID)
   }
}

