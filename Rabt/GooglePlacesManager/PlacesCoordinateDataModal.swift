//
//  PlacesCoordinateDataModal.swift
//  Rabt
//
//  Created by Asad Khan on 21/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import Foundation

import UIKit

class GooglePlacesResponseDataModel: Codable {
    let  status, message: String?
    private enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
    }
}

class PlacesCoordinateDataModal : GooglePlacesResponseDataModel {

    let result: PlacesCoordinateResult?
    enum CodingKeys: String, CodingKey {
        case result
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        result    = try values.decodeIfPresent(PlacesCoordinateResult.self, forKey: .result)

        try super.init(from: decoder)
    }
}

struct PlacesCoordinateResult: Codable{
  
    let geometry: Geometry?
    enum CodingKeys: String, CodingKey {
        case geometry = "geometry"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        geometry = try values.decodeIfPresent(Geometry.self, forKey: .geometry)
   }
}

// MARK: - Prediction
struct Geometry: Codable {
    let location: Location?
    enum CodingKeys: String, CodingKey {
        case location = "location"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        location = try values.decodeIfPresent(Location.self, forKey: .location)
   }
}
struct Location: Codable {
    let lat: Double?
    let lng: Double?
    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case lng = "lng"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        lng = try values.decodeIfPresent(Double.self, forKey: .lng)
   }
}

