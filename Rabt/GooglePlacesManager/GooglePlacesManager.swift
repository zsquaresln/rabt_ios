//
//  GooglePlacesManager.swift
//  Rabt
//
//  Created by Asad Khan on 19/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import GooglePlaces
import Alamofire
import CoreLocation

protocol googlePlacesCustomDelegate{
    func placesSearchCompleted()
    func coordinateFetchingCompleted()
    func placeNameFetchingCompleted()
}
private class recentPlacesMangaer{
    //properties
    static let sharedInstance = recentPlacesMangaer()
    var recentlySelectedPlaces : [Prediction?]? = []
    
}
class GooglePlacesManager: NSObject {
    // properties
    var placesData: [Prediction?]?
    var recentlySelectedPlaces : [Prediction?]?
    let apiKey = "AIzaSyAHIw_y-UEv4LYHCTcSvOPDKMqhLDXXtf0"
    var selectedPlaceCoordinates : Location?
    var delegate :googlePlacesCustomDelegate?
}
//MARK: - places methods
extension GooglePlacesManager{
    func getPlaces(searchString: String) {
        AF.request("https://maps.googleapis.com/maps/api/place/autocomplete/json",parameters:[
            "input": searchString,
            "type": "(cities)",
            "key": apiKey
        ]
        ).validate().responseJSON { response in
            switch response.result {
            case .success:
                let data =   try? JSONDecoder().decode(placesModalData.self, from: response.data!)
                self.placesData = data?.predictions
                self.delegate?.placesSearchCompleted()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getCoordinateFrom(placeId: String) {
        AF.request("https://maps.googleapis.com/maps/api/place/details/json",parameters:[
            "placeid": placeId,
            "key": apiKey
        ]
        ).validate().responseJSON { response in
            switch response.result {
            case .success:
                let data =   try? JSONDecoder().decode(PlacesCoordinateDataModal.self, from: response.data!)
                self.selectedPlaceCoordinates = data?.result?.geometry?.location
                self.delegate?.coordinateFetchingCompleted()
                print("")
            case .failure(let error):
                print(error)
            }
        }
    }
    func getPlaceNameFrom(coordinate: CLLocation) {
        CLGeocoder().reverseGeocodeLocation(coordinate, completionHandler:
                                    {(placemarks, error) in
                                        if (error != nil)
                                        {
                                            print("reverse geodcode fail: \(error!.localizedDescription)")
                                        }
                                        guard let pm = placemarks?.first else {
                                            print("No placemark from Apple: \(String(describing: error))")
                                            return
                                        }
                                        var addressString : String = ""
                                        
                                        if pm.subLocality != nil {
                                            addressString = addressString + pm.subLocality! + ", "
                                        }
                                        if pm.thoroughfare != nil {
                                            addressString = addressString + pm.thoroughfare! + ", "
                                        }
                                        if pm.locality != nil {
                                            addressString = addressString + pm.locality! + ", "
                                        }
                                        if pm.country != nil {
                                            addressString = addressString + pm.country! + ", "
                                        }
                                        if pm.postalCode != nil {
                                            addressString = addressString + pm.postalCode! + " "
                                            
                                        }
            PrayerTimeManger.shared.selectedLocationName = pm.locality
            self.delegate?.placeNameFetchingCompleted()
                                        }
                                            
                                    )
       }
    
}
//MARK: - recent places methods
extension GooglePlacesManager{
    func getRecentSelectedPlaces(){
        self.recentlySelectedPlaces = recentPlacesMangaer.sharedInstance.recentlySelectedPlaces
    }
    func cacheSelectedPlace(location:Prediction){
        if recentPlacesMangaer.sharedInstance.recentlySelectedPlaces?.count ?? 0 > 0{
            for place in recentPlacesMangaer.sharedInstance.recentlySelectedPlaces!{
                if place?.placeID == location.placeID{
                    return
                }
            }
        }
        recentPlacesMangaer.sharedInstance.recentlySelectedPlaces?.append(location)
    }
    func clearRecentPlacesCache(){
        recentPlacesMangaer.sharedInstance.recentlySelectedPlaces = nil
    }
}
