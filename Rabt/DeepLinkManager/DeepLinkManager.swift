//
//  DeepLinkManager.swift
//  Rabt
//
//  Created by Asad Khan on 06/03/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class DeepLinkManager: NSObject {
    func performDeepLinkIfAny(notification : [AnyHashable : Any]){
        if let isArtist = notification["type"] as? String, isArtist.lowercased() == "artist"{
            // show artis detail screen
            let storyboard = UIStoryboard.init(name: "ListenNew", bundle: nil)
            let VC = storyboard.instantiateViewController(withIdentifier: "ArtistDetailController") as! ArtistDetailController
            var artistId = -1
            if let id = notification["id"] as? Int{
                artistId = id
            }else{
                artistId = Int(notification["id"] as? String ?? "-1") ?? -1
            }
            if notification.keys.contains("track"){
                if let id = notification["track"] as? Int{
                    VC.notificationPlayableIndex = id
                }
                if let id = notification["track"] as? String {
                    VC.notificationPlayableIndex  = Int(id) ?? -1
                }
            }
            VC.category_type = notification["category_type"] as? String ?? ""
            getPlaylistDetailData(playlist_id:  Int(notification["playlist_id"] as? String ?? "-1") ?? -1, category_type:  notification["category_type"] as? String ?? "", category_id:  Int(notification["category_id"] as? String ?? "-1") ?? -1, vC: VC)
//            VC.audioDetailViewModel.selectedArtist = ArtistModal.init(from: ["id":artistId])
//            VC.selectedSegment = .None
           
        }
    }
    
    func getPlaylistDetailData(playlist_id: Int, category_type:String ,category_id:Int?, vC: ArtistDetailController) {
        let apiManager = ApiManager()
        let url = ApiUrls.playlistDetailServicesApi
       // (self.parentController as? ListenContainerController)?.loadingView.showLoading()
        //self.showSpinner(onView: self.view)
        var params = ["playlist_id":playlist_id, "category_type":category_type] as [String : Any]
        if category_id != nil {
             params["category_id"] = category_id
        }
        apiManager.WebService(url: url, parameter: params, method: .get, encoding: .queryString, {
            (dataModel: PlaylistDetailModel?) in
            //(self.parentController as? ListenContainerController)?.loadingView.hideLoading()
            DispatchQueue.main.async {
                let listenModel = ListenViewModal()
                listenModel.playlistDetail = dataModel?.data
                listenModel.parentController = vC
                vC.audioDetailViewModel = listenModel
                singeltonClass.shared.naviagtionController?.pushViewController(vC, animated: true)
                singeltonClass.shared.naviagtionController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            }
           
            
        }){ error in
            print(error.localizedDescription)
        }
    }
}
