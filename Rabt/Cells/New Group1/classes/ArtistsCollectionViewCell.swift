//
//  ArtistsCollectionViewCell.swift
//  Rabt
//
//  Created by Ali Sher on 15/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import IBAnimatable
class ArtistsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contantView: UIView!
    
    @IBOutlet weak var imgView: AnimatableImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var lockImageView: UIImageView!
    
    @IBOutlet weak var totalPlaysLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
