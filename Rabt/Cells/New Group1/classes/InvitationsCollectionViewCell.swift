//
//  InvitationsCollectionViewCell.swift
//  Rabt
//
//  Created by Muhammad on 28/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class InvitationsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contantView: UIView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var membersLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
