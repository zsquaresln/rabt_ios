//
//  prayCollectionViewCell.swift
//  Rabt
//
//  Created by Ali Sher on 25/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class PrayBookCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var contantView: UIView!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
