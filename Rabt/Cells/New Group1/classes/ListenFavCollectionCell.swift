//
//  ListenFavCollectionCell.swift
//  Rabt
//
//  Created by Tayyab Faran on 10/09/2023.
//  Copyright © 2023 Ali Sher. All rights reserved.
//

import UIKit
import IBAnimatable
class ListenFavCollectionCell: UICollectionViewCell {

    @IBOutlet weak var containerView: AnimatableView!
    @IBOutlet weak var iconView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
