//
//  ListenSearchCollectionCell.swift
//  Rabt
//
//  Created by Tayyab Faran on 10/09/2023.
//  Copyright © 2023 Ali Sher. All rights reserved.
//

import UIKit

class ListenSearchCollectionCell: UICollectionViewCell {

    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBtnTapped: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
