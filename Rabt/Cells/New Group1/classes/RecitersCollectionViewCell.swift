//
//  RecitersCollectionViewCell.swift
//  Rabt
//
//  Created by Tayyab Faran on 17/02/2024.
//  Copyright © 2024 Ali Sher. All rights reserved.
//

import UIKit

class RecitersCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contantView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var playsLabel: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
