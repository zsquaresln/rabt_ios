//
//  AudioBookCollectionViewCell.swift
//  Rabt
//
//  Created by Muhammad on 09/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class AudioBookCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contantView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
