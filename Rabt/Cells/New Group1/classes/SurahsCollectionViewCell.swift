//
//  SurahsCollectionViewCell.swift
//  Rabt
//
//  Created by Tayyab Faran on 17/02/2024.
//  Copyright © 2024 Ali Sher. All rights reserved.
//

import UIKit

class SurahsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contantView: UIView!
    
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var surahTitleIMG: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
