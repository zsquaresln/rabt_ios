//
//  ChaptersCollectionViewCell.swift
//  Rabt
//
//  Created by Muhammad on 30/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class ChaptersCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contantView: UIView!
    
    @IBOutlet weak var bookmarkBtn: UIButton!
    @IBOutlet weak var numberLbl: UILabel!
    
    
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
