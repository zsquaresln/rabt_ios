//
//  PrayerKnowledgeTableViewCell.swift
//  Rabt
//
//  Created by Muhammad on 02/08/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import IBAnimatable
class PrayerKnowledgeTableViewCell: UITableViewCell {

    @IBOutlet weak var contantView: UIView!
    @IBOutlet weak var imgView: AnimatableImageView!
    @IBOutlet weak var numberLbl: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
