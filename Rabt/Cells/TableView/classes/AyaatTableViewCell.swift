//
//  AyaatTableViewCell.swift
//  Rabt
//
//  Created by Muhammad on 30/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class AyaatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contantView: UIView!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var ayaatNumberLbl: UILabel!
    @IBOutlet weak var arabicTextLbl: UILabel!
    
    @IBOutlet weak var favouriteBtn: UIButton!
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var englishTextLbl: UILabel!

    
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var TafseerLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
