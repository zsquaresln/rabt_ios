//
//  recomendedArtistsTableViewCell.swift
//  Rabt
//
//  Created by Ali Sher on 15/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import IBAnimatable
class recomendedArtistsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contantView: UIView!
    
    
    @IBOutlet weak var seeAllicon: UIImageView!
    @IBOutlet weak var seeAllBtn: UIButton!
    @IBOutlet weak var seeAllView: AnimatableView!
    
    
    @IBOutlet weak var titleTL: UILabel!
    
    @IBOutlet weak var collectionTopConstraintToSeeView: NSLayoutConstraint!
    @IBOutlet weak var collectionTopConstraintToContainer: NSLayoutConstraint!
    @IBOutlet weak var dataCollectionView: UICollectionView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
