//
//  prayTimingsTableViewCell.swift
//  Rabt
//
//  Created by Ali Sher on 25/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class prayTimingsTableViewCell: UITableViewCell {

    @IBOutlet weak var contantView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var audioBtn: UIButton!
    
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
