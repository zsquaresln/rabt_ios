//
//  MembersOfGroupTableViewCell.swift
//  Rabt
//
//  Created by Muhammad on 28/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class MembersOfGroupTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var contantView: UIView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var membersLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
