//
//  TableCellWithCollectionView.swift
//  Rabt
//
//  Created by Tayyab Faran on 03/03/2024.
//  Copyright © 2024 Ali Sher. All rights reserved.
//

import UIKit

class TableCellWithCollectionView: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
