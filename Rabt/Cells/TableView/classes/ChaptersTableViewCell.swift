//
//  ChaptersTableViewCell.swift
//  Rabt
//
//  Created by Muhammad on 26/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class ChaptersTableViewCell: UITableViewCell {

    @IBOutlet weak var contantView: UIView!
    
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var surahTitleIMG: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
