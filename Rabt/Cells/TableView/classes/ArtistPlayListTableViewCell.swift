//
//  ArtistPlayListTableViewCell.swift
//  Rabt
//
//  Created by Muhammad on 13/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class ArtistPlayListTableViewCell: UITableViewCell {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var contantView: UIView!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var playOrPauseImgView: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
