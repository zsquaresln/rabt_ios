//
//  MasterViewController.swift
//  GleapTechnician
//
//  Created by Admin on 11/30/21.
//

import UIKit
import StickyTabBarViewController
import AVFoundation
import MediaPlayer
 class singeltonClass{
    static let shared = singeltonClass()
     var naviagtionController :UINavigationController?
     var tabBarController :MainTabBarController?
     var recentlyPlayed: [Any]?
     var isOfflineShown = false
}
var kAPPDelegate = singeltonClass.shared
var tabController: StickyViewControllerSupportingTabBarController? {
    if let tabBarController = kAPPDelegate.tabBarController {
        return tabBarController
    }
    return nil
}
class MasterViewController: UIViewController {
   // var kAppParentNavigation : UINavigationController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
//MARK: - Helper Functions
  func logoutGuest()  {
        NotificationsHandler().removeAllNotificationsData()
        PrayerTimeManger.shared.shouldRefreshPrayerTimings = true
        PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
        PrayerTimeManger.shared.prayerTimeCoordinates = nil
        PrayerTimeManger.shared.prayerData = nil
        PrayerTimeManger.shared.selectedLocationName = nil
        UserDefaults.standard.removeObject(forKey: "access_token")
        UserDefaults.standard.synchronize()
        GooglePlacesManager().clearRecentPlacesCache()
        clearDataAndLogout()
        
    }
    func clearDataAndLogout(){
        let dict = ["status": isPlaying ? "1" : "0", "artist": isSameArtist ? "1" : "0", "already": isAlreadyPlayAllClcked ? "1" : "0"]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "playOrPaused"), object: nil, userInfo: dict)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "closeListen"), object: nil, userInfo: nil)
        self.dismiss(animated: false, completion: nil)
        UserDefaults.standard.removeObject(forKey: "favourites")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.removeObject(forKey: "userinfo")
        UserDefaults.standard.synchronize()
        
        self.tabBarController?.tabBar.isHidden = true
       
        try? AVAudioSession.sharedInstance().setActive(false, options: .notifyOthersOnDeactivation) // Where self.session is AVAudioSession. You should do it with do-catch to good error catching.
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [:]
        let storyboard = AuthenticationStoryBoard
        let VC = storyboard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
        if self.navigationController == nil{
            kAPPDelegate.naviagtionController?.pushViewController(VC, animated: true)
        }else{
            self.navigationController?.pushViewController(VC, animated: true)
        }
        self.tabBarController?.tabBar.isHidden = true
    }
//MARK:- Navigation options
    func show<T: UIViewController>(storyBoard: storyBoardIdentifiers , controllerIdentifier : String ,navigation: UINavigationController?, configure :((T) -> Void))  {
        let conStoryBoard = UIStoryboard.init(name: storyBoard.rawValue, bundle: nil)
        let controller = conStoryBoard.instantiateViewController(withIdentifier: controllerIdentifier) as? T
        navigation?.pushViewController(controller!, animated: true)
       // navigation?.show(controller!, sender: self)
        configure(controller!)
        kAPPDelegate.naviagtionController = navigation
    }
    func moveBackToPreviousController(){
        kAPPDelegate.naviagtionController?.popViewController(animated: true)
    }
    func moveBackTo(controllerNo:Int){
        kAPPDelegate.naviagtionController?.popToViewController((kAPPDelegate.naviagtionController?.viewControllers[(kAPPDelegate.naviagtionController?.viewControllers.count)! - controllerNo])!, animated: true)
    }
    
    
}
@nonobjc extension UIViewController {
    func add(_ child: UIViewController, frame: CGRect? = nil) {
        addChild(child)

        if let frame = frame {
            child.view.frame = frame
        }else{
            child.view.frame  = UIScreen.main.bounds
        }

        view.addSubview(child.view)
        child.didMove(toParent: self)
    }

    func remove() {
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}
