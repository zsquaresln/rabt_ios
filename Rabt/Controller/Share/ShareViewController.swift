//
//  ShareViewController.swift
//  Rabt
//
//  Created by Muhammad on 28/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func logoutBtnCicked(_ sender: Any) {
        
        UserDefaults.standard.removeObject(forKey: "userinfo")
        UserDefaults.standard.synchronize()
        
        self.tabBarController?.tabBar.isHidden = true
        
        let storyboard = AuthenticationStoryBoard
        let VC = storyboard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
        self.navigationController!.pushViewController(VC, animated: false)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
}
