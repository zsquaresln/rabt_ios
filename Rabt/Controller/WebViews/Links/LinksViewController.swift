//
//  LinksViewController.swift
//  Rabt
//
//  Created by Muhammad on 03/08/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import WebKit
import SwiftData

class LinksViewController: UIViewController {
    
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    
    var selectedTag = -1
    var allLinks : [String:Any] = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: self.getURL())!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        
        webView.navigationDelegate = self
        webView.uiDelegate = self
        spinnerView.isHidden = true
        
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    func getURL() -> String {
        
        var url = ""
        
        if selectedTag == 1 {
            
            titleLbl.text = "What's New"
            url = allLinks["help"] as? String ?? ""
            
        }else if selectedTag == 2 {
            
            titleLbl.text = "FAQ / Contact Us"
            url = allLinks["faqs"] as? String ?? ""
            
        }else if selectedTag == 3 {
            
            titleLbl.text = "Community Guidelines"
            url = allLinks["guidelines"] as? String ?? ""
            
        }else if selectedTag == 4 {
            
            titleLbl.text = "Terms Of Service"
            url = allLinks["terms"] as? String ?? ""
            
        }else if selectedTag == 5 {
            
            titleLbl.text = "Privacy Policy"
            url = allLinks["privacy"] as? String ?? ""
            
        }
        
        return url
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
}



extension LinksViewController: WKNavigationDelegate, WKUIDelegate{
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        spinnerView.isHidden = false
        spinnerView.startAnimating()
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        spinnerView.isHidden = true
        spinnerView.stopAnimating()
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        spinnerView.isHidden = true
        spinnerView.stopAnimating()
    }
}
