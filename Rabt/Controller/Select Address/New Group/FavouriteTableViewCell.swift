//
//  FavouriteTableViewCell.swift
//  RoundedShapes
//
//  Created by Ali Sher on 01/10/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit

class FavouriteTableViewCell: UITableViewCell {

    @IBOutlet var contantView: UIView!
    @IBOutlet var typeImgView: UIImageView!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var deleteBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
