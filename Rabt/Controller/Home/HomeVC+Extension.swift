//
//  HomeVC+Extension.swift
//  Rabt
//
//  Created by Ali Sher on 15/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import Foundation
import SwiftData


extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource,
                              UICollectionViewDelegateFlowLayout {
    

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return artistsArray.count
        } else if collectionView.tag == 1 {
            return islamicFinanceArray.count
        } else if collectionView.tag == 2 {
            return storiesQuranArray.count
        } else if collectionView.tag == 3 {
            return storiesProphetsArray.count
        } else {
            return historyKaabaArray.count
        }
    //   return 2
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 162, height: 215)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : ArtistsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistsCell", for: indexPath) as! ArtistsCollectionViewCell
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        
        var dict : [String:Any]! = [:]
        if collectionView.tag == 0 {
            dict = artistsArray[indexPath.item] as? [String:Any]
        } else if collectionView.tag == 1 {
            dict = islamicFinanceArray[indexPath.item] as? [String:Any]
        } else if collectionView.tag == 2 {
            dict = storiesQuranArray[indexPath.item] as? [String:Any]
        } else if collectionView.tag == 3 {
            dict = storiesProphetsArray[indexPath.item] as? [String:Any]
        } else {
            dict = historyKaabaArray[indexPath.item] as? [String:Any]
        }
        
        print(dict)
        
        if dict["disable"] as? Int == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true{
            cell.lockImageView.image = UIImage(named: "whitePlayIcon")
            
            //cell.lockImageView.isHidden = true
        } else {
            cell.lockImageView.image = UIImage(named: "greenLockIcon")
            
            //cell.lockImageView.isHidden = false
        }
        cell.nameLbl.text = dict["name"] as? String
        cell.totalPlaysLabel.text =  "\(dict["subtext"] as? String ?? "") - \(dict["no_of_plays"] as? String ?? "")"
        
        let url_str = dict["image"] as? String ?? ""
        let url = URL(string: url_str)
        cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
        
//        GeneralMethods.CreateCardView(radius: 16, view: cell.contantView)
//        cell.contantView.clipsToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var dict : [String:Any]! = [:]
        if collectionView.tag == 0 {
            dict = artistsArray[indexPath.item] as? [String:Any]
        } else if collectionView.tag == 1 {
            dict = islamicFinanceArray[indexPath.item] as? [String:Any]
        } else if collectionView.tag == 2 {
            dict = storiesQuranArray[indexPath.item] as? [String:Any]
        } else if collectionView.tag == 3 {
            dict = storiesProphetsArray[indexPath.item] as? [String:Any]
        } else {
            dict = historyKaabaArray[indexPath.item] as? [String:Any]
        }
        if dict["disable"] as? Int == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true{
//            let storyboard = UIStoryboard.init(name: "ListenNew", bundle: nil)
//            let VC = storyboard.instantiateViewController(withIdentifier: "ArtistDetailController") as! ArtistDetailController
//            self.navigationController!.pushViewController(VC, animated: true)
//            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    //        if playlistModel?.category_type?.lowercased() == "artist" {
    //            categoryId = playlistModel?.category_id
    //        }
            var categoryId: Int?
            if collectionView.tag == 0 {
             categoryId = dict["category_id"] as? Int
            }
            audioDetailModel?.parentController = self
            lastSelectedCategoryType = dict["category_type"] as? String ?? ""
            audioDetailModel?.getPlaylistDetailData(playlist_id: dict["id"] as? Int ?? 0, category_type: lastSelectedCategoryType , category_id: categoryId)
           
        } else {
            if let tabCon = tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
                subsriptionController.delegate = self
                tabCon.add(subsriptionController)
            }
        }
    }
}
