//
//  HomeViewController.swift
//  Rabt
//
//  Created by Ali Sher on 26/05/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData
import CoreLocation
import TPInAppReceipt
import GoogleMobileAds
//import Adhan

class HomeViewController: MyLocationManagerViewController {
    
    
    
    @IBOutlet weak var loadingView: LoadingOrNoRecordView!
    
    @IBOutlet weak var dataScrollView: UIScrollView!
    
    
    @IBOutlet weak var greetingTL: UILabel!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileImgView: UIImageView!
    
    @IBOutlet weak var nextPrayerView: UIView!
    @IBOutlet weak var nextPrayerRemaningTimeLbl: UILabel!
    @IBOutlet weak var nextPrayerNameAndTimeLbl: UILabel!
    @IBOutlet weak var nextPrayerIcon: UIImageView!
    
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var locationEnableBtn: UIButton!
    @IBOutlet weak var locationIcon: UIImageView!
    
    @IBOutlet weak var inspirationView: UIView!
    @IBOutlet weak var inspirationImgView: UIImageView!
    @IBOutlet weak var inspirationTitleView: UIView!
    @IBOutlet weak var inspirationTitleTextLbl: UILabel!
    
    @IBOutlet weak var inspirationTitleLbl: UILabel!
    @IBOutlet weak var inspirationDetailsLbl: UILabel!
    
    
    @IBOutlet weak var latestReadView: UIView!
    @IBOutlet weak var latestReadImgView: UIImageView!
    @IBOutlet weak var latestReadLbl: UILabel!
    
    
    
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var seeAllView: UIView!
    @IBOutlet weak var islamicFinanceSeeAllView: UIView!
    @IBOutlet weak var storiesQuranSeeAllView: UIView!
    @IBOutlet weak var storiesProphetsSeeAllView: UIView!
    @IBOutlet weak var historyKaabaSeeAllView: UIView!
    @IBOutlet weak var seeAllArrowIcon: UIImageView!
    
    
    @IBOutlet weak var lastViewedView: UIView!
    @IBOutlet weak var lastViewedImgView: UIImageView!
    @IBOutlet weak var lastViewedNameLbl: UILabel!
    @IBOutlet weak var lastViewedDescriptionLbl: UILabel!
    
    
    @IBOutlet weak var dataCollectionView: UICollectionView!
    @IBOutlet weak var historyKaabaCollectionView: UICollectionView!
    @IBOutlet weak var headingHistoryKaaba: UILabel!
    @IBOutlet weak var storiesProphetsCollectionView: UICollectionView!
    @IBOutlet weak var headingStoriesProhets: UILabel!
    @IBOutlet weak var storiesQuranColletionView: UICollectionView!
    @IBOutlet weak var headingStoriesQuran: UILabel!
    @IBOutlet weak var islamicFinanceCollectionView: UICollectionView!
    @IBOutlet weak var headingIslamicFinance: UILabel!
    
    @IBOutlet weak var stackViewContainer: UIStackView!
    @IBOutlet weak var historyKaabaView: UIView!
    @IBOutlet weak var storeisProphetsView: UIView!
    @IBOutlet weak var islamicFinanceView: UIView!
    @IBOutlet weak var storiesQuranView: UIView!
    
    @IBOutlet weak var ramadanView: UIView!
    @IBOutlet weak var ramadanHeadingLabel: UILabel!
    @IBOutlet weak var seharLabel: UILabel!
    @IBOutlet weak var aftarLabel: UILabel!
    

    @IBOutlet weak var unlockPremiumView: UIView!
    
    @IBOutlet weak var premiumIconView: UIImageView!
    
    @IBOutlet weak var charityView: UIView!
    @IBOutlet weak var charityImageView: UIImageView!
    //@IBOutlet weak var nativeAddView: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var bannerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var donateNowView: UIView!
    @IBOutlet weak var donateNowHeightConstraint: NSLayoutConstraint!
    
    var nextPrayerTimer: Timer?
   var homeAnalyticsHit = false
    var loadingLabel: UILabel?
    var imgView: UIImageView?
    var noRecordLabel: UILabel?
    var homeScreenData : [String:Any] = [:]
    var artistsArray : [Any] = []
    var islamicFinanceArray : [Any] = []
    var storiesQuranArray : [Any] = []
    var storiesProphetsArray : [Any] = []
    var historyKaabaArray : [Any] = []
    var lastViewed : [String:Any] = [:]
    
    var chapterId = 0
    var verseId = 0
    static var quranImages = [[String:Any]]()
    var isFirstTime = true
    var locationHandler : LocationHandler = LocationHandler()
    var prayerTimeManger : PrayerTimeManger = PrayerTimeManger()
    var appVersionManager  = AppVersionManager()
    var adLoader: GADAdLoader = GADAdLoader()
    var audioDetailModel : ListenViewModal?
    var lastSelectedCategoryType : String = ""
    // var nativeAds = [GADNativeAd]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        audioDetailModel = ListenViewModal()
        audioDetailModel?.parentController = self
        //        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
        //            // do nothing
        //        }else{
        //          //  IAPRecieptValidator.referestSubscriptionReceipts()
        //            RabtPremiumProduct.store.restorePurchases()
        //            if IAPRecieptValidator.isVIPuser(identifier: RabtPremiumProduct.SwiftShopping){
        //                unlockPremiumView.isHidden = true
        //            }
        //
        //        }
        
        stackViewContainer.setCustomSpacing(16, after: nextPrayerView)
        stackViewContainer.setCustomSpacing(16, after: dataView)
       

        stackViewContainer.setCustomSpacing(16, after: charityView)
        stackViewContainer.setCustomSpacing(16, after: ramadanView)
        stackViewContainer.setCustomSpacing(16, after: inspirationView)
        stackViewContainer.setCustomSpacing(16, after: latestReadView)
        stackViewContainer.setCustomSpacing(16, after:lastViewedView )
        stackViewContainer.setCustomSpacing(16, after:islamicFinanceView )
        stackViewContainer.setCustomSpacing(16, after: unlockPremiumView)
        stackViewContainer.setCustomSpacing(16, after:storiesQuranView )
        stackViewContainer.setCustomSpacing(16, after:storeisProphetsView )
        stackViewContainer.setCustomSpacing(16, after:historyKaabaView )
        if let url = URL(string: splashSingelton.sharedInstance.charity_image){
            charityImageView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            charityView.isHidden = false
        }else{
            charityView.isHidden = true
        }
        
        loadAds()
        userInfo = UserDefaults.standard.value(forKey: "userinfo") as! [String:Any]
        setTextAndDesign()
        
        dataCollectionView.delegate = self
        dataCollectionView.dataSource = self
        islamicFinanceCollectionView.delegate = self
        islamicFinanceCollectionView.dataSource = self
        storiesQuranColletionView.delegate = self
        storiesQuranColletionView.dataSource = self
        storiesProphetsCollectionView.delegate = self
        storiesProphetsCollectionView.dataSource = self
        historyKaabaCollectionView.delegate = self
        historyKaabaCollectionView.dataSource = self
        dataCollectionView.backgroundColor = .clear
        islamicFinanceCollectionView.backgroundColor = .clear
        storiesQuranColletionView.backgroundColor = .clear
        storiesProphetsCollectionView.backgroundColor = .clear
        historyKaabaCollectionView.backgroundColor = .clear
        dataCollectionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        islamicFinanceCollectionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        storiesQuranColletionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        storiesProphetsCollectionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        historyKaabaCollectionView.register(UINib(nibName: "ArtistsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "artistsCell")
        self.navigationController?.navigationBar.isHidden = true
        
        showLoader()
        inspirationView.isHidden = true
        lastViewedView.isHidden = true
        latestReadView.isHidden = true
        dataView.isHidden = true
        islamicFinanceView.isHidden = true
        storiesQuranView.isHidden = true
        storeisProphetsView.isHidden = true
        historyKaabaView.isHidden = true
        
        loadingView.isHidden = true
        
        if GeneralMethods.isKeyPresentInUserDefaults(key: "homeData") {
            
            homeScreenData = UserDefaults.standard.value(forKey: "homeData") as? [String:Any] ?? [:]
            self.setData()
            isFirstTime = false
        }
        DispatchQueue.global().async {
            do {
                let update = try self.appVersionManager.isUpdateAvailable()
                
                print("update",update)
                DispatchQueue.main.async {
                    if update{
                        self.appVersionManager.parentController = self
                        self.appVersionManager.popupUpdateDialogue()
                    }
                }
            } catch {
                print(error)
            }
        }
        self.tabBarController?.tabBar.isHidden = false
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if splashSingelton.sharedInstance.splashDataModel?.home_screen_banner == false{
            bannerView.isHidden = true
        } else {
            bannerView.isHidden = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("subscriptionCheck"), object: nil)
        setPremiumViews()

        homeAnalyticsHit = false
        singeltonClass.shared.naviagtionController = self.navigationController

        self.setTabBarCorner()
        
        self.tabBarController?.tabBar.isHidden = false
        nextPrayerTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(getTimings), userInfo: nil, repeats: true)
        getTimings()
        
       // let text = String(format: "%@, %@!", GlobalMethods.greetingLogic(), userInfo["full_name"] as? String ?? "")
        greetingTL.text = userInfo["full_name"] as? String ?? ""//text
        
        let url_str = userInfo["picture"] as? String ?? ""
        let url = URL(string: url_str)
        profileImgView.sd_setImage(with: url , placeholderImage: UIImage(named: "PlaceHolder.jpg"))
        
       // GeneralMethods.RoundedCorners(shadow: .clear, view: profileView)
        profileView.clipsToBounds = true
        
        getHomeData()
        self.tabBarController?.selectedIndex = 0
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if let needToshowNotification = UserDefaults.standard.object(forKey: "isFromNotification") as? [AnyHashable:Any]{
            UserDefaults.standard.removeObject(forKey: "isFromNotification")
            UserDefaults.standard.synchronize()
            let notiData = needToshowNotification //as [String:Any]
            print("Notification arrived = \(notiData)")
            DeepLinkManager().performDeepLinkIfAny(notification: notiData )
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        nextPrayerTimer?.invalidate()
        nextPrayerTimer = nil
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        setPremiumViews()
        
    }
    
    func setPremiumViews(){
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            // do nothing
            premiumIconView.isHidden = true
            dataCollectionView.reloadData()
            islamicFinanceCollectionView.reloadData()
            storiesQuranColletionView.reloadData()
            storiesProphetsCollectionView.reloadData()
            historyKaabaCollectionView.reloadData()
            DispatchQueue.main.async{
                self.donateNowView.isHidden = false
                    self.donateNowHeightConstraint.constant = 50
                self.bannerViewHeightConstraint.constant = 50
            }
        }else{
            //  IAPRecieptValidator.referestSubscriptionReceipts()
            //            if IAPRecieptValidator.isVIPuser(identifier: RabtPremiumProduct.SwiftShopping){
            //                unlockPremiumView.isHidden = true
            //                premiumIconView.isHidden = false
            //            }else{
            //                unlockPremiumView.isHidden = false
            //                premiumIconView.isHidden = true
            //            }
            //            IAPRecieptValidator.isVIPuser(identifier: RabtPremiumProduct.SwiftShopping, completionHandler: { status in
            //                if status == true{
            //                    DispatchQueue.main.async{
            //                        self.unlockPremiumView.isHidden = true
            //                        self.premiumIconView.isHidden = false
            //                    }
            //                }else{
            //                    DispatchQueue.main.async{
            //                        self.unlockPremiumView.isHidden = false
            //                        self.premiumIconView.isHidden = true
            //                    }
            //                }
            //            })
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip && isAutoRenewable{
                DispatchQueue.main.async{
                    self.premiumIconView.isHidden = false
                    self.unlockPremiumView.isHidden = true
                    DispatchQueue.main.async {
                        self.donateNowHeightConstraint.constant = 0
                        self.bannerViewHeightConstraint.constant = 0
                    }
                    self.charityView.isHidden = true
                }
            }else if isVip{
                DispatchQueue.main.async{
                    self.premiumIconView.isHidden = false
                    self.unlockPremiumView.isHidden = true
                    DispatchQueue.main.async {
                        self.donateNowHeightConstraint.constant = 0
                        self.bannerViewHeightConstraint.constant = 0
                    }
                }
                self.charityView.isHidden = true
            }
            else{
                DispatchQueue.main.async{
                    self.premiumIconView.isHidden = true
                    self.unlockPremiumView.isHidden = false
                    DispatchQueue.main.async {
                        self.donateNowHeightConstraint.constant = 50
                        self.bannerViewHeightConstraint.constant = 50
                    }
                }
                if let url = URL(string: splashSingelton.sharedInstance.charity_image){
                    charityImageView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
                    charityView.isHidden = false
                }else{
                    charityView.isHidden = true
                }
            }
            dataCollectionView.reloadData()
            islamicFinanceCollectionView.reloadData()
            storiesQuranColletionView.reloadData()
            storiesProphetsCollectionView.reloadData()
            historyKaabaCollectionView.reloadData()
        }
    }
    func showLoader() -> Void {
        
        loadingLabel = (loadingView!.viewWithTag(100) as! UILabel)
        imgView = (loadingView!.viewWithTag(101) as! UIImageView)
        noRecordLabel = (loadingView!.viewWithTag(102) as! UILabel)
        
        loadingLabel!.textColor = .black
        noRecordLabel!.textColor = .black
        
        loadingLabel!.text = "Loading"
        noRecordLabel!.text = "no record found"
        
        loadingLabel!.isHidden = false
        noRecordLabel!.isHidden = true
        imgView!.isHidden = true
        
    }
    
    func setTextAndDesign() -> Void {
        
   //     GeneralMethods.CreateCardViewWithoutShadow(radius: 10, view: nextPrayerView)
    //    nextPrayerView.backgroundColor = .white
        
     //   GeneralMethods.ChangeImgViewTintColor(imgView: locationIcon, color: .black)
        
        GeneralMethods.CreateCardViewWithoutBackColor(radius: 10, view: inspirationView)
        inspirationView.clipsToBounds = true
        
        GeneralMethods.CreateCardViewWithoutBackColor(radius: 5, view: inspirationTitleView)
        inspirationTitleView.clipsToBounds = true
        
       // GeneralMethods.CreateCardViewWithoutBackColor(radius: 10, view: latestReadView)
       // latestReadView.clipsToBounds = true
        
       // GeneralMethods.CreateCardViewWithoutBackColor(radius: 10, view: lastViewedView)
       // lastViewedView.clipsToBounds = true
        
        GeneralMethods.RoundedCorners(shadow: .clear, view: seeAllView)
        GeneralMethods.RoundedCorners(shadow: .clear, view: historyKaabaSeeAllView)
        GeneralMethods.RoundedCorners(shadow: .clear, view: storiesQuranSeeAllView)
        GeneralMethods.RoundedCorners(shadow: .clear, view: islamicFinanceSeeAllView)
        GeneralMethods.RoundedCorners(shadow: .clear, view: storiesProphetsSeeAllView)
        GeneralMethods.ChangeImgViewTintColor(imgView: seeAllArrowIcon, color: GlobalVariables.DarkGrayColor)
        
    }
    
    func setData() -> Void {
        
        self.loadingView.isHidden = true
        
        artistsArray = homeScreenData["Artist"] as? [Any] ?? []
        if artistsArray.count > 0 {
            
            dataView.isHidden = false
        }else {
            
            dataView.isHidden = true
        }
        
        let islamicFinanceDict = homeScreenData["IslamicFinaceArtist"] as? [String:Any] ?? [:]
        islamicFinanceArray = islamicFinanceDict["item"] as? [Any] ?? []
        headingIslamicFinance.text = islamicFinanceDict["title"] as? String
        if islamicFinanceArray.count > 0 {
            
            islamicFinanceView.isHidden = false
        }else {
            
            islamicFinanceView.isHidden = true
        }
        
        let storiesQuranDict = homeScreenData["StoriesFromQuranArtist"] as? [String:Any] ?? [:]
        storiesQuranArray = storiesQuranDict["item"] as? [Any] ?? []
        headingStoriesQuran.text = storiesQuranDict["title"] as? String
        if storiesQuranArray.count > 0 {
            
            storiesQuranView.isHidden = false
        }else {
            
            storiesQuranView.isHidden = true
        }
        
        let storiesProphetsDict = homeScreenData["StoriesOfProphetArtist"] as? [String:Any] ?? [:]
        storiesProphetsArray = storiesProphetsDict["item"] as? [Any] ?? []
        headingStoriesProhets.text = storiesProphetsDict["title"] as? String
        if storiesProphetsArray.count > 0 {
            
            storeisProphetsView.isHidden = false
        }else {
            
            storeisProphetsView.isHidden = true
        }
        
        let historyKaabaDict = homeScreenData["HistoryOfKaabaArtist"] as? [String:Any] ?? [:]
        historyKaabaArray = historyKaabaDict["item"] as? [Any] ?? []
        headingHistoryKaaba.text = historyKaabaDict["title"] as? String
        if historyKaabaArray.count > 0 {
            
            historyKaabaView.isHidden = false
        }else {
            
            historyKaabaView.isHidden = true
        }
        
        
        if let quotes = homeScreenData["Quotes"] as? [String:Any] {
            
            inspirationView.isHidden = false
            let url_str = quotes["image"] as? String ?? ""
            let url = URL(string: url_str)
            inspirationImgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            
        }else {
            
            inspirationView.isHidden = true
        }
        if let QuranImages = homeScreenData["Quran_Images"] as? [[String:Any]]{
            HomeViewController.quranImages = QuranImages
        }
        if let quran = homeScreenData["Quran"] as? [String:Any] {
            
            latestReadView.isHidden = false
            
            let chapterImageQuranArray = HomeViewController.quranImages.filter{
                String(($0["position_id"] as? Int ?? 0))  == quran["chapter_no"] as? String ?? ""
                
            }
//            if chapterImageQuranArray.count > 0{
//                let url_str = (chapterImageQuranArray.first?["image"] ?? "") as? String ?? ""
//                let url = URL(string: url_str)
//                latestReadImgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
//            }
            
            
            latestReadLbl.text = quran["text"] as? String
            
            chapterId = Int(quran["chapter_no"] as? String ?? "0") ?? 0
            verseId = Int(quran["verses_read"] as? String ?? "0") ?? 0
            
            
        }else {
            
            latestReadView.isHidden = true
        }
        
        if let recent = homeScreenData["Recent_Artist"] as? [String:Any] {
            
            lastViewed = recent
            lastViewedView.isHidden = false
            let url_str = recent["image"] as? String ?? ""
            let url = URL(string: url_str)
            lastViewedImgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
          //  lastViewedDescriptionLbl.text = recent["des"] as? String
            lastViewedNameLbl.text = recent["name"] as? String
            
        }else {
            
            lastViewedView.isHidden = true
        }
        
        dataCollectionView.reloadData()
        islamicFinanceCollectionView.reloadData()
        storiesQuranColletionView.reloadData()
        storiesProphetsCollectionView.reloadData()
        historyKaabaCollectionView.reloadData()
    }
    
    @objc func getTimings() -> Void {
        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (notifications) in
            print("num of pending notifications \(notifications.count)")
            
        })
        if PrayerTimeManger.shared.selectedLocationName == nil{
            locationLbl.text = ""
        }
        if !(locationHandler.isLocationServicesEnabled()){
            locationLbl.isHidden = true
            //  locationEnableBtn.isHidden = false
            locationEnableBtn.setTitle("Click here to enable location", for: .normal)
            // set Mecca as default location
            PrayerTimeManger.shared.prayerTimeCoordinates = ["latitude":"21.422487","longitude":"39.826206"]
            prayerTimeManger.delegate = self
            prayerTimeManger.getPrayerTimings()
        }
        else{
            locationLbl.isHidden = false
            //locationEnableBtn.isHidden = true
            locationEnableBtn.setTitle("", for: .normal)
            if PrayerTimeManger.shared.prayerTimeCoordinates != nil{
                if let customLocationCoordinates = UserDefaults.standard.value(forKey: "selectedPrayerTimesCoordinate") as? [String:String]{
                    // use user save locatoin coordinates for prayer timings
                    PrayerTimeManger.shared.prayerTimeCoordinates = customLocationCoordinates
                    
                }
                prayerTimeManger.delegate = self
                prayerTimeManger.getPrayerTimings()
            } else{
                // get current loation of user to fetch prayer timings
                locationHandler.delegate = self
                locationHandler.getCurrentLocation()
            }
        }
        
        //        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        //        let date = cal.dateComponents([.year, .month, .day], from: Date())
        //        var coordinates = Coordinates(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude)
        //        let formatter = DateFormatter()
        //        formatter.timeStyle = .short
        //        formatter.timeZone = TimeZone(identifier: self.getCurrentTimeZone())!
        //        if !(isLocationServicesEnabled()){
        //            locationLbl.isHidden = true
        //          //  locationEnableBtn.isHidden = false
        //            locationEnableBtn.setTitle("Click here to enable location", for: .normal)
        //            // set Mecca as default location
        //            coordinates = Coordinates(latitude: 21.422487, longitude: 39.826206)
        //            formatter.timeZone  = TimeZone(identifier: "AST")!
        //            UserDefaults.standard.setValue("Umm al-Qura University, Makkah", forKey: "calculationMethod")
        //        }
        //        else{
        //            locationLbl.isHidden = false
        //            //locationEnableBtn.isHidden = true
        //            locationEnableBtn.setTitle("", for: .normal)
        //            coordinates = Coordinates(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude)
        //            formatter.timeZone  = TimeZone(identifier: self.getCurrentTimeZone())!
        //            if UserDefaults.standard.value(forKey: "calculationMethod") == nil{
        //                UserDefaults.standard.setValue("Muslim World League", forKey: "calculationMethod")
        //            }
        //        }
        //        var params = GlobalMethods.getCalculationMethod()
        //        params.madhab = .hanafi
        //        if let prayers = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params) {
        //
        //
        //            prayerTimings = ["fajr": formatter.string(from: prayers.fajr),
        //                             "sunrise": formatter.string(from: prayers.sunrise),
        //                             "dhuhr": formatter.string(from: prayers.dhuhr),
        //                             "asr": formatter.string(from: prayers.asr),
        //                             "maghrib": formatter.string(from: prayers.maghrib),
        //                             "isha": formatter.string(from: prayers.isha),
        //            ]
        //
        //            let current = prayers.currentPrayer()
        //            if current == .isha {
        //
        //                let nextDate = cal.date(byAdding: .day, value: 1, to: Date())!
        //                let nextDate1 = cal.dateComponents([.year, .month, .day], from: nextDate)
        //                if let prayers1 = PrayerTimes(coordinates: coordinates, date: nextDate1, calculationParameters: params) {
        //
        //                    nextPrayer = prayers1.nextPrayer()!
        //                    nextPrayerTime = prayers1.time(for: nextPrayer!)
        //                    self.setPrayerData()
        //                    self.getNextPrayer()
        //                }
        //
        //            }else {
        //
        //                nextPrayer = prayers.nextPrayer()!
        //                nextPrayerTime = prayers.time(for: nextPrayer!)
        //                self.setPrayerData()
        //                self.getNextPrayer()
        //            }
        //
        //            let icon = GlobalMethods.getPrayerIcon(prayer: nextPrayer!)
        //            nextPrayerIcon.image = UIImage(named: icon)
        //
        //        }
    }
    
    //    func setPrayerData() -> Void {
    //
    //        locationLbl.text = myLocationPlacemark?.locality
    //    }
    //
    //    func getNextPrayer() -> Void {
    //
    //        if nextPrayer == .fajr {
    //
    //            nextPrayerName = "Fajr"
    //
    //        }else if nextPrayer == .dhuhr {
    //
    //            nextPrayerName = "Dhuhr"
    //
    //        }else if nextPrayer == .asr {
    //
    //            nextPrayerName = "Asr"
    //
    //        }else if nextPrayer == .maghrib {
    //
    //            nextPrayerName = "Maghrib"
    //
    //        }else if nextPrayer == .isha {
    //
    //            nextPrayerName = "Isha"
    //
    //        }
    //
    //        let time_Str = GeneralMethods.convertDateToString(date: nextPrayerTime, format: "hh:mm a")
    //        nextPrayerNameAndTimeLbl.text = String(format: "%@ %@", nextPrayerName, time_Str)
    //
    //        let diffComponents = Calendar.current.dateComponents([.hour, .minute], from: Date(), to: nextPrayerTime)
    //        let hours = diffComponents.hour ?? 1
    //        let minutes = diffComponents.minute ?? 0
    //
    //        var min_text = ""
    //        var hours_text = ""
    //        if minutes < 10 {
    //            min_text = "0\(minutes)"
    //        }else{
    //            min_text = "\(minutes)"
    //        }
    //        if hours < 10 {
    //            hours_text = "0\(hours)"
    //        }else{
    //            hours_text = "\(hours)"
    //        }
    //        let text = "\(hours_text):\(min_text) mins"
    //        nextPrayerRemaningTimeLbl.text = "Next Prayer in" + " " + text
    //
    //    }
    func loadAds() {
        if splashSingelton.sharedInstance.splashDataModel?.home_screen_banner == true{
            // Load Banner
            bannerView.adUnitID = AdManager().bannerId
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
        }
      
        //        // Load Native
        //        let options = GADMultipleAdsAdLoaderOptions()
        //        options.numberOfAds = 1
        //        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ GADSimulatorID ]
        //        adLoader = GADAdLoader(
        //            adUnitID: AdManager().nativeadUnitID, rootViewController: self,
        //            adTypes: [.native], options: [options])
        //        adLoader.delegate = self
        //        adLoader.load(GADRequest())
    }
    //    func showAd(){
    //        if  nativeAds.count > 0{
    //            nativeAddView.isHidden = false
    //            let adView = Bundle.main.loadNibNamed("NativeAdView", owner: nil, options: nil)
    //            if let nativeAdView = adView?.first as? GADNativeAdView{
    //                let nativeAd = nativeAds[0]
    //                /// Set the native ad's rootViewController to the current view controller.
    //                nativeAd.rootViewController = self
    //                nativeAdView.translatesAutoresizingMaskIntoConstraints = false
    //                nativeAdView.tag = 50
    //                nativeAddView?.addSubview(nativeAdView)
    //                let viewDictionary = ["_nativeAdView": nativeAdView]
    //                nativeAddView?.addConstraints(
    //                    NSLayoutConstraint.constraints(
    //                        withVisualFormat: "H:|[_nativeAdView]|",
    //                        options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
    //                )
    //                nativeAddView?.addConstraints(
    //                    NSLayoutConstraint.constraints(
    //                        withVisualFormat: "V:|[_nativeAdView]|",
    //                        options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
    //                )
    //                setupNativeAdView(nativeAdView: nativeAdView,nativeAd:nativeAd)
    //            }
    //        }
    //    }
    //MARK: - IBActions
    
    @IBAction func QiblaDirectionBtnTapped(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Qibla", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "QiblaDirectionController") as! QiblaDirectionController
        self.navigationController!.pushViewController(VC, animated: true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func profileBtnClicked(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "NewProfile", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "NewSettingsController") as! NewSettingsController
        self.navigationController!.pushViewController(VC, animated: true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func continueBtnClicked(_ sender: Any) {
        
        if isCollapsedShown {
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "closeListen"), object: nil, userInfo: nil)
            
        }
        if dollar.chapters.count > (chapterId - 1) &&  (chapterId - 1) >= 0{
            let chapter: Chapter = dollar.chapters[chapterId - 1] as Chapter
            dollar.setAndSaveCurrentChapter(chapter)
            
            let storyboard = QuranStoryBoard
            let VC = storyboard.instantiateViewController(withIdentifier: "QuranChapterViewController") as! QuranChapterViewController
            VC.verseIndex = verseId
            self.navigationController!.pushViewController(VC, animated: true)
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        
        
    }
    
    
    @IBAction func lastViewedBtnClicked(_ sender: Any) {
        
        let dict = lastViewed
        
//        let storyboard = UIStoryboard.init(name: "ListenNew", bundle: nil)
//        let VC = storyboard.instantiateViewController(withIdentifier: "ArtistDetailController") as! ArtistDetailController
////        VC.artistDetailViewModal.selectedArtist = ArtistModal.init(from: dict)
////        VC.selectedSegment = .None
//        self.navigationController!.pushViewController(VC, animated: true)
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if dict["disable"] as? Int == 0 || IAPRecieptValidator.sharedInstance.isSubscriptionActive == true{
//            let storyboard = UIStoryboard.init(name: "ListenNew", bundle: nil)
//            let VC = storyboard.instantiateViewController(withIdentifier: "ArtistDetailController") as! ArtistDetailController
//            self.navigationController!.pushViewController(VC, animated: true)
//            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    //        if playlistModel?.category_type?.lowercased() == "artist" {
    //            categoryId = playlistModel?.category_id
    //        }
            audioDetailModel?.parentController = self
            lastSelectedCategoryType = dict["category_type"] as? String ?? ""
            audioDetailModel?.getPlaylistDetailData(playlist_id: dict["id"] as? Int ?? 0, category_type: lastSelectedCategoryType , category_id: dict["category_id"] as? Int)
           
        } else {
            if let tabCon = tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
                subsriptionController.delegate = self
                tabCon.add(subsriptionController)
            }
        }
        
    }
    
    @IBAction func seeAllBtnClicked(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "ListenNew", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "AllArtistViewController") as! AllArtistViewController
        VC.selectedSegment = .None
        self.navigationController!.pushViewController(VC, animated: true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    @IBAction func unlockPremiumBtnTapped(_ sender: Any) {
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.delegate = self
            tabCon.add(subsriptionController)
        }
    }
    
    @IBAction func inspirationViewTapped(_ sender: Any) {
        
        
    }
    @IBAction func charityBtnTapped(_ sender: Any) {
//        if let url = URL(string: splashSingelton.sharedInstance.charityLink) {
//            UIApplication.shared.open(url)
//        }
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.delegate = self
            tabCon.add(subsriptionController)
        }
        let userData = UserDefaults.standard.value(forKey: "userinfo") as? [String:Any]
        AnalyticsManger.logEventWith(name: "track_donation", attributes: ["username": userData?["user_name"] ?? "Guest" , "devicetype" : "iOS", "email": userData?["email"] ?? "Guest", "phone": userData?["phone"] ?? "Guest","location":PrayerTimeManger.shared.selectedLocationName ?? ""])
    }
    
    
    //    override func viewDidLayoutSubviews() {
    //      super.viewDidLayoutSubviews()
    //
    //        guard let headerView = dataTableView.tableHeaderView else {
    //            return
    //          }
    //
    //        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    //
    //        if headerView.frame.size.height != size.height {
    //
    //            headerView.frame.size.height = size.height
    //            dataTableView.tableHeaderView = headerView
    //            dataTableView.layoutIfNeeded()
    //
    //        }
    //
    //    }
    
    
    func getHomeData() -> Void {
        
        let apiManager = ApiManager()
        let url = ApiUrls.homeDataApi
        
        if isFirstTime {
            
            // self.showSpinner(onView: self.view)
            isFirstTime = false
        }
        let parameter = ["latitude":myLocation.coordinate.latitude ,"longitude":myLocation.coordinate.longitude,"address": myLocationPlacemark?.locality ?? "Makkah","device_type":"ios"] as [String : Any]
        apiManager.WebService(url: url, parameter: parameter, method: .get, encoding: .queryString, { responseData in
            
            // self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                
                let apiData = responseData["data"] as? [String:Any] ?? [:]
                self.homeScreenData = apiData
                kAPPDelegate.recentlyPlayed = apiData["Recently_Played"] as? [Any]
                if GeneralMethods.isKeyPresentInUserDefaults(key: "homeData") {
                    UserDefaults.standard.value(forKey: "homeData")
                }
                UserDefaults.standard.setValue(apiData, forKey: "homeData")
                UserDefaults.standard.synchronize()
                self.setData()
                
            }else{
                self.setData()
                //GlobalMethods.showAlert(msg: responseData.value(forKey: "message") as! String)
            }
            print("success")
        }) { error in
            
            self.setData()
            //  self.removeSpinner()
            print("error")
        }
    }
    
    //MARK:- IBActions
    @IBAction func locationEnableBtnTapped(_ sender: Any) {
        if !(isLocationServicesEnabled()){
            parentController = self
            enableUsersLocationServicesAuthorization()
            //locationHandler.allowLocationServices()
        }else{
            let placeStoryboard = UIStoryboard.init(name: "Places", bundle: nil)
            let placeController = placeStoryboard.instantiateViewController(withIdentifier: "PlaceSelectionController") as! PlaceSelectionController
            placeController.delegate = self
            self.present(placeController, animated: true, completion: nil)
        }
    }
}
extension HomeViewController : locationHandlerDelegate{
    func locationUpdatedWith(coordinates: CLLocation) {
        let selectedPrayerTimesCoordinate = ["latitude": "\(coordinates.coordinate.latitude)", "longitude": "\(coordinates.coordinate.longitude)"]
        PrayerTimeManger.shared.prayerTimeCoordinates = selectedPrayerTimesCoordinate
        getTimings()
    }
    
    func locationServicesDisabledError() {
        
    }
    
    func locationUpdatedWith(address: String, city: String) {
        
    }
}
extension HomeViewController : prayerTimeDetailsDelegate{
    func prayersLocationNameDidFetched() {
        locationLbl.text = PrayerTimeManger.shared.selectedLocationName
        if homeAnalyticsHit == false{
            let userData = UserDefaults.standard.value(forKey: "userinfo") as? [String:Any]
            AnalyticsManger.logEventWith(name: "track_home", attributes: ["username": userData?["user_name"] ?? "Guest" , "devicetype" : "iOS", "email": userData?["email"] ?? "Guest", "phone": userData?["phone"] ?? "Guest","location":myLocationPlacemark?.country ?? ""])
            homeAnalyticsHit = true
        }
       
    }
    
    func prayerTimeFetchedWith(prayersData: prayerData?, nextPrayerName: String,nextPrayerTime:String, nextPrayerCountDown: String) {
        nextPrayerRemaningTimeLbl.text = nextPrayerCountDown
        nextPrayerNameAndTimeLbl.text = "\(nextPrayerName) \(nextPrayerTime)"
        switch nextPrayerName.lowercased(){
        case "fajr" :
            //nextPrayerIcon.image = UIImage(named: "fajr.png")
            nextPrayerIcon.image = UIImage(named: "qiblaInnerIcon")
            
            break
        case "dhuhr" :
           // nextPrayerIcon.image = UIImage(named: "dhuhr.png")
            nextPrayerIcon.image = UIImage(named: "qiblaInnerIcon")
            break
        case "asr" :
            //nextPrayerIcon.image = UIImage(named: "asr.png")
            nextPrayerIcon.image = UIImage(named: "qiblaInnerIcon")
            break
        case "maghrib" :
           // nextPrayerIcon.image = UIImage(named: "magrib.png")
            nextPrayerIcon.image = UIImage(named: "qiblaInnerIcon")
            break
        case "isha" :
            //nextPrayerIcon.image = UIImage(named: "isha.png")
            nextPrayerIcon.image = UIImage(named: "qiblaInnerIcon")
            break
        default:
            break
        }
        if PrayerTimeManger.shared.shouldRefreshPrayerNotifications == true{
            NotificationsHandler().scheduleAllEnabledNotifications()
        }
        if prayersData?.timings?.status == 1{
            // show ramadan card
            ramadanView.isHidden = false
            ramadanHeadingLabel.text = prayersData?.timings?.title
            seharLabel.text = prayersData?.timings?.sehar
            aftarLabel.text = prayersData?.timings?.aftar
        }else{
            ramadanView.isHidden = true
        }
    }
}
extension HomeViewController : googlePlacesCustomDelegate{
    func placesSearchCompleted() {
        
    }
    
    func coordinateFetchingCompleted() {
        //custom location selected so referesh timings
        getTimings()
    }
    
    func placeNameFetchingCompleted() {
        locationLbl.text = PrayerTimeManger.shared.selectedLocationName
    }
}
extension HomeViewController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
        
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.setPremiumViews()
            }
        })
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
            tabCon.add(subsriptionController)
        }
    }
    func subscriptionDidCancel(endDate: String){
        DispatchQueue.main.async {
            self.setPremiumViews()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let tabCon = self.tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
                subsriptionController.endDate = endDate
                tabCon.add(subsriptionController)
            }
        }
        
    }
}
//extension HomeViewController: GADNativeAdLoaderDelegate,GADNativeAdDelegate,GADVideoControllerDelegate{
//
//    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
//        nativeAds.append(nativeAd)
//        nativeAd.delegate = self
//        showAd()
//    }
//
//    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
//
//    }
//    func setupNativeAdView(nativeAdView : GADNativeAdView,nativeAd:GADNativeAd){
//        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
//        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
//
//        // Some native ads will include a video asset, while others do not. Apps can use the
//        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
//        // UI accordingly.
//        let mediaContent = nativeAd.mediaContent
//        if mediaContent.hasVideoContent {
//            // By acting as the delegate to the GADVideoController, this ViewController receives messages
//            // about events in the video lifecycle.
//            mediaContent.videoController.delegate = self
//            //  videoStatusLabel.text = "Ad contains a video asset."
//        } else {
//            //videoStatusLabel.text = "Ad does not contain a video."
//        }
//
//        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
//        // ratio of the media it displays.
//        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
//            let heightConstraint = NSLayoutConstraint(
//                item: mediaView,
//                attribute: .height,
//                relatedBy: .equal,
//                toItem: mediaView,
//                attribute: .width,
//                multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
//                constant: 0)
//            heightConstraint.isActive = true
//        }
//
//        // These assets are not guaranteed to be present. Check that they are before
//        // showing or hiding them.
//        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
//        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
//
//        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
//        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
//
//        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
//        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
//
//        // (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from: nativeAd.starRating)
//        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
//
//        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
//        nativeAdView.storeView?.isHidden = nativeAd.store == nil
//
//        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
//        nativeAdView.priceView?.isHidden = nativeAd.price == nil
//
//        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
//        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
//
//        // In order for the SDK to process touch events properly, user interaction should be disabled.
//        nativeAdView.callToActionView?.isUserInteractionEnabled = false
//
//        // Associate the native ad view with the native ad object. This is
//        // required to make the ad clickable.
//        // Note: this should always be done after populating the ad views.
//        nativeAdView.nativeAd = nativeAd
//    }
//
//}

extension HomeViewController {
    func playlistDetailFetched() {
        if audioDetailModel?.playlistDetail?.audioData?.count ?? 0 > 0 {
            let audioData = audioDetailModel?.playlistDetail?.audioData
            if audioData?.count ?? 0 == 1 {
                // play audio
                audioDetailModel?.setUpPlayerFor(selectedAudioType: .Nasheet, selectedIndex: 0, categoryType: lastSelectedCategoryType)
            } else {
                // show audio listing page
               // audioDetailModel?.playlistDetail = nil
                self.show(storyBoard: .ListenNew, controllerIdentifier: "ArtistDetailController", navigation: self.navigationController){
                    controller in
                    (controller as! ArtistDetailController).audioDetailViewModel = self.audioDetailModel
                    (controller as! ArtistDetailController).category_type = lastSelectedCategoryType
                    
                }
            }
        }
    }
    
}


extension HomeViewController: audioPlayerDelegate {
    func didAudioChanged(playingIndex: Int, isPlaying: Bool) {
    }
    func audioPlayerClosed() {
        isCollapsedShown = false
        self.setTabBarCorner()
//        DispatchQueue.main.async {
//            self.playlistTableView.reloadData()
//        }
       
    }
    func audioDidFinished() {
//        DispatchQueue.main.async {
//            self.playlistTableView.reloadData()
//        }
    }
    func audioFavouriteStatusChanged(isFavourite:Bool ,selectedType: FavouriteType , audioId: Int) {
        audioDetailModel?.setFavourite(isFavourite: isFavourite, selectedType: selectedType, audioId: audioId)
        audioDetailModel?.getFavouritesData()
    }
}
