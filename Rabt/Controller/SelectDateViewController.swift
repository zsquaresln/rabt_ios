//
//  SelectDateViewController.swift
//  Rabt
//
//  Created by Ali Sher on 27/04/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit

protocol selectDateDelegate {
    
    func didSelectDate(date: Date)
    
}

class SelectDateViewController: UIViewController {
    
    var delegate: selectDateDelegate?
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var dtPicker: UIDatePicker!
    
    
    var selectedDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 0.6)
        UIView.animate(withDuration: 0.7, animations: {
            self.contentView.frame = self.contentView.frame.offsetBy(dx: 0, dy: -200)
        })
        
        contentView.layer.cornerRadius = 16
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        dtPicker.maximumDate = Date()
        
    }
    
    @IBAction func okBtnClicked(_ sender: Any) {
        
        self.view.removeFromSuperview()
        delegate?.didSelectDate(date: selectedDate)
        
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        
        self.view.removeFromSuperview()
        
    }
    
    @IBAction func pickerValueChanged(_ sender: Any) {
        
        selectedDate = dtPicker.date
        
    }
}
