//
//  SplashDataModel.swift
//  Rabt
//
//  Created by Asad Khan on 20/01/2023.
//  Copyright © 2023 Ali Sher. All rights reserved.
//

import UIKit

class SplashDataModel: NSObject {
    var artist_screen_scroll_native : Bool?
    var audio_screen_banner : Bool?
    var home_screen_banner : Bool?
    var listen_screen_banner : Bool?
    var listen_screen_books_favorite_native : Bool?
    var listen_screen_books_nasheet_native : Bool?
    var listen_screen_books_native : Bool?
    var listen_screen_books_scroll_native : Bool?
    var listen_screen_favorite_banner : Bool?
    var listen_screen_favorite_native : Bool?
    var listen_screen_native : Bool?
    var listen_screen_recommended_banner : Bool?
    var listen_screen_recommended_native : Bool?
    var prayer_knowledge_banner : Bool?
    var prayer_screen_banner : Bool?
    var prayer_screen_native : Bool?
    var prayer_scroll_native : Bool?
    
    init(ads_screens: [String:Any]?){
        artist_screen_scroll_native = ads_screens?["artist_screen_scroll_native"] as? Bool
        audio_screen_banner = ads_screens?["audio_screen_banner"] as? Bool
        home_screen_banner = ads_screens?["home_screen_banner"] as? Bool
        listen_screen_banner = ads_screens?["listen_screen_banner"] as? Bool
        listen_screen_books_favorite_native = ads_screens?["listen_screen_books_favorite_native"] as? Bool
        listen_screen_books_nasheet_native = ads_screens?["listen_screen_books_nasheet_native"] as? Bool
        listen_screen_books_native = ads_screens?["listen_screen_books_native"] as? Bool
        listen_screen_books_scroll_native = ads_screens?["listen_screen_books_scroll_native"] as? Bool
        listen_screen_favorite_banner = ads_screens?["listen_screen_favorite_banner"] as? Bool
        listen_screen_favorite_native = ads_screens?["listen_screen_favorite_native"] as? Bool
        listen_screen_native = ads_screens?["listen_screen_native"] as? Bool
        listen_screen_recommended_banner = ads_screens?["listen_screen_recommended_banner"] as? Bool
        listen_screen_recommended_native = ads_screens?["listen_screen_recommended_native"] as? Bool
        prayer_knowledge_banner = ads_screens?["prayer_knowledge_banner"] as? Bool
        prayer_screen_banner = ads_screens?["prayer_screen_banner"] as? Bool
        prayer_screen_native = ads_screens?["prayer_screen_native"] as? Bool
        prayer_scroll_native = ads_screens?["prayer_scroll_native"] as? Bool
        
    }
    
}

