//
//  SplashViewController.swift
//  OnlineMaid
//
//  Created by Ali Sher on 25/06/2020.
//  Copyright © 2020 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData
import ProgressHUD
class splashSingelton{
    static let sharedInstance = splashSingelton()
    var appstoreVersion : String = ""
    var isForceUpdate : Bool = false
    var updateMessage : String = ""
    var charityLink : String = ""
    
    //   var adsMessage : String = ""
    var premiumConfig : [String:Any] = [:]
    var freemiumConfig : [String:Any] = [:]
    
    var url_live_stream : String = ""
    var charity_image : String = ""
    var listen_charity_image : String = ""
    var adsMessage : String = ""
    var adRewardedDays : Int = 1
    var isOtpEnabled : Bool?
    var splashDataModel : SplashDataModel?
    
  
    func disableAllAds() {
        self.splashDataModel?.artist_screen_scroll_native = false
        self.splashDataModel?.audio_screen_banner = false
        self.splashDataModel?.home_screen_banner = false
        self.splashDataModel?.listen_screen_banner = false
        self.splashDataModel?.listen_screen_books_favorite_native = false
        self.splashDataModel?.listen_screen_books_nasheet_native = false
        self.splashDataModel?.listen_screen_books_native = false
        self.splashDataModel?.listen_screen_books_scroll_native = false
        self.splashDataModel?.listen_screen_favorite_banner = false
        self.splashDataModel?.listen_screen_favorite_native = false
        self.splashDataModel?.listen_screen_native = false
        self.splashDataModel?.listen_screen_recommended_banner = false
        self.splashDataModel?.listen_screen_recommended_native = false
        self.splashDataModel?.prayer_knowledge_banner = false
        self.splashDataModel?.prayer_screen_banner = false
        self.splashDataModel?.prayer_screen_native = false
        self.splashDataModel?.prayer_scroll_native = false
    }
}
class SplashViewController: MyLocationManagerViewController {
    
    @IBOutlet weak var logoIcon: UIImageView!
    // Gif handling
    var isGifCompletelyLoaded: Bool = false
    var isSplashFetched: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RabtPremiumProduct.store.restorePurchases()
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {isPurchaseSchemeActive,error in
            //self.perform(#selector(self.getSplashData), with: nil, afterDelay: 0)
            self.getSplashData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        let url = Bundle.main.url(forResource: "splashAnimation", withExtension: "gif")

                DispatchQueue.main.async {

                    self.logoIcon.sd_setImage(with: url!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                        self.logoIcon.image = UIImage(named: "splashGreen")
                        self.isGifCompletelyLoaded = true
                        if self.isSplashFetched {
                           // move to home
                            self.MoveToView()
                        }
                    })
                }
    }
    
    @objc func getSplashData(){
        
        if GeneralMethods.CheckInternetConnection() {
            let apiManager = ApiManager()
            let url = ApiUrls.getSplashDataApi
            // self.showSpinner(onView: self.view)
            
            apiManager.WebService(url: url, parameter: nil, method: .get, encoding: .queryString, { responseData in
                self.isSplashFetched = true
                // self.removeSpinner()
                let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
                if result == 1{
                    if let splashData = responseData["data"] as? [String:Any]{
                        splashSingelton.sharedInstance.appstoreVersion = splashData["version_code_ios"] as? String ?? ""
                        splashSingelton.sharedInstance.updateMessage = splashData["message"] as? String ?? ""
                       // splashSingelton.sharedInstance.adsMessage = splashData["reward_message"] as? String ?? ""
                        if splashData["version_force_ios"] as? String  == "1"{
                            splashSingelton.sharedInstance.isForceUpdate = true
                        }else if splashData["version_force_ios"] as? String  == "0"{
                            splashSingelton.sharedInstance.isForceUpdate = false
                        }
//                        if let charityLink = splashData["url_link_charity"] as? String ,
                        if let charityImage =  splashData["charity_image"] as? String{
                            splashSingelton.sharedInstance.charity_image = charityImage
                            //splashSingelton.sharedInstance.charityLink = charityLink
                        }
                        if let listenCharityImage =  splashData["listen_charity_image"] as? String{
                            splashSingelton.sharedInstance.listen_charity_image = listenCharityImage
                            //splashSingelton.sharedInstance.charityLink = charityLink
                        }
                        if let premiumConfig = splashData["premium_array_v1"] as? [String:Any]{
                            splashSingelton.sharedInstance.premiumConfig = premiumConfig
                        }
                        if let freemiumConfig = splashData["fremium_array_v1"] as? [String:Any]{
                            splashSingelton.sharedInstance.freemiumConfig = freemiumConfig
                        }
                        if let streamUrl = splashData["url_live_stream"] as? String{
                            splashSingelton.sharedInstance.url_live_stream = streamUrl
                        }

                        
                        splashSingelton.sharedInstance.adRewardedDays = splashData["expire_time"] as? Int ?? 5
                        splashSingelton.sharedInstance.splashDataModel = SplashDataModel.init(ads_screens: splashData["ads_screens"] as? [String:Any])
                    }
                }
                if self.isGifCompletelyLoaded {
                    DispatchQueue.main.async {
                        self.MoveToView()
                    }
                }
                print("success")
            }) { error in
                self.isSplashFetched = true
                if self.isGifCompletelyLoaded {
                    DispatchQueue.main.async {
                        self.MoveToView()
                    }
                }
                print("error")
            }
        }
        else{
            DispatchQueue.main.async {
                let storyboard = AuthenticationStoryBoard
                let VC = storyboard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
                self.navigationController!.pushViewController(VC, animated: false)
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            }
        }
    }
    
    @objc func MoveToView() {
        
        let userinfo = UserDefaults.standard.value(forKey: "userinfo")
        
        if userinfo != nil {
            
            MoveToHome()
            //            if (userinfo as! [String:Any])["guest_user"] as? Int ?? 0 == 0{
            //                AnalyticsManger.logEventWith(name: "Guest_Login", attributes: ["device_type": "ios",
            //                                                                               "device_id" : UUID().uuidString])
            //            }
            //            else{
            //                AnalyticsManger.logEventWith(name: "User_Login", attributes: ["device_type": "ios",
            //                                                                              "phone": (userinfo as! [String:Any])["phone"] as? String ?? "",
            //                                                                              "name" : (userinfo as! [String:Any])["name"] as? String ?? "","device_id" : UUID().uuidString])
            //            }
        }else{
            
            let storyboard = AuthenticationStoryBoard
            let VC = storyboard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
            self.navigationController!.pushViewController(VC, animated: false)
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            
        }
    }
    
    func MoveToHome() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = storyboard.instantiateViewController(withIdentifier: "myTabBar") as! UITabBarController
            self.revealViewController().pushFrontViewController(VC, animated: true)
        }
    }
}

extension UITextField {
    
    func setTextField() {
        let attributeString = [
            NSAttributedString.Key.foregroundColor: GlobalVariables.LightGrayColor,
            NSAttributedString.Key.font: self.font!
        ] as [NSAttributedString.Key : Any]
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: attributeString)
        self.textColor = .black
    }
    
    func setDarkTextField() {
        let attributeString = [
            NSAttributedString.Key.foregroundColor: GlobalVariables.PlaceHolderColor,
            NSAttributedString.Key.font: self.font!
        ] as [NSAttributedString.Key : Any]
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: attributeString)
        self.textColor = .white
    }
    
    var isEmpty: Bool {
        return text?.isEmpty ?? true
    }
    
}

var vSpinner : UIView?

extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.3, green: 0.3, blue: 0.3, alpha: 0.5)
        
        var x = self.view.frame.size.width/2
        x -= 50
        
        var y = self.view.frame.size.height/2
        y -= 50
        
        
        
        let spinnerView1 = UIView.init(frame: CGRect(x: x, y: y, width: 100, height: 100))
        spinnerView1.backgroundColor = .black
        spinnerView1.layer.cornerRadius = 16
        
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.frame = CGRect(x: 25, y: 16, width: 50, height: 50)
        spinnerView1.addSubview(ai)
        
        let textLabel = UILabel.init(frame: CGRect(x: 5, y: 60, width: 90, height: 40))
        textLabel.backgroundColor = .clear
        textLabel.textColor = .white
        spinnerView1.layer.cornerRadius = 16
        textLabel.text = "Please Wait"
        textLabel.textAlignment = .center
        textLabel.numberOfLines = 2
        textLabel.font = UIFont.boldSystemFont(ofSize: 13)
        spinnerView1.addSubview(textLabel)
        
        //DispatchQueue.main.async {
        spinnerView.addSubview(spinnerView1)
        onView.addSubview(spinnerView)
        //}
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        //DispatchQueue.main.async {
        vSpinner?.removeFromSuperview()
        vSpinner = nil
        //}
    }
    

}
