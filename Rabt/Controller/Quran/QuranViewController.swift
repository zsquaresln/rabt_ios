//
//  QuranViewController.swift
//  Rabt
//
//  Created by Muhammad on 30/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData
import StickyTabBarViewController

class QuranViewController: UIViewController {
    
    

    @IBOutlet weak var lastReadHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var typesCollectionView: UICollectionView!
    @IBOutlet weak var listCollectionView: UICollectionView!
    @IBOutlet weak var loadingView: LoadingOrNoRecordView!
    @IBOutlet weak var latestReadLbl: UILabel!
    @IBOutlet weak var latestReadView: UIView!
    @IBOutlet weak var donateNowView: UIView!
    @IBOutlet weak var donateNowHeightConstraint: NSLayoutConstraint!
    //@IBOutlet weak var latestReadImgView: UIImageView!
//    @IBOutlet weak var chaptersTableView: UITableView!
    
    
    var typesArray = ["All".localized(), "My Favourite".localized(), "Reciters".localized(), "Recommended".localized()]
    var selectedType = "All".localized()
    
    var favourites : [Any] = []
    var allReciters: Array<Reciter>!
    var recommendedChaptersArray = [Chapter]()
    var favTapped = false
    var chapterId = 0
    var verseId = 0
    var homeScreenData: [String: Any]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        typesCollectionView.backgroundColor = .clear
        typesCollectionView.delegate = self
        typesCollectionView.dataSource = self
        typesCollectionView.register(UINib(nibName: "TypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "typeCell")
        listCollectionView.register(UINib(nibName: "RecitersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RecitersCollectionViewCell")
        listCollectionView.register(UINib(nibName: "SurahsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SurahsCollectionViewCell")
        
        var arr : [Any] = []
        
        if GeneralMethods.isKeyPresentInUserDefaults(key: "favourites") {
            
            arr = UserDefaults.standard.value(forKey: "favourites") as? [Any] ?? []
            
        }
        favourites = arr
        if self.selectedType == "My Favourite"{
            if favourites.count == 0{
                loadingView.msgLbl.textColor = .white
                loadingView.msgLbl.text = "No Favourites Yet!\nBuild your own Favourite list by clicking Heart button next to each item."
                self.loadingView.isHidden = false
            }else{
                self.loadingView.isHidden = true
            }
           
        }else{
            self.loadingView.isHidden = true
        }
        
        self.allReciters = dollar.reciters
        
        listCollectionView.backgroundColor = .clear
        listCollectionView.delegate = self
        listCollectionView.dataSource = self
        
     //   setTextandDesign()
        perform(#selector(forReload), with: nil, afterDelay: 0.1)
    }
    
//    func setTextandDesign() -> Void {
//
//        GeneralMethods.ChangeImgViewTintColor(imgView: quranIcon, color: .white)
//    }
    
    @objc func forReload() -> Void{
        
        typesCollectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        singeltonClass.shared.naviagtionController = self.navigationController
     //   AnalyticsManger.logEventWith(name: "Screen_View", attributes: ["source": "\(selectedType.lowercased()) surah screen"])
        self.tabBarController?.tabBar.isHidden = false
        //self.closeListner()
        let userData = UserDefaults.standard.value(forKey: "userinfo") as? [String:Any]
        AnalyticsManger.logEventWith(name: "track_quran", attributes: ["username": userData?["user_name"] ?? "Guest" , "devicetype" : "iOS", "email": userData?["email"] ?? "Guest", "phone": userData?["phone"] ?? "Guest","location":PrayerTimeManger.shared.selectedLocationName ?? ""])
        
        setLastReadData()
        getHomeData()
        setPremiumViews()
    }
    
    
    //MARK: - Helper Functions
    func setPremiumViews(){
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            // do nothing
            DispatchQueue.main.async{
                self.donateNowView.isHidden = false
                    self.donateNowHeightConstraint.constant = 50
            }
        }else{
           
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip && isAutoRenewable{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                }
            }else if isVip{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                }
            }
            else{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = false
                        self.donateNowHeightConstraint.constant = 50
                }
            }
        }
    }
    func setLastReadData() {
        if GeneralMethods.isKeyPresentInUserDefaults(key: "homeData") {
            
             homeScreenData = UserDefaults.standard.value(forKey: "homeData") as? [String:Any] ?? [:]
            if let quran = homeScreenData["Quran"] as? [String:Any] {
                lastReadHeightConstraint.constant = 132
                latestReadView.isHidden = false
                
                let chapterImageQuranArray = HomeViewController.quranImages.filter{
                    String(($0["position_id"] as? Int ?? 0))  == quran["chapter_no"] as? String ?? ""
                    
                }
                if chapterImageQuranArray.count > 0{
                    let url_str = (chapterImageQuranArray.first?["image"] ?? "") as? String ?? ""
                    // let url = URL(string: url_str)
                    //latestReadImgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
                }
                
                
                latestReadLbl.text = quran["text"] as? String
                
                chapterId = Int(quran["chapter_no"] as? String ?? "0") ?? 0
                verseId = Int(quran["verses_read"] as? String ?? "0") ?? 0
                
                
            }else {
                
                latestReadView.isHidden = true
                lastReadHeightConstraint.constant = 0
            }
        }
    }
    // MARK: - API Methods
    func getHomeData() -> Void {
        
        let apiManager = ApiManager()
        let url = ApiUrls.homeDataApi
        
       
        let parameter = ["latitude":myLocation.coordinate.latitude ,"longitude":myLocation.coordinate.longitude,"address": myLocationPlacemark?.locality ?? "Makkah","device_type":"ios"] as [String : Any]
        apiManager.WebService(url: url, parameter: parameter, method: .get, encoding: .queryString, { responseData in
            
            // self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                
                let apiData = responseData["data"] as? [String:Any] ?? [:]
                self.homeScreenData = apiData
                kAPPDelegate.recentlyPlayed = apiData["Recently_Played"] as? [Any]
                if GeneralMethods.isKeyPresentInUserDefaults(key: "homeData") {
                    UserDefaults.standard.value(forKey: "homeData")
                }
                UserDefaults.standard.setValue(apiData, forKey: "homeData")
                UserDefaults.standard.synchronize()
                self.setLastReadData()
                
            }else{
                self.setLastReadData()
                //GlobalMethods.showAlert(msg: responseData.value(forKey: "message") as! String)
            }
            print("success")
        }) { error in
            
            self.setLastReadData()
            //  self.removeSpinner()
            print("error")
        }
    }
 
    //MARK: - IBActions
    @IBAction func unlockPremiumBtnTapped(_ sender: Any) {
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.delegate = self
            tabCon.add(subsriptionController)
        }
    }
    @IBAction func continueBtnClicked(_ sender: Any) {
        
        if isCollapsedShown {
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "closeListen"), object: nil, userInfo: nil)
            
        }
        if dollar.chapters.count > (chapterId - 1) &&  (chapterId - 1) >= 0{
            let chapter: Chapter = dollar.chapters[chapterId - 1] as Chapter
            dollar.setAndSaveCurrentChapter(chapter)
            
            let storyboard = QuranStoryBoard
            let VC = storyboard.instantiateViewController(withIdentifier: "QuranChapterViewController") as! QuranChapterViewController
            VC.verseIndex = verseId
            self.navigationController!.pushViewController(VC, animated: true)
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        
        
    }
}
extension QuranViewController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
        
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.setPremiumViews()
            }
        })
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
            tabCon.add(subsriptionController)
        }
    }
    func subscriptionDidCancel(endDate: String){
        DispatchQueue.main.async {
            self.setPremiumViews()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let tabCon = self.tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
                subsriptionController.endDate = endDate
                tabCon.add(subsriptionController)
            }
        }
        
    }
}
