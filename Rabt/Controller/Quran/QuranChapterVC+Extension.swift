//
//  QuranChapterVC+Extension.swift
//  Rabt
//
//  Created by Muhammad on 29/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData

extension QuranChapterViewController {
    
    @objc func addOrRemoveClicked(sender: UIButton) -> Void {
        
        let verse = dollar.currentChapter.verses[sender.tag]
        
        if BookmarkService.sharedInstance().has(verse) {
            
            BookmarkService.sharedInstance().remove(verse)
        }else {
            
            BookmarkService.sharedInstance().add(verse)
        }
        
        dataTableView.reloadData()
    }
    
    fileprivate func contentForCell (_ verse: Verse, cell: AyaatTableViewCell) {
        //"﴾﴿"
        let numbers = ""//"\(verse.chapterId + 1):\(verse.id)"
        cell.arabicTextLbl.text = !dollar.showTranslation ? verse.translation != "" ? "\(numbers)  \(verse.arabic)" : verse.arabic : verse.arabic
        cell.englishTextLbl.text = dollar.showTransliteration ?  verse.transcription : ""
        cell.TafseerLbl.text = dollar.showTranslation ? verse.translation != "" ? "\(numbers) \(verse.translation)" : "" : ""
        
        cell.ayaatNumberLbl.text = "- \(verse.id)"
        
    }
}


extension QuranChapterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dollar.currentChapter.verses.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("AyaatTableViewCell", owner: self, options: nil)?.first as! AyaatTableViewCell

        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        
        GeneralMethods.CreateCardViewWithoutShadow(radius: 16, view: cell.contantView)
        let verse = dollar.currentChapter.verses[indexPath.row]
        //set up the font
        cell.arabicTextLbl.font = currentArabicFont
        
        if verse.chapterId == 0 {
            
            cell.ayaatNumberLbl.isHidden = false
            
        }else {
            
            cell.view2.isHidden = false
            cell.view3.isHidden = false
            cell.ayaatNumberLbl.isHidden = false
            
            if indexPath.row == 0 {
                
                cell.view2.isHidden = true
                cell.view3.isHidden = true
                cell.ayaatNumberLbl.isHidden = true
                cell.ayaatNumberLbl.text = ""
                
            }
        }
        cell.favouriteBtn.isHidden = true
        if indexPath.row == 0 {
            
            //cell.favouriteBtn.isHidden = true
            
        }else {
            
           // cell.favouriteBtn.isHidden = false
            
            if BookmarkService.sharedInstance().has(verse) {
                //colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
                GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.AppColor)
            }
            else{
                GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.LightGrayColor)
            }
        }
        cell.favouriteBtn.backgroundColor = .clear
        
        cell.favouriteBtn.tag = indexPath.row
        cell.favouriteBtn.addTarget(self, action: #selector(addOrRemoveClicked(sender:)), for: .touchUpInside)
        
        
        //set up the content
        contentForCell(verse, cell:cell)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if currentAudioChapter.isDownloaded {
            
            let verse = dollar.currentChapter.verses[indexPath.row]
            self.service.setPlayVerse(verse)
            self.service.isFullRepeat = false
            self.service.fullRepeatCount = 0
            self.service.play(verse)
            self.updateControls()
            
        }else if currentAudioChapter.isDownloading {
            
            self.updateControls()
        }else{
            
            downloadClickedHandler()
        }
    }
}
