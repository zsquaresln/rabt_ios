//
//  QuranChapterViewController.swift
//  Rabt
//
//  Created by Muhammad on 05/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData

class QuranChapterViewController: UIViewController {
    
 //   @IBOutlet weak var quranIcon: UIImageView!
    
    @IBOutlet weak var progress: UIProgressView!
    @objc var activityIndicatorView: UIActivityIndicatorView!
    
    
    @IBOutlet weak var spinerView: UIView!
    @IBOutlet weak var spiner: UIActivityIndicatorView!
    
    @IBOutlet weak var chapterNameView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var meaningLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var previousBtn: UIButton!
    @IBOutlet weak var donateNowView: UIView!
    @IBOutlet weak var donateNowHeightConstraint: NSLayoutConstraint!
    
    
    @objc var currentArabicFont: UIFont!
    @objc var currentLatinFont: UIFont!

    @objc var isScrolling: Bool = false
    @objc var currentVerseIndex: Int = 0
    var currentAudioChapter:AudioChapter!
    
    
    @objc let service: AudioService = AudioService.sharedInstance()
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var dataTableView: UITableView!
    
    
    @IBOutlet weak var playOrPauseBtn: UIButton!
    
    var verseIndex = 1
    var isPaused = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   AnalyticsManger.logEventWith(name: "Surah_Opened", attributes: ["surah_id": dollar.currentChapter.id ,"surah_name":dollar.currentChapter.name,"reciter":dollar.currentReciter.name] )
        //self.view.bringSubviewToFront(dataTableView)
        
        dataTableView.backgroundColor = .clear
        dataTableView.delegate = self
        dataTableView.dataSource = self
        
        service.initDelegation(self)
        
        currentAudioChapter = dollar.currentReciter.audioChapters[dollar.currentChapter.id]
        activityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicatorView.startAnimating()
        progress.setProgress(0, animated: false)
        
        self.registerNotification()
        
        if playerViewBottom != nil {
            playerViewBottom.isHidden = true
        }
        
        setNextAndBackIcons()
        
        currentArabicFont = UIFont.arabicFont()
        currentLatinFont = UIFont.latin()
        
        setTextAndDesgin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setPremiumViews()
        
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        progress.isHidden = true
        spinerView.isHidden = true
        spiner.stopAnimating()
        currentAudioChapter.isDownloading = false
        self.stopDownloading(currentAudioChapter)
    }
    
    //MARK: - Helper Functions
    func setPremiumViews(){
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            // do nothing
            DispatchQueue.main.async{
                self.donateNowView.isHidden = false
                    self.donateNowHeightConstraint.constant = 50
            }
        }else{
           
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip && isAutoRenewable{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                }
            }else if isVip{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                }
            }
            else{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = false
                        self.donateNowHeightConstraint.constant = 50
                }
            }
        }
    }
    func setTextAndDesgin() -> Void {
        
     //   GeneralMethods.ChangeImgViewTintColor(imgView: quranIcon, color: .white)
        
        GeneralMethods.CreateCardViewWithoutBackColor(radius: spinerView.frame.size.height/2, view: spinerView)
        spinerView.clipsToBounds = true
        spinerView.isHidden = true
        
    }
    
    @objc func registerNotification() {
        let notifier: NotificationCenter = NotificationCenter.default
        notifier.addObserver(self, selector: #selector(newChapterSelectedHandler(_:)), name:Notification.Name(rawValue: kNewChapterSelectedNotification), object: nil)
//        notifier.addObserver(self, selector: #selector(newVerseSelectedHandler(_:)), name:NSNotification.Name(rawValue: kNewVerseSelectedNotification), object: nil)
        notifier.addObserver(self, selector: #selector(progressUpdatedHandler(_:)), name:Notification.Name(rawValue: kProgressUpdatedNotification), object: nil)
        notifier.addObserver(self, selector: #selector(downloadStartedHandler(_:)), name:Notification.Name(rawValue: kDownloadStartedNotification), object: nil)
        notifier.addObserver(self, selector: #selector(downloadCompleteHandler(_:)), name:Notification.Name(rawValue: kDownloadCompleteNotification), object: nil)
        notifier.addObserver(self, selector: #selector(downloadErrorHandler(_:)), name:Notification.Name(rawValue: kDownloadErrorNotification), object: nil)
        notifier.addObserver(self, selector: #selector(downloadCancelHandler(_:)), name:Notification.Name(rawValue: kDownloadCancelNotification), object: nil)
        notifier.addObserver(self, selector: #selector(downloadCancelAllHandler(_:)), name:Notification.Name(rawValue: kDownloadCancelAllNotification), object: nil)
        
    }
    
    @objc func updatePlayControls(){
        
        if service.isPlaying()  {
            playOrPauseBtn.imageView?.contentMode = .scaleAspectFill
            playOrPauseBtn.setImage(UIImage(named: "Pause"), for: .normal)
           
            
        }else {
            playOrPauseBtn.imageView?.contentMode = .scaleToFill
            playOrPauseBtn.setImage(UIImage(named: "Play"), for: .normal)
            
        }
    }
    
    @objc func updateControls() {
        
        // when plying
        if (service.isPlaying() || service.isPaused == true ) {
            updatePlayControls()
            return
        }
        
        spinerView.isHidden = false
        spiner.startAnimating()
        
        if currentAudioChapter != nil && currentAudioChapter.isDownloading {
            
            progress.isHidden = false
            progress.trackTintColor = UIColor.white
            progress.setProgress(currentAudioChapter.downloadProgress, animated: false)
        }else{
            
            progress.isHidden = true
            spinerView.isHidden = true
            spiner.stopAnimating()
        }
    }
    
    
    @objc func showAudioControlsHandler (){
        
        service.play()
        updatePlayControls()
    }
    
    @objc func downloadClickedHandler (){
        
        func handler () {
            self.startDownload(currentAudioChapter) { () -> Void in
                self.updateControls()
            }
        }
        if isPro {
            handler()
        }
        else{
            if currentAudioChapter.id == 0 || currentAudioChapter.id == 1 {
                handler()
            }
            else{
                self.askUserForPurchasingProVersion(FlurryEvent.downloadFromChapter)
            }
        }
    }
    
    
    //MARK: Next Or Previous Chapter
    @IBAction func nextBtnClicked(_ sender: Any) {
        
        service.stopAndReset()
        
        let chapter = dollar.currentChapter
        var idd = chapter?.id ?? 0
        idd += 1
        
        let chapter1: Chapter = dollar.chapters[idd] as Chapter
        dollar.setAndSaveCurrentChapter(chapter1)
        
        setNextAndBackIcons()
        self.updatePlayControls()
        
        spinerView.isHidden = true
        progress.isHidden = true
        
        currentAudioChapter.downloadTask?.cancel()
        
        dataTableView.reloadData()
        
    }
    
    @IBAction func previousBtnClicked(_ sender: Any) {
        
        service.stopAndReset()
        
        let chapter = dollar.currentChapter
        var idd = chapter?.id ?? 0
        idd -= 1
        
        let chapter1: Chapter = dollar.chapters[idd] as Chapter
        dollar.setAndSaveCurrentChapter(chapter1)
        
        setNextAndBackIcons()
        self.updatePlayControls()
        
        spinerView.isHidden = true
        progress.isHidden = true
        
        currentAudioChapter.downloadTask?.cancel()
        
        dataTableView.reloadData()
    }
    
    func setNextAndBackIcons() -> Void {
        
        currentAudioChapter = dollar.currentReciter.audioChapters[dollar.currentChapter.id]
        let chapter = dollar.currentChapter
        
        nameLbl.text = chapter?.name
        meaningLbl.text = chapter?.english_name
        
        previousBtn.isUserInteractionEnabled = true
        previousBtn.alpha = 1.0
        
        nextBtn.isUserInteractionEnabled = true
        nextBtn.alpha = 1.0
        
        if chapter?.id == 0 {
            
            previousBtn.isUserInteractionEnabled = false
            previousBtn.alpha = 0.5
            
        }else if chapter?.id == 113 {
            
            nextBtn.isUserInteractionEnabled = false
            nextBtn.alpha = 0.5
        }
    }
    
    //MARK: Back Clicked
    @IBAction func unlockPremiumBtnTapped(_ sender: Any) {
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.delegate = self
            tabCon.add(subsriptionController)
        }
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        
        service.pausePlaying()
        service.isPaused = true
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: Play or Pause Clicked
    @IBAction func playOrPauseBtnClicked(_ sender: Any) {
        
        if service.isPlaying() {
            
            isPaused = true
            service.pausePlaying()
            self.updatePlayControls()
            
        }else {
            
            if currentAudioChapter.isDownloaded {
                
                if isPaused {
                    
                    service.resumePlaying()
                    
                }else {
                    
                    if verseIndex >= dollar.currentChapter.verses.count{
                        
                        verseIndex = 0
                    }
                    
                    let verse = dollar.currentChapter.verses[verseIndex]
                    self.service.setPlayVerse(verse)
                    self.service.isFullRepeat = false
                    self.service.fullRepeatCount = 0
                    self.service.play(verse)
                }
                
                self.updatePlayControls()
                
            }else if currentAudioChapter.isDownloading {
                
                self.updateControls()
                
                
            }else{
                
                downloadClickedHandler()
                
            }
        }
    }
    
    
    @objc func highlightText(_ searchText: String, inLabel label: UILabel){
        //First, get a mutable copy of the label's attributedText.
        let attributedText = label.attributedText!.mutableCopy() as! NSMutableAttributedString
        //Then create an NSRange for the entire length of the text, and remove any background color text attributes that already exist within it.
        let attributedTextRange = NSMakeRange(0, attributedText.length)
        attributedText.removeAttribute(NSAttributedString.Key.backgroundColor, range: attributedTextRange)
        //As with find and replace, next create a regular expression using your convenience initializer and fetch an array of all matches for the regular expression within the label’s text.
        do {
            let regex = try NSRegularExpression(pattern: searchText, options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, (label.text!).count)
            let matches = regex.matches(in: label.text!, options: [], range: range)
            //Loop through each match (casting them as NSTextCheckingResult objects), and add a yellow colour background attribute for each one.
            for match in matches as [NSTextCheckingResult] {
                let matchRange = match.range
                
                attributedText.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.yellow, range: matchRange)
            }
        } catch _ {
        }
        //Finally, update the UITextView with the highlighted results.
        label.attributedText = attributedText.copy() as? NSAttributedString
    }
    
    func updateQuran(ayat: Int, chapter: Int , chapterName:String) -> Void {
        
        let parameter = ["chapter_no": String(chapter + 1),
                         "total_verses": String(dollar.currentChapter.verses.count),
                         "verses_read": String(ayat),
                         "surah_name" : chapterName
        ]
        
        let apiManager = ApiManager()
        let url = ApiUrls.quranReadApi
        
        print(parameter)
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            
        //    self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                
                
                
                
            }else{
                
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
            }
            print("success")
            
        }) { error in
         //   self.removeSpinner()
            print("error")
        }
    }
}


extension QuranChapterViewController: AudioDelegate {
    
    func playNextChapter() {
        
        isPaused = false
        service.stopPlaying()
        self.updatePlayControls()
        
        let verseId = dollar.currentChapter.verses.count - 1
        let verse = dollar.currentChapter.verses[verseId]
        let chapterName = dollar.currentChapter.name
        if let row: Int = dollar.currentChapter.verses.firstIndex(of: verse) {
            
            updateQuran(ayat: row + 1, chapter: verse.chapterId ,chapterName: chapterName)
        }
        
    }
    
    func scrollToVerse(_ verseId: Int, searchText: String?) {
        
        dataTableView.reloadData()
        
        if verseId < dollar.currentChapter.verses.count {
            let verse = dollar.currentChapter.verses[verseId]
            if let row: Int = dollar.currentChapter.verses.firstIndex(of: verse) {
                let indexPath: IndexPath = IndexPath(row: row, section: 0)
                //tableView.scrollToNearestSelectedRowAtScrollPosition(UITableViewScrollPosition.Bottom, animated: true)
                dataTableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: false)
                if searchText != "" && searchText != nil {
                    if let cell: AyaatTableViewCell  = dataTableView.cellForRow(at: indexPath) as? AyaatTableViewCell{
                        var label: UILabel!
                        //skip highlighting arabic texts
                        if dollar.searchOption != SearchOption.searchOptionArabic {
                            if dollar.searchOption == SearchOption.searchOptionTrasliteration {
                                label = cell.englishTextLbl
                            }
                            else if dollar.searchOption == SearchOption.searchOptionTraslation {
                                label = cell.TafseerLbl
                            }
                            highlightText(searchText!, inLabel: label)
                        }
                    }
                }else{
                     //change background to yellow for highlight
                    if let cell: AyaatTableViewCell  = dataTableView.cellForRow(at: indexPath) as? AyaatTableViewCell{
                        //cell.arabicTextLbl.textColor = GlobalVariables.AppColor
                    }
                    let chapterName = dollar.currentChapter.name
                    updateQuran(ayat: row, chapter: verse.chapterId,chapterName: chapterName)
                }
            }
        }
    }
}


extension QuranChapterViewController {
    
    @objc func progressUpdatedHandler(_ notification: Notification){
        //Action take on Notification
        if let notifChapter: AudioChapter = notification.userInfo!["audiChapter"] as? AudioChapter {
            if notifChapter.id == currentAudioChapter.id {
                progress.setProgress(notifChapter.downloadProgress, animated: true)
            }
        }
    }
    
    @objc func downloadStartedHandler(_ notification: Notification){
        if let notifChapter: AudioChapter = notification.userInfo!["audiChapter"] as? AudioChapter {
            if notifChapter.id == currentAudioChapter.id {
                spinerView.isHidden = false
                self.playOrPauseBtn.isHidden = true
                self.updateControls()
            }
        }
    }
    
    @objc func downloadCompleteHandler(_ notification: Notification){
        if let notifChapter: AudioChapter = notification.userInfo!["audiChapter"] as? AudioChapter {
            if notifChapter.id == currentAudioChapter.id {
                self.playOrPauseBtn.isHidden = false
                spinerView.isHidden = true
                service.play()
                self.updateControls()
                print("Surah Downloaded")
            }
        }
    }
    
    @objc func downloadCancelHandler (_ notification: Notification){
        if let notifChapter: AudioChapter = notification.userInfo!["audiChapter"] as? AudioChapter {
            if notifChapter.id == currentAudioChapter.id {
                self.updateControls()
            }
        }
    }
    
    @objc func downloadCancelAllHandler (_ notification: Notification){
        self.updateControls()
    }

    
    //todo, move this and the downloadview version to a global version
    @objc func downloadErrorHandler(_ notification: Notification){
        self.updateControls()
        
    }
    
    @objc func newChapterSelectedHandler(_ notification: Notification){
        // sets the title
         if let userInfo = notification.userInfo {
            if let chapter: Chapter = userInfo["chapter"] as? Chapter {
                if chapter.id != currentAudioChapter.id {
                    if self.service.isPlaying() {
                        self.service.stopAndReset()
                    }
                    self.service.isPaused = false
                }
            }
        }
        //self.delegate?.toggleChaptersPanel!()
    }
    
    
}

extension QuranChapterViewController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
        
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.setPremiumViews()
            }
        })
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
            tabCon.add(subsriptionController)
        }
    }
    func subscriptionDidCancel(endDate: String){
        DispatchQueue.main.async {
            self.setPremiumViews()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let tabCon = self.tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
                subsriptionController.endDate = endDate
                tabCon.add(subsriptionController)
            }
        }
        
    }
}
