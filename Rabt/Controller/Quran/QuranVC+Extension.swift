//
//  QuranVC+Extension.swift
//  Rabt
//
//  Created by Muhammad on 30/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import Foundation
import SwiftData


extension QuranViewController {
    
    func quranCell(cell: SurahsCollectionViewCell, index: Int) -> SurahsCollectionViewCell {
        
        //cell.selectionStyle = .none
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear

            var chapter: Chapter!
            var url_str = ""
            if self.selectedType == "Recommended"{
                chapter = recommendedChaptersArray[index]
                url_str = recommendedChaptersArray[index].image
            }else{
                chapter = dollar.chapters[index] as Chapter
                if HomeViewController.quranImages.count > index{
                    url_str = (HomeViewController.quranImages[index]["image"] ?? "") as! String
                }
            }
            cell.nameLbl.text = chapter.name.local
            cell.nameLbl.font = kCellTextLabelFont
            
            
            //(HomeViewController.quranImages.first?["image"] ?? "") as! String
            let url = URL(string: url_str)
            cell.surahTitleIMG.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            
            var verses = chapter.verses.count
            if (chapter.id != kTaubahIndex) && (chapter.id != kFatihaIndex) {
                verses -= 1
            }
            cell.typeLbl.text = "#\(chapter.revelationLocation)" + " - \(verses) ayāt"
            
            let ind = index + 1
        cell.numberLbl.text = "\(ind)"
            
            let i = self.getChapterIndex(arr: favourites, str: index)
        
        if self.selectedType == "Recommended"{
           let fav = favourites.filter( { ($0 as AnyObject)["number"] as! Int == chapter.id}).first
            if fav != nil{
               // GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.AppColor)
                cell.favouriteBtn.setImage(UIImage(named: "heartWhiteFilled"), for: .normal)
            }else{
                //GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.LightGrayColor)
                cell.favouriteBtn.setImage(UIImage(named: "heartWhiteEmpty"), for: .normal)
            }
        }else{
            if i >= 0 && i <= 114 {
                
                //GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.AppColor)
                cell.favouriteBtn.setImage(UIImage(named: "heartWhiteFilled"), for: .normal)
                
            }else {
                
                //GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.LightGrayColor)
                cell.favouriteBtn.setImage(UIImage(named: "heartWhiteEmpty"), for: .normal)
            }
        }
        
           
            
            
           // cell.favouriteBtn.backgroundColor = .clear
            
            cell.favouriteBtn.tag = index
            cell.favouriteBtn.addTarget(self, action: #selector(addOrRemoveClicked(sender:)), for: .touchUpInside)
            
          //  GeneralMethods.RoundedCorners(shadow: .clear, view: cell.numberLbl)
           // cell.numberLbl.clipsToBounds = true
            
            //GeneralMethods.CreateCardViewWithoutBackColor(radius: 16, view: cell.contantView)
           // cell.contantView.backgroundColor = .white
           // cell.contantView.clipsToBounds = true

        return cell
        
    }
    
    func getRandom()->Int{
        let num = Int(arc4random_uniform(113))
        if var arr = UserDefaults.standard.value(forKey: "randomArray") as? [String] {
            if arr.contains("\(num)"){
                let ran = getRandom()
                return ran
            }else{
                arr.append("\(num)")
                UserDefaults.standard.setValue(arr, forKey: "randomArray")
                return num
            }
        }else{
            UserDefaults.standard.setValue(["\(num)"], forKey: "randomArray")
            return num
        }
    }
    
    
    func quranFavouriteCell(cell: SurahsCollectionViewCell, index: Int) -> SurahsCollectionViewCell {
        
      //  cell.selectionStyle = .none
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        
        let dict = favourites[index] as! [String:Any]
        let ind1 = dict["number"] as! Int
        if HomeViewController.quranImages.count > ind1{
            let url_str = (HomeViewController.quranImages[ind1]["image"] ?? "") as! String
            let url = URL(string: url_str)
            cell.surahTitleIMG.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
        }
        
        let chapter: Chapter = dollar.chapters[ind1] as Chapter
        cell.nameLbl.text = chapter.name.local
        cell.nameLbl.font = kCellTextLabelFont
        print(chapter.description)
        
        var verses = chapter.verses.count
        if (chapter.id != kTaubahIndex) && (chapter.id != kFatihaIndex) {
            verses -= 1
        }
        cell.typeLbl.text = "#\(chapter.revelationLocation)" + " - \(verses) ayāt"
        
        let ind = chapter.id + 1
        cell.numberLbl.text = "\(ind)"
        
       // GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.AppColor)
        cell.favouriteBtn.setImage(UIImage(named: "heartWhiteFilled"), for: .normal)
        
       // cell.favouriteBtn.backgroundColor = .clear
        
        cell.favouriteBtn.tag = index
        cell.favouriteBtn.addTarget(self, action: #selector(removeFavourite(sender:)), for: .touchUpInside)
        
//        GeneralMethods.RoundedCorners(shadow: .clear, view: cell.numberLbl)
//        cell.numberLbl.clipsToBounds = true
        
//        GeneralMethods.CreateCardViewWithoutBackColor(radius: 16, view: cell.contantView)
//        cell.contantView.backgroundColor = .white
//        cell.contantView.clipsToBounds = true
        
        return cell
        
    }
    
    
    func recitersCell(cell: RecitersCollectionViewCell, index: Int) -> RecitersCollectionViewCell {
        
       // cell.selectionStyle = .none
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        
        let reciter: Reciter = self.allReciters[index]
        cell.nameLbl.text = reciter.name
       // cell.nameLbl.font = kCellTextLabelFont
        
        if isPro { //#colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
            if reciter == dollar.currentReciter {
                
             //   GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.AppColor)
                cell.favouriteBtn.setImage(UIImage(named: "filledGreenHeart"), for: .normal)
            }else{
                
              //  GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.LightGrayColor)
                cell.favouriteBtn.setImage(UIImage(named: "emptyGreenHeart"), for: .normal)
            }
        }else {
            if index == 0 {
               // GeneralMethods.ChangeBtnTintColor(btn: cell.favouriteBtn, color: GlobalVariables.AppColor)
                cell.favouriteBtn.setImage(UIImage(named: "filledGreenHeart"), for: .normal)
            }
            else{
               // cell.lock()
            }
        }
        cell.playsLabel.isHidden = true
        cell.imgView.image = UIImage(named: reciter.image)
        
        cell.favouriteBtn.backgroundColor = .clear
        
        cell.favouriteBtn.tag = index
        cell.favouriteBtn.addTarget(self, action: #selector(favouriteReciterClicked(sender:)), for: .touchUpInside)
        cell.playBtn.tag = index
        cell.playBtn.addTarget(self, action: #selector(playReciterClicked(sender:)), for: .touchUpInside)
//        GeneralMethods.CreateCardViewWithoutBackColor(radius: 16, view: cell.contantView)
//        cell.contantView.backgroundColor = .white
//        cell.contantView.clipsToBounds = true
        
        return cell
        
    }
    
    @objc func favouriteReciterClicked(sender: UIButton) -> Void {
        
        if isPro {
            
            let reciter: Reciter = self.allReciters[sender.tag]
            dollar.currentReciter = reciter
            dollar.setPersistentObjectForKey(sender.tag as AnyObject, key: kCurrentReciterKey)
            listCollectionView.reloadData()
            NotificationCenter.default.post(name: Notification.Name(rawValue: kReciterChangedNotification), object: nil,  userInfo: nil)
        }
        else if sender.tag != 0 {
            self.askUserForPurchasingProVersion(FlurryEvent.reciterSelected)
        }
        
    }
    
    @objc func playReciterClicked(sender: UIButton) -> Void {
        
        favouriteReciterClicked(sender: sender)
        selectedType = "All"
        typesCollectionView.reloadData()
        typesCollectionView.layoutIfNeeded()
        
        let indexpath = IndexPath(item: 0, section: 0) as IndexPath
        typesCollectionView.scrollToItem(at: indexpath, at: .right, animated: false)
        
        listCollectionView.reloadData()
    }
    
    @objc func addOrRemoveClicked(sender: UIButton) -> Void {
        if self.selectedType == "My Favourite" {
            return
        }
        let tag = sender.tag
        var chapter: Chapter!
        if self.selectedType == "Recommended"{
            chapter = recommendedChaptersArray[tag]
        }else{
            chapter = dollar.chapters[tag] as Chapter
        }
            
        let num = chapter.id
        
        checkFavList(num: num, index: tag)
        
    }
    
    @objc func removeFavourite(sender: UIButton) -> Void {
        
        let index = sender.tag
        
        favourites.remove(at: index)
        if self.selectedType == "My Favourite"{
            if favourites.count == 0{
                loadingView.msgLbl.textColor = .white
                loadingView.msgLbl.text = "No Favourites Yet!\nBuild your own Favourite list by clicking Heart button next to each item."
                self.loadingView.isHidden = false
            }else{
                self.loadingView.isHidden = true
            }
           
        }else{
            self.loadingView.isHidden = true
        }
        
        UserDefaults.standard.setValue(favourites, forKey: "favourites")
        UserDefaults.standard.synchronize()
        
        listCollectionView.reloadData()
    }
    
    
    func checkFavList(num: Int,index:Int) -> Void {
        
        let index = self.getChapterIndex(arr: favourites, str: num)
        if index >= 0 && index <= 114 {
            
            favourites.remove(at: index)
            
        }else {
            
            let dict = ["favourite": "1", "number": num] as [String : Any]
            favourites.append(dict)
            
        }
        
        let arr = favourites.sorted{(($0 as! [String:Any])["number"] as! Int)  < (($1 as! [String:Any])["number"] as! Int)}//favourites.sortedArray(using: [NSSortDescriptor(key: "number", ascending: true)])
        
        favourites = []
        for i in 0..<arr.count {
            
            let dict = arr[i] as! [String:Any]
            favourites.append(dict)
            
        }
        
        UserDefaults.standard.setValue(favourites, forKey: "favourites")
        UserDefaults.standard.synchronize()
        if self.selectedType == "My Favourite"{
            if favourites.count == 0{
                loadingView.msgLbl.textColor = .white
                loadingView.msgLbl.text = "No Favourites Yet!\nBuild your own Favourite list by clicking Heart button next to each item."
                self.loadingView.isHidden = false
            }else{
                self.loadingView.isHidden = true
            }
           
        }else{
            self.loadingView.isHidden = true
        }
        
        listCollectionView.reloadData()
        
    }
    
    func getChapterIndex(arr: [Any], str: Int) -> Int {
        let convertedToNSArray =  NSArray(array: arr)
        let index = convertedToNSArray.indexOfObject(passingTest: { data, idx, stop in
            let dict = data as! [String:Any]
            return dict["number"] as? Int ?? 0 == str
        })
        
        return index
    }
    
}


extension QuranViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView == typesCollectionView {
            
            return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
            
       
        
        if isCollapsedShown {
            return UIEdgeInsets(top: 8, left: 16, bottom: 50, right: 16)
        }
        
        return UIEdgeInsets(top: 0, left: 16, bottom: 16, right: 16)
        } else {
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == typesCollectionView {
            return typesArray.count
        } else {
            if selectedType == "All".localized() {
                
                return 114
                
            }else if selectedType == "My Favourite".localized() {
                
                return favourites.count
                
            }else if selectedType == "Reciters".localized() {
                
                return allReciters.count
                
            }else {
                return recommendedChaptersArray.count
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == typesCollectionView {
            return CGSize(width: 70, height: 30)
        } else {
            if selectedType == "Reciters".localized() {
                
                return CGSize(width: (collectionView.frame.size.width / 2) - 8, height: ((collectionView.frame.size.width / 2) - 8) * 1.267)
                
            }
            
            return CGSize(width: collectionView.frame.size.width, height: 75)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == typesCollectionView {
            let cell : TypeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "typeCell", for: indexPath) as! TypeCollectionViewCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            
            let type = typesArray[indexPath.item]
            
            cell.typeLbl.text = type
            
           // GeneralMethods.RoundedCorners(shadow: .clear, view: cell.contantView)
            
            if type == selectedType {
                
                cell.typeLbl.textColor = UIColor.init(hexString: "326962")
                cell.contantView.backgroundColor = UIColor.init(hexString: "DBDBDB")
                
            }else{
                
                cell.typeLbl.textColor = .white
                cell.contantView.backgroundColor = .clear
                
            }
            
            return cell
        } else {
            if selectedType == "Reciters".localized() {
                var cell : RecitersCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecitersCollectionViewCell", for: indexPath) as! RecitersCollectionViewCell
                cell = self.recitersCell(cell: cell, index: indexPath.row)
                
                return cell
                
            }else if selectedType == "My Favourite".localized() {
                var cell : SurahsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SurahsCollectionViewCell", for: indexPath) as! SurahsCollectionViewCell
                cell = self.quranFavouriteCell(cell: cell, index: indexPath.row)
                
                return cell
                
            }else {
                
                var cell : SurahsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SurahsCollectionViewCell", for: indexPath) as! SurahsCollectionViewCell
                    cell = self.quranCell(cell: cell, index: indexPath.row)
                return cell
                
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == typesCollectionView {
            selectedType = typesArray[indexPath.item]
            //    AnalyticsManger.logEventWith(name: "Screen_View", attributes: ["source": "\(selectedType.lowercased()) surah screen"])
            if selectedType == "Recommended"{
                UserDefaults.standard.removeObject(forKey: "randomArray")
                recommendedChaptersArray = getRecommendedChapters()
            }
            if self.selectedType == "My Favourite"{
                if favourites.count == 0{
                    loadingView.msgLbl.textColor = .white
                    loadingView.msgLbl.text = "No Favourites Yet!\nBuild your own Favourite list by clicking Heart button next to each item."
                    self.loadingView.isHidden = false
                }else{
                    self.loadingView.isHidden = true
                }
            }else{
                self.loadingView.isHidden = true
            }
            typesCollectionView.reloadData()
            typesCollectionView.layoutIfNeeded()
            
            let indexpath = IndexPath(item: indexPath.item, section: 0) as IndexPath
            typesCollectionView.scrollToItem(at: indexpath, at: .right, animated: false)
            
            listCollectionView.reloadData()
        } else {
            if isCollapsedShown {
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "closeListen"), object: nil, userInfo: nil)
                
            }
            
            if selectedType == "Reciters".localized() {
                
                
                
            }else if selectedType == "My Favourite".localized() {
                
                let dict = favourites[indexPath.row] as! [String:Any]
                let str = dict["number"] as? String ?? String(dict["number"] as? Int ?? 0)
                let index = Int(str) ?? 0
                
                let chapter: Chapter = dollar.chapters[index] as Chapter
                dollar.setAndSaveCurrentChapter(chapter)
                
                let storyboard = QuranStoryBoard
                let VC = storyboard.instantiateViewController(withIdentifier: "QuranChapterViewController") as! QuranChapterViewController
                self.navigationController!.pushViewController(VC, animated: true)
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                
            }else if selectedType == "Recommended"{
                let chapter: Chapter = self.recommendedChaptersArray[indexPath.row]
                dollar.setAndSaveCurrentChapter(chapter)
                
                let storyboard = QuranStoryBoard
                let VC = storyboard.instantiateViewController(withIdentifier: "QuranChapterViewController") as! QuranChapterViewController
                self.navigationController!.pushViewController(VC, animated: true)
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            }else {
                
                let chapter: Chapter = dollar.chapters[indexPath.row] as Chapter
                dollar.setAndSaveCurrentChapter(chapter)
                
                let storyboard = QuranStoryBoard
                let VC = storyboard.instantiateViewController(withIdentifier: "QuranChapterViewController") as! QuranChapterViewController
                self.navigationController!.pushViewController(VC, animated: true)
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                
            }
        }
    }
    
    func getRecommendedChapters()-> [Chapter]{
        var chaptersArray = [Chapter]()
        var chapter: Chapter!
        let CurrentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone =  TimeZone(identifier: self.getCurrentTimeZone())!
        dateFormatter.dateFormat = "ccc"
        let friday = dateFormatter.string(from: CurrentDate as Date)
        let isFriday = "Fri"
        var url_str = ""
            if friday == isFriday {
                print("It's Friday")
                chapter = dollar.chapters.filter( { $0.name.contains("Al-Kahf")}).first
                let indexx = dollar.chapters.firstIndex{$0.name == "Al-Kahf"}
                if HomeViewController.quranImages.count > indexx ?? 0{
                    url_str = (HomeViewController.quranImages[indexx ?? 0]["image"] ?? "") as? String ?? ""
                    chapter.image = url_str
                }
               
                return [chapter]
            }else {
                for _ in 0...4{
                    let randomInt = getRandom()
                    chapter = dollar.chapters[randomInt] as Chapter
                    if HomeViewController.quranImages.count > randomInt{
                        url_str = (HomeViewController.quranImages[randomInt]["image"] ?? "") as! String
                        chapter.image = url_str
                    }
                   
                    chaptersArray.append(chapter)
                }
                print("otherDay")
                return chaptersArray
            }
    }
}


//extension QuranViewController: UITableViewDelegate, UITableViewDataSource {
//
////    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
////
////        if selectedType == "All".localized() {
////
////            return 114
////
////        }else if selectedType == "My Favourite".localized() {
////
////            return favourites.count
////
////        }else if selectedType == "Reciters".localized() {
////
////            return allReciters.count
////
////        }else {
////            return recommendedChaptersArray.count
////        }
////    }
//
////    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
////
////        if selectedType == "Reciters".localized() {
////
////            var cell = Bundle.main.loadNibNamed("RecitersTableViewCell", owner: self, options: nil)?.first as! RecitersTableViewCell
////            cell = self.recitersCell(cell: cell, index: indexPath.row)
////
////            return cell
////
////        }else if selectedType == "My Favourite".localized() {
////
////            var cell = Bundle.main.loadNibNamed("ChaptersTableViewCell", owner: self, options: nil)?.first as! ChaptersTableViewCell
////            cell = self.quranFavouriteCell(cell: cell, index: indexPath.row)
////
////            return cell
////
////        }else {
////
////            var cell = Bundle.main.loadNibNamed("ChaptersTableViewCell", owner: self, options: nil)?.first as! ChaptersTableViewCell
////                cell = self.quranCell(cell: cell, index: indexPath.row)
////            return cell
////
////        }
////    }
//
////    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
////
////        if selectedType == "Reciters".localized() {
////
////            return 108
////
////        }
////
////        return 108
////
////    }
//
////    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
////
////        if isCollapsedShown {
////
////            return 50
////
////        }
////
////        return 0
////    }
////
////
////    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
////
////        if isCollapsedShown {
////
////            let v = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
////            v.backgroundColor = .clear
////            return v
////
////        }
////
////        return nil
////    }
//
////    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////
////        if isCollapsedShown {
////
////            NotificationCenter.default.post(name: Notification.Name(rawValue: "closeListen"), object: nil, userInfo: nil)
////
////        }
////
////        if selectedType == "Reciters".localized() {
////
////
////
////        }else if selectedType == "My Favourite".localized() {
////
////            let dict = favourites[indexPath.row] as! [String:Any]
////            let str = dict["number"] as? String ?? String(dict["number"] as? Int ?? 0)
////            let index = Int(str) ?? 0
////
////            let chapter: Chapter = dollar.chapters[index] as Chapter
////            dollar.setAndSaveCurrentChapter(chapter)
////
////            let storyboard = QuranStoryBoard
////            let VC = storyboard.instantiateViewController(withIdentifier: "QuranChapterViewController") as! QuranChapterViewController
////            self.navigationController!.pushViewController(VC, animated: true)
////            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
////
////        }else if selectedType == "Recommended"{
////            let chapter: Chapter = self.recommendedChaptersArray[indexPath.row]
////            dollar.setAndSaveCurrentChapter(chapter)
////
////            let storyboard = QuranStoryBoard
////            let VC = storyboard.instantiateViewController(withIdentifier: "QuranChapterViewController") as! QuranChapterViewController
////            self.navigationController!.pushViewController(VC, animated: true)
////            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
////        }else {
////
////            let chapter: Chapter = dollar.chapters[indexPath.row] as Chapter
////            dollar.setAndSaveCurrentChapter(chapter)
////
////            let storyboard = QuranStoryBoard
////            let VC = storyboard.instantiateViewController(withIdentifier: "QuranChapterViewController") as! QuranChapterViewController
////            self.navigationController!.pushViewController(VC, animated: true)
////            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
////
////        }
////    }
//}
