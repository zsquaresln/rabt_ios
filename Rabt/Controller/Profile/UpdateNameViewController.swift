//
//  UpdateProfileNameViewController.swift
//  Rabt
//
//  Created by Ali Sher on 03/09/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData

protocol updateNameDelegate {
    
    func updateName() -> Void
    
}

class UpdateNameViewController: UIViewController {

    var delegate: updateNameDelegate?
    
    
    @IBOutlet var fnameTF: UITextField!
    @IBOutlet var lNameTF: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet var saveBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTextAndDesign()
        
    }
    
    func setTextAndDesign() -> Void {
        
        fnameTF.placeholder = "First Name".localized()
        fnameTF.setTextField()

        lNameTF.placeholder = "Last Name".localized()
        lNameTF.setTextField()
        
        emailTextField.placeholder = "Email".localized()
        emailTextField.setTextField()
        
        let fullname = userInfo["full_name"] as? String
        let nameArray = fullname?.components(separatedBy: " ")
        if nameArray?.count ?? 0 > 1{
            fnameTF.text = nameArray?[0]
            lNameTF.text = nameArray?[1]
        }
        emailTextField.text = userInfo["email"] as? String
        if emailTextField.text?.trimmingCharacters(in: .whitespaces) != ""{
            emailTextField.isEnabled = false
            emailTextField.backgroundColor = .lightGray
        }
        GeneralMethods.CreateCardViewWithoutShadow(radius: saveBtn.frame.size.height/2, view: saveBtn)
        saveBtn.backgroundColor = GlobalVariables.AppColor
        
    }
    func isValidEmailAddress() -> Bool {
      
      var returnValue = true
      let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
      
      do {
          let regex = try NSRegularExpression(pattern: emailRegEx)
          let nsString = (emailTextField.text ?? "") as NSString
          let results = regex.matches(in: (emailTextField.text ?? ""), range: NSRange(location: 0, length: nsString.length))
          
          if results.count == 0
          {
              returnValue = false
          }
          
      } catch let error as NSError {
          print("invalid regex: \(error.localizedDescription)")
          returnValue = false
      }
      
      return  returnValue
  }
    @IBAction func closeBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        
        if fnameTF.isEmpty {
            
            self.view.makeToast("Please enter first name".localized())
            
        }else if lNameTF.isEmpty {
            
            self.view.makeToast("Please enter last name".localized())
            
        }else if emailTextField.isEmpty {
            
            self.view.makeToast("Please enter Email".localized())
            
        }else if isValidEmailAddress() == false {
            
            self.view.makeToast("Please enter correct Email".localized())
            
        }else {
                        
            self.updateName()
        }
        
    }
    
    func updateName() -> Void {
        
        let parameter = ["full_name": "\(fnameTF.text!) \(lNameTF.text!)","email":emailTextField.text!
        ]
        
        let apiManager = ApiManager()
        let url = ApiUrls.updateNameApi
        //self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            
           // self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1 {
                
                let apiData = responseData["data"] as! [String:Any]
                self.MoveToHome(dict: apiData)
                
            }else{
                
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
                
            }
            print("success")
            
        }) { error in
          //  self.removeSpinner()
            print("error")
        }
    }
    
    func MoveToHome(dict: [String:Any]) -> Void {
        
        UserDefaults.standard.setValue(dict, forKey: "userinfo")
        UserDefaults.standard.synchronize()
        
        userInfo = dict
        
        self.dismiss(animated: true, completion: nil)
        delegate?.updateName()
        
    }
}
