//
//  SettingsViewController.swift
//  Rabt
//
//  Created by Muhammad on 03/08/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData
import AVFoundation
import MediaPlayer
//MARK:********* change *********

class SettingsViewController: MasterViewController {
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var imgBGview: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var profileArrowIcon: UIImageView!
    
    
    @IBOutlet weak var notificaionsView: UIView!
    @IBOutlet weak var pauseSwitch: UISwitch!
    @IBOutlet weak var fewerSwitch: UISwitch!
    
    @IBOutlet weak var notificaionSettingArrowIcon: UIImageView!
    
    
   // @IBOutlet weak var interestsView: UIView!
  //  @IBOutlet weak var interestsArrowIcon: UIImageView!
    
    @IBOutlet weak var othersView: UIView!
    @IBOutlet weak var whatNewArrowIcon: UIImageView!
    @IBOutlet weak var fAQArrowIcon: UIImageView!
    @IBOutlet weak var guidelinesArrowIcon: UIImageView!
    @IBOutlet weak var termsArrowIcon: UIImageView!
    @IBOutlet weak var privacyArrowIcon: UIImageView!
    
    
    @IBOutlet weak var logoutView: UIView!
    
    @IBOutlet weak var logutLabel: UILabel!
    @IBOutlet weak var versionLbl: UILabel!
    
    var navController = UINavigationController()
    
    var linksData : [String:Any] = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.value(forKey: "pauseNotifications") as? Bool == true{
            pauseSwitch.setOn(true, animated: false)
        }
        setTextAndDesign()
        showUserData()
        
        if GeneralMethods.isKeyPresentInUserDefaults(key: "linksData") {
            
            linksData = UserDefaults.standard.value(forKey: "linksData") as! [String:Any]
            
        }
        
        getLinks()
    }
    
    func setTextAndDesign() -> Void {
        
        GeneralMethods.CreateCardViewWithoutBackColor(radius: 12, view: profileView)
        GeneralMethods.ChangeImgViewTintColor(imgView: profileArrowIcon, color: GlobalVariables.DarkGrayColor)
        
        GeneralMethods.CreateCardViewWithoutBackColor(radius: 12, view: notificaionsView)
        GeneralMethods.ChangeImgViewTintColor(imgView: notificaionSettingArrowIcon, color: GlobalVariables.DarkGrayColor)
        
        
        GeneralMethods.CreateCardViewWithoutBackColor(radius: 12, view: othersView)
        GeneralMethods.ChangeImgViewTintColor(imgView: whatNewArrowIcon, color: GlobalVariables.DarkGrayColor)
        GeneralMethods.ChangeImgViewTintColor(imgView: fAQArrowIcon, color: GlobalVariables.DarkGrayColor)
        GeneralMethods.ChangeImgViewTintColor(imgView: guidelinesArrowIcon, color: GlobalVariables.DarkGrayColor)
        GeneralMethods.ChangeImgViewTintColor(imgView: termsArrowIcon, color: GlobalVariables.DarkGrayColor)
        GeneralMethods.ChangeImgViewTintColor(imgView: privacyArrowIcon, color: GlobalVariables.DarkGrayColor)
        
        GeneralMethods.CreateCardViewWithoutBackColor(radius: 12, view: logoutView)
        
        versionLbl.text = "Version".localized() + " \(Bundle.main.versionNumber) (\(Bundle.main.buildNumber))"
        
    }
    
    func showUserData() -> Void {
        
        nameLbl.text = userInfo["full_name"] as? String
        userNameLbl.text = userInfo["user_name"] as? String ?? "----"
        
        let url_str = userInfo["picture"] as? String ?? ""
        let url = URL(string: url_str)
        imgView.sd_setImage(with: url , placeholderImage: UIImage(named: "PlaceHolder.jpg"))
        
        GeneralMethods.RoundedCorners(shadow: .clear, view: imgBGview)
        imgBGview.clipsToBounds = true
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            logutLabel.localizeKey = "Login"
            logutLabel.text = "Login"
        }
        
    }
    
    
    func getLinks() -> Void {
        
        let apiManager = ApiManager()
        let url = ApiUrls.getLinksApi
        
        apiManager.WebService(url: url, parameter: nil, method: .get, encoding: .queryString, { responseData in
            
          //  self.removeSpinner()
            
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                
                let apiData = responseData["data"] as! [String:Any]
                self.linksData = apiData
                
                UserDefaults.standard.setValue(self.linksData, forKey: "linksData")
                UserDefaults.standard.synchronize()
                
            }else{
                
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
                
            }
            print("success")
            
        }) { error in
          //  self.removeSpinner()
            print("error")
        }
        
    }
    
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func linksBtnClicked(_ sender: UIButton) {
        var path = ""
        if sender.tag == 4{
            path = "https://www.rabt.co.uk"
        }else if sender.tag == 5{
            path = "https://rabt.co.uk/tos/"//http://35.173.108.2/terms"
        }else{
            path = "https://rabt.co.uk/tos/"//"http://35.173.108.2/privacy-policy"
        }
        guard let url = URL(string:path) else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
//        let tag = sender.tag
//
//        let vc = LinksViewController(nibName: "LinksViewController", bundle: nil)
//        vc.selectedTag = tag
//        vc.allLinks = linksData
//        self.modalPresentationStyle = .overFullScreen
//        self.present(vc, animated: true)
    }
    
    @IBAction func updateNameBtnClicked(_ sender: Any) {
                if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
                    return
                }
        else{
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "UpdateNameViewController") as! UpdateNameViewController
        vc.delegate = self
        self.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true)
        }
        
    }
    
    @IBAction func pauseNotificationValueChanged(_ sender: Any) {
        if pauseSwitch.isOn == false{
            UserDefaults.standard.setValue(false, forKey: "pauseNotifications")
            UserDefaults.standard.synchronize()
            PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
            NotificationsHandler().scheduleAllEnabledNotifications()
        }
        else{
            UserDefaults.standard.setValue(true, forKey: "pauseNotifications")
            UserDefaults.standard.synchronize()
            PrayerTimeManger.shared.shouldRefreshPrayerNotifications = false
            NotificationsHandler().pauseAllNotifications()
        }
       
    }
    
    @IBAction func logoutBtnClicked(_ sender: Any) {
        NotificationsHandler().removeAllNotificationsData()
        PrayerTimeManger.shared.shouldRefreshPrayerTimings = true
        PrayerTimeManger.shared.shouldRefreshPrayerNotifications = true
        PrayerTimeManger.shared.prayerTimeCoordinates = nil
        PrayerTimeManger.shared.prayerData = nil
        PrayerTimeManger.shared.selectedLocationName = nil
        UserDefaults.standard.removeObject(forKey: "access_token")
        UserDefaults.standard.synchronize()
        GooglePlacesManager().clearRecentPlacesCache()
        if (userInfo["guest_user"] as? Int) == 0 || (userInfo["guest_user"] as? String) == "0" {

            clearDataAndLogout()
        }
        else{
            MoveToLogout()
        }
    }
    
    
    func MoveToLogout() -> Void {

        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout".localized(), preferredStyle: .alert)
        let ok = UIAlertAction(title: "Yes".localized(), style: .default, handler: { action in
            DispatchQueue.main.async {
             //   self.clearDataAndLogout()
            }
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "No".localized(), style: .default, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    

}


extension SettingsViewController: updateNameDelegate {
    
    func updateName() {
       
        showUserData()
        
    }
    
}
