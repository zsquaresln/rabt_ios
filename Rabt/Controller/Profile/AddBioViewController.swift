//
//  AddBioViewController.swift
//  Rabt
//
//  Created by Muhammad on 31/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData

protocol bioDataDelegate {
    
    func didEditBioData(str: String) -> Void
    
}

class AddBioViewController: UIViewController {
    
    var delegate: bioDataDelegate?
    
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var detailsTextView: UITextView!
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    var bioText = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6044058248)
        setTextAndDesgin()
        
    }
    
    func setTextAndDesgin() -> Void {
        
        GeneralMethods.CreateCardView(radius: 16, view: contentView)
        contentView.clipsToBounds = true
        
        GeneralMethods.RoundedCorners(shadow: .clear, view: saveBtn)
        saveBtn.backgroundColor = GlobalVariables.AppColor
        
        GeneralMethods.ChangeBtnTintColor(btn: closeBtn, color: .black)
        
        detailsTextView.text = "Add bio".localized()
        detailsTextView.textColor = UIColor.lightGray
        detailsTextView.delegate = self
        
        if bioText != "" {
            
            detailsTextView.text = bioText
            detailsTextView.textColor = UIColor.black
            
        }
        
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        
        self.view.removeFromSuperview()
        
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        
        if detailsTextView.textColor == UIColor.lightGray || detailsTextView.text == ""{
            
            self.view.makeToast("Please enter bio data".localized())
            
        }else {
            saveBtn.isEnabled = false
            self.updateBio()
            

            
        }
        
    }
    
    func saveData(dict: [String:Any]) -> Void {
        
        UserDefaults.standard.set(dict, forKey: "userinfo")
        UserDefaults.standard.synchronize()
        
        userInfo = dict
        
        self.view.removeFromSuperview()
        delegate?.didEditBioData(str: detailsTextView.text!)
        
    }
    
    
    func updateBio() -> Void {
        
        let parameter = [
                         "bio": self.detailsTextView.text!]
        
        let apiManager = ApiManager()
        
        
        let url = ApiUrls.updateImageAndBioApi
        //self.showSpinner(onView: self.view)
        
        let imagesArray : [Any] = []
        
        apiManager.WebService1WithImages(url: url, images: imagesArray, parameter: parameter, method: .post, encoding: .queryString) { responseData in
            
            //self.removeSpinner()
            self.saveBtn.isEnabled = true
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                
                var apiData = responseData["data"] as! [String:Any]
                apiData = apiData["User"] as! [String:Any]
                
                self.saveData(dict: apiData)
                
                
            }else{
                
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
                
            }
            
            print("success")
            
        } onFailure: { error in
            self.saveBtn.isEnabled = true
           // self.removeSpinner()
            print("error")
            
        }
        
    }
    
}


extension AddBioViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add bio"
            textView.textColor = UIColor.lightGray
        }
    }
    
}
