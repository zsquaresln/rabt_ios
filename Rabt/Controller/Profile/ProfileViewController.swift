//
//  ProfileViewController.swift
//  Rabt
//
//  Created by Ali Sher on 28/07/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData

class ProfileViewController: UIViewController {
    
    
    @IBOutlet weak var navView: UIView!
    
    @IBOutlet weak var navViewHeightCons: NSLayoutConstraint!
    
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var settingBtn: UIButton!
    
    
    
    @IBOutlet weak var dataScrollView: UIScrollView!
    
    @IBOutlet weak var cornerView: UIView!

    @IBOutlet weak var imgBGView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var addBioLbl: UILabel!
    @IBOutlet weak var addBioBtn: UIButton!
    
    
    @IBOutlet weak var followersView: UIView!
    
    @IBOutlet weak var invitationsView: UIView!
    @IBOutlet weak var invitationsBtn: UIButton!
    @IBOutlet weak var invitationsCollectionView: UICollectionView!
    
    @IBOutlet weak var membersView: UIView!
    @IBOutlet weak var membersViewHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var membersTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTextAndDesign()
        
        invitationsCollectionView.backgroundColor = .clear
        invitationsCollectionView.delegate = self
        invitationsCollectionView.dataSource = self
        invitationsCollectionView.register(UINib(nibName: "InvitationsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "invitationCell")

        
        membersTableView.backgroundColor = .clear
        membersTableView.delegate = self
        membersTableView.dataSource = self
        
        
        membersTableView.isScrollEnabled = false
        membersViewHeightCons.constant = 0 * 5 + 60
        
        dataScrollView.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        
        followersView.isHidden = true
        invitationsView.isHidden = true
        membersView.isHidden = true
        
        showUserData()
    }
    
    func setTextAndDesign() -> Void {
        
        addBioBtn.setTitle("Add Bio".localized(), for: .normal)
                
        GeneralMethods.setCorners(radius: 24, corners: [.topLeft, .topRight], view: cornerView)
        cornerView.clipsToBounds = true
        
        GeneralMethods.CreateCardViewWithoutBackColor(radius: invitationsBtn.frame.size.height/2, view: invitationsBtn)
        
        if GeneralMethods.isIphoneXShaped() {
            
            navViewHeightCons.constant = 90
            
        }else {
            
            navViewHeightCons.constant = 70
            
        }
    }
    
    func showUserData() -> Void {
        
        nameLbl.text = userInfo["full_name"] as? String
        userNameLbl.text = userInfo["user_name"] as? String ?? "----"
        
        let url_str = userInfo["picture"] as? String ?? ""
        let url = URL(string: url_str)
        imgView.sd_setImage(with: url , placeholderImage: UIImage(named: "PlaceHolder.jpg"))
        
        GeneralMethods.RoundedCorners(shadow: .clear, view: imgView)
        imgView.clipsToBounds = true
        
        GeneralMethods.RoundedCorners(shadow: .clear, view: imgBGView)
        imgBGView.clipsToBounds = true
        
        addBioLbl.text = ""
        addBioLbl.isHidden = true
        
        let bio = userInfo["bio"] as? String ?? ""
        if bio != "" {
            
            addBioLbl.text = bio
            addBioLbl.isHidden = false
            addBioBtn.setTitle("", for: .normal)
            
        }
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func imgBtnClicked(_ sender: Any) {
        
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            return
        }else{
            let VC = self.storyboard!.instantiateViewController(withIdentifier: "UpdatePictureViewController") as? UpdatePictureViewController
            VC?.delegate = self
            self.modalPresentationStyle = .overFullScreen
            self.present(VC!, animated: true)
        }
    }
    
    
    @IBAction func shareProfileBtnClicked(_ sender: Any) {
        if let urlStr = NSURL(string: "https://www.rabt.co.uk") {
            let title = "\nAssalamu Alaikum, I wanted to tell you about this app Rabt. This is the best app if you want to come closer to Islam and be more connected with Allah. May Allah bless us in spreading his message. Download the app on this link:"
            let objectsToShare = [title, urlStr] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.setValue("Rabt: Connecting with Allah", forKey: "subject")
            if UIDevice.current.userInterfaceIdiom == .pad {
                if let popup = activityVC.popoverPresentationController {
                    popup.sourceView = self.view
                    popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                }
            }

            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func settingBtnClicked(_ sender: Any) {
        
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
        VC?.navController = self.navigationController!
        self.modalPresentationStyle = .overFullScreen
        self.present(VC!, animated: true)
        
    }
    
    
    
    @IBAction func addorEditBioClicked(_ sender: Any) {
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            return
        }
        else{
        let obj = self.storyboard!.instantiateViewController(withIdentifier: "AddBioViewController") as? AddBioViewController
        obj?.bioText = addBioLbl.text ?? ""
        obj?.delegate = self
        addChild(obj!)
        view.addSubview(obj!.view)
        obj!.didMove(toParent: self)
        }
        
    }
    
}

extension ProfileViewController: bioDataDelegate {
    
    func didEditBioData(str: String) {
        
        addBioLbl.text = str
        addBioLbl.isHidden = false
        addBioBtn.setTitle("", for: .normal)
    }
    
}


extension ProfileViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y > 70 {
            
            UIView.transition(with: navView, duration: 1.0,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.navView.isHidden = false
                              })
            
        }else {
            
            UIView.transition(with: navView, duration: 1.0,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.navView.isHidden = true
                              })
            
        }
    }
}

extension ProfileViewController: profileImageDelegate {
    
    func didChangeImage(img: UIImage) {
        
        imgView.image = img
        
    }
    
}
