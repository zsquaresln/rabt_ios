//
//  UpdatePictureViewController.swift
//  Rabt
//
//  Created by Muhammad on 03/08/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData

protocol profileImageDelegate {
    
    func didChangeImage(img: UIImage) -> Void
    
}

class UpdatePictureViewController: UIViewController {
    
    var delegate: profileImageDelegate?
    
    
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var imgBGview: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTextAndDesign()
        
    }
    
    func setTextAndDesign() -> Void {
        
        let url_str = userInfo["picture"] as? String ?? ""
        let url = URL(string: url_str)
        imgView.sd_setImage(with: url , placeholderImage: UIImage(named: "PlaceHolder.jpg"))

        
        GeneralMethods.RoundedCorners(shadow: .clear, view: imgBGview)
        imgBGview.clipsToBounds = true
        
        GeneralMethods.RoundedCorners(shadow: .clear, view: doneBtn)
        doneBtn.backgroundColor = GlobalVariables.AppColor
        
    }
    
    
    @IBAction func selectImgClicked(_ sender: Any) {
        
        let obj = SelectImage()
        obj.VC = self
        obj.imgView = imgView
        obj.delegate = self
        obj.select()
        
    }
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        
        updatePicture()
        
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func saveData(dict: [String:Any]) -> Void {
        
        UserDefaults.standard.set(dict, forKey: "userinfo")
        UserDefaults.standard.synchronize()
        
        userInfo = dict
        
        self.dismiss(animated: true, completion: nil)
        delegate?.didChangeImage(img: imgView.image!)
        
    }
    
    func updatePicture() -> Void {
        
        let bio = userInfo["bio"] as? String ?? ""
        
        let parameter = [
                         "bio": bio]
        
        let apiManager = ApiManager()
        
        let url = ApiUrls.updateImageAndBioApi
        //self.showSpinner(onView: self.view)
                
        apiManager.WebServiceWithImage(url: url, imgName: "image", image: imgView.image!, parameter: parameter, method: .post, encoding: .queryString) { responseData in
            
           // self.removeSpinner()
            
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                
                var apiData = responseData["data"] as! [String:Any]
                apiData = apiData["User"] as! [String:Any]
                
                self.saveData(dict: apiData)
                
            }else{
                
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
                
            }
            
        } onFailure: { error in
            
            //self.removeSpinner()
            print("error")
            
        }
    }
}


extension UpdatePictureViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[.originalImage] as? UIImage
        imgView.image = image
        
        picker.dismiss(animated: true)

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
}
