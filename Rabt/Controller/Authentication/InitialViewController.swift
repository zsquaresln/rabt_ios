//
//  InitialViewController.swift
//  Rabt
//
//  Created by Ali Sher on 08/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData

class InitialViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var logoIcon: UIImageView!
    
    @IBOutlet weak var contactUsTV: UITextView!
    @IBOutlet weak var startedBtn: UIButton!
    
    var guestLoginInfoDict : [String:Any]? = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startedBtn.isHidden = true
       
        UIView.animate(withDuration: 0.7, animations: {
            self.contentView.frame = self.contentView.frame.offsetBy(dx: 0, dy: -150)
        })
        
        GeneralMethods.ChangeImgViewTintColor(imgView: logoIcon, color: .white)
        
        setText()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getSplashData()
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func setText() -> Void {
        
        let attributedString = NSMutableAttributedString(string: "contact us text".localized())
        let url = URL(string: GlobalVariables.contactUsUrl)!
        
        
        let range: NSRange = attributedString.mutableString.range(of: "Contact Us".localized(), options: .caseInsensitive)
        
        attributedString.setAttributes([.link: url], range: range)
        
        self.contactUsTV.attributedText = attributedString
        self.contactUsTV.isUserInteractionEnabled = true
        self.contactUsTV.isEditable = false
        self.contactUsTV.textAlignment = .center
        
        self.contactUsTV.textColor = GlobalVariables.TitleFontColor
        self.contactUsTV.linkTextAttributes = [
            .foregroundColor: UIColor.black,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
      //  GeneralMethods.CreateCardViewWithoutShadow(radius: startedBtn.frame.size.height/2, view: startedBtn)
        
        
    }
    
    @IBAction func startedBtnClicked(_ sender: Any) {
        
        let storyboard = AuthenticationStoryBoard
        let VC = storyboard.instantiateViewController(withIdentifier: "PhoneNumberViewController") as! PhoneNumberViewController
        VC.guestLoginInfoDict = guestLoginInfoDict
        self.navigationController!.pushViewController(VC, animated: true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
}
extension InitialViewController{
    func getSplashData(){
        
        
        let apiManager = ApiManager()
        let url = ApiUrls.getSplashDataApi
       // self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: nil, method: .get, encoding: .queryString, { responseData in
            self.startedBtn.isHidden = false
           // self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                if let splashData = responseData["data"] as? [String:Any]{
                    self.guestLoginInfoDict = splashData
                    splashSingelton.sharedInstance.appstoreVersion = splashData["version_code_ios"] as? String ?? ""
                    splashSingelton.sharedInstance.updateMessage = splashData["message"] as? String ?? ""
                    splashSingelton.sharedInstance.adsMessage = splashData["reward_message"] as? String ?? ""
                    if splashData["version_force_ios"] as? String  == "1"{
                        splashSingelton.sharedInstance.isForceUpdate = true
                    }else if splashData["version_force_ios"] as? String  == "0"{
                        splashSingelton.sharedInstance.isForceUpdate = false
                    }
                    if let charityLink = splashData["url_link_charity"] as? String , let charityImage =  splashData["charity_image"] as? String{
                        splashSingelton.sharedInstance.charity_image = charityImage
                        splashSingelton.sharedInstance.charityLink = charityLink
                    }
                    splashSingelton.sharedInstance.adRewardedDays = splashData["expire_time"] as? Int ?? 5
                    splashSingelton.sharedInstance.splashDataModel = SplashDataModel.init(ads_screens: splashData["ads_screens"] as? [String:Any])

                }
              
            }
            print("success")
            
        }) { error in
          //  self.removeSpinner()
            self.startedBtn.isHidden = false
            print("error")
        }
    }
}
