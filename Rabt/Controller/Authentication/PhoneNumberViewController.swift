//
//  PhoneNumberViewController.swift
//  Rabt
//
//  Created by Ali Sher on 01/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData
import Toast


class PhoneNumberViewController: BasePhoneNumberViewController {
    
    @IBOutlet weak var phoneView: PhoneView!
    @IBOutlet weak var agreementTextView: UITextView!
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var guestView: UIView!
    @IBOutlet weak var guestLoginBtn: UIButton!
    var guestLoginInfoDict : [String:Any]? = [:]
    var phone = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if guestLoginInfoDict?["guest_user"] as? String == "0" || guestLoginInfoDict?["guest_user"] as? Int == 0 {
            guestView.isHidden = false
            let btnTitle = guestLoginInfoDict?["label"] as? String
            guestLoginBtn.setTitle(btnTitle, for: .normal)
        }else{
            guestView.isHidden = true
        }
        self.navigationController?.navigationBar.isHidden = true
        self.PhoneMainView = phoneView
        self.selectedCountryCode = "PK"
        
        self.LoadPhoneView()
        setTextAndDesign()
    }
    
    func setTextAndDesign() -> Void {
        
        //GeneralMethods.CreateCardViewWithoutBackColor(radius: 8, view: phoneView)
       // phoneView.clipsToBounds = true
        phoneView.layer.borderColor = UIColor.black.cgColor
        phoneView.layer.borderWidth = 1
        self.countryCodeLabel?.textColor = .black
        GeneralMethods.ChangeImgViewTintColor(imgView: self.downArrowIcon!, color: .black)
        
        self.phoneTF?.setTextField()
        
        self.phoneIcon!.layer.cornerRadius = self.phoneIcon!.frame.size.height/2
        self.phoneIcon?.clipsToBounds = true
        
        let attributedString = NSMutableAttributedString(string: "agreement text".localized())
        let url = URL(string: "https://rabt.co.uk/tos/")!
        let url1 = URL(string: "https://rabt.co.uk/tos/")!
        
        
        let range: NSRange = attributedString.mutableString.range(of: "Terms of Service".localized(), options: .caseInsensitive)
        let range1: NSRange = attributedString.mutableString.range(of: "Privacy Policy".localized(), options: .caseInsensitive)
        
        attributedString.setAttributes([.link: url], range: range)
        attributedString.setAttributes([.link: url1], range: range1)
        
        self.agreementTextView.attributedText = attributedString
        self.agreementTextView.isUserInteractionEnabled = true
        //self.agreementTextView.isSelectable = false
        self.agreementTextView.isEditable = false
        
        self.agreementTextView.textColor = UIColor.black
        self.agreementTextView.linkTextAttributes = [
            .foregroundColor: UIColor.black,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        self.agreementTextView.textAlignment = .center
      //  GeneralMethods.CreateCardViewWithoutShadow(radius: nextBtn.frame.size.height/2, view: nextBtn)
        
    }
    @IBAction func guestLoginBtnTapped(_ sender: Any) {
        guestLoginBtn.isEnabled = false
        var uniqueID = ""
        if let id = UserDefaults.standard.value(forKey: "uniqueId") as? String{
            uniqueID = id
        }else{
            uniqueID  = UIDevice.current.identifierForVendor?.uuidString ?? ""
            UserDefaults.standard.set(UIDevice.current.identifierForVendor?.uuidString ?? "", forKey: "uniqueId")
        }
        let parameter = ["device_type": "ios",
                         "phone": "",
                         "guest_user" :"0",
                         "device_id" : uniqueID,
                         "device_token" : UserDefaults.standard.value(forKey: "device_token") as? String ?? ""
        ]
        
        let apiManager = ApiManager()
        let url = ApiUrls.loginApi
        //self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            
          //  self.removeSpinner()
            
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 0 {
                
               // self.moveToRegister()
                
            }else if result == 1{
                
                let apiData = responseData["data"] as! [String:Any]
//                AnalyticsManger.logEventWith(name: "Guest_Login", attributes: ["device_type": "ios",
//                                                                               "device_id" : UUID().uuidString])
                self.MoveToHome(dict: apiData)
                
            }else{
                
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
                self.guestLoginBtn.isEnabled = true
            }
            print("success")
            
        }) { error in
          //  self.removeSpinner()
            print("error")
            self.guestLoginBtn.isEnabled = true
        }
        
    }
    @IBAction func nextBtnClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        if phoneTF!.isEmpty {
            
            self.view.makeToast(String(format: "invalidphnumber".localized(), ExampleNumber))
            
        }else if !self.IsThisValidNumber(str: phoneTF!.text!) {
            
            self.view.makeToast(String(format: "invalidphnumber".localized(), ExampleNumber))
            
        }else {
            if splashSingelton.sharedInstance.isOtpEnabled == true{
                self.sendOTP()
            }else{
                LoginUser()
            }
            
        }
    }
    
    
    func sendOTP() -> Void {
        
        var phone = ""
        if EFINumber.contains("+") {
            phone = EFINumber
        }else{
            phone = "+\(EFINumber)"
        }
        
        let parameter = [
                         "phone": phone
        ]
        
        let apiManager = ApiManager()
        let url = ApiUrls.sendOTPApi
       // self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            
           // self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                
                let apiData = responseData["data"] as! [String:Any]
                let otp =  apiData["otp"] as? String ?? String(apiData["otp"] as? Int ?? 0)
                
                self.moveToOTP(otp: otp)
                
            }else{
                
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
                
            }
            
            print("success")
            
        }) { error in
          //  self.removeSpinner()
            print("error")
        }
    }
    
    func moveToOTP(otp: String) -> Void {
        
        let storyboard = AuthenticationStoryBoard
        let VC = storyboard.instantiateViewController(withIdentifier: "PinVerificationViewController") as! PinVerificationViewController
        VC.phone = EFINumber
        VC.pin = otp
        self.navigationController!.pushViewController(VC, animated: true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension PhoneNumberViewController{
    func LoginUser() -> Void {
        
        
        if EFINumber.contains("+") {
            phone = EFINumber
        }else{
            phone = "+\(EFINumber)"
        }
        var uniqueID = ""
        if let id = UserDefaults.standard.value(forKey: "uniqueId") as? String{
            uniqueID = id
        }else{
            uniqueID  = UIDevice.current.identifierForVendor?.uuidString ?? ""
            UserDefaults.standard.set(UIDevice.current.identifierForVendor?.uuidString ?? "", forKey: "uniqueId")
        }
        let parameter = ["device_type": "ios",
                         "phone": phone,
                         "guest_user" :"1",
                         "device_id" : uniqueID,
                         "device_token" : UserDefaults.standard.value(forKey: "device_token") as? String ?? ""
        ]
        
        let apiManager = ApiManager()
        let url = ApiUrls.loginApi
        //self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            
            //  self.removeSpinner()
            
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 0 {
                
                self.moveToRegister()
               
            }else if result == 1{
                
                let apiData = responseData["data"] as! [String:Any]
                self.MoveToHome(dict: apiData)
//                AnalyticsManger.logEventWith(name: "User_Login", attributes: ["device_type": "ios",
//                                                                              "phone": self.phone,
//                                                                              "name" : apiData["name"] as? String ?? "",
//                                                                               "device_id" : UUID().uuidString])
            }else{
                
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
            }
            print("success")
            
        }) { error in
            //  self.removeSpinner()
            print("error")
        }
    }
    
    func moveToRegister() -> Void {
        
        let storyboard = AuthenticationStoryBoard
        let VC = storyboard.instantiateViewController(withIdentifier: "NameViewController") as! NameViewController
        VC.phone = phone
        self.navigationController!.pushViewController(VC, animated: true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    func MoveToHome(dict: [String:Any]) -> Void {
        self.guestLoginBtn.isEnabled = true
        UserDefaults.standard.setValue(dict, forKey: "userinfo")
        UserDefaults.standard.synchronize()
        
        let access_token = dict["access_token"] as? String ?? ""//GeneralMethods.getValue(dic: dict, val: "access_token")
        UserDefaults.standard.setValue(access_token, forKey: "access_token")
        UserDefaults.standard.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "myTabBar") as! UITabBarController
        if let revealCon = revealViewController(){
            revealCon.pushFrontViewController(VC, animated: true)
        }
        
        
    }
}

