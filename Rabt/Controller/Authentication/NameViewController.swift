//
//  PinVerificationViewController.swift
//  Rabt
//
//  Created by Ali Sher on 01/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData

class NameViewController: MasterViewController {
    
    @IBOutlet weak var subTitleLbl: UILabel!
    
    
    @IBOutlet weak var fnameView: UIView!
    @IBOutlet weak var fnameTF: UITextField!
    
    @IBOutlet weak var lnameView: UIView!
    //@IBOutlet weak var lnameTF: UITextField!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var nextBtn: UIButton!
    
    @IBOutlet weak var dobBtn: UIButton!
    
    var enteredPin = ""
    var phone = ""
    var dob = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTextAndDesign()
        
    }
    
    func setTextAndDesign() -> Void {
        
//        GeneralMethods.CreateCardViewWithoutBackColor(radius: 6, view: fnameView)
//        fnameView.clipsToBounds = true
        //fnameTF.placeholder = "First Name".localized()
        fnameTF.setTextField()

       // GeneralMethods.CreateCardViewWithoutBackColor(radius: 6, view: lnameView)
       // lnameView.clipsToBounds = true
//        lnameTF.placeholder = "Last Name".localized()
//        lnameTF.setTextField()
        
      //  GeneralMethods.CreateCardViewWithoutBackColor(radius: 6, view: emailView)
      //  emailView.clipsToBounds = true
      //  emailTextField.placeholder = "Email Address".localized()
        emailTextField.setTextField()
        
      //  GeneralMethods.CreateCardViewWithoutShadow(radius: nextBtn.frame.size.height/2, view: nextBtn)
        
      //  subTitleLbl.text = "We use your real name to create genuine experiences".localized()
        
       // let underLine = "real name".localized()
        
//        let string: NSMutableAttributedString = NSMutableAttributedString(string: subTitleLbl.text!)
//        string.setUnderLineText(textForAttribute: underLine, withColor: .white)
//
//        subTitleLbl.attributedText = string
        
    }
    func isValidEmailAddress() -> Bool {
      
      var returnValue = true
      let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
      
      do {
          let regex = try NSRegularExpression(pattern: emailRegEx)
          let nsString = (emailTextField.text ?? "") as NSString
          let results = regex.matches(in: (emailTextField.text ?? ""), range: NSRange(location: 0, length: nsString.length))
          
          if results.count == 0
          {
              returnValue = false
          }
          
      } catch let error as NSError {
          print("invalid regex: \(error.localizedDescription)")
          returnValue = false
      }
      
      return  returnValue
  }
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func dobBtnClicked(_ sender: Any) {
//        let storyboard = MainStoryBoard
//        let VC = storyboard.instantiateViewController(withIdentifier: "SelectDateViewController") as! SelectDateViewController
        let storyBoard = MainStoryBoard
        let obj = storyBoard.instantiateViewController(withIdentifier: "SelectDateViewController") as? SelectDateViewController
        obj?.delegate = self
        addChild(obj!)
        view.addSubview(obj!.view)
        obj!.didMove(toParent: self)
       
    }
    @IBAction func nextBtnClicked(_ sender: Any) {
        self.view.endEditing(true)
        
        if fnameTF.isEmpty {
            
            self.view.makeToast("Please enter your name".localized())
            
        }
//        else if lnameTF.isEmpty {
//
//            self.view.makeToast("Please enter last name".localized())
//
//        }
        else if emailTextField.isEmpty {
            
            self.view.makeToast("Please enter Email".localized())
            
        }else if isValidEmailAddress() == false {
            
            self.view.makeToast("Please enter correct Email".localized())
            
        } else if dob == "" || dob.lowercased() == "date of birth"{
            self.view.makeToast("Please enter Date of Birth".localized())
            
        } else {
                        
            self.registerUser()
        }
    }
    
    func registerUser() -> Void {
        
        if !phone.contains("+") {
            phone = "+\(phone)"
        }
        
        let parameter = ["device_type": "ios",
                         "full_name": "\(fnameTF.text!)",
                         "phone": phone,
                         "email": emailTextField.text!,
                         "dob": dob
        ]
        
        let apiManager = ApiManager()
        let url = ApiUrls.registerApi
       // self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            
           // self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1 {
                
                let apiData = responseData["data"] as! [String:Any]
//                AnalyticsManger.logEventWith(name: "User_Signup", attributes: ["device_type": "ios",
//                                                                               "phone": self.phone,"name":"\(self.fnameTF.text ?? "") \(self.lnameTF.text ?? "")",
//                                                                               "device_id" : UUID().uuidString])
//                
//                AnalyticsManger.logEventWith(name: "User_Login", attributes: ["device_type": "ios",
//                                                                              "phone": self.phone,
//                                                                              "name" : apiData["name"] as? String ?? "",
//                                                                               "device_id" : UUID().uuidString])
                self.MoveToHome(dict: apiData)
                
            }else{
                
                //GlobalMethods.showAlert(msg: responseData["message"] as! String)
                let alert = UIAlertController(title: "Error", message: responseData["message"] as? String ?? "", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style: .default, handler: { action in
                })
                alert.addAction(ok)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
            }
            print("success")
            
        }) { error in
          //  self.removeSpinner()
            print("error")
        }
        
    }
    
    
    func MoveToHome(dict: [String:Any]) -> Void {
        
        UserDefaults.standard.setValue(dict, forKey: "userinfo")
        UserDefaults.standard.synchronize()
        
        let access_token = dict["access_token"] as? String ?? ""//GeneralMethods.getValue(dic: dict, val: "access_token")
        UserDefaults.standard.setValue(access_token, forKey: "access_token")
        UserDefaults.standard.synchronize()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "myTabBar") as! UITabBarController
        revealViewController().pushFrontViewController(VC, animated: true)
        
    }
    
}

extension NameViewController: selectDateDelegate {
    func didSelectDate(date: Date) {
        
        dob = PrayerTimeManger.shared.getStringDateWith(format: "dd/MM/yyyy", date: date)
        dobBtn.setTitle(dob, for: .normal)
        dobBtn.setTitleColor(UIColor.black, for: .normal)
    }
}
