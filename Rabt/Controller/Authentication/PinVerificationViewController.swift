//
//  PinVerificationViewController.swift
//  Rabt
//
//  Created by Ali Sher on 01/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData
import SVPinView


class PinVerificationViewController: UIViewController {
    
    @IBOutlet weak var pinView: SVPinView!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    
    
    
    var enteredPin = ""
    var pin = "12345"
    var phone = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTextAndDesign()
    }
    
    func setTextAndDesign() -> Void {
        
        pinView.pinLength = 5
        pinView.style = .box
        pinView.interSpace = 16
        pinView.fieldBackgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.595397534)
        pinView.activeFieldBackgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5960884354)
        pinView.borderLineColor = GlobalVariables.LineAndSubViewsColor
        pinView.activeBorderLineColor = GlobalVariables.LineAndSubViewsColor
        pinView.fieldCornerRadius = 8
        pinView.keyboardType = .phonePad
        pinView.activeBorderLineThickness = 2
        
        pinView.didFinishCallback = { [weak self] pin1 in
            self!.enteredPin = pin1
        }
        
        GeneralMethods.CreateCardViewWithoutShadow(radius: nextBtn.frame.size.height/2, view: nextBtn)
        
        subTitleLbl.text = "Use a 5 digit code to verify your number".localized()
        
        let underLine = "5 digit code".localized()
        let string: NSMutableAttributedString = NSMutableAttributedString(string: subTitleLbl.text!)
        string.setUnderLineText(textForAttribute: underLine, withColor: .white)
        
        subTitleLbl.attributedText = string
        
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        if enteredPin == "" {
            
            self.view.makeToast("Please enter a valid pin code".localized())
            
        }else if enteredPin != pin {
            
            self.view.makeToast("Please enter a valid pin code".localized())
            
        }else {
            
            LoginUser()
        }
    }
    
    func LoginUser() -> Void {
        
        if !phone.contains("+") {
            phone = "+\(phone)"
        }
        var uniqueID = ""
        if let id = UserDefaults.standard.value(forKey: "uniqueId") as? String{
            uniqueID = id
        }else{
            uniqueID  = UIDevice.current.identifierForVendor?.uuidString ?? ""
            UserDefaults.standard.set(UIDevice.current.identifierForVendor?.uuidString ?? "", forKey: "uniqueId")
        }
        let parameter = ["device_type": "ios",
                         "phone": phone,
                         "guest_user" :"1",
                         "device_id" : uniqueID,
                         "device_token" : UserDefaults.standard.value(forKey: "device_token") as? String ?? ""
        ]
        
        let apiManager = ApiManager()
        let url = ApiUrls.loginApi
        //self.showSpinner(onView: self.view)
        
        apiManager.WebService(url: url, parameter: parameter, method: .post, encoding: .queryString, { responseData in
            
            //  self.removeSpinner()
            
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 0 {
                
                self.moveToRegister()
               
            }else if result == 1{
                
                let apiData = responseData["data"] as! [String:Any]
                self.MoveToHome(dict: apiData)
//                AnalyticsManger.logEventWith(name: "User_Login", attributes: ["device_type": "ios",
//                                                                              "phone": self.phone,
//                                                                              "name" : apiData["name"] as? String ?? "",
//                                                                               "device_id" : UUID().uuidString])
            }else{
                
                GlobalMethods.showAlert(msg: responseData["message"] as! String)
            }
            print("success")
            
        }) { error in
            //  self.removeSpinner()
            print("error")
        }
    }
    
    func moveToRegister() -> Void {
        
        let storyboard = AuthenticationStoryBoard
        let VC = storyboard.instantiateViewController(withIdentifier: "NameViewController") as! NameViewController
        VC.phone = phone
        self.navigationController!.pushViewController(VC, animated: true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    func MoveToHome(dict: [String:Any]) -> Void {
        
        UserDefaults.standard.setValue(dict, forKey: "userinfo")
        UserDefaults.standard.synchronize()
        
        let access_token = dict["access_token"] as? String ?? ""//GeneralMethods.getValue(dic: dict, val: "access_token")
        UserDefaults.standard.setValue(access_token, forKey: "access_token")
        UserDefaults.standard.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "myTabBar") as! UITabBarController
        if let revealCon = revealViewController(){
            revealCon.pushFrontViewController(VC, animated: true)
        }
        
        
    }
}
