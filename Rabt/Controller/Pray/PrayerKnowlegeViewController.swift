//
//  PrayerKnowlegeViewController.swift
//  Rabt
//
//  Created by Muhammad on 02/08/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData
import IBAnimatable
class PrayerKnowlegeViewController: UIViewController {
    
    
    @IBOutlet weak var titleLbl: UILabel!
   // @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var dataTableView: UITableView!
    
    @IBOutlet weak var bannerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: UIView!
 //   @IBOutlet weak var bannerImgView: UIImageView!
    
    @IBOutlet weak var previousBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    @IBOutlet weak var donateNowView: UIView!
    @IBOutlet weak var donateNowHeightConstraint: NSLayoutConstraint!
    
    var allBooks : [Any] = []
    var bookIndex = 0
    
    
    
    var details : [Any] = []
    var selectedIndex = -1
    var lastSelectedIndex = -1
    var selectedKnowledgeBook : [String:Any] = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        dataTableView.backgroundColor = .clear
        dataTableView.delegate = self
        dataTableView.dataSource = self
        
        getBookData()
        
        setTextAndDesgin()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setPremiumViews()
    }
    //MARK: - Helper Functions
    func setPremiumViews(){
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            // do nothing
            DispatchQueue.main.async{
                self.donateNowView.isHidden = false
                    self.donateNowHeightConstraint.constant = 50
                self.bannerViewHeightConstraint.constant = 50
            }
        }else{
           
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip && isAutoRenewable{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                    self.bannerViewHeightConstraint.constant = 0
                }
            }else if isVip{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                    self.bannerViewHeightConstraint.constant = 0
                }
            }
            else{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = false
                        self.donateNowHeightConstraint.constant = 50
                    self.bannerViewHeightConstraint.constant = 50
                }
            }
        }
    }
    @objc func getBookData() -> Void {
        
        //self.removeSpinner()
        
        selectedKnowledgeBook = allBooks[bookIndex] as? [String:Any] ?? [:]
        
        details = selectedKnowledgeBook["details"] as? [Any] ?? []
        titleLbl.text = selectedKnowledgeBook["title"] as? String
        
        let url_str = selectedKnowledgeBook["background_image"] as? String ?? ""
        let url = URL(string: url_str)
     //   bgImgView.sd_setImage(with: url , placeholderImage: UIImage(named: "PrayBackground"))
        
//        let url_str1 = selectedKnowledgeBook["banner_image"] as? String ?? ""
//        let url1 = URL(string: url_str1)
//        bannerImgView.sd_setImage(with: url1 , placeholderImage: UIImage(named: ""))

        
        dataTableView.reloadData()
        
        
        if bookIndex == 0 {
            
            previousBtn.alpha = 0.6
            previousBtn.isUserInteractionEnabled = false
            
            nextBtn.alpha = 1.0
            nextBtn.isUserInteractionEnabled = true
            
        }else if bookIndex == allBooks.count - 1 {
            
            previousBtn.alpha = 1.0
            previousBtn.isUserInteractionEnabled = true
            
            nextBtn.alpha = 0.6
            nextBtn.isUserInteractionEnabled = false
            
        }else {
            
            previousBtn.alpha = 1.0
            previousBtn.isUserInteractionEnabled = true
            
            nextBtn.alpha = 1.0
            nextBtn.isUserInteractionEnabled = true
            
        }
        
    }
    
    func setTextAndDesgin() -> Void {
        
        GeneralMethods.setCorners(radius: 16, corners: [.topLeft, .topRight], view: bannerView)
        bannerView.clipsToBounds = true
        
        if allBooks.count == 1 {
            
            previousBtn.isHidden = true
            nextBtn.isHidden = true
            
        }
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func previousClicked(_ sender: Any) {
        selectedIndex = -1
        bookIndex -= 1
       // self.showSpinner(onView: self.view)
        perform(#selector(getBookData), with: nil, afterDelay: 0.0)
    }
    
    @IBAction func nextClicked(_ sender: Any) {
        selectedIndex = -1
        bookIndex += 1
      //  self.showSpinner(onView: self.view)
        perform(#selector(getBookData), with: nil, afterDelay: 0.0)
        
    }
    @IBAction func unlockPremiumBtnTapped(_ sender: Any) {
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.delegate = self
            tabCon.add(subsriptionController)
        }
    }
}


extension PrayerKnowlegeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return details.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("PrayerKnowledgeTableViewCell", owner: self, options: nil)?.first as! PrayerKnowledgeTableViewCell

        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        
        let dict = details[indexPath.row] as? [String:Any] ?? [:]
        
      
        let attributedString = NSMutableAttributedString.init(string: dict["title"] as? String ?? "")
        attributedString.appendWith(text: dict["arabic_name"] as? String ?? "",addNewLine: true,color: UIColor.black)
        cell.titleLbl.attributedText = attributedString
        cell.titleLbl.numberOfLines = 2
        
//        cell.titleLbl.sizeToFit()
//        cell.titleLbl.lineBreakMode = .byTruncatingTail
//        cell.detailsLbl.sizeToFit()
//        cell.detailsLbl.lineBreakMode = .byTruncatingTail
        cell.detailsLbl.numberOfLines = 0
        cell.detailsLbl.text = ""
        cell.arrowImageView.image = UIImage(named: "downGreenIcon")
        if indexPath.row == selectedIndex {
          //  cell.layoutIfNeeded()
          //  UIView.animate(withDuration: 2, animations: {
                
                cell.titleLbl.numberOfLines = 0
            cell.detailsLbl.text = dict["details"] as? String
                
          //  })
            
            cell.arrowImageView.image = UIImage(named: "upGreenIcon")
            
        }
        
       // GeneralMethods.CreateCardView(radius: 0, view: cell.contantView)
        
      //  cell.contantView.backgroundColor = .white
       // cell.contantView.clipsToBounds = true
        
//        if indexPath.row == details.count - 1 {
//
//            GeneralMethods.setCorners(radius: 16, corners: [.bottomLeft, .bottomRight], view: cell.contantView)
//
//        }
        
     //   GeneralMethods.RoundedCorners(shadow: .clear, view: cell.numberLbl)
     //   cell.numberLbl.clipsToBounds = true
        
        cell.numberLbl.text = String(indexPath.row + 1)
        
        let url_str = dict["image"] as? String ?? ""
        let url = URL(string: url_str)
        cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
//        if indexPath.row == selectedIndex{
//            cell.imgView.cornerSides = [.topLeft]
//        }else{
//            cell.imgView.cornerSides = [.topLeft,.bottomLeft]
//        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndex == indexPath.row  {
            return UITableView.automaticDimension
        }else{
            return 80
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lastSelectedIndex = selectedIndex
        if indexPath.row == selectedIndex {
            lastSelectedIndex = -1
            selectedIndex = -1
            
        }else {
            
            selectedIndex = indexPath.row
            
        }
        self.dataTableView.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .none)
       //
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.dataTableView.scrollToRow(at: IndexPath(row: indexPath.row, section: 0), at: .top, animated: false)
        }
        
//        if lastSelectedIndex != selectedIndex{
//
//            dataTableView.scrollToRow(at: IndexPath(row: indexPath.row, section: 0), at: .top, animated: false)
//        }
    }
}
extension PrayerKnowlegeViewController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
        
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.setPremiumViews()
            }
        })
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
            tabCon.add(subsriptionController)
        }
    }
    func subscriptionDidCancel(endDate: String){
        DispatchQueue.main.async {
            self.setPremiumViews()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let tabCon = self.tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
                subsriptionController.endDate = endDate
                tabCon.add(subsriptionController)
            }
        }
        
    }
}
