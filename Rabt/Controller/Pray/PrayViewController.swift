//
//  PrayViewController.swift
//  Rabt
//
//  Created by Ali Sher on 25/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import UIKit
import SwiftData
//import Adhan
import CoreLocation
//import RealmSwift
import GoogleMobileAds

class PrayViewController: LoaderViewController {
    
    
    @IBOutlet weak var bannerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var dataScrollView: UIScrollView!
    @IBOutlet weak var loadingView: LoadingOrNoRecordView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var notifyView: UIView!
    @IBOutlet weak var notifyIcon: UIImageView!
    @IBOutlet weak var navNextPrayerLbl: UILabel!
    @IBOutlet weak var navViewHeightCons: NSLayoutConstraint!
    // Next Pray View
    
    @IBOutlet weak var nextPrayView: UIView!
    @IBOutlet weak var nextPrayerRemaningTimeLbl: UILabel!
    @IBOutlet weak var nextPrayerIcon: UIImageView!
    @IBOutlet weak var nextPrayerNameAndTimeLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
   // @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var locationEnableBtn: UIButton!
    //  Prayers View
    
    @IBOutlet weak var prayersView: UIView!
    @IBOutlet weak var prayersTableView: UITableView!
    
    //Prayer Books View
    @IBOutlet weak var prayerKnowledgeBooksView: UIView!
    @IBOutlet weak var prayerKnowledgeBooksHeightCons: NSLayoutConstraint!
    @IBOutlet weak var prayKnowledgeBooksCollectionView: UICollectionView!
    @IBOutlet weak var fixedAdView: UIView!
    @IBOutlet weak var donateNowView: UIView!
    @IBOutlet weak var donateNowHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var donateNowCardHeightConstraint: NSLayoutConstraint!
    //Properties
    var prayerAnalyticsHit = false
    var prayerKnowledgeBooks : [Any] = []
    var nextPrayerTimer: Timer?
    var locationHandler : LocationHandler = LocationHandler()
    var prayerTimeManger : PrayerTimeManger = PrayerTimeManger()
    var todayPrayersData : prayerData?
    var upcomingPrayerName : String?
    var upcomingPrayerRemainingTime : String?
    var locationManager : MyLocationManagerViewController!
    var prayerNotificatonsHandler : NotificationsHandler? = NotificationsHandler()
    
    var adLoader: GADAdLoader = GADAdLoader()
    var nativeAds = [GADNativeAd]()
    var fixedNativeAd = [GADNativeAd]()
//    var heightConstraint: NSLayoutConstraint?
    var booksAndAdsArray : [Any]? = []
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = MyLocationManagerViewController()
        prayerNotificatonsHandler?.delegate = self
        dataScrollView.delegate = self
        self.navigationController?.navigationBar.isHidden = true
        navView.isHidden = true
        setTextAndDesign()
        registerCollectionViewCells()
        prayKnowledgeBooksCollectionView.isScrollEnabled = false
        setCollectionViewHeight()
        prayersTableView.isScrollEnabled = false
        prayersTableView.backgroundColor = .clear
        prayersTableView.delegate = self
        prayersTableView.dataSource = self
        self.LoaderView = loadingView
        self.showLoader()
        loadingView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prayerAnalyticsHit = false
        singeltonClass.shared.naviagtionController = self.navigationController
        //  AnalyticsManger.logEventWith(name: "Screen_View", attributes: ["source": "pray screen"])
        self.getPrayersServices()
        nextPrayerTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(getTimings), userInfo: nil, repeats: true)
        getTimings()
        
        if splashSingelton.sharedInstance.splashDataModel?.prayer_screen_banner == false{
            
            bannerView.isHidden = true
        }
        if splashSingelton.sharedInstance.splashDataModel?.prayer_scroll_native == false{
            nativeAds = []
            booksAndAdsArray = []
            if prayerKnowledgeBooks.count ?? 0 > 0 {
                booksAndAdsArray?.append(contentsOf: prayerKnowledgeBooks)
            }
            reloadCollectionView()
            showFixedAd()
        }
        setPremiumViews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        nextPrayerTimer?.invalidate()
        nextPrayerTimer = nil
        
    }
    
    //MARK: - Helper Functions
    func setPremiumViews(){
        if ((userInfo as? [String:Any])?["guest_user"] as? Int) == 0 || ((userInfo as? [String:Any])?["guest_user"] as? String) == "0" {
            // do nothing
            DispatchQueue.main.async{
                self.donateNowView.isHidden = false
                    self.donateNowHeightConstraint.constant = 50
                self.donateNowCardHeightConstraint.constant = 145
                self.bannerHeightConstraint.constant = 50
                
            }
        }else{
           
            let (isVip,isAutoRenewable) = IAPRecieptValidator.sharedInstance.isVIPuser()
            if isVip && isAutoRenewable{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                    self.donateNowCardHeightConstraint.constant = 0
                    self.bannerHeightConstraint.constant = 0
                }
            }else if isVip{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = true
                        self.donateNowHeightConstraint.constant = 0
                    self.donateNowCardHeightConstraint.constant = 0
                    self.bannerHeightConstraint.constant = 0
                }
            }
            else{
                DispatchQueue.main.async{
                    self.donateNowView.isHidden = false
                        self.donateNowHeightConstraint.constant = 50
                    self.donateNowCardHeightConstraint.constant = 145
                    self.bannerHeightConstraint.constant = 50
                }
            }
        }
    }
    func registerCollectionViewCells(){
      
        prayKnowledgeBooksCollectionView.register(UINib(nibName: "PrayBookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "prayBookCell")
        prayKnowledgeBooksCollectionView.register(UINib(nibName: "NativeAdsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NativeAdsCollectionCell")
        reloadCollectionView()
    }
    func reloadCollectionView(){
        addNativeAdsToBooksData()
        prayKnowledgeBooksCollectionView.backgroundColor = .clear
        prayKnowledgeBooksCollectionView.delegate = self
        prayKnowledgeBooksCollectionView.dataSource = self
        prayKnowledgeBooksCollectionView.reloadData()
        self.setCollectionViewHeight()
    }
    func setTextAndDesign() -> Void {
        
       // GeneralMethods.CreateCardViewWithoutBackColor(radius: 8, view: nextPrayView)
       // nextPrayView.clipsToBounds = true
        
        //GeneralMethods.CreateCardViewWithoutBackColor(radius: 8, view: prayersView)
        prayersView.clipsToBounds = true
        
        GeneralMethods.ChangeImgViewTintColor(imgView: notifyIcon, color: .black)
        GeneralMethods.RoundedCorners(shadow: .clear, view: notifyView)
        
        if GeneralMethods.isIphoneXShaped() {
            
            navViewHeightCons.constant = 100
        }else {
            
            navViewHeightCons.constant = 75
        }
        
        prayersView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3043686224)
   //     GeneralMethods.ChangeImgViewTintColor(imgView: locationIcon, color: .black)
    }
    
    func getPrayersServices() -> Void {
        
        let apiManager = ApiManager()
        let url = ApiUrls.prayerServicesApi
        
        apiManager.WebService(url: url, parameter: nil, method: .get, encoding: .queryString, { responseData in
            
            // self.removeSpinner()
            let result = responseData["result"] as? Int ?? Int(responseData["result"] as? String ?? "-1")
            if result == 1{
                
                let apiData = responseData["data"] as? [Any] ?? []
                self.prayerKnowledgeBooks = apiData
                self.booksAndAdsArray?.append(contentsOf: self.prayerKnowledgeBooks)
                DispatchQueue.main.async {
                    self.reloadCollectionView()
                }
            }else{
                
                //GlobalMethods.showAlert(msg: responseData.value(forKey: "message") as! String)
                
            }
            print("success")
        }) { error in
            // self.removeSpinner()
            print("error")
        }
    }
    
    @objc func getTimings() -> Void {
        if PrayerTimeManger.shared.selectedLocationName == nil{
            locationLbl.text = ""
        }
        if !(locationHandler.isLocationServicesEnabled()){
            locationLbl.isHidden = true
            //  locationEnableBtn.isHidden = false
            locationEnableBtn.setTitle("Click here to enable location", for: .normal)
            // set Mecca as default location
            PrayerTimeManger.shared.prayerTimeCoordinates = ["latitude":"21.422487","longitude":"39.826206"]
            prayerTimeManger.delegate = self
            prayerTimeManger.getPrayerTimings()
        }
        else{
            locationLbl.isHidden = false
            //locationEnableBtn.isHidden = true
            locationEnableBtn.setTitle("", for: .normal)
            if PrayerTimeManger.shared.prayerTimeCoordinates != nil{
                if let customLocationCoordinates = UserDefaults.standard.value(forKey: "selectedPrayerTimesCoordinate") as? [String:String]{
                    // use user save locatoin coordinates for prayer timings
                    PrayerTimeManger.shared.prayerTimeCoordinates = customLocationCoordinates
                    
                }
                prayerTimeManger.delegate = self
                prayerTimeManger.getPrayerTimings()
            } else{
                // get current loation of user to fetch prayer timings
                locationHandler.delegate = self
                locationHandler.getCurrentLocation()
            }
        }
    }
    func setNotifyMeViewData() -> Void {
        
        DispatchQueue.main.async {
            self.navNextPrayerLbl.text = "\(self.upcomingPrayerName ?? "") in \(self.upcomingPrayerRemainingTime?.replacingOccurrences(of: "Next Prayer in ", with: "") ?? "")"
            let prayersNotificationData = self.prayerNotificatonsHandler?.getAllEnabledPrayers()
            if prayersNotificationData!.contains(self.upcomingPrayerName!){
                self.notifyIcon.image = UIImage(named: "AudioON")
            }else{
                self.notifyIcon.image = UIImage(named: "AudioOFF")
            }
            GeneralMethods.ChangeImgViewTintColor(imgView: self.notifyIcon, color: .black)
        }
    }
    func addNativeAdsToBooksData() {
        if nativeAds.count <= 0 {
            loadAds()
            return
        }
        if (prayerKnowledgeBooks.count ?? 0) < 5{
            return
        }
        
        // let adInterval = ((booksViewModal.latestAudioBooks?.count ?? 0) / nativeAds.count) + 1
        booksAndAdsArray = []
        booksAndAdsArray?.append(contentsOf: prayerKnowledgeBooks)
        var index = 4
        for nativeAd in nativeAds {
            if index < (booksAndAdsArray?.count ?? 0) {
                booksAndAdsArray?.insert(nativeAd, at: index)
                index += 5
            } else {
                break
            }
        }
    }
    
    func loadAds() {
        
        if prayerKnowledgeBooks.count ?? 0 == 0{
            return
        }
        if nativeAds.count > 0{
            return
        }
        
        if splashSingelton.sharedInstance.splashDataModel?.prayer_screen_banner == true{
            //Load Banner
            bannerView.adUnitID = AdManager().bannerId
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
        }
        if splashSingelton.sharedInstance.splashDataModel?.prayer_scroll_native == true{
            // Load native ads
            let options = GADMultipleAdsAdLoaderOptions()
            
            var numberOfAds = (prayerKnowledgeBooks.count ?? 0) / 4
            if numberOfAds > 5{
                numberOfAds = 5
            }
            options.numberOfAds = numberOfAds
            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ GADSimulatorID ]
            adLoader = GADAdLoader(
                adUnitID: AdManager().nativeadUnitID, rootViewController: self,
                adTypes: [.native], options: [options])
            adLoader.delegate = self
            adLoader.load(GADRequest())
        }
       
    }
    
        func showFixedAd(){
            if splashSingelton.sharedInstance.splashDataModel?.prayer_scroll_native == false{
                fixedNativeAd = []
                fixedAdView.isHidden = true
             return
            }
            if  fixedNativeAd.count > 0{
                fixedAdView.isHidden = false
                let adView = Bundle.main.loadNibNamed("NativeAdView", owner: nil, options: nil)
                if let nativeAdView = adView?.first as? GADNativeAdView{
                    let nativeAd = fixedNativeAd[0]
                    /// Set the native ad's rootViewController to the current view controller.
                    nativeAd.rootViewController = self
                    nativeAdView.translatesAutoresizingMaskIntoConstraints = false
                    nativeAdView.tag = 50
                    fixedAdView?.addSubview(nativeAdView)
                    let viewDictionary = ["_nativeAdView": nativeAdView]
                    fixedAdView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "H:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    fixedAdView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "V:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    setupNativeAdView(nativeAdView: nativeAdView,nativeAd:nativeAd)
                }
            }
        }
    //MARK: - IBActions
    
    @IBAction func locationEnableBtnTapped(_ sender: Any) {
        if !(locationManager.isLocationServicesEnabled()){
            locationManager.prayController = self
            locationManager.enableUsersLocationServicesAuthorization()
            //locationHandler.allowLocationServices()
        }else{
            let placeStoryboard = UIStoryboard.init(name: "Places", bundle: nil)
            let placeController = placeStoryboard.instantiateViewController(withIdentifier: "PlaceSelectionController") as! PlaceSelectionController
            placeController.delegate = self
            self.present(placeController, animated: true, completion: nil)
        }
        
    }
    @IBAction func notifyMeBtnClicked(_ sender: Any) {
        print("Notify tapped")
        let prayersNotificationList: [String]! = NotificationsHandler().getAllEnabledPrayers()
        if prayersNotificationList.contains(upcomingPrayerName ?? "empty"){
            prayerNotificatonsHandler?.removeScheduledNotificationsFor(prayer: NotificationsHandler().getPrayerTypeFrom(name: upcomingPrayerName!))
        }else{
            prayerNotificatonsHandler?.scheduleNotificationFor(prayer: NotificationsHandler().getPrayerTypeFrom(name: upcomingPrayerName!))
        }
        GeneralMethods.ChangeImgViewTintColor(imgView: notifyIcon, color: .black)
    }
    
    @IBAction func unlockPremiumBtnTapped(_ sender: Any) {
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubsriptionOptionController") as! SubsriptionOptionController
            subsriptionController.delegate = self
            tabCon.add(subsriptionController)
        }
    }
    
    @IBAction func calculationMethodBtnClicked(_ sender: Any) {
    }
}

extension PrayViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y > 70 {
            
            // UIView.transition(with: navView, duration: 1.0,
            //      options: .transitionCrossDissolve,
            //      animations: {
            self.navView.isHidden = false
            //          })
        }else {
            
            //   UIView.transition(with: navView, duration: 1.0,
            //            options: .transitionCrossDissolve,
            //            animations: {
            self.navView.isHidden = true
            //                     })
        }
    }
}


extension PrayViewController : locationHandlerDelegate{
    func locationUpdatedWith(coordinates: CLLocation) {
        let selectedPrayerTimesCoordinate = ["latitude": "\(coordinates.coordinate.latitude)", "longitude": "\(coordinates.coordinate.longitude)"]
        PrayerTimeManger.shared.prayerTimeCoordinates = selectedPrayerTimesCoordinate
        getTimings()
    }
    
    func locationServicesDisabledError() {
        
    }
    
    func locationUpdatedWith(address: String, city: String) {
        
    }
}
extension PrayViewController : prayerTimeDetailsDelegate{
    func prayersLocationNameDidFetched() {
        locationLbl.text = PrayerTimeManger.shared.selectedLocationName
        if prayerAnalyticsHit == false{
            let userData = UserDefaults.standard.value(forKey: "userinfo") as? [String:Any]
            AnalyticsManger.logEventWith(name: "track_prayer", attributes: ["username": userData?["user_name"] ?? "Guest" , "devicetype" : "iOS", "email": userData?["email"] ?? "Guest", "phone": userData?["phone"] ?? "Guest","location":myLocationPlacemark?.country ?? ""])
            prayerAnalyticsHit = true
        }
    }
    
    func prayerTimeFetchedWith(prayersData: prayerData?, nextPrayerName: String,nextPrayerTime:String, nextPrayerCountDown: String) {
        todayPrayersData = prayersData
        upcomingPrayerName = nextPrayerName
        upcomingPrayerRemainingTime = nextPrayerCountDown
        nextPrayerRemaningTimeLbl.text = nextPrayerCountDown
        nextPrayerNameAndTimeLbl.text = "\(nextPrayerName) \(nextPrayerTime)"
        switch nextPrayerName.lowercased(){
        case "fajr" :
            nextPrayerIcon.image = UIImage(named: "fajr.png")
            break
        case "dhuhr" :
            nextPrayerIcon.image = UIImage(named: "dhuhr.png")
            break
        case "asr" :
            nextPrayerIcon.image = UIImage(named: "asr.png")
            break
        case "maghrib" :
            nextPrayerIcon.image = UIImage(named: "magrib.png")
            break
        case "isha" :
            nextPrayerIcon.image = UIImage(named: "isha.png")
            break
        default:
            break
        }
        if PrayerTimeManger.shared.shouldRefreshPrayerNotifications == true{
            prayerNotificatonsHandler?.scheduleAllEnabledNotifications()
        }
        DispatchQueue.main.async {
            /// set notifyme view
            self.setNotifyMeViewData()
            /// reload data for prayers
            self.prayersTableView.reloadData()
        }
        
    }
}
extension PrayViewController : googlePlacesCustomDelegate{
    func placesSearchCompleted() {
        
    }
    
    func coordinateFetchingCompleted() {
        //custom location selected so referesh timings
        getTimings()
    }
    
    func placeNameFetchingCompleted() {
        locationLbl.text = PrayerTimeManger.shared.selectedLocationName
    }
}
extension PrayViewController : prayerNotifcationsDelegate{
    
    func notificationSetFor(Prayer: PrayerType) {
        /// reload tableView
        DispatchQueue.main.async {
            /// set notifyme view
            self.setNotifyMeViewData()
            /// reload data for prayers
            self.prayersTableView.reloadData()
        }
//        prayersTableView.reloadData()
//        /// set notifyme view
//        setNotifyMeViewData()
    }
    
    func notificationRemovedFor(Prayer: PrayerType) {
        DispatchQueue.main.async {
            /// set notifyme view
            self.setNotifyMeViewData()
            /// reload data for prayers
            self.prayersTableView.reloadData()
        }
//        /// reload tableview
//        prayersTableView.reloadData()
//        /// set notifyme view
//        setNotifyMeViewData()
    }
    
    func notificationsPausedFromSettings() {
        DispatchQueue.main.async {
            /// set notifyme view
            self.setNotifyMeViewData()
            /// reload data for prayers
            self.prayersTableView.reloadData()
        }
//        /// show popup to enable notifications from  app settings
//        prayersTableView.reloadData()
//        /// set notifyme view
//        setNotifyMeViewData()
    }
    
    func notificationsEnabledFromSettings() {
        
    }
    func notificationsDisabledByUser() {
        /// show popup to enable notifications from iphone settings
    }
    
}
extension PrayViewController: GADNativeAdLoaderDelegate,GADNativeAdDelegate,GADVideoControllerDelegate{
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        if fixedNativeAd.count == 0{
            // show first ad as fixed
            fixedNativeAd.append(nativeAd)
            showFixedAd()
        }else{
            nativeAds.append(nativeAd)
            addNativeAdsToBooksData()
            nativeAd.delegate = self
            reloadCollectionView()
        }
       
        
        
        //        collectionviewAUction.reloadData()
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        
    }
    func setupNativeAdView(nativeAdView : GADNativeAdView,nativeAd:GADNativeAd){
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        
        // Some native ads will include a video asset, while others do not. Apps can use the
        // GADVideoController's hasVideoContent property to determine if one is present, and adjust their
        // UI accordingly.
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
            // By acting as the delegate to the GADVideoController, this ViewController receives messages
            // about events in the video lifecycle.
            mediaContent.videoController.delegate = self
            //  videoStatusLabel.text = "Ad contains a video asset."
        } else {
            //videoStatusLabel.text = "Ad does not contain a video."
        }
        
        // This app uses a fixed width for the GADMediaView and changes its height to match the aspect
        // ratio of the media it displays.
        if let mediaView = nativeAdView.mediaView, nativeAd.mediaContent.aspectRatio > 0 {
            let heightConstraint = NSLayoutConstraint(
                item: mediaView,
                attribute: .height,
                relatedBy: .equal,
                toItem: mediaView,
                attribute: .width,
                multiplier: CGFloat(1 / nativeAd.mediaContent.aspectRatio),
                constant: 0)
            heightConstraint.isActive = true
        }
        
        // These assets are not guaranteed to be present. Check that they are before
        // showing or hiding them.
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        
        // (nativeAdView.starRatingView as? UIImageView)?.image = imageOfStars(from: nativeAd.starRating)
        nativeAdView.starRatingView?.isHidden = nativeAd.starRating == nil
        
        (nativeAdView.storeView as? UILabel)?.text = nativeAd.store
        nativeAdView.storeView?.isHidden = nativeAd.store == nil
        
        (nativeAdView.priceView as? UILabel)?.text = nativeAd.price
        nativeAdView.priceView?.isHidden = nativeAd.price == nil
        
        (nativeAdView.advertiserView as? UILabel)?.text = nativeAd.advertiser
        nativeAdView.advertiserView?.isHidden = nativeAd.advertiser == nil
        
        // In order for the SDK to process touch events properly, user interaction should be disabled.
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
        
        // Associate the native ad view with the native ad object. This is
        // required to make the ad clickable.
        // Note: this should always be done after populating the ad views.
        nativeAdView.nativeAd = nativeAd
    }
    
}

extension PrayViewController : subscriptionDelegate{
    func rewardAdDidWatchedSuccessfully(playIndex:Int) {
        
    }
    func premiumContentPurchased(){
        IAPRecieptValidator.sharedInstance.fetchLatestReceiptData(completion: {status,error in
            DispatchQueue.main.async {
                self.setPremiumViews()
            }
        })
        if let tabCon = tabBarController?.parent{
            let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
            let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionActivatedController") as! SubscriptionActivatedController
            tabCon.add(subsriptionController)
        }
    }
    func subscriptionDidCancel(endDate: String){
        DispatchQueue.main.async {
            self.setPremiumViews()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let tabCon = self.tabBarController?.parent{
                let storyBoard = UIStoryboard.init(name: "Subscription", bundle: nil)
                let subsriptionController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionUnsubscribedController") as! SubscriptionUnsubscribedController
                subsriptionController.endDate = endDate
                tabCon.add(subsriptionController)
            }
        }
        
    }
}
