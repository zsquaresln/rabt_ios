////
////  PrayerCalculationMethodsViewController.swift
////  Rabt
////
////  Created by Muhammad on 02/09/2021.
////  Copyright © 2021 Ali Sher. All rights reserved.
////
//
//import UIKit
//import SwiftData
//
//protocol calculationMethodDelegate {
//
//    func didChanged() -> Void
//
//}
//
//class PrayerCalculationMethodsViewController: UIViewController {
//
//    var delegate: calculationMethodDelegate?
//
//
//    @IBOutlet weak var contentView: UIView!
//    @IBOutlet weak var cancelBtn: UIButton!
//    @IBOutlet weak var dataTableView: UITableView!
//
//    var calculationsMethods : [Any] = []
//    var selected = ""
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6044058248)
//
//        calculationsMethods = ["Muslim World League", "Egyptian General Authority of Survey", "University of Islamic Sciences, Karachi", "Umm al-Qura University, Makkah", "The Gulf Region", "Moonsighting Committee", "North America (ISNA)", "Kuwait", "Qatar", "Singapore", "Institute of Geophysics, University of Tehran", "Turkey"]
//
//        selected = UserDefaults.standard.value(forKey: "calculationMethod") as! String
//
//        setDesign()
//        dataTableView.backgroundColor = .clear
//        dataTableView.delegate = self
//        dataTableView.dataSource = self
//
//    }
//
//
//    func setDesign() -> Void {
//
//        GeneralMethods.CreateCardView(radius: 16, view: contentView)
//
//    }
//
//    @IBAction func cancelBtnClicked(_ sender: Any) {
//
//        self.view.removeFromSuperview()
//
//    }
//
//
//}
//
//
//extension PrayerCalculationMethodsViewController: UITableViewDelegate, UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        return 12
//
//    }
//
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = Bundle.main.loadNibNamed("PrayerCalculationMethodsTableViewCell", owner: self, options: nil)?.first as! PrayerCalculationMethodsTableViewCell
//
//        cell.selectionStyle = .none
//        cell.contentView.backgroundColor = .clear
//        cell.backgroundColor = .clear
//
//        let text = calculationsMethods[indexPath.row] as! String
//        cell.nameLbl.text = text
//
//        cell.imgView.image = UIImage(named: "RadioUnChecked.png")
//        GeneralMethods.ChangeImgViewTintColor(imgView: cell.imgView, color: GlobalVariables.DarkGrayColor)
//
//        if text == selected {
//
//            cell.imgView.image = UIImage(named: "RadioChecked.png")
//            GeneralMethods.ChangeImgViewTintColor(imgView: cell.imgView, color: GlobalVariables.AppColor)
//
//        }
//
//        return cell
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return UITableView.automaticDimension
//
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let text = calculationsMethods[indexPath.row] as! String
//        UserDefaults.standard.setValue(text, forKey: "calculationMethod")
//        UserDefaults.standard.synchronize()
//        selected = text
//
//        tableView.reloadData()
//        self.view.removeFromSuperview()
//        delegate?.didChanged()
//
//    }
//
//}
