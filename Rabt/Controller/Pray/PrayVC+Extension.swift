//
//  PrayVC+Extension.swift
//  Rabt
//
//  Created by Ali Sher on 25/06/2021.
//  Copyright © 2021 Ali Sher. All rights reserved.
//

import Foundation
import SwiftData
import GoogleMobileAds

extension PrayViewController {
    
    func setCollectionViewHeight() -> Void {
        
        var width: CGFloat = 0.0
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            width = (self.view.frame.size.width - 32) / 3
        } else {
            
            width = (self.view.frame.size.width - 32) / 2
        }
        
        var total = prayerKnowledgeBooks.count
        var adsTotal = ((booksAndAdsArray?.count ?? 0) - prayerKnowledgeBooks.count)
        if adsTotal < 0{
            adsTotal = 0
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            if total % 3 != 0 {
                if total % 3 == 1{
                    total = total + 2
                }else{
                    total = total + 1
                }
            }
        }
        else{
            if total % 2 != 0 {
                
                total += 1
            }
        }
        
        
        var rows = total / 2
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            rows = total / 3
        }
        var height = width + (CGFloat( rows) * 10.0)
        height = CGFloat(rows) * height
        // ad native ads height
        height = height + CGFloat((313 * adsTotal))
        if isCollapsedShown {
            
            height += 50
            
        }
        
        prayerKnowledgeBooksHeightCons.constant = height
    }
    
    @objc func muteOrUnmuteClicked(sender: UIButton) -> Void {
        let prayersNotificationList: [String]! = NotificationsHandler().getAllEnabledPrayers()
        if sender.tag == 0{
            if prayersNotificationList.contains("Fajr"){
                /// remove notification
                prayerNotificatonsHandler?.removeScheduledNotificationsFor(prayer: .Fajr)
            }else{
                /// set notification
                prayerNotificatonsHandler?.scheduleNotificationFor(prayer: .Fajr)
            }
        }else if sender.tag == 1{
            if prayersNotificationList.contains("Dhuhr"){
                /// remove notification
                prayerNotificatonsHandler?.removeScheduledNotificationsFor(prayer: .Dhuhr)
            }else{
                /// set notification
                prayerNotificatonsHandler?.scheduleNotificationFor(prayer: .Dhuhr)
            }
        }else if sender.tag == 2{
            if prayersNotificationList.contains("Asr"){
                /// remove notification
                prayerNotificatonsHandler?.removeScheduledNotificationsFor(prayer: .Asr)
            }else{
                /// set notification
                prayerNotificatonsHandler?.scheduleNotificationFor(prayer: .Asr)
            }
        }else if sender.tag == 3{
            if prayersNotificationList.contains("Maghrib"){
                /// remove notification
                prayerNotificatonsHandler?.removeScheduledNotificationsFor(prayer: .Maghrib)
            }else{
                /// set notification
                prayerNotificatonsHandler?.scheduleNotificationFor(prayer: .Maghrib)
            }
        }else if sender.tag == 4{
            if prayersNotificationList.contains("Isha"){
                /// remove notification
                prayerNotificatonsHandler?.removeScheduledNotificationsFor(prayer: .Isha)
            }else{
                /// set notification
                prayerNotificatonsHandler?.scheduleNotificationFor(prayer: .Isha)
            }
        }
    }
}


extension PrayViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if isCollapsedShown {
            
            return UIEdgeInsets(top: 0, left: 16, bottom: 60, right: 16)
            
        }
        
        return UIEdgeInsets(top: 0, left: 16, bottom: 16, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return booksAndAdsArray?.count ?? 0 //prayerKnowledgeBooks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("tayyab")
        print(indexPath.row)
        if let _ = booksAndAdsArray![indexPath.row] as? GADNativeAd{
            if nativeAds.count > 0{
                if let _ = (booksAndAdsArray?[indexPath.row] as? GADNativeAd)?.mediaContent.hasVideoContent{
                    let width = CGFloat(UIScreen.main.bounds.width) - 36
                    return CGSize (width: width, height: 313)
                }
                
            }
            let width = CGFloat (collectionView.frame.size.width)
            return CGSize (width: width, height: 120)
        }else{
            var width: CGFloat = 0.0
            if UIDevice.current.userInterfaceIdiom == .pad {
                
                width = (self.view.frame.size.width - 32) / 3
            } else {
                
                width = (self.view.frame.size.width - 32) / 2
            }
            width -= 8
            
            return CGSize(width: width, height: width + 30)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let book = booksAndAdsArray![indexPath.row] as? GADNativeAd {
            
            let cell: NativeAdsCollectionCell! = collectionView.dequeueReusableCell(withReuseIdentifier: "NativeAdsCollectionCell", for: indexPath as IndexPath) as? NativeAdsCollectionCell
            if cell.addView?.subviews.count == 0 && nativeAds.count > 0{
                let adView = Bundle.main.loadNibNamed("NativeAdView", owner: nil, options: nil)
                if let nativeAdView = adView?.first as? GADNativeAdView{
                    let nativeAd = booksAndAdsArray?[indexPath.row] as! GADNativeAd
                    /// Set the native ad's rootViewController to the current view controller.
                    nativeAd.rootViewController = self
                    nativeAdView.translatesAutoresizingMaskIntoConstraints = false
                    nativeAdView.tag = 50
                    cell.addView?.addSubview(nativeAdView)
                    let viewDictionary = ["_nativeAdView": nativeAdView]
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "H:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    cell.addView?.addConstraints(
                        NSLayoutConstraint.constraints(
                            withVisualFormat: "V:|[_nativeAdView]|",
                            options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: viewDictionary)
                    )
                    setupNativeAdView(nativeAdView: nativeAdView,nativeAd:nativeAd)
                }
            }
            return cell
        }else{
            let cell : PrayBookCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "prayBookCell", for: indexPath) as! PrayBookCollectionViewCell
            cell.backgroundColor = .clear
            cell.contentView.backgroundColor = .clear
            
            GeneralMethods.CreateCardView(radius: 8, view: cell.contantView)
            cell.contantView.clipsToBounds = true
            
            let dict = booksAndAdsArray?[indexPath.item] as? [String:Any] ?? [:]
            cell.nameLbl.text = dict["title"] as? String ?? ""
            
            let url_str = dict["image"] as? String ?? ""
            let url = URL(string: url_str)
            cell.imgView.sd_setImage(with: url , placeholderImage: UIImage(named: ""))
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if  booksAndAdsArray![indexPath.row] as? GADNativeAd == nil{
            let book = booksAndAdsArray?[indexPath.row] as? [String:Any]
            if let selectedBookIndex = prayerKnowledgeBooks.firstIndex(where: {($0 as?[String:Any])?["id"] as? Int == book?["id"] as? Int}){
                let storyboard = PrayerStoryBoard
                let VC = storyboard.instantiateViewController(withIdentifier: "PrayerKnowlegeViewController") as! PrayerKnowlegeViewController
                VC.allBooks = prayerKnowledgeBooks
                VC.bookIndex = selectedBookIndex
                self.navigationController!.pushViewController(VC, animated: true)
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            }
        }
        
    }
}

extension PrayViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if todayPrayersData == nil{
            return 0
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("prayTimingsTableViewCell", owner: self, options: nil)?.first as! prayTimingsTableViewCell
        
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        
        cell.lineView.isHidden = false
        
        let prayersNotificationList: [String]! = NotificationsHandler().getAllEnabledPrayers()
        
        if indexPath.row == 0 {
            
            
            if prayersNotificationList.contains("Fajr") {
                
                cell.audioBtn.setImage(UIImage(named: "AudioON"), for: .normal)
            }else {
                
                cell.audioBtn.setImage(UIImage(named: "AudioOFF"), for: .normal)
            }
            
            cell.nameLbl.text = "Fajr".localized()
            cell.timeLbl.text = prayerTimeManger.getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: todayPrayersData?.timings?.fajr?.components(separatedBy: " ").first ?? "")
            
        }else if indexPath.row == 1 {
            
            if prayersNotificationList.contains("Dhuhr") {
                
                cell.audioBtn.setImage(UIImage(named: "AudioON"), for: .normal)
            }else {
                
                cell.audioBtn.setImage(UIImage(named: "AudioOFF"), for: .normal)
            }
            
            cell.nameLbl.text = "Dhuhr".localized()
            cell.timeLbl.text = prayerTimeManger.getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: todayPrayersData?.timings?.dhuhr?.components(separatedBy: " ").first ?? "")
            
        }else if indexPath.row == 2 {
            
            if prayersNotificationList.contains("Asr") {
                
                cell.audioBtn.setImage(UIImage(named: "AudioON"), for: .normal)
            }else {
                
                cell.audioBtn.setImage(UIImage(named: "AudioOFF"), for: .normal)
            }
            
            cell.nameLbl.text = "Asr".localized()
            cell.timeLbl.text = prayerTimeManger.getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: todayPrayersData?.timings?.asr?.components(separatedBy: " ").first ?? "")
            
        }else if indexPath.row == 3 {
            
            if prayersNotificationList.contains("Maghrib") {
                
                cell.audioBtn.setImage(UIImage(named: "AudioON"), for: .normal)
            }else {
                
                cell.audioBtn.setImage(UIImage(named: "AudioOFF"), for: .normal)
            }
            
            cell.nameLbl.text = "Maghrib".localized()
            cell.timeLbl.text = prayerTimeManger.getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: todayPrayersData?.timings?.maghrib?.components(separatedBy: " ").first ?? "")
            
        }else if indexPath.row == 4 {
            
            if prayersNotificationList.contains("Isha") {
                
                cell.audioBtn.setImage(UIImage(named: "AudioON"), for: .normal)
            }else {
                
                cell.audioBtn.setImage(UIImage(named: "AudioOFF"), for: .normal)
            }
            
            cell.nameLbl.text = "Isha".localized()
            cell.timeLbl.text = prayerTimeManger.getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: todayPrayersData?.timings?.isha?.components(separatedBy: " ").first ?? "")
            
        }
        
        cell.audioBtn.tag = indexPath.row
        cell.audioBtn.addTarget(self, action: #selector(muteOrUnmuteClicked(sender:)), for: .touchUpInside)
        
      //  GeneralMethods.ChangeBtnTintColor(btn: cell.audioBtn, color: .black)
        
        if cell.nameLbl.text == upcomingPrayerName {
            if prayersNotificationList.contains(upcomingPrayerName ?? "") {
                
                cell.audioBtn.setImage(UIImage(named: "AudioONGreen"), for: .normal)
            }else {
                
                cell.audioBtn.setImage(UIImage(named: "AudioOFFGreen"), for: .normal)
            }
            GeneralMethods.CreateCardViewWithoutShadow(radius: 8, view: cell.contantView)
            cell.contantView.backgroundColor = .white
            
            cell.nameLbl.font = UIFont.boldSystemFont(ofSize: 20.0)
            cell.timeLbl.font = UIFont.boldSystemFont(ofSize: 20.0)
            cell.nameLbl.textColor = UIColor.init(hexString: "326962")
            cell.timeLbl.textColor = UIColor.init(hexString: "326962")
            
        }else {
            
            cell.contantView.backgroundColor = .clear
            cell.nameLbl.font = UIFont.systemFont(ofSize: 17.0)
            cell.timeLbl.font = UIFont.systemFont(ofSize: 17.0)
            
            cell.nameLbl.textColor = .white
            cell.timeLbl.textColor = .white
            
        }
        
        //let time_arr = time.components(separatedBy: " ")
        //let time_Str = time_arr[0]
        //     cell.timeLbl.text = GlobalMethods.convertDateFormat(dateStr: time_Str, sourceformat: "HH:mm", destformat: "hh:mm a")
        if indexPath.row == 4 {
            cell.lineView.isHidden = true
        }
        cell.lineView.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
}
