//
//  NotificationsHandler.swift
//  Rabt
//
//  Created by Asad Khan on 26/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import UserNotifications
protocol prayerNotifcationsDelegate{
    func notificationSetFor(Prayer: PrayerType)
    func notificationRemovedFor(Prayer: PrayerType)
    func notificationsPausedFromSettings()
    func notificationsEnabledFromSettings()
    func notificationsDisabledByUser()
}
enum PrayerType : String{
    case Fajr
    case Dhuhr
    case Asr
    case Maghrib
    case Isha
}
class NotificationsHandler: NSObject {
    
    var delegate : prayerNotifcationsDelegate?
    
    
}
extension NotificationsHandler{
    func scheduleNotificationFor(prayer:PrayerType){
        /// step1: check is user have paused all notifications from settins
        if UserDefaults.standard.value(forKey: "pauseNotifications") as? Bool == true{
            delegate?.notificationsPausedFromSettings()
            return
        }
        /// step2: fetch and remove all notifications of selected prayer type
        removeNotificationFor(prayer: prayer)
        /// step3: save prayer name in userdefaults for all enabled prayers
        if var prayersNotificationList = UserDefaults.standard.value(forKey: "prayersNotificationList") as? [String]{
            if !(prayersNotificationList.contains(prayer.rawValue)){
                prayersNotificationList.append(prayer.rawValue)
                UserDefaults.standard.set(prayersNotificationList, forKey: "prayersNotificationList")
            }
        }
        else{
            UserDefaults.standard.set([prayer.rawValue], forKey: "prayersNotificationList")
        }
        
        /// step4 : check authorization and schedule notification
        getNotificationSettings(){ status in
            if status == true{
                var identifierIndex = 0
                for selectedPrayerData in PrayerTimeManger.shared.prayerData ?? []{
                    let content = UNMutableNotificationContent()
                    var  prayerDate = selectedPrayerData?.date?.gregorian?.date
                    var nextPrayerTime : String = ""
                    var notificatotionFormattedPrayerTime : String = ""
                    switch prayer {
                    case .Fajr:
                        notificatotionFormattedPrayerTime = selectedPrayerData?.timings?.fajr ?? ""
                        nextPrayerTime = PrayerTimeManger().getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.fajr?.components(separatedBy: " ").first ?? "")
                    case .Dhuhr:
                        notificatotionFormattedPrayerTime = selectedPrayerData?.timings?.dhuhr ?? ""
                        nextPrayerTime = PrayerTimeManger().getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.dhuhr?.components(separatedBy: " ").first ?? "")
                    case .Asr:
                        notificatotionFormattedPrayerTime = selectedPrayerData?.timings?.asr ?? ""
                        nextPrayerTime = PrayerTimeManger().getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.asr?.components(separatedBy: " ").first ?? "")
                    case .Maghrib:
                        notificatotionFormattedPrayerTime = selectedPrayerData?.timings?.maghrib ?? ""
                        nextPrayerTime = PrayerTimeManger().getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.maghrib?.components(separatedBy: " ").first ?? "")
                    case .Isha:
                        notificatotionFormattedPrayerTime = selectedPrayerData?.timings?.isha ?? ""
                        nextPrayerTime = PrayerTimeManger().getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.isha?.components(separatedBy: " ").first ?? "")
                    }
                   
                    content.title = "\(prayer.rawValue) at \(nextPrayerTime)"
                    //View prayer times in \(PrayerTimeManger.shared.selectedLocationName ?? "")
                    content.body = ""
                    content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "adhan.mp3"))
              //      content.badge = 1
                    /// date format is dd-MM-yyyy
                  //  prayerDate = "03-03-2022"
                   // notificatotionFormattedPrayerTime = "05:14 ()"
                    let dateComponentsArray = prayerDate?.components(separatedBy: "-")
                    /// time format is HH:mm
                    let timeComponentsArray = (notificatotionFormattedPrayerTime.components(separatedBy: " ").first ?? "").components(separatedBy: ":")
                    //let trigger = UNCalendarNotificationTrigger(dateMatching: DateComponents(calendar: Calendar.current, year: Int(dateComponentsArray![2]), month: Int(dateComponentsArray![1]), day: Int(dateComponentsArray![0]), hour: Int(timeComponentsArray[0]), minute: Int(timeComponentsArray[1])), repeats: false)
                    //Receive with date
                    var dateInfo = DateComponents()
                    dateInfo.day = Int(dateComponentsArray?[0] ?? "0") ?? 0 //Put your day
                    dateInfo.month = Int(dateComponentsArray?[1] ?? "0") ?? 0 //Put your month
                   // dateInfo.year = 2022 // Put your year
                    dateInfo.hour = Int(timeComponentsArray[0]) ?? 0//Put your hour
                    dateInfo.minute = Int(timeComponentsArray[1]) ?? 0//Put your minutes
                    let trigger = UNCalendarNotificationTrigger(dateMatching: dateInfo, repeats: true)
                    let request = UNNotificationRequest(identifier: "\(prayer.rawValue)\(identifierIndex)", content: content, trigger: trigger)
                    identifierIndex = identifierIndex + 1
                    UNUserNotificationCenter.current().add(request) { error in
                        
                        guard error == nil else { return }
                        
                      //  print("Notification scheduled for date: \(prayerDate ?? "") , time : \(notificatotionFormattedPrayerTime)")
                    }
                }
            }else{
                self.delegate?.notificationsDisabledByUser()
                return
            }
        }

        delegate?.notificationSetFor(Prayer: prayer)
    }
    func removeScheduledNotificationsFor(prayer: PrayerType){
        /// step 1 remove all notifications for selected prayer
        removeNotificationFor(prayer: prayer)
        /// remove name of prayer for scheduled notificatons list
        if let prayersNotificationList = UserDefaults.standard.value(forKey: "prayersNotificationList") as? [String]{
            let newNotificationList = prayersNotificationList.filter { $0 != prayer.rawValue }
            UserDefaults.standard.set(newNotificationList, forKey: "prayersNotificationList")
        }
        delegate?.notificationRemovedFor(Prayer: prayer)
    }
    func pauseAllNotifications(){
        /// remove all scheduled notifications
        removeNotificationFor(prayer:.Fajr)
        removeNotificationFor(prayer:.Dhuhr)
        removeNotificationFor(prayer:.Asr)
        removeNotificationFor(prayer:.Maghrib)
        removeNotificationFor(prayer:.Isha)
        /// save pause notifcaitons check in userdefaults
        UserDefaults.standard.set(true, forKey: "pauseNotifications")
        PrayerTimeManger.shared.shouldRefreshPrayerNotifications = false
        delegate?.notificationsPausedFromSettings()
    }
    func scheduleAllEnabledNotifications(){
        
        /// remove all scheduled notficaitons
        removeNotificationFor(prayer:.Fajr)
        removeNotificationFor(prayer:.Dhuhr)
        removeNotificationFor(prayer:.Asr)
        removeNotificationFor(prayer:.Maghrib)
        removeNotificationFor(prayer:.Isha)
        /// step1: check is user have paused all notifications from settins
        if UserDefaults.standard.value(forKey: "pauseNotifications") as? Bool == true{
            delegate?.notificationsPausedFromSettings()
            return
        }
        if UserDefaults.standard.value(forKey: "prayersNotificationList") as? [String] == nil{
            UserDefaults.standard.set(["Fajr","Dhuhr","Asr","Maghrib","Isha"], forKey: "prayersNotificationList")
        }
        /// scheduled notifications for all enabled prayers
        if let prayersNotificationList = UserDefaults.standard.value(forKey: "prayersNotificationList") as? [String]{
            getNotificationSettings(){ status in
                if status == true{
                    if prayersNotificationList.contains("Fajr"){
                        self.scheduleNotificationFor(prayer: .Fajr)
                    }
                    if prayersNotificationList.contains("Dhuhr"){
                        self.scheduleNotificationFor(prayer: .Dhuhr)
                    }
                    if prayersNotificationList.contains("Asr"){
                        self.scheduleNotificationFor(prayer: .Asr)
                    }
                    if prayersNotificationList.contains("Maghrib"){
                        self.scheduleNotificationFor(prayer: .Maghrib)
                    }
                    if prayersNotificationList.contains("Isha"){
                        self.scheduleNotificationFor(prayer: .Isha)
                    }
                }else{
                    self.delegate?.notificationsDisabledByUser()
                    return
                }
            }
        }
        /// save pause notifcaitons check in userdefaults
        UserDefaults.standard.set(false, forKey: "pauseNotifications")
        PrayerTimeManger.shared.shouldRefreshPrayerNotifications = false
        delegate?.notificationsEnabledFromSettings()
    }
    private func removeNotificationFor(prayer:PrayerType){
        /// as notifications are set for next 15 days, add number with notificatoin name as a unique identifier like Fajr0, Fajr1...
        var identifiersArray : [String] = []
        for index in 0...14{
           // print("identifier : \(prayer.rawValue)\(index)")
            identifiersArray.append("\(prayer.rawValue)\(index)")
        }
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiersArray)
    }
}

//MARK: - Other relevent functions
extension NotificationsHandler{
    func removeAllNotificationsData(){
        /// step1 remove all set notifications
        removeNotificationFor(prayer:.Fajr)
        removeNotificationFor(prayer:.Dhuhr)
        removeNotificationFor(prayer:.Asr)
        removeNotificationFor(prayer:.Maghrib)
        removeNotificationFor(prayer:.Isha)
        /// remove names on enabled prayers
        UserDefaults.standard.removeObject(forKey: "prayersNotificationList")
        /// remove pause notifications check
        UserDefaults.standard.removeObject(forKey: "pauseNotifications")
    }
    func isNotificationsPaused() -> Bool{
        if UserDefaults.standard.value(forKey: "pauseNotifications") as? Bool == true{
            return true
        }
        return false
    }
    func getAllEnabledPrayers() -> [String]{
        if UserDefaults.standard.value(forKey: "pauseNotifications") as? Bool == true{
            return []
        }
        var prayersNotificationList: [String]!
        if let prayersList = UserDefaults.standard.value(forKey: "prayersNotificationList") as? [String]{
            prayersNotificationList = prayersList
        }
        else{
            prayersNotificationList = []
        }
        return prayersNotificationList
    }
    func getPrayerTypeFrom(name:String) -> PrayerType{
        if name == "Fajr" || name == "fajr"{
            return .Fajr
        }else if name == "Dhuhr" || name == "dhuhr"{
            return .Dhuhr
        }
        else if name == "Asr" || name == "asr"{
            return .Asr
        }
        else if name == "Maghrib" || name == "maghrib"{
            return .Maghrib
        }
        else{
            return .Isha
        }
    }
    
}


extension NotificationsHandler{
    private func requestAuthorization(completion: @escaping (Bool) -> Void)
    {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
            
            completion(granted)
        }
    }
   private func getNotificationSettings(completion: @escaping (Bool) -> Void)
    {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            
            switch settings.authorizationStatus {
            case .notDetermined:
                self.requestAuthorization(){ status in
                    completion(status)
                }
            case .authorized, .provisional:
                completion(true)
            default:
                completion(false)
                break // Do nothing
            }
        }
    }
}
