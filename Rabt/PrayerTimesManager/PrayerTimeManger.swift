//
//  PrayerTimeManger.swift
//  Rabt
//
//  Created by Asad Khan on 19/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import CoreLocation
protocol prayerTimeDetailsDelegate{
    func prayerTimeFetchedWith(prayersData: prayerData? , nextPrayerName: String, nextPrayerTime:String ,nextPrayerCountDown: String)
    func prayersLocationNameDidFetched()
}
class PrayerTimeManger: NSObject {
static let shared = PrayerTimeManger()
    var shouldRefreshPrayerTimings = true
    var shouldRefreshPrayerNotifications = true
    var prayerTimeCoordinates : [String:String]?
    var prayerData : [prayerData?]?
    var selectedLocationName : String?
    var placesManger: GooglePlacesManager? = GooglePlacesManager()
    var delegate : prayerTimeDetailsDelegate?
}

extension PrayerTimeManger{
    func getPrayerTimings(){
        if PrayerTimeManger.shared.selectedLocationName == nil{
            let coordinates = CLLocation.init(latitude: Double(PrayerTimeManger.shared.prayerTimeCoordinates!["latitude"]!) ?? 0.0, longitude: Double(PrayerTimeManger.shared.prayerTimeCoordinates!["longitude"]!) ?? 0.0)
            placesManger?.delegate = self
            placesManger?.getPlaceNameFrom(coordinate: coordinates)
        }
        else{
            delegate?.prayersLocationNameDidFetched()
        }
        if PrayerTimeManger.shared.shouldRefreshPrayerTimings{
            getPrayerTimingsFromApi()
            return
        }
        if  PrayerTimeManger.shared.prayerData == nil{
            getPrayerTimingsFromApi()
            return
        }
        // call timing fetched protocol to show data
        self.calculateNextPrayerData()
        
    }
   private func getPrayerTimingsFromApi(){
       // this case will run if user have selected custom location
       var parameters : [String:String]? =  PrayerTimeManger.shared.prayerTimeCoordinates
       if let customLocationCoordinates = UserDefaults.standard.value(forKey: "selectedPrayerTimesCoordinate") as? [String:String]{
           // use user save locatoin coordinates for prayer timings
           PrayerTimeManger.shared.prayerTimeCoordinates = customLocationCoordinates
           parameters = customLocationCoordinates
       }
       parameters?["date"] = getStringDateWith(format: "dd-MM-yyyy", date: Date())
       let apiManager = ApiManager()
       let url = ApiUrls.prayerTimingsApi
       //self.showSpinner(onView: self.view)
       apiManager.WebService(url: url, parameter: parameters, method: .post, encoding: .queryString, {
           (dataModel: PrayerTimeModal?) in
           PrayerTimeManger.shared.prayerData = dataModel?.data
           PrayerTimeManger.shared.shouldRefreshPrayerTimings = false
           self.calculateNextPrayerData()
       }){ error in
           print(error.localizedDescription)
       }
    }
    private func calculateNextPrayerData(){
       
       let (nextPrayerName,nextPrayerTime,nextPrayerCountdown,selectedPrayerData) = fetchPrayerDetailsFromDate()
        delegate?.prayerTimeFetchedWith(prayersData: selectedPrayerData, nextPrayerName: nextPrayerName,nextPrayerTime:nextPrayerTime, nextPrayerCountDown: nextPrayerCountdown)
    }
    private func fetchPrayerDetailsFromDate() ->(String,String,String,prayerData?){
        let currentDate = getStringDateWith(format: "dd-MM-yyyy", date: Date())
        let currentTime = getStringDateWith(format: "HH:mm", date: Date())
      //  let selectedPrayerDataArray = PrayerTimeManger.shared.prayerData?.filter{$0?.date?.gregorian?.date == dateOfPrayer}
        if let indexOfTodayPrayers = PrayerTimeManger.shared.prayerData?.firstIndex(where: {$0?.date?.gregorian?.date == currentDate}){
            if indexOfTodayPrayers > -1 && indexOfTodayPrayers < PrayerTimeManger.shared.prayerData?.count ?? 0{
                var selectedPrayerData = PrayerTimeManger.shared.prayerData?[indexOfTodayPrayers]
                // check if time is not passed isha prayer , if yes than select next day prayer times
                var  prayerDate = selectedPrayerData?.date?.gregorian?.date
                let ishaTime = selectedPrayerData?.timings?.isha?.components(separatedBy: " ").first
                if getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)") > getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(ishaTime ?? "")") {
                    // time is more than isha so move to next day prayers
                    if indexOfTodayPrayers + 1 < PrayerTimeManger.shared.prayerData?.count ?? 0{
                        selectedPrayerData = PrayerTimeManger.shared.prayerData?[indexOfTodayPrayers + 1]
                        prayerDate = selectedPrayerData?.date?.gregorian?.date
                    }else{
                        return ("","","",nil)
                    }
                     
                }
                var nextPrayerName = "Fajr"
                var nextPrayerTime = "00:00 AM"
                var nextPrayerCountDown = "0h 0min"
                //find upcoming prayer
                if getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.fajr?.components(separatedBy: " ").first ?? "")") > getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)") {
                    nextPrayerName = "Fajr"
                    nextPrayerTime = getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.fajr?.components(separatedBy: " ").first ?? "")
                    nextPrayerCountDown = getDateCountdown(nextPrayer: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.fajr?.components(separatedBy: " ").first ?? "")"), currentTime: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)"))
                }else if getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.dhuhr?.components(separatedBy: " ").first ?? "")") > getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)") {
                    nextPrayerName = "Dhuhr"
                    nextPrayerTime = getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.dhuhr?.components(separatedBy: " ").first ?? "")
                    nextPrayerCountDown = getDateCountdown(nextPrayer: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.dhuhr?.components(separatedBy: " ").first ?? "")"), currentTime: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)"))
                }else if getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.asr?.components(separatedBy: " ").first ?? "")") > getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)") {
                    nextPrayerName = "Asr"
                    nextPrayerTime = getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.asr?.components(separatedBy: " ").first ?? "")
                    nextPrayerCountDown = getDateCountdown(nextPrayer: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.asr?.components(separatedBy: " ").first ?? "")"), currentTime: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)"))
                }else if getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.maghrib?.components(separatedBy: " ").first ?? "")") > getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)") {
                    nextPrayerName = "Maghrib"
                    nextPrayerTime = getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.maghrib?.components(separatedBy: " ").first ?? "")
                    nextPrayerCountDown = getDateCountdown(nextPrayer: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.maghrib?.components(separatedBy: " ").first ?? "")"), currentTime: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)"))
                }else if getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.isha?.components(separatedBy: " ").first ?? "")") > getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)") {
                    nextPrayerName = "Isha"
                    nextPrayerTime = getStringDateFromAnotherStringDate(oldFormat: "HH:mm", newFormat: "h:mm a", dateString: selectedPrayerData?.timings?.isha?.components(separatedBy: " ").first ?? "")
                    nextPrayerCountDown = getDateCountdown(nextPrayer: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(prayerDate ?? "") \(selectedPrayerData?.timings?.isha?.components(separatedBy: " ").first ?? "")"), currentTime: getDateWith(format: "dd-MM-yyyy HH:mm", stringDate: "\(currentDate) \(currentTime)"))
                }
                return (nextPrayerName,nextPrayerTime,nextPrayerCountDown,selectedPrayerData)
            }
        }
       
        return ("","","",nil)
    }
}
extension PrayerTimeManger : googlePlacesCustomDelegate{
    func placeNameFetchingCompleted() {
        delegate?.prayersLocationNameDidFetched()
    }
    
    func placesSearchCompleted() {
        
    }
    
    func coordinateFetchingCompleted() {
        
    }
    
    
}
// MARK: - Formatters
extension PrayerTimeManger{
    func getStringDateFromAnotherStringDate(oldFormat: String, newFormat: String, dateString:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = oldFormat
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = newFormat
        if date == nil{
            AnalyticsManger.logEventWith(name: "date_time_format_error", attributes: ["conversionFormat": oldFormat , "conversionDate" : dateString, "user_location": myLocationPlacemark?.country ?? "", "googlePlace":PrayerTimeManger.shared.selectedLocationName ?? ""])
            return dateFormatter.string(from: Date())
        }
        return dateFormatter.string(from: date ?? Date())
    }
    func getStringDateWith(format: String, date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    func getDateWith(format: String, stringDate: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = format
        let defaultDateString = getStringDateWith(format: format, date: Date())
        if let orignalDate = dateFormatter.date(from: stringDate){
            return orignalDate
        }else{
            //log event to check date format that crashing certain users
            AnalyticsManger.logEventWith(name: "date_time_format_error", attributes: ["conversionFormat": format , "conversionDate" : stringDate, "user_location": myLocationPlacemark?.country ?? "", "googlePlace":PrayerTimeManger.shared.selectedLocationName ?? ""])
            return dateFormatter.date(from: defaultDateString) ?? Date()
        }
      
    }
     func getDateCountdown(nextPrayer: Date, currentTime: Date) -> String {
           let hour = Calendar.current.dateComponents([.hour], from: currentTime, to: nextPrayer).hour
         let modifiedTime = Calendar.current.date(byAdding: .hour, value: hour!, to: currentTime)!
         let minute = Calendar.current.dateComponents([.minute], from: modifiedTime, to: nextPrayer).minute
         //return "Next Prayer in \(hour ?? 0)h \(minute ?? 0)min"
         return "\(hour ?? 0)h \(minute ?? 0)min"
       }
    func getDatesDifference(firstDate:Date,secondDate:Date) -> Int{
        let calendar = Calendar.current

        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: firstDate)
        let date2 = calendar.startOfDay(for: secondDate)

        let components = calendar.dateComponents([.day], from: date1, to: date2)
        return components.day ?? 0
    }
}
