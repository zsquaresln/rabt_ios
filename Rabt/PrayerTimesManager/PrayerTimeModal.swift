//
//  PrayerTimeModal.swift
//  Rabt
//
//  Created by Asad Khan on 25/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit

class PrayerTimeModal: NetworkCommonResponseDataModel {
  
    let data: [prayerData?]?
    enum CodingKeys: String, CodingKey {
        case data
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        data    = try values.decodeIfPresent([prayerData].self, forKey: .data)
        
        try super.init(from: decoder)
    }
}
struct prayerData: Codable {
    let timings: Timings?
       let date: PrayerDate?

       enum CodingKeys: String, CodingKey {
           case timings = "timings"
           case date = "date"
       }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        timings = try values.decodeIfPresent(Timings.self, forKey: .timings)
        date = try values.decodeIfPresent(PrayerDate.self, forKey: .date)
   }
}
struct Timings: Codable {
    let fajr, dhuhr, asr, maghrib: String?
        let isha: String?
    let aftar, sehar ,title : String?
    let status : Int?
   

        enum CodingKeys: String, CodingKey {
            case fajr = "Fajr"
            case dhuhr = "Dhuhr"
            case asr = "Asr"
            case maghrib = "Maghrib"
            case isha = "Isha"
            case aftar = "aftar"
            case sehar = "sehar"
            case title = "title"
            case status = "status"
        }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        fajr = try values.decodeIfPresent(String.self, forKey: .fajr)
        dhuhr = try values.decodeIfPresent(String.self, forKey: .dhuhr)
        asr = try values.decodeIfPresent(String.self, forKey: .asr)
        maghrib = try values.decodeIfPresent(String.self, forKey: .maghrib)
        isha = try values.decodeIfPresent(String.self, forKey: .isha)
        aftar = try values.decodeIfPresent(String.self, forKey: .aftar)
        sehar = try values.decodeIfPresent(String.self, forKey: .sehar)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
   }
}

struct PrayerDate: Codable {
    let gregorian: calenderData?

    enum CodingKeys: String, CodingKey {
        case gregorian = "gregorian"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        gregorian = try values.decodeIfPresent(calenderData.self, forKey: .gregorian)
   }
}

struct calenderData: Codable {
    let date: String?

    enum CodingKeys: String, CodingKey {
        case date = "date"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        date = try values.decodeIfPresent(String.self, forKey: .date)
   }
}
