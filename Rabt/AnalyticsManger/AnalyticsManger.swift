//
//  AnalyticsManger.swift
//  Rabt
//
//  Created by Asad Khan on 10/02/2022.
//  Copyright © 2022 Ali Sher. All rights reserved.
//

import UIKit
import FirebaseAnalytics
 class AnalyticsManger: NSObject {
    static func logEventWith(name: String , attributes : [String:Any]?){
        Analytics.logEvent(name, parameters: attributes)
    }
    
}
